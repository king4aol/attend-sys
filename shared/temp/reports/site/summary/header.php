<html>
<head><title> reports </title>
<style>
    table{
        font-size: 20px;
    }
tabel,tr
{
height:40px;
}
tabel,td
{
padding-left:5px;
}
table, th, td {
    border: 1px solid #4bacc6;
    border-collapse: collapse;
}
</style>
<body>
<div style="width:980px;height:auto;margin:0 auto;">
 <div style="width:100%;height:100%;">
  <div style="width:50%;float:left;">
      <img src="<?=$otlLogo?>" width="84px" height="21px">
  </div>
  <div style="width:49%;float:left;text-align:right;">
  <img src="<?=$companyLogo?>" width="84px" height="21px">
  </div>
<div style="width:100%;height:100px;float:left;text-align:center;">
<p style="font-family:calibri;font-size: 22px"> Site/Office Summary Report </p>
<p style="font-family:calibri;font-size: 11px"> <?=date("d/m/y h:i a")?></p>
</div>
<div style="width:100%;height:79px;float:left;font-family:calibri">
    <p style="line-height:0px;"><span style="font-weight:bold">Site/Office: </span><span> <?=$title?> <span></p>
    <p style="line-height:8px;"><span style="font-weight:bold">Time Zone: </span><span> <?=$timezone?> UTC <span></p>
 <?php if($all != "today"){ ?>
    <p style="line-height:0px;"><span style="font-weight:bold">From Date: </span><span> <?=date("d/m/Y", $fromTime)?>  </span>  <span style="font-weight:bold">To Date: </span> <span> <?=date("d/m/Y", $toTime-86400)?>  <span>  </p>
                <?php }else{?>
    <p style="line-height:0px;"><span style="font-weight:bold">Date: </span><span> <?=date("d/m/Y")?>  </span> </p>               
                <?php }?>
    <p style="line-height:8px;"><span style="font-weight:bold">Total Cost: </span><span id="final_cost"> <span></p>
    </div>

<table style="width:100%;font-family:calibri;">
  <tr style="border:#4bacc6;color:#333;height:50px;border-bottom:2px solid #4bacc6;">
    <td style="text-align:left;width:7%"> # </td>
	<td style="text-align:left;width:30%">Date </td>
	<td style="text-align:left;width:20%">Employee Pin</td> 
	<td style="text-align:left;width:30%">Name </td>
	<td style="text-align:left;width:10%">Check-in Time</td>	
	<td style="text-align:left;width:10%">Total Hrs Worked</td>	
	<td style="text-align:left;width:10%">Total Break Durations</td>
	<td style="text-align:left;width:10%">Rate/Hr</td>
	<td style="text-align:left;width:10%">Total Cost</td>
  </tr>