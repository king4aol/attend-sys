<div style="height: 100%">
<table style="width:100%">
  <tr style="background:#4bacc6;color:#fff;">
    <td style="text-align:left;width:20%">Date: </td>
	<td style="text-align:left;width:80%"><?=date("d-m-Y", $row['check_in_timestamp'])?></td>   
  </tr>
  <tr>
    <td style="text-align:left;width:20%">Employee Pin: </td>
	<td style="text-align:left;width:80%"><?=$row['pin']?></td>   
  </tr>
  <tr>
    <td style="text-align:left;width:20%">Name: </td>
	<td style="text-align:left;width:80%"><?=$row['name']?></td>   
  </tr>
  <tr>
    <td style="text-align:left;width:20%">Site:</td>
	<td style="text-align:left;width:80%"><?=$row['office_name']." (".$row['timezone']." UTC)"?></td>   
  </tr>
    <tr>
    <td style="text-align:left;width:20%">Check-in: </td>
	<td style="text-align:left;width:80%"><?=date("H:i", $row['check_in_timestamp'])?></td>   
  </tr>
  <tr>
    <td style="text-align:left;width:20%">Check-Out: </td>
	<td style="text-align:left;width:80%"><?=date("H:i", $row['check_out_timestamp'])?></td>   
  </tr>
  <tr>
    <td style="text-align:left;width:20%">Break - Check-in: </td>
	<td style="text-align:left;width:80%"><?=($row['check_out_timestamp'] != $row['break_in_timestamp']) ? date("H:i", $row['break_in_timestamp']) : '-'?></td>   
  </tr>
    <tr>
    <td style="text-align:left;width:20%">Break - Check-out: </td>
	<td style="text-align:left;width:80%"><?=($row['check_out_timestamp'] != $row['break_out_timestamp']) ? date("H:i", $row['break_out_timestamp']) : '-'?></td>   
  </tr>
  <tr>
    <td style="text-align:left;width:20%">Rate/Hourly: </td>
	<td style="text-align:left;width:80%"><span style="width:50%;float:left;height:33px;line-height:28px;">$<?=$row['pay_rate']?> </span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
            
            <span style="width:19%;border-left:1px solid #4bacc6;border-right:1px solid #4bacc6;float:left;height:37px;line-height:33px;padding-left:50px;">&nbsp;&nbsp;Total Shift Cost:&nbsp;&nbsp;</span><span style="width:28%;float:left;height:33px;line-height:28px;padding-left:5px;">&nbsp;&nbsp;$<?=round($totalCost, 2)?></span></td>   
  </tr>
</table>
<div style="width:99%;float:left;border:2px solid #4bacc6;padding:3px;border-top:none;">
<p> Shift </p>
<div style="width:25%;float:left;"><img style="margin-left: 40px;width: 150px;height: 150px" src="<?=is_file(_SHARED_UPLOADS_."/photos/".$row['cin_image'])? _SHARED_UPLOADS_."/photos/{$row['cin_image']}":_SHARED_UPLOADS_."/nopreview.png";?>" ></div>
<div style="width:25%;float:left;"><img style="margin-left: 40px;width: 150px;height: 150px" src="<?=is_file(_SHARED_UPLOADS_."/photos/".$row['cout_image'])? _SHARED_UPLOADS_."/photos/{$row['cout_image']}":_SHARED_UPLOADS_."/nopreview.png";?>"></div>
<div style="width:25%;float:left;"><img style="margin-left: 40px;width: 150px;height: 150px" src="<?=is_file(_SHARED_UPLOADS_."/photos/".$row['bin_image']) && ($row['check_out_timestamp'] != $row['break_in_timestamp'])? _SHARED_UPLOADS_."/photos/{$row['bin_image']}":_SHARED_UPLOADS_."/nopreview.png";?>" ></div>
<div style="width:25%;float:left;"><img style="margin-left: 40px;width: 150px;height: 150px" src="<?=is_file(_SHARED_UPLOADS_."/photos/".$row['bout_image']) && ($row['check_out_timestamp'] != $row['break_out_timestamp'])? _SHARED_UPLOADS_."/photos/{$row['bout_image']}":_SHARED_UPLOADS_."/nopreview.png";?>"></div>
<div style="width:100%;float:left;border-top:none;text-align:center;">
 <div style="width:234px;float:left;font-weight:bold"> Check-in </div> 
 <div style="width:253px;float:left;font-weight:bold"> Check-out </div> 
 <div style="width:234px;float:left;font-weight:bold"> Break-Start </div> 
 <div style="width:249px;float:left;font-weight:bold"> Break-End </div>  </div>
</div>
</div>