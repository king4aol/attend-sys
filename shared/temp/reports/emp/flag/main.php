<tr style="border:#4bacc6;color:#333;height:25px;font-weight:bold;<?=($index%2 != 0)?'background:#d2eaf1;' : ''?>font-family:calibri;">
    <td style="text-align:left;width:5%"><?=$index++?></td>
	<td style="text-align:left;width:20%"><?=date("d-m-Y", $report['check_in_timestamp'])?></td> 
	<td style="text-align:left;width:25%"><?=$report['office_name'];?></td>
        <td style="text-align:left;width:20%"><?=date("H:i", $report['check_in_timestamp'])?></td>
	<td style="text-align:left;width:10%;text-align:center;"><img src="<?=$flagLogo?>" width="20px"></td>	
        <?php if($report['ciStatus'] == _LATE_){
            date_default_timezone_set($this->config['timezone']);
            $a = date("H:i", $report['timing_from']);
            $expectedEntry = strtotime($a);

//            date_default_timezone_set($timezone);
            $a1 = date("H:i", $report['check_in_timestamp'] + ($seconds));
            $realEntry = strtotime($a1);

            $late = ceil(($realEntry - $expectedEntry) / 60) - $report['emp_grace_time'];
            date_default_timezone_set($timezone);
            ?>
	<td style="text-align:left;width:20%"><?=$late." mins"?></td>
        <?php }else{ ?>
	<td style="text-align:left;width:20%">-</td>
        <?php }?>
        <?php if($report['boStatus'] == _LATE_){
            $late = ceil(($report['break_out_timestamp'] - $report['break_in_timestamp']) / 60) - $report['emp_break_time'];
            ?>
	<td style="text-align:left;width:20%"><?=$late." mins"?></td>
        <?php }else{ ?>
	<td style="text-align:left;width:20%">-</td>
        <?php }?>
  </tr>