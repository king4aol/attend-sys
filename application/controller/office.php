<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of office
 *
 * @author Msajid
 */
class officeController extends controller
{
   private $requiredBit = 0;
   private $isError = false;
   private $errorMsgs;
   private $model;
   
   public function add()
   {
       if(session::isLogin())
        {
           if(isset($_POST['adminsubmit']) && !isset($_POST['office']))
            {
               //checking admin
               if(is_numeric($_POST['admin']))
                {
                    $adminModel = $this->loadModel("admin");
                    if($adminModel)
                    {
                        if($adminModel->adminExists($_POST['admin'], _CLIENT_, TRUE))
                        {
                            $admins = $_POST['admin'];
                            $_SESSION[__CLASS__.__FUNCTION__] = $admins;
                        }
                        else
                        {
                           $this->isError = true;
                           $this->errorMsgs['admin_error'] = "Company Does Not Exists.";
                        }
                    }                    
                    else
                    {
                       $this->isError = true;
                       $this->errorMsgs['admin_error'] = "Company does Not Exists";
                    }
                    $values['admin'] = $_POST['admin'];
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['admin_error'] = "Invalid Company Selected";
                }
                if($this->isError)
                {
                    unset($_POST['adminsubmit']);
                    $adminModel = $this->loadModel("admin");
                    if($adminModel)
                    {
                        $admins = $adminModel->viewAdmins();
                    }
                    $error = $this->errorMsgs;
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("admins", "csrf", "error", "values");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
               }
               else
               {
                   $csrf = csrf::generateCsrf(__FUNCTION__);
                   $this->data = compact("csrf", "values");
                   $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
               }
                
            } 
            elseif(isset($_POST['office']))
            {
                if(isset($_POST['csrf']))
                {
                    if(!csrf::validateCsrf($_POST['csrf'], __FUNCTION__))
                    {
                        $this->errorMsgs;
                        $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                    }
                }
                else
                {
                    $this->errorMsgs;
                    $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                }
                //validating office name
                if(isset($_POST['name']) && !@empty($_POST['name']))
                {
                    if(preg_match("/^[ \w#-:,]+$/", $_POST['name']))
                    {
                        $officeName = trim($_POST['name']);
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['name_error'] = "Name Can Only have alphabets,numeric,-,_,#,:, ,";
                    }
                    $values['name'] = securestr::clean($_POST['name']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['name_error'] = "office name is required";
                }
                
                //validatin address
                if(isset($_POST['address']) && !@empty($_POST['address']))
                {
                    if(preg_match("/^[ \w#-:]+$/", $_POST['address']))
                    {
                        $address = $_POST['address'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['address_error'] = "Address can only have alphabets,numeric,-,_,#,:";
                    }
                    $values['address'] = securestr::clean($_POST['address']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['address_error'] = "Address is required";
                }
                
                
                if(!$this->isError)
                {
                    if(isset($_SESSION[__CLASS__.__FUNCTION__]))
                    {
                        $admin_id = $_SESSION[__CLASS__.__FUNCTION__];
                    }
                    else 
                    {
                        $admin_id = session::getAdminId();
                    }
                    
                    $this->model = $this->loadModel("admin");
                    $membershipType = $this->model->getMembershipType($admin_id);
                    
                    $this->model = $this->loadModel("membership_types");
                    $officesAllowed = $this->model->getOfficesAllow($membershipType, FALSE);
                    $totalOffices = 0;
                    
                    if($officesAllowed == -1)
                    {
                        $officesAllowed = 9999999999;
                    }
                    else 
                    {
                        $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                        $totalOffices = $this->model->countOffices($admin_id);
                    }
                    if($officesAllowed <= $totalOffices)
                    {
                        $this->isError = true;
                        //$this->errorMsgs['error'] = "You're Not Allowed To Add More Then $officesAllowed Offices";
												$this->errorMsgs['error'] = "You are not allowed to add more than one site/office. Please upgrade your package";
                    }
                }
                else 
                {
                    $_POST['adminsubmit'] = '';
                    unset($_POST['office']);
                    $this->errorMsgs['error'] = "Something Went Wrong";
                    $error = $this->errorMsgs;
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("admins", "csrf", "error", "values" ,"res");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
                    die();
                }
                if(!$this->isError)
                {
                    //insert into office
                    if(isset($_SESSION[__CLASS__.__FUNCTION__]))
                    {
                        $admin_id = $_SESSION[__CLASS__.__FUNCTION__];
                    }
                    else 
                    {
                        $admin_id = session::getAdminId();
                    }
                    $this->model = $this->loadModel("office");
                    $this->model->db->transaction();
                    $officeId = $this->model->insertOffice($admin_id, $officeName);
                    //insert into users
                    $this->model = $this->loadModel("users");
                    $userId = $this->model->insertUsers($officeId, _OFFICE_);
                    //insert into addresses
                    $this->model = $this->loadModel("addresses");
                    $this->model->insertAddress($userId, $address);
                    
                    if($this->model->db->commit())
                    {
                        $_POST['adminsubmit'] = '';
                        unset($_POST['office']);
                        $res['response'] = "Site/Office added successfully.";
                        
                        //Science
                        $_POST['success'] = $res['response'];
                        $_SESSION[__CLASS__.'adminId'] = $_SESSION[__CLASS__.__FUNCTION__];
                        $this->model = $this->loadModel("admin");
                        $_SESSION[__CLASS__.'adminUser'] = $this->model->idToUsername($_SESSION[__CLASS__.'adminId']);
                        $controller = $this->loadController("office");
                        $controller->view('success');
                        die();
                        //Science End
                        
                        $error = $this->errorMsgs;
                        $csrf = csrf::generateCsrf(__FUNCTION__);
                        $this->data = compact("admins", "csrf", "error", "values" ,"res");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
                    }
                    else
                    {
                        $_POST['adminsubmit'] = '';
                        unset($_POST['office']);
                        $this->errorMsgs['error'] = "Something Went Wrong";
                        $error = $this->errorMsgs;
                        $csrf = csrf::generateCsrf(__FUNCTION__);
                        $this->data = compact("admins", "csrf", "error", "values" ,"res");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
					}
                }
                else
                {
                    unset($_POST['office']);
                    $_POST['adminsubmit'] = '';
                    $this->javascripts[] = "{$this->config['domain']}"._PUBLIC_PATH_."js/jquery.validate.min.js";
                    //loading countries model
                    $adminModel = $this->loadModel("admin");
                    $admins = [];
                    if($adminModel)
                    {
                        $admins = $adminModel->viewAdmins();
                    }
                    $error = $this->errorMsgs;
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("admins", "csrf", "error", "values", "res");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data); 
                }
            }
            else
            {
                $this->javascripts[] = "{$this->config['domain']}"._PUBLIC_PATH_."js/jquery.validate.min.js";
                //loading admin model
                $adminModel = $this->loadModel("admin");
                $admins = [];

                if($adminModel)
                {
                    $admins = $adminModel->viewAdmins();
                }
                $csrf = csrf::generateCsrf(__FUNCTION__);
                $this->data = compact("admins", "csrf", "error", "values", "res");
                $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data); 
            }
        }
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
            die();
        }
   }
   
   public function view($perform = '', $limit = 10, $offset = 0)
   {
        if(session::isLogin())
        {
            if(!is_numeric($limit) || !is_numeric($offset) || $limit <= 0 || $offset < 0)
            {
                $limit = 10;
                $offset = 0;
            }
            if($perform == 'success' || $perform == 'load' && isset($_SESSION[__CLASS__.'adminUser']))
            {
                $_POST['submit'] = '';
                $_POST['client'] = $_SESSION[__CLASS__.'adminUser'];
            }
            if(!isset($_POST['submit']))
            {
                $this->javascripts[] = "{$this->config['domain']}"._PUBLIC_PATH_."js/jquery.validate.min.js";
                //loading countries model
                $adminModel = $this->loadModel("admin");
                $admins = [];

                if($adminModel)
                {
                    $admins = $adminModel->viewAdmins();
                }
                $error = $this->errorMsgs;
                $this->data = compact("admins", "error", "values");
                $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data); 
            }
            else
            {
                if(isset($_POST['client']) && !@empty($_POST['client']))
                {
                    $user = $_POST['client'];
                    if(preg_match("/^[\w-$@._]+$/", $user))
                    {
                        $_SESSION[__CLASS__.'adminUser'] = $_POST['client'];
                        $this->model = $this->loadModel("admin");
                        $adminId = $this->model->usernameToId($user);
                        session::sessionStart();
                        $_SESSION[__CLASS__.'adminId'] = $adminId;
                        if($this->model)
                        {
                            if($this->model->adminExists($user, _CLIENT_))
                            {
                                $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                                $offices = $this->model->view($user, TRUE, $offset, $limit);
                                $total = $this->model->countOffices($user);
                                $next = $offset + $limit;
                                if($next > $total)
                                {
                                    $next = $offset;
                                }
                                $previous = $offset - $limit;
                                if($previous < 0)
                                {
                                    $previous = 0;
                                }
                                $current = ceil($offset / $limit);
                                $current++;
                                if($current == 0)
                                {
                                    $current = 1;
                                }
								
                                $error = $this->errorMsgs;
								
                                $this->data = compact("offices", "error", "limit", "offset", "total", "next", "previous", "current");

                                $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                            }
                            else
                            {
                                $this->isError = true;
                                $this->errorMsgs['admin_error'] = "Invalid Company Selected";
                                unset($_POST['submit']);
                                $adminModel = $this->loadModel("admin");
                                if($adminModel)
                                {
                                    $admins = $adminModel->viewAdmins();
                                }
                                $error = $this->errorMsgs;
                                $csrf = csrf::generateCsrf(__FUNCTION__);
                                $this->data = compact("admins", "csrf", "error", "values");
                                $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                            }
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['admin_error'] = "Invalid Company Selected";
                            unset($_POST['submit']);
                            $adminModel = $this->loadModel("admin");
                            if($adminModel)
                            {
                                $admins = $adminModel->viewAdmins();
                            }
                            $error = $this->errorMsgs;
                            $csrf = csrf::generateCsrf(__FUNCTION__);
                            $this->data = compact("admins", "csrf", "error", "values");
                            $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                        }
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['admin_error'] = "Invalid Company Selected";
                        unset($_POST['submit']);
                        $adminModel = $this->loadModel("admin");
                        if($adminModel)
                        {
                            $admins = $adminModel->viewAdmins();
                        }
                        $error = $this->errorMsgs;
                        $csrf = csrf::generateCsrf(__FUNCTION__);
                        $this->data = compact("admins", "csrf", "error", "values");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                    }
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['admin_error'] = "Invalid Company Selected";
                    unset($_POST['submit']);
                    $adminModel = $this->loadModel("admin");
                    if($adminModel)
                    {
                        $admins = $adminModel->viewAdmins();
                    }
                    $error = $this->errorMsgs;
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("admins", "csrf", "error", "values");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                }
                
            }
        }
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login"); 
            die();
        }
   }
   //
    public function action($officeId = '', $action = '')
    {
        if(session::isLogin())
        {
            $action = strtolower($action);
            if($action == "delete")
            {
                $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                
                if($this->model)
                {
                    //if($this->model->adminExists($oofice, _OFFICE_))
                    //{
                    if($this->model->adminToOfficeRel(session::getAdminId(), $officeId))
                    {
                        $performed = $this->model->actions($officeId ,$action);
                        if($performed)
                        {
                            ob_get_clean();
                            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."office/view/success"); 
                            die();
                        }
                        else
                        {
                            $controller = $this->loadController("error");
                            if($controller)
                            {
                                $controller->_custom("Something Went Wrong.", "Something Went Wrong. Please Try Again Later.");
                            }
                        }
                    }
                    else
                    {
                        $controller = $this->loadController("error");
                            if($controller)
                            {
                                $controller->_custom("Something Went Wrong.", "Something Went Wrong. Please Try Again Later.");
                            }
                    }
                    /*}
                    else
                    {
                        $controller = $this->loadController("error");
                        if($controller)
                        {
                            $controller->_custom("Office Does Not Exists.", "Office Does Not Exists. Please Check The Spellings.");
                        }
                    }*/
                }
            }
        }
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login"); 
            die();
        }
    }
    
    //edit offices
    
    public function edit($office = '')
    {
        if(session::isLogin())
        {
            if(isset($_POST['submit']))
            {
                if(isset($_POST['csrf']))
                {
                    if(!csrf::validateCsrf($_POST['csrf'], __FUNCTION__))
                    {
                        $this->errorMsgs;
                        $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                    }
                }
                else
                {
                    $this->errorMsgs;
                    $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                }
                //validating office name
                if(isset($_POST['name']) && !@empty($_POST['name']))
                {
                    if(preg_match("/^[ \w#-:,]+$/", $_POST['name']))
                    {
                        $officeName = trim($_POST['name']);
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['name_error'] = "Name Can Only have alphabets,numeric,-,_,#,:, ,";
                    }
                    $values['name'] = securestr::clean($_POST['name']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['name_error'] = "office name is required";
                }
                
                //validatin address
                if(isset($_POST['address']) && !@empty($_POST['address']))
                {
                    if(preg_match("/^[ \w#-:]+$/", $_POST['address']))
                    {
                        $address = $_POST['address'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['address_error'] = "Address can only have alphabets,numeric,-,_,#,:";
                    }
                    $values['address'] = securestr::clean($_POST['address']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['address_error'] = "Address is required";
                }
               
                if(!$this->isError)
                {
                    $this->model = $this->loadModel(str_replace("Controller", "",__CLASS__));
                    if($this->model->adminToOfficeRel(session::getAdminId(), $office))
                    {
                        $offices = $this->model->update($office, $officeName, $address);
                        $offices = $offices[0];
                        $csrf = csrf::generateCsrf(__FUNCTION__);
                        $res['response'] = "Site/Office updated successfully.";
                        
                        //Science
                        $_POST['success'] = $res['response'];
                        $_SESSION[__CLASS__.'adminId'] = $offices['admin_id'];
                        $this->model = $this->loadModel("admin");
                        $_SESSION[__CLASS__.'adminUser'] = $this->model->idToUsername($_SESSION[__CLASS__.'adminId']);
                        $controller = $this->loadController("office");
                        $controller->view('success');
                        die();
                        //Science End
                        
                        $this->data = compact("offices", "office", "values", "csrf", "res");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
                    }
                    else 
                    {
                        $controller = $this->loadController("error");
                        if($controller)
                        {
                            $controller->_custom("Something Went Wrong.", "Something Went Wrong. Please Try Again Later.");
                        }
                    }
                }
                else
                {
                    $this->model = $this->loadModel(str_replace("Controller", "",__CLASS__));
                    if($this->model->adminToOfficeRel(session::getAdminId(), $office))
                    {
                        $offices = $this->model->getOffice($office);                    
                        $offices = $offices[0];
                        $csrf = csrf::generateCsrf(__FUNCTION__);
						$this->errorMsgs['error'] = "Something went wrong.";
                        $error = $this->errorMsgs;
						$this->data = compact("offices", "office", "values" , "error", "csrf");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
                    }
                    else 
                    {
                        $controller = $this->loadController("error");
                        if($controller)
                        {
                            $controller->_custom("Something Went Wrong.", "Something Went Wrong. Please Try Again Later.");
                        }
                    }
                }
            }
            else
            {
                $this->model = $this->loadModel(str_replace("Controller", "",__CLASS__));
                if($this->model->adminToOfficeRel(session::getAdminId(), $office))
                {
                    $offices = $this->model->getOffice($office);
                    $offices = $offices[0];
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("offices", "csrf");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
                }
                else 
                {
                    $controller = $this->loadController("error");
                    if($controller)
                    {
                        $controller->_custom("Something Went Wrong.", "Something Went Wrong. Please Try Again Later.");
                    }
                }
            }
        }
        else
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
            die();
        }
    }
    //Set Messages
    public function message() 
    { 
        if(session::isLogin())
        {
            if(!isset($_POST['submit']) && !isset($_POST['msg_submit']) && !isset($_POST['ofc_submit']))
            { 
                $this->model = $this->loadModel("admin");
                $admins = $this->model->viewAdmins();
                $error = $this->errorMsgs;
                $csrf = csrf::generateCsrf(__FUNCTION__);
                $this->data = compact("admins");
                $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
            }
            elseif(isset($_POST['submit']) && !isset($_POST['msg_submit']) && !isset($_POST['ofc_submit']))
            {
                //checking admin
                if(isset($_POST['admin']))
                {
                    if(is_numeric($_POST['admin']))
                    {
                        $adminModel = $this->loadModel("admin");
                        if($adminModel)
                        {
                            if($adminModel->adminExists($_POST['admin'], _CLIENT_, TRUE))
                            {
                                $admin = $_POST['admin'];
                                $_SESSION[__CLASS__.__FUNCTION__] = $admin;
                            }
                            else
                            {
                               $this->isError = true;
                               $this->errorMsgs['admin_error'] = "Admin Does Not Exists.";
                            }
                        }                    
                        else
                        {
                           $this->isError = true;
                           $this->errorMsgs['admin_error'] = "Admin does Not Exists";
                        }
                        $values['admin'] = $_POST['admin'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['admin_error'] = "Invalid Admin Selected";
                    }
               }
               else
               {
                    $this->isError = true;
                    $this->errorMsgs['admin_error'] = "Admin Is Required";
               }
               
               if($this->isError)
               {
                    unset($_POST['submit']);
                    $this->model = $this->loadModel("admin");
                    $admins = $this->model->viewAdmins();
                    $error = $this->errorMsgs;
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("admins", "csrf", "error", "values");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
               }
               else
               {
                   if(isset($_SESSION[__CLASS__.__FUNCTION__]))
                   {
                       $admin = $_SESSION[__CLASS__.__FUNCTION__];
                   }
                   else 
                   {
                       $admin = session::getAdminId();
                   }
                   
                   $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                   $offices = $this->model->viewById($admin);
                   $this->data = compact("offices");
                   $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
               }
            }
            elseif(isset($_POST['ofc_submit']))
            {
                if(isset($_POST['office']))
                {
                    if(is_numeric($_POST['office']))
                    {
                        if(isset($_SESSION[__CLASS__.__FUNCTION__]))
                        {
                            $admin = $_SESSION[__CLASS__.__FUNCTION__];
                        }
                        else 
                        {
                            $admin = session::getAdminId();
                        }
                        
                        $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                        if($this->model->adminToOfficeRel($admin, $_POST['office']))
                        {
                            $office = $_POST['office'];
                            $_SESSION[__CLASS__.__FUNCTION__.'_office'] = $office;
                        }
                        else 
                        {
                            $this->isError = true;
                            $this->errorMsgs['office_error'] = "Something Went Wrong";
                        }
                    }
                    else 
                    {
                        $this->isError = true;
                        $this->errorMsgs['office_error'] = "Invalid Office Selected";
                    }
                }
                else 
                {
                    $this->isError = true;
                    $this->errorMsgs['office_error'] = "Office Is Required";
                }
                
                if($this->isError)
                {
                    $_POST['submit'] = '';
                    unset($_POST['ofc_submit']);
                    if(isset($_SESSION[__CLASS__.__FUNCTION__]))
                    {
                        $admin = $_SESSION[__CLASS__.__FUNCTION__];
                    }
                    else 
                    {
                        $admin = session::getAdminId();
                    }
                        
                    $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                    $offices = $this->model->viewById($admin);
                    $error = $this->errorMsgs;
                    $this->data = compact("offices", "error");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                }
                else 
                {
                    $_POST['submit'] = '';
                    $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                    $messages = $this->model->viewMessage($office);
                    $this->data = compact("messages");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                }
            }
            elseif(isset($_POST['msg_submit']))
            {
                if(isset($_SESSION[__CLASS__.__FUNCTION__]))
                {
                    $admin = $_SESSION[__CLASS__.__FUNCTION__];
                }
                else 
                {
                    $admin = session::getAdminId();
                }
                
                if(isset($_SESSION[__CLASS__.__FUNCTION__.'_office']))
                {
                    $office = $_SESSION[__CLASS__.__FUNCTION__.'_office'];
                }
                else 
                {
                    $this->isError = TRUE;
                    $this->errorMsgs['office_error'] = "Something Went Wrong";
                }
                
                $cin_msg = '';
                $cout_msg = '';
                $bin_msg = '';
                $bout_msg = '';
                $a_show_cin = 0;
                $a_show_cout = 0;
                $a_show_bin = 0;
                $a_show_bout = 0;
                
                if(isset($_POST['cin_message']))
                {
                    $cin_msg = htmlentities($_POST['cin_message'], ENT_QUOTES);
                }
                if(isset($_POST['cout_message']))
                {
                    $cout_msg = htmlentities($_POST['cout_message'], ENT_QUOTES);
                }
                if(isset($_POST['bin_message']))
                {
                    $bin_msg = htmlentities($_POST['bin_message'], ENT_QUOTES);
                }
                if(isset($_POST['bout_message']))
                {
                    $bout_msg = htmlentities($_POST['bout_message'], ENT_QUOTES);
                }
                if(isset($_POST['a_cin']))
                {
                    $a_show_cin = 1;
                }
                if(isset($_POST['a_cout']))
                {
                    $a_show_cout = 1;
                }
                if(isset($_POST['a_bin']))
                {
                    $a_show_bin = 1;
                }
                if(isset($_POST['a_bout']))
                {
                    $a_show_bout = 1;
                }
                
                $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                if(!$this->isError)
                {
                    $this->model->updateMessages($office, $cin_msg, $cout_msg, $bin_msg, $bout_msg, $a_show_cin, $a_show_cout, $a_show_bin, $a_show_bout);
                    $messages = $this->model->viewMessage($office);
					$res['response'] = "Successfully Updated.";
                    $this->data = compact("messages", "res");
                }
                else 
                {
                    $offices = $this->model->viewById($admin);
                    $error = $this->errorMsgs;
                    $this->data = compact("offices", "error");
                }
                $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
            }
        }
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login"); 
            die();
        }
    }
    //For Ajax Calls
   public function ajax()
   {
        if(session::isLogin())
        {
           if(isset($_POST['type']) && isset($_POST['action']) && isset($_POST['source']))
           {
               if($_POST['type'] == "office" && $_POST['action'] == "select" && $_POST['source'] == "admin" && isset($_POST['input']))
               {
//                   if(!empty($_POST['input']))
//                   {
                        if(isset($_SESSION['attendanceControlleradminId']))
                        {
                            $admin = $_SESSION['attendanceControlleradminId'];
                        }
                        elseif(isset($_SESSION['officeControllermessage']))
                        {
                            $admin = $_SESSION['officeControllermessage'];
                        }
                        else 
                        {
                            $admin = session::getAdminId();
                        }
                        
                        $query = securestr::clean($_POST['input']);
                        
                        $this->model = $this->loadModel("office");
                        $offices = $this->model->searchOffice($admin, $query);
                        
                        if(sizeof($offices) > 0)
                        {
                            $response = $offices;
                        }
                        else 
                        {
                            $response = NULL;
                            $this->isError = TRUE;
                        }
//                   }
               }
               else 
               {
                    $this->isError = TRUE;
                    $response = "Nothing Found";
               }
           }
           else 
            {
                 $this->isError = TRUE;
                 $response = "Nothing Found";
            }
        }
        else 
        {
            $this->isError = TRUE;
            $response = "You're Not Login";
        }
        
        $this->data = "";
        $this->data['error'] = $this->isError;
        $this->data['response'] = $response;
        
        echo json_encode($this->data);
   }
}
