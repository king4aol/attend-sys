<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of api
 *
 * @author Umar
 */
//Header & Disable Error Reporting
error_reporting(0);
set_time_limit(0);
header('Content-type:application/json;charset=utf-8');
class api2Controller extends controller 
{
    private $apiKey = "12345";
    private $data = [];
    private $isError = FALSE;
    private $errorMsgs = [];
    private $errorCodes = [];
    private $model;
    private $index = 0;
    
    public function __construct() 
    {
        parent::__construct();
        $this->errorCodes = array(
            "authentication_failed" => "100",
            "session_not_started" => "101",
            "session_expired" => "102",
            "incomplete_parameters" => "103",
            "successfull" => "104",
            "unknown_error" => "105",
            "invalid_input" => "106",
            "failed" => "107",
            "blocked" => "108",
            "limit_exceed" => "109",
            "duplicate" => "110",
            "no_office" => "111"
        );
        $this->data['error'] = false;
        $this->data['session_initiated'] = false;
        $this->data['response'] = [];
    }
    
    public function index()
    {
        
    }
    
    //User Exists
    public function validate()
    {
        $user = '';
        
        if(isset($_POST['user']))
        {
            $user = $_POST['user'];
        }
        
        if(!empty($user))
        {
            if(ctype_alnum($user))
            {
                $username = $user;
            }
            else
            {
                $this->isError = true;
                $this->data['response'][$this->index]['error_msg'] = "Invalid Username";
                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
            }
        }
        else
        {
            $this->isError = true;
            $this->data['response'][$this->index]['error_msg'] = "Username is Required";
            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
        }
        
        if(!$this->isError)
        {
            $this->model = $this->loadModel("api");
            $exists = $this->model->usernameExists($username);
            
            if($exists)
            {
                $this->data['response']['isExists'] = TRUE;
            }
            else 
            {
                $this->data['response']['isExists'] = FALSE;
            }
        }
        
        echo json_encode($this->data);
    }
    
    public function initiate()
    {
        /*
        session::sessionStart();
        $key = '';
        if(isset($_POST['api_key']))
        {
            $key = $_POST['api_key'];
        }
        
        if(!empty($key))
        {
            if($key == $this->apiKey)
            {
                $this->data['error'] = false;
                $this->data['session_initiated'] = true;
                $this->data['response'][$this->index++]['phpssid'] = session::getSession();
            }
            else
            {
                $this->isError = true;
                $this->data['error'] = true;
                $this->data['session_initiated'] = false;
                $this->data['response'][$this->index]['error_msg'] = "Invalid Api Key";
                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['authentication_failed'];
            }
        }
        else
        {
            $this->isError = true;
            $this->data['error'] = true;
            $this->data['session_initiated'] = false;
            $this->data['response'][$this->index]['error_msg'] = "Api Key Is Missing";
            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
        }
        //print after json encode
        echo json_encode($this->data);
         
         */
    }

    public function login($api = '')
    {
        //$phpSSId = '';
        $user = '';
        $password = '';
        
        /*if(isset($_POST['phpssid']))
        {
            $phpSSId = $_POST['phpssid'];
        }*/
        
        if(isset($_POST['user']))
        {
            $user = $_POST['user'];
        }
        
        if(isset($_POST['password']))
        {
            $password = $_POST['password'];
        }
        
        if(isset($_POST['uuid']))
        {
            $uuid = $_POST['uuid'];
        }
        
       /* if(!empty($phpSSId))
        {
            if(session::setSession($phpSSId))
            {*/
                if(!empty($api))
                {
                    if($api != $this->apiKey)
                    {
                        $this->isError = true;
                        $this->data['response'][$this->index]['error_msg'] = "Invalid API";
                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['authentication_failed'];
                    }
                }
                else
                {
                    $this->isError = true;
                    $this->data['response'][$this->index]['error_msg'] = "API Key is Missing";
                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                }
                
                if(!empty($user))
                {
                    if(ctype_alnum($user))
                    {
                        $username = $user;
                    }
                    else
                    {
                        $this->isError = true;
                        $this->data['response'][$this->index]['error_msg'] = "Invalid Username";
                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                    }
                }
                else
                {
                    $this->isError = true;
                    $this->data['response'][$this->index]['error_msg'] = "Username is Required";
                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                }

                if(!empty($password))
                {
                    $password = password::encryptPassword($password);
                }
                else
                {
                    $this->isError = true;
                    $this->data['response'][$this->index]['error_msg'] = "Password is Required";
                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                }
                
                if(!empty($uuid))
                {
                    $uuid = securestr::clean($uuid);
                }
                else
                {
                    $this->isError = true;
                    $this->data['response'][$this->index]['error_msg'] = "UUID of Device is Required";
                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                }
            /*}
            else
            {
                $this->isError = true;
                $this->data['response'][$this->index]['error_msg'] = "Session ID is Required";
                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['authentication_failed'];
            }
        }
        else
        {
            $this->isError = true;
            $this->data['response'][$this->index]['error_msg'] = "Session ID is Required";
            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['session_not_started'];
        }
        */
        if(!$this->isError)
        {
            session::sessionStart();
            $this->data['session_initiated'] = true;
            
            $this->model = $this->loadModel("api");
            //$this->model =  $this->loadModel("access");
           // if($this->model->login($username , $password))
            if($this->model->apiLogin($username, $password))
            {
                $this->model = $this->loadModel("membership_types");
                $devicesAllowed = $this->model->getDevicesAllow(session::getMembershipType(), FALSE);
                
                $this->model = $this->loadModel("office");
                $totalOffices = $this->model->countOffices(session::getAdminId());
                    
                if($devicesAllowed == -2)
                {
                    $devicesAllowed = $totalOffices;
                }
                elseif($devicesAllowed == -1)
                {
                    $devicesAllowed = $totalOffices;//9999999999;
                }
                else 
                {
                    $devicesAllowed = 1;
                }
                //Insert Into api_logs
                $this->model = $this->loadModel("api_logs");
                $totalUUIDs = $this->model->countUUID(session::getAdminId());
                $uuidExists = $this->model->uuidExists(session::getAdminId(), $uuid);
                $isUnique = $this->model->isUnique($uuid);
                
                $success = TRUE;
                if($devicesAllowed > $totalUUIDs && !$uuidExists && $isUnique && $totalOffices > 0)
                {
                    //Insert into Logs
                    $this->model = $this->loadModel("logs");
                    session::setLogId($this->model->insertLog(session::getAdminId()));
                    $this->model = $this->loadModel("api_logs");
                    $this->model->insert(session::getLogId(), $uuid, session::getSession());
                }
                elseif($uuidExists && $totalOffices > 0)
                {
                    //Insert into Logs
                    $this->model = $this->loadModel("logs");
                    session::setLogId($this->model->insertLog(session::getAdminId()));
                    $this->model = $this->loadModel("api_logs");
                    $this->model->updateLogId(session::getLogId(), $uuid, session::getSession());
                }
                elseif(!$isUnique && $totalOffices > 0)
                {
                    $success = FALSE;
                    $this->isError = true;
                    $this->data['response'][$this->index]['error_msg'] = "UUID  Already Exists";
                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['duplicate'];
                    $this->data['session_initiated'] = false;
                    session::sessionDestroy();
                }
                elseif($totalOffices == 0)
                {
                    $success = FALSE;
                    $this->isError = true;
                    $this->data['response'][$this->index]['error_msg'] = "No Office Found";
                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['no_office'];
                    $this->data['session_initiated'] = false;
                    session::sessionDestroy();
                }
                else 
                {
                    $success = FALSE;
                    $this->isError = true;
                    $this->data['response'][$this->index]['error_msg'] = "Device Limit Exceed";
                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['limit_exceed'];
                    $this->data['session_initiated'] = false;
                    session::sessionDestroy();
                }
               
                if($success)
                {
                    $this->data['response'][$this->index++]['phpssid'] = session::getSession();
                    
                    $this->model = $this->loadModel("admin");
                    
                    $details = $this->model->loadDetails(session::getUsername());
                    $detail = $details[0];

                    if($detail['logo_path'] != '')
                        $logoPath = _SHARED_UPLOADS_."/logos/".$detail['logo_path'];
                    else
                        $logoPath = _SHARED_UPLOADS_."/logos/default.png";

                    $logo = file_get_contents($logoPath);
                    $base64Logo = base64_encode($logo);

                    $this->data['response'][$this->index++]['name'] = $detail['first_name']." ".$detail['last_name'];
                    $this->data['response'][$this->index++]['admin_id'] = $detail['admin_id'];
                    $this->data['response'][$this->index++]['company_title'] = $detail['company_name'];

                    $country = $this->loadModel("countries");
                    $this->data['response'][$this->index++]['country'] = $country->idToName($_SESSION[$this->config['admin_session_prefix'].'country_id']);
                    $this->data['response'][$this->index++]['logo'] = $base64Logo;

                    $this->model = $this->loadModel("membership_types");
                    $memType = $this->model->getTypeId("Free/Basic");

                    if($detail['membership_type_id'] == $memType)
                    {
                        $this->data['response'][$this->index++]['type'] = 0;
                        
                        $this->model = $this->loadModel("office");
                        $offices = $this->model->view($username);
                        foreach ($offices as $office)
                        {
                            $this->data['response'][$this->index++]['office_id'] = $office['office_id'];
                            break;
                        }
                    }
                    else 
                    {
                        $this->data['response'][$this->index++]['type'] = 1;
                    }
                }
            }
            else
            {
                $this->isError = true;
                $this->data['response'][$this->index]['error_msg'] = "Invalid Username or Password";
                $this->data['response'][$this->index]['error_code'] = $this->errorCodes['authentication_failed']; 
            }
        }
        
        $this->data['error'] = $this->isError;
        
        //print json string
        echo json_encode($this->data);
        die();
    }
    
    //Get Values
    public function get($get = '')
    {
        $phpSSId = '';
        if(isset($_POST['phpssid']))
        {
            $phpSSId = $_POST['phpssid'];
        }
        
        if(!empty($phpSSId))
        {
            if(session::setSession($phpSSId))
            {
                if(session::isLogin())
                {
                    $this->data['session_initiated'] = true;
                    
                    if(strtolower($get) == "offices")
                    {                        error_reporting(E_ALL);
                        $this->model = $this->loadModel("admin");
                        $username = $this->model->IdtoUsername(session::getAdminId());
                        $this->model = $this->loadModel("api_logs");
                        $uuid = $this->model->getUUIDIdByApiLog(session::getLogId());
                       
                        if($username != false)
                        {
                            $this->model = $this->loadModel("office");
                            $offices = $this->model->viewForApi(session::getAdminId(), $uuid);
                            
                            foreach ($offices as $office)
                            {
                                $this->data['response'][$this->index]['office_id'] = $office['office_id'];
                                $this->data['response'][$this->index++]['office_name'] = $office['office_name'];
                            }
                        }
                        else
                        {
                            $this->isError = true;
                            $this->data['response'][$this->index]['error_msg'] = "Something Went Wrong Try Again";
                            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['unknown_error'];
                        }
                    }
                    else if(strtolower($get) == "employees")
                    {
                        $this->model = $this->loadModel("admin");
                        $adminId = session::getAdminId();
                        
                        if($adminId)
                        {
                            $this->model = $this->loadModel("employee");
                            $employees = $this->model->viewEmployee($adminId);
                            
                            foreach ($employees as $employee)
                            {
                                $this->data['response'][$this->index]['emp_id'] = $employee['emp_id'];
                                $this->data['response'][$this->index]['name'] = $employee['emp_first_name']." ".$employee['emp_last_name'];
                                $this->data['response'][$this->index]['emp_pin'] = $employee['pin'];
                                $this->data['response'][$this->index++]['status'] = $employee['status'];
                            }
                        }
                        else
                        {
                            $this->isError = true;
                            $this->data['response'][$this->index]['error_msg'] = "Something Went Wrong Try Again";
                            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['unknown_error'];
                        }
                    }
                    elseif(strtolower($get) == "messages")
                    {
                        if(isset($_POST['office_id']))
                        {
                            if(is_numeric($_POST['office_id']))
                            {
                                $this->model = $this->loadModel("admin");
                            
                                $messages = $this->model->viewMessage(session::getAdminId());

                                $this->data['response']['global']['a_cin_message'] = $messages['a_cin_message'];
                                $this->data['response']['global']['a_cout_message'] = $messages['a_cout_message'];
                                $this->data['response']['global']['a_bin_message'] = $messages['a_bin_message'];
                                $this->data['response']['global']['a_bout_message'] = $messages['a_bout_message'];
                                
                                $this->model = $this->loadModel("office");
                                
                                $messages = $this->model->viewMessage($_POST['office_id']);
                                
                                $this->data['response']['office']['o_cin_message'] = $messages['o_cin_message'];
                                $this->data['response']['office']['o_cout_message'] = $messages['o_cout_message'];
                                $this->data['response']['office']['o_bin_message'] = $messages['o_bin_message'];
                                $this->data['response']['office']['o_bout_message'] = $messages['o_bout_message'];
                                $this->data['response']['office']['a_show_cin'] = $messages['a_show_cin'];
                                $this->data['response']['office']['a_show_cout'] = $messages['a_show_cout'];
                                $this->data['response']['office']['a_show_bin'] = $messages['a_show_bin'];
                                $this->data['response']['office']['a_show_bout'] = $messages['a_show_bout'];
                                
                                if(isset($_POST['timezone']))
                                {
                                    if(preg_match("/^(?:Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])$/", $_POST['timezone']))
                                    {
                                        $this->model->setTimezone($_POST['office_id'], $_POST['timezone']);
                                    }
                                    else 
                                    {
                                        $this->isError = true;
                                        $this->data['response'][$this->index]['error_msg'] = "Invalid Timezone".$_POST['timezone'];
                                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                                    }
                                }
                                else 
                                {
                                    $this->isError = true;
                                    $this->data['response'][$this->index]['error_msg'] = "Timezone is Required";
                                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                                }
                                $this->model = $this->loadModel("api_logs");
                                $this->model->updateOffice(session::getLogId(), $_POST['office_id']);
                            }
                            else 
                            {
                                $this->isError = true;
                                $this->data['response'][$this->index]['error_msg'] = "Invalid Office Id";
                                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                            }
                        }
                        else 
                        {
                            $this->isError = true;
                            $this->data['response'][$this->index]['error_msg'] = "Office Id is Required";
                            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                        }
                    }
                    elseif(strtolower($get) == "emp_status")
                    {
                        if(isset($_POST['emp_id']) && isset($_POST['checkin_id']))
                        {
                            if(is_numeric($_POST['emp_id']) && is_numeric($_POST['checkin_id']))
                            {
                                $this->model = $this->loadModel("employee");
                                if($this->model->isActive($_POST['emp_id']))
                                {
                                    $this->data['response'][$this->index++]['status'] = _ACTIVE_;
                                }
                                else 
                                {
                                    $this->data['response'][$this->index++]['status'] = _DEACTIVE_;
                                }
                                
                                $this->model = $this->loadModel("check_in");
                                $checkin_id = $this->model->checkInId($_POST['checkin_id'], $_POST['emp_id']);
                                
                                if($checkin_id)
                                {
                                    $this->model = $this->loadModel("check_out");
                                    $checkin_timestamp = $this->model->checkoutByCheckinId($checkin_id);
                                    if(is_array($checkin_timestamp))
                                    {
                                        $this->data['response'][$this->index++]['checkout_time'] = $checkin_timestamp['check_out_timestamp'];
                                    }
                                    else 
                                    {
                                        $this->data['response'][$this->index++]['checkout_time'] = NULL;
                                    }
                                }
                                else 
                                {
                                    $this->data['response'][$this->index++]['checkout_time'] = NULL;
                                }
                            }
                            else 
                            {
                            	$this->isError = true;
                            	$this->data['response'][$this->index]['error_msg'] = "Invalid Employee Id or Check-in Id";
                            	$this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                            }
                        }
                        else 
                        {
                            $this->isError = true;
                            $this->data['response'][$this->index]['error_msg'] = "Employee Id or Check-in Id is Required";
                            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                        }
                    }
                    elseif(strtolower($get) == "bulk_checkouts")
                    {
						if(isset($_POST['checkin_array']))
						{
							$data = json_decode($_POST['checkin_array']);
							if(json_last_error() == JSON_ERROR_NONE)
							{
								$this->model = $this->loadModel("check_out");
								$this->data['response'] = $this->model->get(session::getAdminId(), $data);
							}
                            else 
                            {
                            	$this->isError = true;
                            	$this->data['response'][$this->index]['error_msg'] = "Invalid Array Format";
                            	$this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                            }
						}
						else 
						{
							$this->isError = true;
							$this->data['response'][$this->index]['error_msg'] = "Checkin's Array Required";
							$this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
						}
					}
                    else
                    {
                        $this->isError = true;
                        $this->data['response'][$this->index]['error_msg'] = "Unknown Value";
                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                    }
                }
                else
                {
                    $this->isError = true;
                    $this->data['response'][$this->index]['error_msg'] = "Not Logged Inn";
                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['authentication_failed'];
                }
            }
            else
            {
                $this->isError = true;
                $this->data['response'][$this->index]['error_msg'] = "Invalid Session Id";
                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
            }
        }
        else
        {
            $this->isError = true;
            $this->data['response'][$this->index]['error_msg'] = "Session Id Is Missing";
            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
        }
        
        //Final Output
        $this->data['error'] = $this->isError;
        
        //print json string
        echo json_encode($this->data);
        die();
    }
    //Logout
    public function logout()
    {
        $phpSSId = '';
        if(isset($_POST['phpssid']))
        {
            $phpSSId = $_POST['phpssid'];
        }
        
        if(!empty($phpSSId))
        {
            if(session::setSession($phpSSId))
            {
                if(session::isLogin())
                {
                    session::sessionDestroy();
                    $this->isError = false;
                    $this->data['session_initiated'] = false;
                    $this->data['response'][$this->index]['message'] = "Logout Successfully";
                }
                else
                {
                    $this->isError = true;
                    $this->data['response'][$this->index]['error_msg'] = "Session Is Already Expired";
                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['session_expired'];
                }
            }
            else
            {
                $this->isError = true;
                $this->data['response'][$this->index]['error_msg'] = "Invalid Session Id";
                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
            }
        }
        else
        {
            $this->isError = true;
            $this->data['response'][$this->index]['error_msg'] = "Session Id Required";
            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
        }
        //Final Output
        $this->data['error'] = $this->isError;
        
        //print json string
        echo json_encode($this->data);
        die();
    }
    
    //Receive Data Api
    public function action($action = '', $type = '') 
    {
        $phpSSId = '';
        if(isset($_POST['phpssid']))
        {
            $phpSSId = $_POST['phpssid'];
        }
        
        if(!empty($phpSSId))
        {
            if(session::setSession($phpSSId))
            {
                if(session::isLogin())
                {
                    $this->data['session_initiated'] = true;
                    
                    $admin_id = session::getAdminId();
                    if(!empty($action))
                    {
                        if($action == "check")
                        {
                            if($type == "in")
                            {
                                //Validate Office 
                                if(isset($_POST['office_id']))
                                {
                                    if(is_numeric($_POST['office_id']))
                                    {
                                        /*if($_POST['office_id'] == 0)
                                        {
                                            $memType = session::getMembershipType();
                                            $this->model = $this->loadModel("membership_types");
                                            if($memType != $this->model->getTypeId("Free/Basic"))
                                            {
                                                $this->isError = true;
                                                $this->data['response'][$this->index]['error_msg'] = "Office Cannot Be 0";
                                                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['unknown_error'];
                                            }
                                        }
                                        else 
                                        {*/
                                            $this->model = $this->loadModel("office");
                                            $officeExist = $this->model->adminToOfficeRel($admin_id, $_POST['office_id']);
                                            if($officeExist)
                                            {
                                                $office_id = $_POST['office_id'];
                                            }
                                            else
                                            {
                                                $this->isError = true;
                                                $this->data['response'][$this->index]['error_msg'] = "Office Does Not Belongs to This Admin";
                                                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['unknown_error'];
                                            }
                                        //}
                                    }
                                    else
                                    {
                                        $this->isError = true;
                                        $this->data['response'][$this->index]['error_msg'] = "Invalid Office Id";
                                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                                    }
                                    
                                }
                                else
                                {
                                    $this->isError = true;
                                    $this->data['response'][$this->index]['error_msg'] = "Office Id is Required";
                                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                }

                                //Validate Employee
                                if(isset($_POST['emp_id']))
                                {
                                    $BLOCKED = FALSE;
                                    if(is_numeric($_POST['emp_id']))
                                    {
                                        $this->model = $this->loadModel("employee");
                                        $employeeExist = $this->model->adminToEmployeeRel($admin_id, $_POST['emp_id']);
                                        if($employeeExist)
                                        {
                                            $emp_id = $_POST['emp_id'];
                                            $this->model = $this->loadModel("employee");
                                            if(!$this->model->isActive($emp_id))
                                            {
                                                //$this->isError = true;
                                                $BLOCKED = TRUE;
                                                $this->data['response'][$this->index]['error_msg'] = "Employee Is Blocked. Please Contact with HR";
                                                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['blocked'];
                                            }
                                        }
                                        else
                                        {
                                            $this->isError = true;
                                            $this->data['response'][$this->index]['error_msg'] = "Employee Does Not Belongs to This Admin";
                                            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                                        }
                                    }
                                    else
                                    {
                                        $this->isError = true;
                                        $this->data['response'][$this->index]['error_msg'] = "Invalid Employee Id";
                                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['unknown_error'];
                                    }
                                }
                                else
                                {
                                    $this->isError = true;
                                    $this->data['response'][$this->index]['error_msg'] = "Employee Id is Required";
                                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                }

                                //Validate Times Stemp
                                if(isset($_POST['timestmp']))
                                {
                                    if(is_numeric($_POST['timestmp']))
                                    {
                                        $time_stmp = $_POST['timestmp'];
                                    }
                                    else
                                    {
                                        $this->isError = true;
                                        $this->data['response'][$this->index]['error_msg'] = "Invalid Timestemp";
                                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                                    }
                                }
                                else
                                {
                                    $this->isError = true;
                                    $this->data['response'][$this->index]['error_msg'] = "Timestemp is Required";
                                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                }

                                //Validate Device Id
                                if(isset($_POST['checkin_id']))
                                {
                                    if(is_numeric($_POST['checkin_id']))
                                    {
                                        $device_id = $_POST['checkin_id'];
                                    }
                                    else
                                    {
                                        $this->isError = true;
                                        $this->data['response'][$this->index]['error_msg'] = "Invalid CheckIn Id".$_POST['checkin_id'];
                                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                                    }
                                }
                                else
                                {
                                    $this->isError = true;
                                    $this->data['response'][$this->index]['error_msg'] = "CheckIn Id is Required";
                                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                }
                                //Get Image 
                                if(isset($_POST['image_base']))
                                {
                                    if(!empty($_POST['image_base']))
                                    {
                                        if(!$this->isError)
                                        {
                                            if($_POST['image_base'] == "no_image")
                                            {
                                                $memType = session::getMembershipType();
                                                $this->model = $this->loadModel("membership_types");
                                                if($memType != $this->model->getTypeId("Free/Basic"))
                                                {
                                                    $this->isError = true;
                                                    $this->data['response'][$this->index]['error_msg'] = "Employee's Photograph is not Attached";
                                                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                                }
                                                else
                                                {
                                                    $image = "nopreview.png";
                                                }
                                            }
                                            else 
                                            {
                                                $image = image::img($_POST['image_base']);
                                                if(!$image)
                                                {
                                                    $this->isError = true;
                                                    $this->data['response'][$this->index]['error_msg'] = "Something Went Wrong While Saving Image";
                                                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['unknown_error'];
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        $this->isError = true;
                                        $this->data['response'][$this->index]['error_msg'] = "Employee's Photograph is not Attached";
                                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                    }
                                }
                                else
                                {
                                    $this->isError = true;
                                    $this->data['response'][$this->index]['error_msg'] = "Employee's Photograph is Required";
                                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                }

                                if(!$this->isError)
                                {
                                    $this->model = $this->loadModel("check_in");
                                    //$last = $this->model->lastCheckin($emp_id);
                                    $unCheckedouts = $this->model->checkinWithoutCheckout($emp_id);
                                    if(is_array($unCheckedouts))
                                    {
                                        $first = $unCheckedouts[0];
                                        if($first['check_in_timestamp'] > $time_stmp)
                                        {
                                            $unCheckedouts = '';
                                            $BLOCKED = FALSE;
                                            $this->data['response'] = '';
                                            $this->index = 0;
                                        }
                                    }
                                    //if(is_array($last))
                                    if(is_array($unCheckedouts) && !$BLOCKED)
                                    {
                                        $this->model = $this->loadModel("check_out");
                                        //$checkedOut = $this->model->checkoutDone($last['check_in_id']);
                                        $checkedOut = FALSE;
                                        if(!$checkedOut)
                                        {
                                            $this->model = $this->loadModel("employee");
                                            $this->model->deactivateEmp($emp_id);
                                            
                                            //$this->isError = TRUE;
                                            $BLOCKED = TRUE;
                                            $this->data['response'][$this->index]['error_msg'] = "Employee Blocked Due To Not Check Out Last Time";
                                            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['blocked'];
                                        }
                                    }
                                    
                                    if(!$this->isError)
                                    {
                                        $this->model = $this->loadModel("office");
                                        $offset = $this->model->getTimezone($office_id);
                                        list($hours, $minutes) = explode(':', $offset);
                                        $seconds = $hours * 60 * 60 + $minutes * 60;
                                        
                                        $status = _IN_TIME_;
                                        $todayTime = date("Y-m-d");
                                        $this->model = $this->loadModel("check_in");
                                        $last = $this->model->lastCheckin($emp_id);
                                        
                                        if(is_array($last))
                                        {
                                            if($todayTime > $last['check_in_timestamp'])
                                            {
                                                $this->model = $this->loadModel("employee");
                                                $graceTime = $this->model->getGraceTime($emp_id);
                                                $fromTimestamp = $this->model->getFromTime($emp_id);
                                                if($graceTime == 0)
                                                {
                                                    $this->model = $this->loadModel("admin");
                                                    $graceTime = $this->model->getGraceTime(session::getAdminId());
                                                }

                                                $a = date("H:i", $fromTimestamp);
                                                $expectedEntry = strtotime($a);
                                                
//                                                $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 1);
//                                                // Workaround for bug #44780
//                                                if($timezone === false) 
//                                                {
//                                                    $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 0);
//                                                }
//                                                date_default_timezone_set($timezone);
                                                
                                                $a1 = date("H:i", $time_stmp + ($seconds));
                                                $realEntry = strtotime($a1);

                                                if($realEntry <= ($expectedEntry + $graceTime*60))
                                                {
                                                    $status = _IN_TIME_;
                                                }
                                                else
                                                {
                                                    $status = _LATE_;
                                                }
                                            }  
                                        }
                                        $this->model = $this->loadModel("check_in");
                                        
                                        $insert = $this->model->insertCheckIn($office_id, $emp_id, $device_id, $time_stmp, $image, $status);
                                        if($insert)
                                        {
                                            if(!$BLOCKED)
                                            {
                                                $this->data['response'][$this->index]['check_in_id'] = $insert;
                                                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['successfull'];
                                            }
                                        }
                                        else
                                        {
                                            $this->isError = true;
                                            $this->data['response'][$this->index]['error_msg'] = "Insertion Failed.";
                                            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['failed'];
                                        }
                                    }
                                }
                                else
                                {
                                    $this->isError = true;
                                    $this->data['response'][$this->index]['error_msg'] = "Something Went Wrong.";
                                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['failed'];
                                }
                                $this->data['error'] = $this->isError;
                            }
                            elseif($type == "out")
                            {
                                $BLOCKED = FALSE;
                                //Validate Times Stemp
                                if(isset($_POST['timestmp']))
                                {
                                    if(is_numeric($_POST['timestmp']))
                                    {
                                        $time_stmp = $_POST['timestmp'];
                                    }
                                    else
                                    {
                                        $this->isError = true;
                                        $this->data['response'][$this->index]['error_msg'] = "Invalid Timestemp";
                                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                                    }
                                }
                                else
                                {
                                    $this->isError = true;
                                    $this->data['response'][$this->index]['error_msg'] = "Timestemp is Required";
                                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                }
                                //Validate Employee
                                if(isset($_POST['emp_id']))
                                {
                                    if(is_numeric($_POST['emp_id']))
                                    {
                                        $this->model = $this->loadModel("employee");
                                        $employeeExist = $this->model->adminToEmployeeRel($admin_id, $_POST['emp_id']);
                                        if($employeeExist)
                                        {
                                            $emp_id = $_POST['emp_id'];
                                        }
                                        else
                                        {
                                            $this->isError = true;
                                            $this->data['response'][$this->index]['error_msg'] = "Employee Does Not Belongs to This Admin";
                                            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                                        }
                                    }
                                    else
                                    {
                                        $this->isError = true;
                                        $this->data['response'][$this->index]['error_msg'] = "Invalid Employee Id";
                                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['unknown_error'];
                                    }
                                }
                                else
                                {
                                    $this->isError = true;
                                    $this->data['response'][$this->index]['error_msg'] = "Employee Id is Required";
                                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                }
                                 //Validate Device Id
                                if(isset($_POST['checkin_id']))
                                {
                                    if(is_numeric($_POST['checkin_id']) )
                                    {
                                        $this->model = $this->loadModel("check_in");
                                        $deviceId = $_POST['checkin_id'];
                                        
                                        $checkin_id = $this->model->checkInId($_POST['checkin_id'], @$emp_id);
                                        $checkinTime = $this->model->checkinTimestamp($checkin_id);
                                        $office_id = $this->model->checkinToOfficeId($checkin_id);
                                        if(!$checkin_id)
                                        {
                                            $this->isError = true;
                                            $this->data['response'][$this->index]['error_msg'] = "Invalid CheckIn Id.";
                                            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                                        }
                                    }
                                    else
                                    {
                                        $this->isError = true;
                                        $this->data['response'][$this->index]['error_msg'] = "Invalid CheckIn Id";
                                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                                    }
                               
                                }
                                else
                                {
                                    $this->isError = true;
                                    $this->data['response'][$this->index]['error_msg'] = "CheckIn Id is Required";
                                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                }
                                
                                //Get Image 
                                if(isset($_POST['image_base']))
                                {
                                    if(!empty($_POST['image_base']))
                                    {
                                        if($_POST['image_base'] == "no_image")
                                        {
                                            $memType = session::getMembershipType();
                                            $this->model = $this->loadModel("membership_types");
                                            if($memType != $this->model->getTypeId("Free/Basic"))
                                            {
                                                $this->isError = true;
                                                $this->data['response'][$this->index]['error_msg'] = "Employee's Photograph is not Attached";
                                                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                            }
                                            else
                                            {
                                                $image = "nopreview.png";
                                            }
                                        }
                                        else 
                                        {
                                            $image = image::img($_POST['image_base']);
                                            if(!$image)
                                            {
                                                $this->isError = true;
                                                $this->data['response'][$this->index]['error_msg'] = "Something Went Wrong While Saving Image";
                                                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['unknown_error'];
                                            }
                                        }
                                    }
                                    else
                                    {
                                        $this->isError = true;
                                        $this->data['response'][$this->index]['error_msg'] = "Employee's Photograph is not Attached";
                                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                    }
                                }
                                else
                                {
                                    $this->isError = true;
                                    $this->data['response'][$this->index]['error_msg'] = "Employee's Photograph is Required";
                                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                }
                                
                                if(!$this->isError)
                                {
                                    $this->model = $this->loadModel("check_in");
                                    if(!isset($emp_id))
                                    {
                                        $emp_id = $this->model->empIdToDeviceId($deviceId);
                                    }
                                    
//                                    New Entry
                                    $unCheckedouts = $this->model->checkinWithoutCheckout($emp_id);
                                    if(is_array($unCheckedouts))
                                    { 
                                        $first = $unCheckedouts[0];
                                        if($first['check_in_timestamp'] < $time_stmp)
                                        { 
                                            $unCheckedouts = '';
                                            $this->model = $this->loadModel("employee");
                                            if(!$this->model->isActive($emp_id))
                                            {
                                                $this->isError = true;
                                                $this->data['response'][$this->index]['error_msg'] = "Employee Is Blocked. Please Contact with HR";
                                                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['blocked'];
                                            }
                                        }
                                    }
                                    else 
                                    { //New Entry End Excluding Inner Data
                                        $this->model = $this->loadModel("employee");
                                        if(!$this->model->isActive($emp_id))
                                        {
                                            $this->isError = true;
                                            $this->data['response'][$this->index]['error_msg'] = "Employee Is Blocked. Please Contact with HR";
                                            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['blocked'];
                                        }
                                    }//Here End New Entry
                                    if($time_stmp < $checkinTime)
                                    {
                                        $this->isError = true;
                                        $this->data['response'][$this->index]['error_msg'] = "Checkout Time Cannot Be Shorter Then Checkin Time";
                                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                                    }
                                    
                                    if(!$this->isError)
                                    {
                                        $this->model = $this->loadModel("office");
                                        $offset = $this->model->getTimezone($office_id);
                                        list($hours, $minutes) = explode(':', $offset);
                                        $seconds = $hours * 60 * 60 + $minutes * 60;
                                        
                                        $this->model = $this->loadModel("check_in");
                                        $emp_id = $this->model->checkinToEmpId($checkin_id);
                                        
                                        $this->model = $this->loadModel("employee");
                                        $toTimestamp = $this->model->getToTime($emp_id);
                                        
                                        $a = date("H:i", $toTimestamp);
                                        $expectedOut = strtotime($a);
                                        
//                                        $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 1);
//                                        // Workaround for bug #44780
//                                        if($timezone === false) 
//                                        {
//                                            $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 0);
//                                        }
//                                        date_default_timezone_set($timezone);
                                        
                                        $a1 = date("H:i", $time_stmp + ($seconds));
                                        $realOut = strtotime($a1);
                                        
                                        if($realOut >= $expectedOut)
                                        {
                                            $status = _OVERTIME_;
                                        }
                                        else 
                                        {
                                            $status = _EARLY_;
                                        }
                                        
                                        $this->model = $this->loadModel("check_out");
                                        if(!$this->model->checkoutExists($checkin_id))
                                        {
                                            $insert = $this->model->insertCheckOut($checkin_id, $time_stmp, $image, $status);
                                            if($insert)
                                            {
                                                $this->data['response'][$this->index]['check_out_id'] = $insert;
                                                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['successfull'];
                                            }
                                            else
                                            {
                                                $this->isError = true;
                                                $this->data['response'][$this->index]['error_msg'] = "Insertion Failed.";
                                                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['failed'];
                                            } 
                                        }
                                        else 
                                        {
                                            $this->data['response'][$this->index]['check_out_id'] = -1;
                                            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['successfull'];
                                        }
                                    }
                                }
                                $this->data['error'] = $this->isError;
                            }
                        }
                        elseif($action == "break")
                        {
                            if($type == "in")
                            {
                                //Validate Employee
                                if(isset($_POST['emp_id']))
                                {
                                    if(is_numeric($_POST['emp_id']))
                                    {
                                        $this->model = $this->loadModel("employee");
                                        $employeeExist = $this->model->adminToEmployeeRel($admin_id, $_POST['emp_id']);
                                        if($employeeExist)
                                        {
                                            $emp_id = $_POST['emp_id'];
                                        }
                                        else
                                        {
                                            $this->isError = true;
                                            $this->data['response'][$this->index]['error_msg'] = "Employee Does Not Belongs to This Admin";
                                            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                                        }
                                    }
                                    else
                                    {
                                        $this->isError = true;
                                        $this->data['response'][$this->index]['error_msg'] = "Invalid Employee Id";
                                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['unknown_error'];
                                    }
                                }
                                else
                                {
                                    $this->isError = true;
                                    $this->data['response'][$this->index]['error_msg'] = "Employee Id is Required";
                                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                }
                                //Validate Device Id
                                if(isset($_POST['checkin_id']))
                                {
                                    if(is_numeric($_POST['checkin_id']))
                                    {
                                        $this->model = $this->loadModel("check_in");
                                        $checkin_id = $this->model->checkInId($_POST['checkin_id'], @$emp_id);
                                        $checkinTime = $this->model->checkinTimestamp($checkin_id);
                                        if(!$checkin_id)
                                        {
                                            $this->isError = true;
                                            $this->data['response'][$this->index]['error_msg'] = "Invalid CheckIn Id";
                                            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                                        }
                                    }
                                    else
                                    {
                                        $this->isError = true;
                                        $this->data['response'][$this->index]['error_msg'] = "Invalid CheckIn Id";
                                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                                    }
                               
                                }
                                else
                                {
                                    $this->isError = true;
                                    $this->data['response'][$this->index]['error_msg'] = "CheckIn Id is Required";
                                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                }
                                //Validate Times Stemp
                                if(isset($_POST['timestmp']))
                                {
                                    if(is_numeric($_POST['timestmp']))
                                    {
                                        $time_stmp = $_POST['timestmp'];
                                    }
                                    else
                                    {
                                        $this->isError = true;
                                        $this->data['response'][$this->index]['error_msg'] = "Invalid Timestemp";
                                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                                    }
                                }
                                else
                                {
                                    $this->isError = true;
                                    $this->data['response'][$this->index]['error_msg'] = "Timestemp is Required";
                                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                }
                                
                                if(isset($_POST['image_base']))
                                {
                                    if(!empty($_POST['image_base']))
                                    {
                                        if($_POST['image_base'] == "no_image")
                                        {
                                            $memType = session::getMembershipType();
                                            $this->model = $this->loadModel("membership_types");
                                            if($memType != $this->model->getTypeId("Free/Basic"))
                                            {
                                                $this->isError = true;
                                                $this->data['response'][$this->index]['error_msg'] = "Employee's Photograph is not Attached";
                                                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                            }
                                            else
                                            {
                                                $image = "nopreview.png";
                                            }
                                        }
                                        else 
                                        {
                                            $image = image::img($_POST['image_base']);
                                            if(!$image)
                                            {
                                                $this->isError = true;
                                                $this->data['response'][$this->index]['error_msg'] = "Something Went Wrong While Saving Image";
                                                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['unknown_error'];
                                            }
                                        }
                                    }
                                    else
                                    {
                                        $this->isError = true;
                                        $this->data['response'][$this->index]['error_msg'] = "Employee's Photograph is not Attached";
                                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                    }
                                }
                                else
                                {
                                    $this->isError = true;
                                    $this->data['response'][$this->index]['error_msg'] = "Employee's Photograph is Required";
                                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                }
                                
                                if(!$this->isError)
                                {
                                    if($time_stmp > $checkinTime)
                                    {
                                        $this->model = $this->loadModel("break_in");
                                        $status = _IN_TIME_;
                                        if(!$this->model->breakinExists($checkin_id))
                                        {
                                            $insert = $this->model->insertBreakIn($checkin_id, $image, $time_stmp, $status);
                                            if($insert)
                                            {
                                                $this->data['response'][$this->index]['break_in_id'] = $insert;
                                                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['successfull'];
                                            }
                                            else
                                            {
                                                $this->isError = true;
                                                $this->data['response'][$this->index]['error_msg'] = "Insertion Failed.";
                                                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['failed'];
                                            }
                                        }
                                        else 
                                        {
                                            $this->data['response'][$this->index]['break_in_id'] = -1;
                                            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['successfull'];
                                        }
                                    }
                                    else
                                    {
                                        $this->isError = true;
                                        $this->data['response'][$this->index]['error_msg'] = "BreakIn Time Cannot Be Shorter Then CheckIn Time.";
                                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                                    }
                                }
                                $this->data['error'] = $this->isError;
                            }
                            elseif($type == "out")
                            {
                                //Validate Employee
                                if(isset($_POST['emp_id']))
                                {
                                    if(is_numeric($_POST['emp_id']))
                                    {
                                        $this->model = $this->loadModel("employee");
                                        $employeeExist = $this->model->adminToEmployeeRel($admin_id, $_POST['emp_id']);
                                        if($employeeExist)
                                        {
                                            $emp_id = $_POST['emp_id'];
                                        }
                                        else
                                        {
                                            $this->isError = true;
                                            $this->data['response'][$this->index]['error_msg'] = "Employee Does Not Belongs to This Admin";
                                            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                                        }
                                    }
                                    else
                                    {
                                        $this->isError = true;
                                        $this->data['response'][$this->index]['error_msg'] = "Invalid Employee Id";
                                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['unknown_error'];
                                    }
                                }
                                else
                                {
                                    $this->isError = true;
                                    $this->data['response'][$this->index]['error_msg'] = "Employee Id is Required";
                                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                }
                                 //Validate Device Id
                                if(isset($_POST['checkin_id']))
                                {
                                    if(is_numeric($_POST['checkin_id']))
                                    {
                                        $this->model = $this->loadModel("check_in");
                                        $checkin_id = $this->model->checkInId($_POST['checkin_id'], @$emp_id);
                                        $this->model = $this->loadModel("break_in");
                                        $breakinTime = $this->model->breakinTimestamp($checkin_id);
                                        if(!$checkin_id)
                                        {
                                            $this->isError = true;
                                            $this->data['response'][$this->index]['error_msg'] = "Invalid CheckIn Id";
                                            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                                        }
                                        elseif(!$breakinTime)
                                        {
                                            $this->isError = true;
                                            $this->data['response'][$this->index]['error_msg'] = "Break Out Without Break In";
                                            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                                       }
                                    }
                                    else
                                    {
                                        $this->isError = true;
                                        $this->data['response'][$this->index]['error_msg'] = "Invalid CheckIn Id";
                                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                                    }
                               
                                }
                                else
                                {
                                    $this->isError = true;
                                    $this->data['response'][$this->index]['error_msg'] = "CheckIn Id is Required";
                                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                }
                                //Validate Times Stemp
                                if(isset($_POST['timestmp']))
                                {
                                    if(is_numeric($_POST['timestmp']))
                                    {
                                        $time_stmp = $_POST['timestmp'];
                                    }
                                    else
                                    {
                                        $this->isError = true;
                                        $this->data['response'][$this->index]['error_msg'] = "Invalid Timestemp";
                                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                                    }
                                }
                                else
                                {
                                    $this->isError = true;
                                    $this->data['response'][$this->index]['error_msg'] = "Timestemp is Required";
                                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                }
                                
                                if(isset($_POST['image_base']))
                                {
                                    if(!empty($_POST['image_base']))
                                    {
                                        if($_POST['image_base'] == "no_image")
                                        {
                                            $memType = session::getMembershipType();
                                            $this->model = $this->loadModel("membership_types");
                                            if($memType != $this->model->getTypeId("Free/Basic"))
                                            {
                                                $this->isError = true;
                                                $this->data['response'][$this->index]['error_msg'] = "Employee's Photograph is not Attached";
                                                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                            }
                                            else
                                            {
                                                $image = "nopreview.png";
                                            }
                                        }
                                        else 
                                        {
                                            $image = image::img($_POST['image_base']);
                                            if(!$image)
                                            {
                                                $this->isError = true;
                                                $this->data['response'][$this->index]['error_msg'] = "Something Went Wrong While Saving Image";
                                                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['unknown_error'];
                                            }
                                        }
                                    }
                                    else
                                    {
                                        $this->isError = true;
                                        $this->data['response'][$this->index]['error_msg'] = "Employee's Photograph is not Attached";
                                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                    }
                                }
                                else
                                {
                                    $this->isError = true;
                                    $this->data['response'][$this->index]['error_msg'] = "Employee's Photograph is Required";
                                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                                }

                                if(!$this->isError)
                                {
                                    if($time_stmp >= $breakinTime)
                                    { 
                                        $this->model = $this->loadModel("check_in");
                                        $emp_id = $this->model->checkinToEmpId($checkin_id);
                                        
                                        $this->model = $this->loadModel("employee");
                                        $breakDuration = $this->model->getBreakDuration($emp_id);
                                                                                
                                        if(($breakinTime - $time_stmp)/60 > $breakDuration)
                                        {
                                            $status = _LATE_;
                                        } 
                                        else 
                                        {
                                            $status = _EARLY_;
                                        }
                                        
                                        
                                        $this->model = $this->loadModel("break_out");
                                        if(!$this->model->breakoutExists($checkin_id))
                                        {
                                            $insert = $this->model->insertBreakOut($checkin_id, $image, $time_stmp, $status);
                                            if($insert)
                                            {
                                                $this->data['response'][$this->index]['break_out_id'] = $insert;
                                                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['successfull'];
                                            }
                                            else
                                            {
                                                $this->isError = true;
                                                $this->data['response'][$this->index]['error_msg'] = "Insertion Failed.";
                                                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['failed'];
                                            }
                                        }
                                        else 
                                        {
                                            $this->data['response'][$this->index]['break_out_id'] = -1;
                                            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['successfull'];
                                        }
                                    }
                                    else
                                    {
                                        $this->isError = true;
                                        $this->data['response'][$this->index]['error_msg'] = "Break Out Time Cannot Be Shorter Then Break In Time.";
                                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                                    }
                                }
                                $this->data['error'] = $this->isError;                                
                            }

                        }
                    }
                }
                else
                {
                    $this->isError = true;
                    $this->data['response'][$this->index]['error_msg'] = "Not Logged Inn";
                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['authentication_failed'];
                }
                
            }
            else
            {
                $this->isError = true;
                $this->data['response'][$this->index]['error_msg'] = "Invalid Session Id";
                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
            }
        }
        else
        {
            $this->isError = true;
            $this->data['response'][$this->index]['error_msg'] = "Session Id Is Missing";
            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
        }

        echo json_encode($this->data);
        die();
    }
}
