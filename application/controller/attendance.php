<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of attendence
 *
 * @author Msajid
 */
class attendanceController extends controller 
{
   private $isError = false;
   private $errorMsgs;
   private $model;
   private $data;
   
   
   public function view($empId = '',$limit = 10, $offset = 0)
   {
       if(session::isLogin())
        {
            $filter = "all";
            $fromTime = 0;
            $toTime = 999999999999;
            if(isset($_POST['filter']))
            {
                if($_POST['filter'] == "all" || $_POST['filter'] == "today" || $_POST['filter'] == "custom")
                {
                    $filter = $_POST['filter'];
                    $_SESSION[__CLASS__.__FUNCTION__.'filter'] = $filter;
                    if($filter == "custom")
                    {
                        if(isset($_POST['from_date']) && @strtotime(str_replace("/", "-", $_POST['from_date'])))
                        {
                            $fromTime = strtotime(str_replace("/", "-", $_POST['from_date']));
                            $values['from_date'] = $_POST['from_date'];
                        }
                        else 
                        {
                            $this->errorMsgs['from_date'] = "Invalid Date";
                        }
                        if(isset($_POST['to_date']) && @strtotime(str_replace("/", "-", $_POST['to_date'])))
                        { 
                            $toTime = strtotime(str_replace("/", "-", $_POST['to_date'])) + 86400;
                            $values['to_date'] = $_POST['to_date'];
                        }
                        else 
                        {
                            $this->errorMsgs['to_date'] = "Invalid Date";
                        }
                        
                        if($fromTime > $toTime)
                        {
                            $this->errorMsgs['to_date'] = "Invalid Date";
                        }
                    }
                    elseif($filter == "all")
                    {
                        $this->model = $this->loadModel("check_in");
                        $fromTime = strtotime(date("d-m-Y", $this->model->firstCheckIn(@$_SESSION[__CLASS__.'empId'], _EMPLOYEE_)));
                        if($fromTime == -68400)
                        {
                            $fromTime = strtotime(date("d-m-Y"));
                        }
                        $toTime = strtotime(date("d-m-Y")) + 86400;
                    }
                    else 
                    {
                        $fromTime = strtotime(date("d-m-Y"));
                        $toTime = strtotime(date("d-m-Y")) + 86400;
                    }
                    
                    $_SESSION[__CLASS__.__FUNCTION__.'filter_from'] = $fromTime;
                    $_SESSION[__CLASS__.__FUNCTION__.'filter_to'] = $toTime;
                }
            }
            elseif(isset($_SESSION[__CLASS__.__FUNCTION__.'filter']))
            {
                $filter = $_SESSION[__CLASS__.__FUNCTION__.'filter'];
                $fromTime = $_SESSION[__CLASS__.__FUNCTION__.'filter_from'];
                $toTime = $_SESSION[__CLASS__.__FUNCTION__.'filter_to'];
            }
            
            if($filter == "today")
            {
                $fromTime = strtotime(date("d-m-Y"));
                $toTime = strtotime(date("d-m-Y")) + 86400;
                $_SESSION[__CLASS__.__FUNCTION__.'filter_from'] = $fromTime;
                $_SESSION[__CLASS__.__FUNCTION__.'filter_to'] = $toTime;
                $_SESSION[__CLASS__.__FUNCTION__.'filter'] = "today";
            }
           if(!is_numeric($limit) || !is_numeric($offset) || $limit <= 0 || $offset < 0)
            {
                $limit = 10;
                $offset = 0;
            }
            
           if(isset($_POST['submit']) && !isset($_POST['emp_submit']) && empty($empId))
            {
                //validate admin
                if(isset($_POST['admin']))
                {
                    if(isset($_POST['csrf']))
                    {
                        if(!csrf::validateCsrf($_POST['csrf'], __FUNCTION__))
                        {
                            $this->errorMsgs;
                            $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                        }
                    }
                    else
                    {
                        $this->errorMsgs;
                        $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                    }
                    if(is_numeric($_POST['admin']))
                    {
                        $this->model = $this->loadModel("admin");
                        if($this->model)
                        {
                            if($this->model->adminExists($_POST['admin'], _CLIENT_,TRUE))
                            {
                                $admin = $_POST['admin'];
                                $adminName = $this->model->idToUsername($admin);
                                session::sessionStart();
                                $_SESSION[__CLASS__.'adminId'] = $admin;
                                unset($_SESSION[__CLASS__.__FUNCTION__.'filter_from']);
                                unset($_SESSION[__CLASS__.__FUNCTION__.'filter_to']);
                                unset($_SESSION[__CLASS__.__FUNCTION__.'filter']);
                            }
                            else
                            {
                               $this->isError = true;
                               $this->errorMsgs['admin_error'] = "Admin Does Not Exists.";
                            }
                        }                    
                        else
                        {
                           $this->isError = true;
                           $this->errorMsgs['admin_error'] = "Admin does Not Exists";
                        }
                        $values['admin'] = $_POST['admin'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['admin_error'] = "Invalid Admin Selected";
                    }
               }
               else
               {
                   $this->isError = true;
                   $this->errorMsgs['admin_error'] = "Admin Name Required";
               }
               
               //date from
               /*
                if(isset($_POST['from']) && !@empty($_POST['from']))
                {
                    $_POST['from'] = str_replace("/", "-", $_POST['from']);
                    @list($year, $month, $day) = explode("-",$_POST['from']);
                    if(@checkdate($month, $day, $year))
                    {
                        $from = $_POST['from'];
                        $date1 = strtotime($from);
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['from_error'] = "Invalid Format of Date From.";
                    }
                    $values['from'] = securestr::clean($_POST['from']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['from_error'] = "Date From Is Required.";
                }
                
                //date to
                if(isset($_POST['to']) && !@empty($_POST['to']))
                {
                    $_POST['to'] = str_replace("/", "-", $_POST['to']);
                    @list($year, $month, $day) = explode("-",$_POST['to']);
                    if(@checkdate($month, $day, $year))
                    {
                        $to = $_POST['to'];
                        $date2 = strtotime($to);
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['to_error'] = "Invalid Format of Date To.";
                    }
                    $values['to'] = securestr::clean($_POST['to']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['to_error'] = "Date To Is Required.";
                }
                
                
                if($date2 < $date1)
                {
                    $this->isError = TRUE;
                    $this->errorMsgs['error'] = "End Date Is Shorter Then Start Date.";
                }
                */
               if($this->isError)
                {
                    unset($_POST['submit']);
                    $this->model = $this->loadModel("admin");
                    $admins = $this->model->viewAdmins();
                    $error = $this->errorMsgs;
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("admins", "csrf", "values", "error");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                }
                else
                {
                    $this->model = $this->loadModel("employee");
                    //$employees = $this->model->view($admin, $from , $to, TRUE);
                    $employees = $this->model->viewEmployee($admin);
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("csrf", "employees");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);  
                }
            }
            elseif(isset($_POST['emp_submit']) || !empty($empId))
            {
                if(!empty($empId) && !isset($_POST['emp_submit']))
                {
                    $_POST['employee'] = $empId;
                    $_POST['csrf'] = '';
                }
                
                if(isset($_POST['employee']))
                {
                    if(isset($_SESSION[__CLASS__.'adminId']))
                    {
                        $admin = $_SESSION[__CLASS__.'adminId'];
                    }
                    else 
                    {
                        $admin = session::getAdminId();
                    }
                    
                    if(isset($_POST['csrf']) || !empty($empId))
                    {
                        if(!csrf::validateCsrf($_POST['csrf'], __FUNCTION__) && empty($empId))
                        {
                            $this->errorMsgs;
                            $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                        }
                    }
                    else
                    {
                        $this->errorMsgs;
                        $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                    }
                    if(is_numeric($_POST['employee']))
                    {
                        $this->model = $this->loadModel("employee");
                        if($this->model)
                        {
                            if($this->model->adminToEmployeeRel($admin, $_POST['employee']))
                            {
                                $emp = $_POST['employee'];
                                $empName = $this->model->idToName($_POST['employee']);
                                session::sessionStart();
                                $_SESSION[__CLASS__.'empId'] = $emp;
                                
                                if(!isset($_SESSION[__CLASS__.__FUNCTION__.'filter']))
                                {   
                                    $this->model = $this->loadModel("check_in");
                                    $fromTime = strtotime(date("d-m-Y", $this->model->firstCheckIn($emp, _EMPLOYEE_)));
                                    $toTime = strtotime(date("d-m-Y")) + 86400;
                                    $_SESSION[__CLASS__.__FUNCTION__.'filter_from'] = $fromTime;
                                    $_SESSION[__CLASS__.__FUNCTION__.'filter_to'] = $toTime;
                                    $_SESSION[__CLASS__.__FUNCTION__.'filter'] = "all";
                                }
                            }
                            else
                            {
                               $this->isError = true;
                               $this->errorMsgs['employee_error'] = "Employee Does Not Exists.";
                            }
                        }                    
                        else
                        {
                           $this->isError = true;
                           $this->errorMsgs['employee_error'] = "Employee does Not Exists";
                        }
                        $values['emp'] = $_POST['employee'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['employee_error'] = "Invalid Employee Selected";
                    }
               }
               else
               {
                   $this->isError = true;
                   $this->errorMsgs['employee_error'] = "Employee Name Required";
               }
               if($this->isError)
                { 
                    $_POST['submit'] = '';
                    unset($_POST['emp_submit']);
                    $error = $this->errorMsgs;
                    $this->model = $this->loadModel("employee");
                    $employees = $this->model->viewEmployee($admin);
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("csrf", "employees", "error");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data); 
                }
                else
                {
                    $_POST['submit'] = '';
                    $_POST['emp_submit'] = '';
                    $error = $this->errorMsgs;
                    $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                    $attendance = $this->model->view($emp, $offset, $limit, $fromTime, $toTime);                    
                    $total = $this->model->countAttendance($emp, _EMPLOYEE_, $filter, $fromTime, $toTime);
                    $next = $offset + $limit;
                    if($next > $total)
                    {
                        $next = $offset;
                    }
                    $previous = $offset - $limit;
                    if($previous < 0)
                    {
                        $previous = 0;
                    }
                    $current = ceil($offset / $limit);
                    $current++;
                    if($current == 0)
                    {
                        $current = 1;
                    }
                    //Export
                   // $exportId = md5(time().rand(0, 999));
                    //exportCSV::setData($attendance, $exportId);
                    $this->data = compact("attendance", "error", "filter", "values", "empName", "exportId", "emp", "total", "next", "previous", "limit", "offset", "current");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);  
                }
            }
            else
            {
               $this->javascripts[] = "{$this->config['domain']}"._PUBLIC_PATH_."js/jquery.validate.min.js";
                //loading countries model
                $adminModel = $this->loadModel("admin");
                $admins = [];

                if($adminModel)
                {
                    $admins = $adminModel->viewAdmins();
                }
                $error = $this->errorMsgs;
                $csrf = csrf::generateCsrf(__FUNCTION__);
                $this->data = compact("admins", "csrf", "error", "values");
                $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
            }
        }
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
            die();
        }
   }
   public function index() 
   {
       
   }
   public function offices($officeId = '',$limit = 10, $offset = 0)
   {
       if(session::isLogin())
        {
            $filter = "all";
            $fromTime = 0;
            $toTime = 999999999999;
            if(isset($_POST['filter']))
            {
                if($_POST['filter'] == "all" || $_POST['filter'] == "today" || $_POST['filter'] == "custom")
                {
                    $filter = $_POST['filter'];
                    $_SESSION[__CLASS__.__FUNCTION__.'filter'] = $filter;
                    if($filter == "custom")
                    {
                        if(isset($_POST['from_date']) && @strtotime(str_replace("/", "-", $_POST['from_date'])))
                        {
                            $fromTime = strtotime(str_replace("/", "-", $_POST['from_date']));
                            $values['from_date'] = $_POST['from_date'];
                        }
                        else 
                        {
                            $this->errorMsgs['from_date'] = "Invalid Date";
                        }
                        if(isset($_POST['to_date']) && @strtotime(str_replace("/", "-", $_POST['to_date'])))
                        { 
                            $toTime = strtotime(str_replace("/", "-", $_POST['to_date'])) + 86400;
                            $values['to_date'] = $_POST['to_date'];
                        }
                        else 
                        {
                            $this->errorMsgs['to_date'] = "Invalid Date";
                        }
                        
                        if($fromTime > $toTime)
                        {
                            $this->errorMsgs['to_date'] = "Invalid Date";
                        }
                    }
                    elseif($filter == "all")
                    {
                        $this->model = $this->loadModel("check_in");
                        $fromTime = $this->model->firstCheckIn(@$_SESSION[__CLASS__.'ofcId'], _OFFICE_);
                        if($fromTime == -68400)
                        {
                            $fromTime = strtotime(date("d-m-Y"));
                        }
                        $toTime = strtotime(date("d-m-Y")) + 86400;
                    }
                    else 
                    {
                        $fromTime = strtotime(date("d-m-Y"));
                        $toTime = strtotime(date("d-m-Y")) + 86400;
                    }
                    
                    $_SESSION[__CLASS__.__FUNCTION__.'filter_from'] = $fromTime;
                    $_SESSION[__CLASS__.__FUNCTION__.'filter_to'] = $toTime;
                }
            }
            elseif(isset($_SESSION[__CLASS__.__FUNCTION__.'filter']))
            {
                $filter = $_SESSION[__CLASS__.__FUNCTION__.'filter'];
                $fromTime = $_SESSION[__CLASS__.__FUNCTION__.'filter_from'];
                $toTime = $_SESSION[__CLASS__.__FUNCTION__.'filter_to'];
            }
            
            if($filter == "today")
            {
                $fromTime = strtotime(date("d-m-Y"));
                $toTime = strtotime(date("d-m-Y")) + 86400;
                $_SESSION[__CLASS__.__FUNCTION__.'filter_from'] = $fromTime;
                $_SESSION[__CLASS__.__FUNCTION__.'filter_to'] = $toTime;
                $_SESSION[__CLASS__.__FUNCTION__.'filter'] = "today";
            }
           if(!is_numeric($limit) || !is_numeric($offset) || $limit <= 0 || $offset < 0)
            {
                $limit = 10;
                $offset = 0;
            }
            
           if(isset($_POST['submit']) && !isset($_POST['ofc_submit']) && empty($officeId))
            {
                //validate admin
                if(isset($_POST['admin']))
                {
                    if(is_numeric($_POST['admin']))
                    {
                        $this->model = $this->loadModel("admin");
                        if($this->model)
                        {
                            if($this->model->adminExists($_POST['admin'], _CLIENT_,TRUE))
                            {
                                $admin = $_POST['admin'];
                                $adminName = $this->model->idToUsername($admin);
                                session::sessionStart();
                                $_SESSION[__CLASS__.'adminId'] = $admin;
                                unset($_SESSION[__CLASS__.__FUNCTION__.'filter_from']);
                                unset($_SESSION[__CLASS__.__FUNCTION__.'filter_to']);
                                unset($_SESSION[__CLASS__.__FUNCTION__.'filter']);
                            }
                            else
                            {
                               $this->isError = true;
                               $this->errorMsgs['admin_error'] = "Admin Does Not Exists.";
                            }
                        }                    
                        else
                        {
                           $this->isError = true;
                           $this->errorMsgs['admin_error'] = "Admin does Not Exists";
                        }
                        $values['admin'] = $_POST['admin'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['admin_error'] = "Invalid Admin Selected";
                    }
               }
               else
               {
                   $this->isError = true;
                   $this->errorMsgs['admin_error'] = "Admin Name Required";
               }
               if($this->isError)
                {
                    unset($_POST['submit']);
                    $this->model = $this->loadModel("admin");
                    $admins = $this->model->viewAdmins();
                    $error = $this->errorMsgs;
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("admins", "csrf", "values", "error");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                }
                else
                {
                    $this->model = $this->loadModel("office");
                    $offices = $this->model->view($admin);
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("csrf", "offices");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);  
                }
            }
            elseif(isset($_POST['ofc_submit']) || !empty($officeId))
            {
                if(!empty($officeId) && !isset($_POST['ofc_submit']))
                {
                    $_POST['office'] = $officeId;
                    $_POST['csrf'] = '';
                }
                
                if(isset($_POST['office']))
                {
                    if(isset($_SESSION[__CLASS__.'adminId']))
                    {
                        $admin = $_SESSION[__CLASS__.'adminId'];
                    }
                    else 
                    {
                        $admin = session::getAdminId();
                    }
                    
                    if(is_numeric($_POST['office']))
                    {
                        $this->model = $this->loadModel("office");
                        if($this->model)
                        {
                            if($this->model->adminToOfficeRel($admin, $_POST['office']))
                            {
                                $ofc = $_POST['office'];
                                $ofcName = $this->model->idToName($_POST['office']);
                                session::sessionStart();
                                $_SESSION[__CLASS__.'ofcId'] = $ofc;
                                if(!isset($_SESSION[__CLASS__.__FUNCTION__.'filter']))
                                {
                                    $this->model = $this->loadModel("check_in");
                                    $fromTime = $this->model->firstCheckIn($ofc, _OFFICE_);
                                    $toTime = strtotime(date("d-m-Y")) + 86400;
                                    $_SESSION[__CLASS__.__FUNCTION__.'filter_from'] = $fromTime;
                                    $_SESSION[__CLASS__.__FUNCTION__.'filter_to'] = $toTime;
                                    $_SESSION[__CLASS__.__FUNCTION__.'filter'] = "all";
                                }
                            }
                            else
                            {
                               $this->isError = true;
                               $this->errorMsgs['office_error'] = "Office Does Not Exists.";
                            }
                        }                    
                        else
                        {
                           $this->isError = true;
                           $this->errorMsgs['office_error'] = "Office does Not Exists";
                        }
                        $values['ofc'] = $_POST['office'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['office_error'] = "Invalid Office Selected";
                    }
               }
               else
               {
                   $this->isError = true;
                   $this->errorMsgs['office_error'] = "Office Name Required";
               }
               if($this->isError)
                { 
                    $_POST['submit'] = '';
                    unset($_POST['ofc_submit']);
                    $error = $this->errorMsgs;
                    $this->model = $this->loadModel("office");
                    $offices = $this->model->view($admin);
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("csrf", "offices");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data); 
                }
                else
                {
                    $_POST['submit'] = '';
                    $_POST['ofc_submit'] = '';
                    $error = $this->errorMsgs;
                    $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                    $attendance = $this->model->viewByOffice($ofc, $offset, $limit, $fromTime, $toTime);                  
                    $total = $this->model->countAttendance($ofc, _OFFICE_, $filter, $fromTime, $toTime);
                    $next = $offset + $limit;
                    if($next > $total)
                    {
                        $next = $offset;
                    }
                    $previous = $offset - $limit;
                    if($previous < 0)
                    {
                        $previous = 0;
                    }
                    $current = ceil($offset / $limit);
                    $current++;
                    if($current == 0)
                    {
                        $current = 1;
                    }
                    //Export
                   // $exportId = md5(time().rand(0, 999));
                    //exportCSV::setData($attendance, $exportId);
                    $this->data = compact("attendance", "ofcName","error", "values", "filter", "exportId", "ofc", "total", "next", "previous", "limit", "offset", "current");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);  
                }
            }
            else
            {
               $this->javascripts[] = "{$this->config['domain']}"._PUBLIC_PATH_."js/jquery.validate.min.js";
                //loading countries model
                $adminModel = $this->loadModel("admin");
                $admins = [];

                if($adminModel)
                {
                    $admins = $adminModel->viewAdmins();
                }
                $error = $this->errorMsgs;
                $csrf = csrf::generateCsrf(__FUNCTION__);
                $this->data = compact("admins", "csrf", "error", "values");
                $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
            }
        }
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
            die();
        }
   }
   
   //Ajax Calls
   public function ajax($type = '') 
   {
       $isError = FALSE;
       $msg = "";
       if($type == 'subscribe')
       {
           if(isset($_POST['value']) && isset($_POST['type']) && isset($_POST['box']) && isset($_SESSION[__CLASS__."reports"]))
           {
               $subscribe_bit = $_POST['value'];
               $report_type = $_POST['type'];
               $isChecked = $_POST['box'];
               $admin = $_SESSION[__CLASS__."reports"];
               
               if($subscribe_bit == _DAILY_ATTND_ || $subscribe_bit == _DAILY_CHECKIN_ || $subscribe_bit == _DAILY_FLAG_)
               {
                   if($report_type == 'daily_attnd' || $report_type == 'daily_checkin' || $report_type == 'daily_flag')
                   {
                        $this->model = $this->loadModel("admin");
                        $bits = $this->model->getReportsBit($admin);
                        if($isChecked == 'true' && !($subscribe_bit & $bits))
                        {
                            $this->model->setReportsBit($admin, $subscribe_bit, '+');
                            $msg = "Email Subscribtion Activated";
                        }
                        elseif($isChecked == 'false' && $subscribe_bit & $bits)
                        {
                            $this->model->setReportsBit($admin, $subscribe_bit, '-');
                            $msg = "Email Subscribtion Deactivated";
                        }
                        else 
                        {
                            $isError = TRUE;
                            $msg = "Something Went Wrong";
                        }
                   }
                   else 
                    {
                        $isError = TRUE;
                        $msg = "Something Went Wrong";
                    }
               }
               else 
                {
                    $isError = TRUE;
                    $msg = "Something Went Wrong";
                }
           }
            else 
            {
                $isError = TRUE;
                $msg = "Something Went Wrong";
            }
       }
       else 
       {
           $isError = TRUE;
           $msg = "Something Went Wrong";
       }
       echo json_encode(compact("isError", "msg"));
       die();
   }
   
   public function exportit($format = 'pdf', $reportType = 'flag', $type = 'normal', $data = '')
   {       
       if(session::isLogin() || isset($_SESSION['cron_script']))
       {
            $report = "flag";
            $fromTime = 0;
            $toTime = strtotime(date("d-m-Y"));
            $filter = "check_in";
            $id = NULL;
            $isError = FALSE;
            
            if($format == 'pdf' || $format == 'csv')
            {
                if($type == 'filtered')
                {
                    if($data == 'employee')
                    { //echo "<pre>".  print_r($_SESSION)."</pre>";
                        if(isset($_SESSION[__CLASS__.'viewfilter_from']) && isset($_SESSION[__CLASS__.'viewfilter_to']) && isset($_SESSION[__CLASS__.'viewfilter']) && isset($_SESSION[__CLASS__.'empId']))
                        {
                            $fromTime = $_SESSION[__CLASS__.'viewfilter_from'];
                            $toTime = $_SESSION[__CLASS__.'viewfilter_to'];
                            $all = $_SESSION[__CLASS__.'viewfilter'];
                            $id = $_SESSION[__CLASS__.'empId'];
                            $filterBy = _EMPLOYEE_;
                            $this->model = $this->loadModel("employee");
                            $admin = $this->model->employeeToAdmin($id);
                            $title = $this->model->idToName($id);
                        }
                        else 
                        {
                            $isError = TRUE;
                        }
                    }
                    elseif($data == 'office')
                    {
                        if(isset($_SESSION[__CLASS__.'officesfilter_from']) && isset($_SESSION[__CLASS__.'officesfilter_to']) && isset($_SESSION[__CLASS__.'officesfilter']))
                        {
                            $fromTime = $_SESSION[__CLASS__.'officesfilter_from'];
                            $toTime = $_SESSION[__CLASS__.'officesfilter_to'];
                            $all = $_SESSION[__CLASS__.'officesfilter'];
                            $id = $_SESSION[__CLASS__.'ofcId'];
                            $filterBy = _OFFICE_;
                            $this->model = $this->loadModel("office");
                            $admin = $this->model->officeToAdmin($id);
                            $title = $this->model->idToName($id);
                            $timezone = $this->model->getTimezone($id);
                        }
                        else 
                        {
                            $isError = TRUE;
                        }
                    }
                    else 
                    {
                        $isError = TRUE;
                    }
                }
                elseif($type == "normal")
                {
                    if($data == 'company')
                    {
                        if(isset($_SESSION[__CLASS__."reports"]))
                        {
                            $this->model = $this->loadModel("check_in");
                            $fromTime = $this->model->firstCheckIn($_SESSION[__CLASS__."reports"], _CLIENT_);
                            $toTime = strtotime(date("m/d/Y")) + 86400;
                            $all = $type;
                            $admin = $id = $_SESSION[__CLASS__."reports"];
                            $filterBy = _CLIENT_;
                            $this->model = $this->loadModel("admin");
                            $title = $this->model->idToUsername($admin);
                        }
                        else 
                        {
                            $isError = TRUE;
                        }
                    }
                    else 
                    {
                        $isError = TRUE;
                    }
                }
                elseif($type == "daily")
                {
                    if($data == 'company')
                    { 
                        if(isset($_SESSION[__CLASS__."reports"]))
                        {
                            $fromTime = strtotime(date("m/d/Y"));
                            $toTime = strtotime(date("m/d/Y")) + 86400;
                            if($type == "daily")
                            {
                                $fromTime = strtotime(date("m/d/Y"));
                            }
                            $all = $type;
                            $admin = $id = $_SESSION[__CLASS__."reports"];
                            $filterBy = _CLIENT_;
                            $this->model = $this->loadModel("admin");
                            $title = $this->model->idToUsername($admin);
                        }
                        else 
                        {
                            $isError = TRUE;
                        }
                    }
                    else 
                    {
                        $isError = TRUE;
                    }
                }
                elseif($type == "custom")
                {
                    if($data == 'company')
                    {
                        if(isset($_SESSION[__CLASS__."reports"]))
                        {
                            $fromTime = $_SESSION[__CLASS__."exportfrom"];
                            $toTime = $_SESSION[__CLASS__."exportto"] + 86400;
                            $all = $type;
                            $admin = $id = $_SESSION[__CLASS__."reports"];
                            $filterBy = _CLIENT_;
                            $this->model = $this->loadModel("admin");
                            $title = $this->model->idToUsername($admin);
                        }
                        else 
                        {
                            $isError = TRUE;
                        }
                    }
                    else 
                    {
                        $isError = TRUE;
                    }
                }
                elseif($type == "filtered_order")
                { 
                    if($data == 'office')
                    {
                        $type = "filtered";
                        if(isset($_SESSION[__CLASS__.'officesfilter_from']) && isset($_SESSION[__CLASS__.'officesfilter_to']) && isset($_SESSION[__CLASS__.'officesfilter']))
                        { 
                            $fromTime = $_SESSION[__CLASS__.'officesfilter_from'];
                            $toTime = $_SESSION[__CLASS__.'officesfilter_to'];
                            $all = $_SESSION[__CLASS__.'officesfilter'];
                            $id = $_SESSION[__CLASS__.'ofcId'];
                            $filterBy = _OFFICE_;
                            $this->model = $this->loadModel("office");
                            $admin = $this->model->officeToAdmin($id);
                            $title = $this->model->idToName($id);
                            $timezone = $this->model->getTimezone($id);
                            $order = "by_emp";
                        }
                        else 
                        {
                            $isError = TRUE;
                        }
                    }
                    else 
                    {
                        $isError = TRUE;
                    }
                }
                
                if(!$isError)
                {
                    if(isset($_SESSION['client_timezone']))
                    {
                        date_default_timezone_set($_SESSION['client_timezone']);
                    }
                    $this->model = $this->loadModel("admin");
                    $companyLogo = $this->model->getlogo($admin);
                    if(empty($companyLogo))
                    {
                        $companyLogo = _SHARED_UPLOADS_."/logos/default.png";
                    }
                    else 
                    {
                        $companyLogo = _SHARED_UPLOADS_."/logos/".$companyLogo;
                    }
                    $otlLogo = _SHARED_UPLOADS_."/logos/default.png";
                    $flagLogo = _SHARED_UPLOADS_."/flag.png";
                    
                    if($reportType == 'flag')
                    {                       
                        $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                        $flagReports = $this->model->flagReportOmi($id, $filterBy, $fromTime, $toTime);
//                        echo "<pre> => <br>";
//                        print_r($flagReports);
//                        echo $this->model->db->error;die("<br>I Am Done");
                        if(is_array($flagReports))
                        { 
                            set_time_limit(0);
                            ini_set('memory_limit', '-1');
                            //Writing Data TO CSV
                            $csv = new PHPExcel();
                            if($format != "pdf")
                            {
                                if($format == "pdf")
                                {
        //                            $rendererName = PHPExcel_Settings::PDF_RENDERER_TCPDF;
        //                            $rendererLibrary = 'tcpdf';
                                    $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                                    $rendererLibrary = 'mpdf';
    //                                $rendererName = PHPExcel_Settings::PDF_RENDERER_DOMPDF;
    //                                $rendererLibrary = 'dom';
                                    $rendererLibraryPath = _LIBRARIES_PATH_."/".$rendererLibrary;
                                    PHPExcel_Settings::setPdfRenderer($rendererName,$rendererLibraryPath); 
                                    $csvWriter = PHPExcel_IOFactory::createWriter($csv, "PDF");
                                    $extenstion = "pdf";
                                }
                                else 
                                {
                                    $csvWriter = PHPExcel_IOFactory::createWriter($csv, "Excel5");
                                    $extenstion = "csv";
                                }

                                if(!isset($_SESSION['cron_script']))
                                {
                                    //Sending Headers
                                    if($format == "pdf")
                                    {
                                        header("Content-Type: application/pdf");
                                    }
                                    else 
                                    {
                                        header("Content-Type: application/csv");
                                    }
                                }
                                $csv->getProperties()->setLastModifiedBy("OTL");
                                $csv->getProperties()->setTitle("OTL - ".ucfirst($type)." Flag Report");
                                $csv->getProperties()->setSubject("Flag Report");
                                $csv->getProperties()->setDescription("This Is Auto Flag Report Generated By OTL");

                                $csv->setActiveSheetIndex(0); 

                                //Adding Image TO Sheet
                                $objDrawing = new PHPExcel_Worksheet_Drawing();
                                $objDrawing->setName('OTL LOGO');
                                $objDrawing->setDescription('OTL LOGO');
                                $objDrawing->setPath($otlLogo);

                                $objDrawing->setWidthAndHeight(120,90);
                                $objDrawing->setResizeProportional(true);
                                $objDrawing->setCoordinates('A1');
                                $objDrawing->setWorksheet($csv->getActiveSheet());

                                $objDrawing = '';

                                //$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
                                $objDrawing = new PHPExcel_Worksheet_Drawing();
                                $objDrawing->setName('Company LOGO');
                                $objDrawing->setDescription('Company LOGO');
                                $objDrawing->setPath($companyLogo);

                                $objDrawing->setWidthAndHeight(150,90);
                                $objDrawing->setResizeProportional(true);
                                $objDrawing->setCoordinates('E1');
                                $objDrawing->setWorksheet($csv->getActiveSheet());

                                $csv->getActiveSheet()->getRowDimension(1)->setRowHeight("30");
                                //End

                                //Writing Heading
                                if($filterBy == _EMPLOYEE_)
                                {
                                    header("Content-Disposition: attachment; filename=\"Employee_Flag_Report.$extenstion\"");
                                    header("Cache-Control: max-age=0");
                                    $csv->getActiveSheet()->SetCellValue('A2', 'Employee Flag Report');
                                }
                                elseif($filterBy == _OFFICE_)
                                {
                                    header("Content-Disposition: attachment; filename=\"Site_Flag_Report.$extenstion\"");
                                    header("Cache-Control: max-age=0");
                                    $csv->getActiveSheet()->SetCellValue('A2', 'Site/Office Flag Report');
                                }
                                elseif($filterBy == _CLIENT_)
                                {
                                    header("Content-Disposition: attachment; filename=\"Company_Flag_Report.$extenstion\"");
                                    header("Cache-Control: max-age=0");
                                    $csv->getActiveSheet()->SetCellValue('A2', 'Company Flag Report');
                                }
                                $csv->getActiveSheet()->mergeCells("A2:F2");
                                $csv->getActiveSheet()->getStyle('A2:F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                $csv->getActiveSheet()->getStyle('A2:F2')->getFont()->setBold()->setSize(18);

                                $csv->getActiveSheet()->SetCellValue('A3', date("d/m/Y h:i a"));
                                $csv->getActiveSheet()->mergeCells("A3:F3");
                                $csv->getActiveSheet()->getStyle('A3:F3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                //Writing Employee or Office Detail
                                if($filterBy == _EMPLOYEE_)
                                {
                                    $csv->getActiveSheet()->SetCellValue('A4', 'Employee Name: '.$title);
                                    $csv->getActiveSheet()->SetCellValue('A5', 'Employee ID: '.$id);
                                    $csv->getActiveSheet()->getStyle('A4:A5')->getFont()->setBold();
                                    $i = 6;
                                }
                                elseif($filterBy == _OFFICE_)
                                {
                                    $csv->getActiveSheet()->SetCellValue('A4', 'Site/Office: '.$title);
                                    $csv->getActiveSheet()->SetCellValue('A5', 'Time Zone: '.$timezone.' UTC');
                                    $csv->getActiveSheet()->getStyle('A4:A5')->getFont()->setBold();
                                    $i = 6; 
                                }
                                elseif($filterBy == _CLIENT_)
                                {
                                    $csv->getActiveSheet()->SetCellValue('A4', 'Company: '.$title);
                                    $csv->getActiveSheet()->getStyle('A4')->getFont()->setBold();
                                    $i = 5; 
                                }
                                
                                if($type == "daily")
                                {
                                    $csv->getActiveSheet()->SetCellValue("A$i", 'Date: '.date("d/m/Y"));
                                    $csv->getActiveSheet()->getStyle("D$i:F$i")->getFont()->setBold();
                                }
                                else 
                                {
                                    if($all == "today")
                                    {
                                        $csv->getActiveSheet()->SetCellValue("A$i", 'Date: '.date("d/m/Y"));
                                        $csv->getActiveSheet()->getStyle("D$i:F$i")->getFont()->setBold();
                                    }
                                    else 
                                    {
                                        //Writing Date
                                        $csv->getActiveSheet()->SetCellValue("A$i", 'From Date: '.date("d/m/Y", $fromTime));
                                        $csv->getActiveSheet()->getStyle("D$i:F$i")->getFont()->setBold();

                                        $csv->getActiveSheet()->SetCellValue("D$i", 'To Date: '.date("d/m/Y", $toTime-86400));
                                        $csv->getActiveSheet()->getStyle("D$i:F$i")->getFont()->setBold();
                                    }
                                }
                                $i++;
                                //Writing Data
                                $csv->getActiveSheet()->SetCellValue("A$i", '#');
                                if($filterBy == _EMPLOYEE_)
                                {
                                    $csv->getActiveSheet()->SetCellValue("B$i", 'Date');
                                    $csv->getActiveSheet()->SetCellValue("C$i", 'Site/Office');
                                    $csv->getActiveSheet()->SetCellValue("D$i", 'Check-in Time');
                                    $csv->getActiveSheet()->SetCellValue("E$i", 'Flag');
                                    $csv->getActiveSheet()->SetCellValue("F$i", 'Late Check-in');
                                    $csv->getActiveSheet()->SetCellValue("G$i", 'Late Break End');
                                    $csv->getActiveSheet()->getStyle("A$i:G$i")->getFont()->setBold(true);
                                }
                                elseif($filterBy == _OFFICE_)
                                {
                                    $csv->getActiveSheet()->SetCellValue("B$i", 'Date');
                                    $csv->getActiveSheet()->SetCellValue("C$i", 'Employee Pin');
                                    $csv->getActiveSheet()->SetCellValue("D$i", 'Name');
                                    $csv->getActiveSheet()->SetCellValue("E$i", 'Check-in Time');
                                    $csv->getActiveSheet()->SetCellValue("F$i", 'Flag');
                                    $csv->getActiveSheet()->SetCellValue("G$i", 'Late Check-in');
                                    $csv->getActiveSheet()->SetCellValue("H$i", 'Late Break End');
                                    $csv->getActiveSheet()->getStyle("A$i:H$i")->getFont()->setBold(true);
                                }
                                elseif($filterBy == _CLIENT_)
                                {
                                    $csv->getActiveSheet()->SetCellValue("B$i", 'Employee Pin');
                                    $csv->getActiveSheet()->SetCellValue("C$i", 'Name');
                                    $csv->getActiveSheet()->SetCellValue("D$i", 'Site/Office');
                                    $csv->getActiveSheet()->SetCellValue("E$i", 'Check-in Time');
                                    $csv->getActiveSheet()->SetCellValue("F$i", 'Flag');
                                    $csv->getActiveSheet()->SetCellValue("G$i", 'Late Check-in');
                                    $csv->getActiveSheet()->SetCellValue("H$i", 'Late Break End');
                                    $csv->getActiveSheet()->getStyle("A$i:H$i")->getFont()->setBold(true);
                                }
                            }
                            else 
                            {
                                require_once _LIBRARIES_PATH_."/mpdf/mpdf.php";
                                $obj = new mPDF('utf-8', 'A4-L');
                                ob_start();
                                if($filterBy == _EMPLOYEE_)
                                {
                                    include_once _SHARED_UPLOADS_."/temp/reports/emp/flag/header.php";
                                }
                                elseif($filterBy == _OFFICE_)
                                {
                                    include_once _SHARED_UPLOADS_."/temp/reports/site/flag/header.php";
                                }
                                elseif($filterBy == _CLIENT_)
                                {
                                    include_once _SHARED_UPLOADS_."/temp/reports/client/flag/header.php";
                                }
                            }
                            //LOOP HERE
                            $index = 1;
                            $timezone = '';
                            $set_offset = '';
                            $cur_offset = '';
                            foreach($flagReports as $report)
                            {
                                if(!empty($report['timezone']))
                                {
                                    $cur_offset = $report['timezone'];
                                    if($cur_offset != $set_offset)
                                    {
                                        $set_offset = $cur_offset;
                                        list($hours, $minutes) = explode(':', $set_offset);
                                        $seconds = $hours * 60 * 60 + $minutes * 60;
                                        $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 1);
                                        // Workaround for bug #44780
                                        if($timezone === false) 
                                        {
                                            $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 0);
                                        }
                                        date_default_timezone_set($timezone);
                                    }
                                }
                                date_default_timezone_set($this->config['timezone']);
                                $myModel = $this->loadModel("employee");
                                $graceTime = $myModel->getGraceTime($report['emp_id']);
                                $breakDuration = $myModel->getBreakDuration($report['emp_id']);
                                $shiftStart = strtotime(date("H:i", ($report['timing_from']+$graceTime*60)));
                                $entry = strtotime(date("H:i", $report['check_in_timestamp'] + ($seconds)));
                                if($shiftStart >= $entry)
                                {
                                    $report['ciStatus'] = _IN_TIME_;
                                }
                                else 
                                {
                                    $report['ciStatus'] = _LATE_;
                                }
                                if(($report['break_out_timestamp'] - $report['break_in_timestamp'])/60 > $breakDuration)
                                {
                                    $report['boStatus'] = _LATE_;
                                } 
                                else 
                                {
                                    $report['boStatus'] = _EARLY_;
                                }
                                date_default_timezone_set($timezone);
                                    
                                if($format != "pdf")
                                {
                                    if($report['ciStatus'] == _LATE_ || $report['boStatus'] == _LATE_)
                                    {
                                        $i++;
                                        $csv->getActiveSheet()->SetCellValue("A{$i}", $index++);
                                        if($filterBy == _EMPLOYEE_)
                                        {
                                            $csv->getActiveSheet()->SetCellValue("B{$i}", date("d-m-Y", $report['check_in_timestamp']));
                                            $csv->getActiveSheet()->SetCellValue("C{$i}", $report['office_name']);
                                            $csv->getActiveSheet()->SetCellValue("D{$i}", date("H:i", $report['check_in_timestamp']));
                                            $hadDraw = FALSE;
                                            if($report['ciStatus'] == _LATE_)
                                            {
                                                date_default_timezone_set($this->config['timezone']);
                                                $a = date("H:i", $report['timing_from']);
                                                $expectedEntry = strtotime($a);
                                                
//                                                date_default_timezone_set($timezone);
                                                $a1 = date("H:i", $report['check_in_timestamp']+($seconds));
                                                $realEntry = strtotime($a1);
                                                
                                                $late = floor(($realEntry - $expectedEntry) / 60) - $report['emp_grace_time'];
                                                
                                                $csv->getActiveSheet()->SetCellValue("F{$i}", $late." mins");
                                                $objDrawing = '';
                                                $objDrawing = new PHPExcel_Worksheet_Drawing();
                                                $objDrawing->setName('Flag Logo');
                                                $objDrawing->setDescription('Flag Logo');
                                                $objDrawing->setPath($flagLogo);

                                                $objDrawing->setWidthAndHeight(30,20);
                                                $objDrawing->setResizeProportional(true);
                                                $objDrawing->setCoordinates("E$i");
                                                $objDrawing->setWorksheet($csv->getActiveSheet());
                                                $hadDraw = TRUE;
                                            }
                                            else 
                                            {
                                                $csv->getActiveSheet()->SetCellValue("F{$i}", "-");
                                            }
                                            if($report['boStatus'] == _LATE_)
                                            {
                                                $late = floor(($report['break_out_timestamp'] - $report['break_in_timestamp']) / 60) - $report['emp_break_time'];
                                                $csv->getActiveSheet()->SetCellValue("G{$i}", $late." mins");
                                                if(!$hadDraw)
                                                {
                                                    $objDrawing = '';
                                                    $objDrawing = new PHPExcel_Worksheet_Drawing();
                                                    $objDrawing->setName('Flag Logo');
                                                    $objDrawing->setDescription('Flag Logo');
                                                    $objDrawing->setPath($flagLogo);

                                                    $objDrawing->setWidthAndHeight(30,20);
                                                    $objDrawing->setResizeProportional(true);
                                                    $objDrawing->setCoordinates("E$i");
                                                    $objDrawing->setWorksheet($csv->getActiveSheet());
                                                }
                                            }
                                            else 
                                            {
                                                $csv->getActiveSheet()->SetCellValue("G{$i}", "-");
                                            }
                                            date_default_timezone_set($timezone);
                                        }
                                        elseif($filterBy == _OFFICE_) 
                                        {
                                            $csv->getActiveSheet()->SetCellValue("B{$i}", date("d-m-Y", $report['check_in_timestamp']));
                                            $csv->getActiveSheet()->SetCellValue("C{$i}", $report['pin']);
                                            $csv->getActiveSheet()->SetCellValue("D{$i}", $report['emp_name']);
                                            $csv->getActiveSheet()->SetCellValue("E{$i}", date("H:i", $report['check_in_timestamp']));
                                            $hadDraw = FALSE;
                                            if($report['ciStatus'] == _LATE_)
                                            {
                                                date_default_timezone_set($this->config['timezone']);
                                                $a = date("H:i", $report['timing_from']);
                                                $expectedEntry = strtotime($a);
                                                
//                                                date_default_timezone_set($timezone);
                                                $a1 = date("H:i", $report['check_in_timestamp'] + ($seconds));
                                                $realEntry = strtotime($a1);
                                                
                                                $late = floor(($realEntry - $expectedEntry) / 60) - $report['emp_grace_time'];
                                                
                                                $csv->getActiveSheet()->SetCellValue("G{$i}", $late." mins");
                                                $objDrawing = '';
                                                $objDrawing = new PHPExcel_Worksheet_Drawing();
                                                $objDrawing->setName('Flag Logo');
                                                $objDrawing->setDescription('Flag Logo');
                                                $objDrawing->setPath($flagLogo);

                                                $objDrawing->setWidthAndHeight(30,20);
                                                $objDrawing->setResizeProportional(true);
                                                $objDrawing->setCoordinates("F$i");
                                                $objDrawing->setWorksheet($csv->getActiveSheet());
                                                $hadDraw = TRUE;
                                            }
                                            else 
                                            {
                                                $csv->getActiveSheet()->SetCellValue("G{$i}", "-");
                                            }
                                            if($report['boStatus'] == _LATE_)
                                            {
                                                $late = floor(($report['break_out_timestamp'] - $report['break_in_timestamp']) / 60) - $report['emp_break_time'];
                                                $csv->getActiveSheet()->SetCellValue("H{$i}", $late." mins");
                                                if(!$hadDraw)
                                                {
                                                    $objDrawing = '';
                                                    $objDrawing = new PHPExcel_Worksheet_Drawing();
                                                    $objDrawing->setName('Flag Logo');
                                                    $objDrawing->setDescription('Flag Logo');
                                                    $objDrawing->setPath($flagLogo);

                                                    $objDrawing->setWidthAndHeight(30,20);
                                                    $objDrawing->setResizeProportional(true);
                                                    $objDrawing->setCoordinates("F$i");
                                                    $objDrawing->setWorksheet($csv->getActiveSheet());
                                                }
                                            }
                                            else 
                                            {
                                                $csv->getActiveSheet()->SetCellValue("H{$i}", "-");
                                            }
                                            date_default_timezone_set($timezone);
                                        }
                                        elseif($filterBy == _CLIENT_) 
                                        {
                                            $csv->getActiveSheet()->SetCellValue("B{$i}", $report['pin']);
                                            $csv->getActiveSheet()->SetCellValue("C{$i}", $report['emp_name']);
                                            $csv->getActiveSheet()->SetCellValue("D{$i}", $report['office_name']);
                                            $csv->getActiveSheet()->SetCellValue("E{$i}", date("H:i", $report['check_in_timestamp']));
                                            $hadDraw = FALSE;
                                            if($report['ciStatus'] == _LATE_)
                                            {
                                                date_default_timezone_set($this->config['timezone']);
                                                $a = date("H:i", $report['timing_from']);
                                                $expectedEntry = strtotime($a);
                                                
//                                                date_default_timezone_set($timezone);
                                                $a1 = date("H:i", $report['check_in_timestamp'] + ($seconds));
                                                $realEntry = strtotime($a1);
                                                
                                                $late = floor(($realEntry - $expectedEntry) / 60) - $report['emp_grace_time'];
                                                
                                                $csv->getActiveSheet()->SetCellValue("G{$i}", $late." mins");
                                                $objDrawing = '';
                                                $objDrawing = new PHPExcel_Worksheet_Drawing();
                                                $objDrawing->setName('Flag Logo');
                                                $objDrawing->setDescription('Flag Logo');
                                                $objDrawing->setPath($flagLogo);

                                                $objDrawing->setWidthAndHeight(30,20);
                                                $objDrawing->setResizeProportional(true);
                                                $objDrawing->setCoordinates("F$i");
                                                $objDrawing->setWorksheet($csv->getActiveSheet());
                                                $hadDraw = TRUE;
                                            }
                                            else 
                                            {
                                                $csv->getActiveSheet()->SetCellValue("G{$i}", "-");
                                            }
                                            if($report['boStatus'] == _LATE_)
                                            {
                                                $late = floor(($report['break_out_timestamp'] - $report['break_in_timestamp']) / 60) - $report['emp_break_time'];
                                                $csv->getActiveSheet()->SetCellValue("H{$i}", $late." mins");
                                                if(!$hadDraw)
                                                {
                                                    $objDrawing = '';
                                                    $objDrawing = new PHPExcel_Worksheet_Drawing();
                                                    $objDrawing->setName('Flag Logo');
                                                    $objDrawing->setDescription('Flag Logo');
                                                    $objDrawing->setPath($flagLogo);

                                                    $objDrawing->setWidthAndHeight(30,20);
                                                    $objDrawing->setResizeProportional(true);
                                                    $objDrawing->setCoordinates("F$i");
                                                    $objDrawing->setWorksheet($csv->getActiveSheet());
                                                }
                                            }
                                            else 
                                            {
                                                $csv->getActiveSheet()->SetCellValue("H{$i}", "-");
                                            }
                                            date_default_timezone_set($timezone);
                                        }
                                    }
                                }
                                else 
                                {
                                    if($report['ciStatus'] == _LATE_ || $report['boStatus'] == _LATE_)
                                    {
                                        $i++;
                                        if($filterBy == _EMPLOYEE_)
                                        {
                                            include _SHARED_UPLOADS_."/temp/reports/emp/flag/main.php";
                                        }
                                        elseif($filterBy == _OFFICE_) 
                                        {
                                            include _SHARED_UPLOADS_."/temp/reports/site/flag/main.php";
                                        }
                                        elseif($filterBy == _CLIENT_) 
                                        {
                                            include _SHARED_UPLOADS_."/temp/reports/client/flag/main.php";
                                        }
                                    }
                                }
                            }
                            
                            if($format != "pdf")
                            {
                                $csv->getActiveSheet()->setTitle("Flag Report - ".date("Y-m-d"));

                                //PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
                                if($filterBy == _EMPLOYEE_)
                                {
                                    foreach(range('A','G') as $columnID) 
                                    {
                                        $csv->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
                                    }
                                }
                                elseif($filterBy == _OFFICE_)
                                {
                                    foreach(range('A','H') as $columnID) 
                                    {
                                        $csv->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
                                    }
                                }
                                elseif($filterBy == _CLIENT_)
                                {
                                    foreach(range('A','H') as $columnID) 
                                    {
                                        $csv->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
                                    }
                                }
                                
                                $styleArray = array(
                                    'borders' => array(
                                        'allborders' => array(
                                            'style' => PHPExcel_Style_Border::BORDER_THIN
                                        )
                                    )
                                );
                                $csv->getActiveSheet()->getStyle(
                                    'A6:' . 
                                    $csv->getActiveSheet()->getHighestColumn() . 
                                    $csv->getActiveSheet()->getHighestRow()
                                )->applyFromArray($styleArray);

                                if(!isset($_SESSION['cron_script']))
                                {
                                    $csvWriter->save("php://output");
                                }
                                else 
                                {
                                    $path = _SHARED_UPLOADS_."/temp/REPORT_".time().rand(999,9999).".".$format;
                                    $csvWriter->save($path);
                                    return $path;
                                }
                            }
                            else 
                            {
                                include_once _SHARED_UPLOADS_."/temp/reports/emp/flag/footer.php";
                                $content = ob_get_contents();
                                ob_end_clean();
                                if(!isset($_SESSION['cron_script']))
                                {
                                    $obj->WriteHTML($content);
                                    if($filterBy == _EMPLOYEE_)
                                    {
                                        $obj->Output("Employee_Flag_Report.pdf", "D");
                                    }
                                    elseif($filterBy == _OFFICE_)
                                    {
                                        $obj->Output("Site_Flag_Report.pdf", "D");
                                    }
                                    elseif($filterBy == _CLIENT_)
                                    {
                                        $obj->Output("Company_Flag_Report.pdf", "D");
                                    }
                                }
                                else 
                                {
                                    $path = _SHARED_UPLOADS_."/temp/REPORT_".time().rand(999,9999).".".$format;                                    
                                    $obj->Output($path, "F");
                                    return $path;
                                }
                                exit();
                            }
                        }
                    }
                    elseif($reportType == 'summary')
                    {
                        $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                        $summaries = $this->model->viewReportBetweenBy($id, $filterBy, $fromTime, $toTime, isset($order) ? $order:'');
                        
                        if(is_array($summaries))
                        { 
                            set_time_limit(0);
                            ini_set('memory_limit', '-1');
                            if($format != "pdf")
                            {
                                //Writing Data TO CSV
                                $csv = new PHPExcel();
                                if($format == "pdf")
                                {
        //                            $rendererName = PHPExcel_Settings::PDF_RENDERER_TCPDF;
        //                            $rendererLibrary = 'tcpdf';
                                    $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                                    $rendererLibrary = 'mpdf';
                                    $rendererLibraryPath = _LIBRARIES_PATH_."/".$rendererLibrary;
                                    PHPExcel_Settings::setPdfRenderer($rendererName,$rendererLibraryPath); 
                                    $csvWriter = PHPExcel_IOFactory::createWriter($csv, "PDF");
                                    $extenstion = "pdf";
                                }
                                else 
                                {
                                    $csvWriter = PHPExcel_IOFactory::createWriter($csv, "Excel5");
                                    $extenstion = "csv";
                                }

                                if(!isset($_SESSION['cron_script']))
                                {
                                    //Sending Headers
                                    if($format == "pdf")
                                    {
                                        header("Content-Type: application/pdf");
                                    }
                                    else 
                                    {
                                        header("Content-Type: application/csv");
                                    }
                                }

                                $csv->getProperties()->setLastModifiedBy("OTL");
                                $csv->getProperties()->setTitle("OTL - ".ucfirst($type)." Summary Report");
                                $csv->getProperties()->setSubject("Summary Report");
                                $csv->getProperties()->setDescription("This Is Auto Flag Report Generated By OTL");

                                $csv->setActiveSheetIndex(0); 

                                //Adding Image TO Sheet
                                $objDrawing = new PHPExcel_Worksheet_Drawing();
                                $objDrawing->setName('OTL LOGO');
                                $objDrawing->setDescription('OTL LOGO');
                                $objDrawing->setPath($otlLogo);

                                $objDrawing->setWidthAndHeight(120,90);
                                $objDrawing->setResizeProportional(true);
                                $objDrawing->setCoordinates('A1');
                                $objDrawing->setWorksheet($csv->getActiveSheet());

                                $objDrawing = '';

                                //$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
                                $objDrawing = new PHPExcel_Worksheet_Drawing();
                                $objDrawing->setName('Company LOGO');
                                $objDrawing->setDescription('Company LOGO');
                                $objDrawing->setPath($companyLogo);

                                $objDrawing->setWidthAndHeight(150,90);
                                $objDrawing->setResizeProportional(true);
                                $objDrawing->setCoordinates('E1');
                                $objDrawing->setWorksheet($csv->getActiveSheet());

                                $csv->getActiveSheet()->getRowDimension(1)->setRowHeight("30");
                                //End

                                //Writing Heading
                                if($filterBy == _EMPLOYEE_)
                                {
                                    header("Content-Disposition: attachment; filename=\"Employee_Summary_Report.$extenstion\"");
                                    header("Cache-Control: max-age=0");
                                    $csv->getActiveSheet()->SetCellValue('A2', 'Employee Summary Report');
                                }
                                elseif($filterBy == _OFFICE_)
                                {
                                    header("Content-Disposition: attachment; filename=\"Site_Summary_Report.$extenstion\"");
                                    header("Cache-Control: max-age=0");
                                    $csv->getActiveSheet()->SetCellValue('A2', 'Site/Office Summary Report');
                                }
                                elseif($filterBy == _CLIENT_)
                                {
                                    header("Content-Disposition: attachment; filename=\"Company_Summary_Report.$extenstion\"");
                                    header("Cache-Control: max-age=0");
                                    $csv->getActiveSheet()->SetCellValue('A2', 'Company Summary Report');
                                }
                                $csv->getActiveSheet()->mergeCells("A2:I2");
                                $csv->getActiveSheet()->getStyle('A2:I2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                $csv->getActiveSheet()->getStyle('A2:I2')->getFont()->setBold()->setSize(18);

                                $csv->getActiveSheet()->SetCellValue('A3', date("d/m/Y h:i a"));
                                $csv->getActiveSheet()->mergeCells("A3:I3");
                                $csv->getActiveSheet()->getStyle('A3:I3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                                //Writing Employee or Office Detail
                                if($filterBy == _EMPLOYEE_)
                                {
                                    $csv->getActiveSheet()->SetCellValue('A4', 'Employee Name: '.$title);
                                    $csv->getActiveSheet()->SetCellValue('A5', 'Employee ID: '.$id);
                                    $csv->getActiveSheet()->getStyle('A4:A5')->getFont()->setBold();
                                    $i = 6;
                                }
                                elseif($filterBy == _OFFICE_)
                                {
                                    $csv->getActiveSheet()->SetCellValue('A4', 'Site/Office: '.$title);
                                    $csv->getActiveSheet()->SetCellValue('A5', 'Time Zone: '.$timezone.' UTC');
                                    $csv->getActiveSheet()->getStyle('A4:A5')->getFont()->setBold();
                                    $i = 6; 
                                }
                                elseif($filterBy == _CLIENT_)
                                {
                                    $csv->getActiveSheet()->SetCellValue('A4', 'Company: '.$title);
                                    $csv->getActiveSheet()->getStyle('A4')->getFont()->setBold();
                                    $i = 5; 
                                }
                                if($all == "today")
                                {
                                    $csv->getActiveSheet()->SetCellValue("A$i", 'Date: '.date("d/m/Y"));
                                    $csv->getActiveSheet()->getStyle("D$i:F$i")->getFont()->setBold();
                                }
                                else 
                                {
                                    //Writing Date
                                    $csv->getActiveSheet()->SetCellValue("A$i", 'From Date: '.date("d/m/Y", $fromTime));
                                    $csv->getActiveSheet()->getStyle("D$i:F$i")->getFont()->setBold();

                                    $csv->getActiveSheet()->SetCellValue("D$i", 'To Date: '.date("d/m/Y", $toTime-86400));
                                    $csv->getActiveSheet()->getStyle("D$i:F$i")->getFont()->setBold();
                                }
                                
                                $i++;
                                $staffID = $i;
                                //Total Cost
                                $csv->getActiveSheet()->SetCellValue("A$i", 'Total Staff Cost: XXXXX');
                                $csv->getActiveSheet()->getStyle("A$i:C$i")->getFont()->setBold();

                                $i++;
                                //Writing Data
                                $csv->getActiveSheet()->SetCellValue("A$i", '#');
                                if($filterBy == _EMPLOYEE_)
                                {
                                    $csv->getActiveSheet()->SetCellValue("B$i", 'Date');
                                    $csv->getActiveSheet()->SetCellValue("C$i", 'Site/Office');
                                    $csv->getActiveSheet()->SetCellValue("D$i", 'Check-in Time');
                                    $csv->getActiveSheet()->SetCellValue("E$i", 'Check-out Time');
                                    $csv->getActiveSheet()->SetCellValue("F$i", 'Total Hrs Worked');
                                    $csv->getActiveSheet()->SetCellValue("G$i", 'Total Break Durations');
                                    $csv->getActiveSheet()->SetCellValue("H$i", 'Rate/Hr');
                                    $csv->getActiveSheet()->SetCellValue("I$i", 'Total Cost');
                                    $csv->getActiveSheet()->getStyle("A$i:I$i")->getFont()->setBold(true);
                                }
                                elseif($filterBy == _OFFICE_)
                                {
                                    $csv->getActiveSheet()->SetCellValue("B$i", 'Date');
                                    $csv->getActiveSheet()->SetCellValue("C$i", 'Employee Pin');
                                    $csv->getActiveSheet()->SetCellValue("D$i", 'Name');
                                    $csv->getActiveSheet()->SetCellValue("E$i", 'Check-in Time');
    //                                $csv->getActiveSheet()->SetCellValue("E$i", 'Check-out Time');
                                    $csv->getActiveSheet()->SetCellValue("F$i", 'Total Hrs Worked');
                                    $csv->getActiveSheet()->SetCellValue("G$i", 'Total Break Durations');
                                    $csv->getActiveSheet()->SetCellValue("H$i", 'Rate/Hr');
                                    $csv->getActiveSheet()->SetCellValue("I$i", 'Total Cost');
                                    $csv->getActiveSheet()->getStyle("A$i:I$i")->getFont()->setBold(true);
                                }
                                elseif($filterBy == _CLIENT_)
                                {
                                    $csv->getActiveSheet()->SetCellValue("B$i", 'Employee Pin');
                                    $csv->getActiveSheet()->SetCellValue("C$i", 'Name');
    //                                $csv->getActiveSheet()->SetCellValue("D$i", 'Check-in Time');
    //                                $csv->getActiveSheet()->SetCellValue("E$i", 'Check-out Time');
                                    $csv->getActiveSheet()->SetCellValue("D$i", 'Total Hrs Worked');
                                    $csv->getActiveSheet()->SetCellValue("E$i", 'Total Break Durations');
                                    $csv->getActiveSheet()->SetCellValue("F$i", 'Rate/Hr');
                                    $csv->getActiveSheet()->SetCellValue("G$i", 'Total Cost');
                                    $csv->getActiveSheet()->getStyle("A$i:G$i")->getFont()->setBold(true);
                                }
                            }
                            else 
                            {
                                require_once _LIBRARIES_PATH_."/mpdf/mpdf.php";
                                $obj = new mPDF('utf-8', 'A4-L');
                                ob_start();
                                if($filterBy == _EMPLOYEE_)
                                {
                                    include_once _SHARED_UPLOADS_."/temp/reports/emp/summary/header.php";
                                }
                                elseif($filterBy == _OFFICE_)
                                {
                                    include_once _SHARED_UPLOADS_."/temp/reports/site/summary/header.php";
                                }
                                elseif($filterBy == _CLIENT_)
                                {
                                    include_once _SHARED_UPLOADS_."/temp/reports/client/summary/header.php";
                                }
                            }
                            $index = 1;
                            $pre = 0;
                            $finalCost = 0;
                            $totalCost = 0;
                            $working = 0;
                            $break = 0;
                            $timezone = '';
                            $set_offset = '';
                            $cur_offset = '';
                            foreach($summaries as $row)
                            {
                                if(!empty($row['timezone']))
                                {
                                    $cur_offset = $row['timezone'];
                                    if($cur_offset != $set_offset)
                                    {
                                        $set_offset = $cur_offset;
                                        list($hours, $minutes) = explode(':', $set_offset);
                                        $seconds = $hours * 60 * 60 + $minutes * 60;
                                        $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 1);
                                        // Workaround for bug #44780
                                        if($timezone === false) 
                                        {
                                            $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 0);
                                        }
                                        date_default_timezone_set($timezone);
                                    }
                                }
                                
                                if($format != "pdf")
                                {
                                    if($filterBy == _EMPLOYEE_)
                                    {
                                        $i++;
                                        $csv->getActiveSheet()->SetCellValue("A{$i}", $index++);
                                        $csv->getActiveSheet()->SetCellValue("B{$i}", date("d-m-Y", $row['check_in_timestamp']));
                                        $csv->getActiveSheet()->SetCellValue("C{$i}", $row['office_name']);
                                        $csv->getActiveSheet()->SetCellValue("D{$i}", date('H:i', $row['check_in_timestamp']));
                                        $csv->getActiveSheet()->SetCellValue("E{$i}", date('H:i', $row['check_out_timestamp']));

                                        $working = (($row['check_out_timestamp'] - $row['check_in_timestamp']) - ($row['break_out_timestamp'] - $row['break_in_timestamp']));
                                        $break = $row['break_out_timestamp'] - $row['break_in_timestamp'];

                                        $hours = floor($working / 3600);
                                        $minutes = floor(($working / 60) % 60);
                                        $break = floor($break / 60);

//                                        $totalCost = (($working/60) - $minutes) * ($row['pay_rate']/60);
                                        $totalCost = $hours * $row['pay_rate'];
                                        $minutes = '';
                                        if($hours != 0)
                                        {
                                            $finalCost += $totalCost;
                                        }
                                        $csv->getActiveSheet()->SetCellValue("F{$i}", ($hours != 0 && $minutes != '0') ? $hours." hrs ".$minutes:"-");
                                        $csv->getActiveSheet()->SetCellValue("G{$i}", $break == 0 ? '-': $break." mins");
                                        $csv->getActiveSheet()->SetCellValue("H{$i}", "$".$row['pay_rate']);
                                        $csv->getActiveSheet()->SetCellValue("I{$i}", ($hours != 0 && $minutes != '0') ? "$".round($totalCost, 2) : '-');
                                    }
                                    elseif($filterBy == _OFFICE_)
                                    {
                                        $i++;
                                        $csv->getActiveSheet()->SetCellValue("A{$i}", $index++);
                                        $csv->getActiveSheet()->SetCellValue("B{$i}", date("d-m-Y", $row['check_in_timestamp']));
                                        $csv->getActiveSheet()->SetCellValue("C{$i}", $row['pin']);
                                        $csv->getActiveSheet()->SetCellValue("D{$i}", $row['name']);
                                        $csv->getActiveSheet()->SetCellValue("E{$i}", date('H:i', $row['check_in_timestamp']));
//                                        $csv->getActiveSheet()->SetCellValue("E{$i}", date('H:i', $row['check_out_timestamp']));

                                        $working = ceil((($row['check_out_timestamp'] - $row['check_in_timestamp']) - ($row['break_out_timestamp'] - $row['break_in_timestamp'])));
                                        $break = ceil(($row['break_out_timestamp'] - $row['break_in_timestamp']));

                                        $hours = floor($working / 3600);
                                        $minutes = floor(($working / 60) % 60);
                                        $break = floor($break / 60);

//                                        $totalCost = (($working/60) - $minutes) * ($row['pay_rate']/60);
                                        $totalCost = $hours * $row['pay_rate'];
                                        $minutes = '';
                                        if($hours != 0)
                                        {
                                            $finalCost += $totalCost;
                                        }
                                        $csv->getActiveSheet()->SetCellValue("F{$i}", ($hours != 0 && $minutes != '0') ? $hours." hrs ".$minutes." ":"-");
                                        $csv->getActiveSheet()->SetCellValue("G{$i}", $break == 0 ? '-': $break." mins");
                                        $csv->getActiveSheet()->SetCellValue("H{$i}", "$".$row['pay_rate']);
                                        $csv->getActiveSheet()->SetCellValue("I{$i}", ($hours != 0 && $minutes != '0') ? "$".round($totalCost, 2):"-");
                                    }
                                    elseif($filterBy == _CLIENT_)
                                    {
                                        $i++;
                                        $csv->getActiveSheet()->SetCellValue("A{$i}", $index++);
                                        $csv->getActiveSheet()->SetCellValue("B{$i}", $row['pin']);
                                        $csv->getActiveSheet()->SetCellValue("C{$i}", $row['name']);
//                                        $csv->getActiveSheet()->SetCellValue("D{$i}", date('H:i', $row['check_in_timestamp']));
//                                        $csv->getActiveSheet()->SetCellValue("E{$i}", date('H:i', $row['check_out_timestamp']));

                                        $working = (($row['check_out_timestamp'] - $row['check_in_timestamp']) - ($row['break_out_timestamp'] - $row['break_in_timestamp']));
                                        $break = $row['break_out_timestamp'] - $row['break_in_timestamp'];

                                        $hours = floor($working / 3600);
                                        $minutes = floor(($working / 60) % 60);
                                        $break = floor($break / 60);

//                                        $totalCost = (($working/60) - $minutes) * ($row['pay_rate']/60);
                                        $totalCost = $hours * $row['pay_rate'];
                                        $minutes = '';
                                        if($hours != 0)
                                        {
                                            $finalCost += $totalCost;
                                        }
                                        $csv->getActiveSheet()->SetCellValue("D{$i}", ($hours != 0 && $minutes != '0') ? $hours." hrs ".$minutes." ":"-");
                                        $csv->getActiveSheet()->SetCellValue("E{$i}", $break == 0 ? '-': $break." mins");
                                        $csv->getActiveSheet()->SetCellValue("F{$i}", "$".$row['pay_rate']);
                                        $csv->getActiveSheet()->SetCellValue("G{$i}", ($hours != 0 && $minutes != '0') ? "$".round($totalCost, 2):"-");
                                    }
                                }
                                else 
                                {
                                    if($filterBy == _EMPLOYEE_)
                                    {
                                        $working = (($row['check_out_timestamp'] - $row['check_in_timestamp']) - ($row['break_out_timestamp'] - $row['break_in_timestamp']));
                                        $break = $row['break_out_timestamp'] - $row['break_in_timestamp'];

                                        $hours = floor($working / 3600);
                                        $minutes = floor(($working / 60) % 60);
                                        $break = floor($break / 60);

//                                        $totalCost = (($working/60) - $minutes) * ($row['pay_rate']/60);
                                        $totalCost = $hours * $row['pay_rate'];
                                        $minutes = '';
                                        if($hours != 0)
                                        {
                                            $finalCost += $totalCost;
                                        }
                                        include _SHARED_UPLOADS_."/temp/reports/emp/summary/main.php";
                                    }
                                    elseif($filterBy == _OFFICE_) 
                                    {
                                        $working = ceil((($row['check_out_timestamp'] - $row['check_in_timestamp']) - ($row['break_out_timestamp'] - $row['break_in_timestamp'])));
                                        $break = ceil(($row['break_out_timestamp'] - $row['break_in_timestamp']));

                                        $hours = floor($working / 3600);
                                        $minutes = floor(($working / 60) % 60);
                                        $break = floor($break / 60);

//                                        $totalCost = (($working/60) - $minutes) * ($row['pay_rate']/60);
                                        $totalCost = $hours * $row['pay_rate'];
                                        $minutes = '';
                                        if($hours != 0)
                                        {
                                            $finalCost += $totalCost;
                                        }
                                        include _SHARED_UPLOADS_."/temp/reports/site/summary/main.php";
                                    }
                                    elseif($filterBy == _CLIENT_) 
                                    {
                                        $working = (($row['check_out_timestamp'] - $row['check_in_timestamp']) - ($row['break_out_timestamp'] - $row['break_in_timestamp']));
                                        $break = $row['break_out_timestamp'] - $row['break_in_timestamp'];

                                        $hours = floor($working / 3600);
                                        $minutes = floor(($working / 60) % 60);
                                        $break = floor($break / 60);

//                                        $totalCost = (($working/60) - $minutes) * ($row['pay_rate']/60);
                                        $totalCost = $hours * $row['pay_rate'];
                                        $minutes = '';
                                        if($hours != 0)
                                        {
                                            $finalCost += $totalCost;
                                        }
                                        include _SHARED_UPLOADS_."/temp/reports/client/summary/main.php";
                                    }
                                }
                            }
                            
                            if($format != "pdf")
                            {
                                //Total Cost
                                $csv->getActiveSheet()->SetCellValue("A$staffID", "Total Staff Cost: $".round($finalCost, 2));
                                $csv->getActiveSheet()->getStyle("A$staffID:C$staffID")->getFont()->setBold();

                                $csv->getActiveSheet()->setTitle("Summary Report - ".date("Y-m-d"));
                                $styleArray = array(
                                    'borders' => array(
                                        'allborders' => array(
                                            'style' => PHPExcel_Style_Border::BORDER_THIN
                                        )
                                    )
                                );
                                //PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
                                if($filterBy == _CLIENT_)
                                {
                                    foreach(range('A','G') as $columnID) 
                                    {
                                        $csv->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
                                    }
                                    $csv->getActiveSheet()->getStyle(
                                        'A7:G' . 
                                        $csv->getActiveSheet()->getHighestRow()
                                    )->applyFromArray($styleArray);
                                }
                                else 
                                {
                                    foreach(range('A','I') as $columnID) 
                                    {
                                        $csv->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
                                    }
                                    $csv->getActiveSheet()->getStyle(
                                        'A7:I' . 
                                        $csv->getActiveSheet()->getHighestRow()
                                    )->applyFromArray($styleArray);
                                }
                  
                                if(!isset($_SESSION['cron_script']))
                                {
                                    $csvWriter->save("php://output");
                                }
                                else 
                                {
                                    $path = _SHARED_UPLOADS_."/temp/REPORT_".time().rand(999,9999).".".$format;
                                    $csvWriter->save($path);
                                    return $path;
                                }
                            }
                            else 
                            {
                                include_once _SHARED_UPLOADS_."/temp/reports/emp/summary/footer.php";
                                $content = ob_get_contents();
                                ob_end_clean();
                                
                                $dom = new DOMDocument;
                                $dom->loadHTML($content);
                                $node = $dom->getElementById("final_cost");
                                $fragment = $dom->createDocumentFragment();
                                $fragment->appendXML("$".round($finalCost, 2));
                                $node->appendChild($fragment);
                                $content = $dom->saveHTML();
                                
                                if(!isset($_SESSION['cron_script']))
                                {
                                    $obj->WriteHTML($content);
                                    if($filterBy == _EMPLOYEE_)
                                    {
                                        $obj->Output("Employee_Summary_Report.pdf", "D");
                                    }
                                    elseif($filterBy == _OFFICE_)
                                    {
                                        $obj->Output("Site_Summary_Report.pdf", "D");
                                    }
                                    elseif($filterBy == _CLIENT_)
                                    {
                                        $obj->Output("Company_Summary_Report.pdf", "D");
                                    }
                                }
                                else 
                                {
                                    $path = _SHARED_UPLOADS_."/temp/REPORT_".time().rand(999,9999).".".$format;                                    
                                    $obj->Output($path, "F");
                                    return $path;
                                }
                                exit();
                            }                            
                        }
                    }
                    elseif($reportType == 'detailed')
                    { 
                        $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                        $details = $this->model->viewDetailedReportBetweenBy($id, $filterBy, $fromTime, $toTime);
                        
                        if(is_array($details))
                        { 
                            set_time_limit(0);
                            ini_set('memory_limit', '-1');
                            if($format != "pdf")
                            {
                                //Writing Data TO CSV
                                $csv = new PHPExcel();
                                if($format == "pdf")
                                {
        //                            $rendererName = PHPExcel_Settings::PDF_RENDERER_TCPDF;
        //                            $rendererLibrary = 'tcpdf';
                                    $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                                    $rendererLibrary = 'mpdf';
                                    $rendererLibraryPath = _LIBRARIES_PATH_."/".$rendererLibrary;
                                    PHPExcel_Settings::setPdfRenderer($rendererName,$rendererLibraryPath); 
                                    $csvWriter = PHPExcel_IOFactory::createWriter($csv, "PDF");
                                    $extenstion = "pdf";
                                }
                                else 
                                {
                                    $csvWriter = PHPExcel_IOFactory::createWriter($csv, "Excel5");
                                    $extenstion = "csv";
                                }

                                if(!isset($_SESSION['cron_script']))
                                {
                                    //Sending Headers
                                    if($format == "pdf")
                                    {
                                        header("Content-Type: application/pdf");
                                    }
                                    else 
                                    {
                                        header("Content-Type: application/csv");
                                    }
                                }

                                $csv->getProperties()->setLastModifiedBy("OTL");
                                $csv->getProperties()->setTitle("OTL - ".ucfirst($type)." Detailed Report");
                                $csv->getProperties()->setSubject("Detailed Report");
                                $csv->getProperties()->setDescription("This Is Auto Detailed Report Generated By OTL");

                                $csv->setActiveSheetIndex(0); 

                                //Adding Image TO Sheet
                                $objDrawing = new PHPExcel_Worksheet_Drawing();
                                $objDrawing->setName('OTL LOGO');
                                $objDrawing->setDescription('OTL LOGO');
                                $objDrawing->setPath($otlLogo);

                                $objDrawing->setWidthAndHeight(84,21);
                                $objDrawing->setResizeProportional(true);
                                $objDrawing->setCoordinates('A1');
                                $objDrawing->setWorksheet($csv->getActiveSheet());

                                $objDrawing = '';

                                //$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
                                $objDrawing = new PHPExcel_Worksheet_Drawing();
                                $objDrawing->setName('Company LOGO');
                                $objDrawing->setDescription('Company LOGO');
                                $objDrawing->setPath($companyLogo);

                                $objDrawing->setWidthAndHeight(84,21);
                                $objDrawing->setResizeProportional(true);
                                $objDrawing->setCoordinates('D1');
                                $objDrawing->setWorksheet($csv->getActiveSheet());

                                $csv->getActiveSheet()->getRowDimension(1)->setRowHeight("30");
                                //End

                                //Writing Heading
                                if($filterBy == _EMPLOYEE_)
                                {
                                    header("Content-Disposition: attachment; filename=\"Employee_Detailed_Report.$extenstion\"");
                                    header("Cache-Control: max-age=0");
                                    $csv->getActiveSheet()->SetCellValue('A2', 'Employee Detailed Report');
                                }
                                elseif($filterBy == _OFFICE_)
                                {
                                    header("Content-Disposition: attachment; filename=\"Site_Detailed_Report.$extenstion\"");
                                    header("Cache-Control: max-age=0");
                                    $csv->getActiveSheet()->SetCellValue('A2', 'Site/Office Detailed Report');
                                }
                                elseif($filterBy == _CLIENT_)
                                {
                                    header("Content-Disposition: attachment; filename=\"Company_Detailed_Report.$extenstion\"");
                                    header("Cache-Control: max-age=0");
                                    $csv->getActiveSheet()->SetCellValue('A2', 'Company Detailed Report');
                                }
                                $csv->getActiveSheet()->mergeCells("A2:F2");
                                $csv->getActiveSheet()->getStyle('A2:F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                $csv->getActiveSheet()->getStyle('A2:F2')->getFont()->setBold()->setSize(18);

                                $csv->getActiveSheet()->SetCellValue('A3', date("d/m/Y h:i a"));
                                $csv->getActiveSheet()->mergeCells("A3:F3");
                                $csv->getActiveSheet()->getStyle('A3:F3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                                //Writing Employee or Office Detail
                                if($filterBy == _EMPLOYEE_)
                                {
                                    $csv->getActiveSheet()->SetCellValue('A4', 'Employee Name: '.$title);
                                    $csv->getActiveSheet()->SetCellValue('A5', 'Employee ID: '.$id);
                                    $csv->getActiveSheet()->getStyle('A4:A5')->getFont()->setBold();
                                    $i = 6;
                                }
                                elseif($filterBy == _OFFICE_)
                                {
                                    $csv->getActiveSheet()->SetCellValue('A4', 'Site/Office: '.$title);
                                    $csv->getActiveSheet()->SetCellValue('A5', 'Time Zone: '.$timezone.' UTC');
                                    $csv->getActiveSheet()->getStyle('A4:A5')->getFont()->setBold();
                                    $i = 6; 
                                }
                                elseif($filterBy == _CLIENT_)
                                {
                                    $csv->getActiveSheet()->SetCellValue('A4', 'Company: '.$title);
                                    $csv->getActiveSheet()->getStyle('A4')->getFont()->setBold();
                                    $i = 5; 
                                }
                                if($all == "today")
                                {
                                    $csv->getActiveSheet()->SetCellValue("A$i", 'Date: '.date("d/m/Y"));
                                    $csv->getActiveSheet()->getStyle("D$i:F$i")->getFont()->setBold();
                                }
                                else
                                {
                                    //Writing Date
                                    $csv->getActiveSheet()->SetCellValue("A$i", 'From Date: '.date("d/m/Y", $fromTime));
                                    $csv->getActiveSheet()->getStyle("D$i:F$i")->getFont()->setBold();

                                    $csv->getActiveSheet()->SetCellValue("D$i", 'To Date: '.date("d/m/Y", $toTime-86400));
                                    $csv->getActiveSheet()->getStyle("D$i:F$i")->getFont()->setBold();
                                }
                            }
                            else 
                            {
                                require_once _LIBRARIES_PATH_."/mpdf/mpdf.php";
                                $obj = new mPDF('utf-8', 'A4-L');
                                ob_start();
                                if($filterBy == _EMPLOYEE_)
                                {
                                    include_once _SHARED_UPLOADS_."/temp/reports/emp/detailed/header.php";
                                }
                                elseif($filterBy == _OFFICE_)
                                {
                                    include_once _SHARED_UPLOADS_."/temp/reports/site/detailed/header.php";
                                }
                                elseif($filterBy == _CLIENT_)
                                {
                                    include_once _SHARED_UPLOADS_."/temp/reports/client/detailed/header.php";
                                }
                            }
                            $timezone = '';
                            $set_offset = '';
                            $cur_offset = '';
                            //Business Logic
                            foreach($details as $row)
                            {
                                if(!empty($row['timezone']))
                                {
                                    $cur_offset = $row['timezone'];
                                    if($cur_offset != $set_offset)
                                    {
                                        $set_offset = $cur_offset;
                                        list($hours, $minutes) = explode(':', $set_offset);
                                        $seconds = $hours * 60 * 60 + $minutes * 60;
                                        $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 1);
                                        // Workaround for bug #44780
                                        if($timezone === false) 
                                        {
                                            $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 0);
                                        }
                                        date_default_timezone_set($timezone);
                                    }
                                }
                                
                                if($format != "pdf")
                                {
                                    $i++;
                                    $startBold = $i;
                                    $csv->getActiveSheet()->SetCellValue("A$i", 'Date: ');
                                    $csv->getActiveSheet()->SetCellValue("B$i", date("d-m-Y", $row['check_in_timestamp']));
                                    $csv->getActiveSheet()->mergeCells("B$i:F$i");
                                    $csv->getActiveSheet()->getStyle("A$i:F$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('0099ff');
                                    if($filterBy == _EMPLOYEE_)
                                    {
                                        $i++;
                                        $csv->getActiveSheet()->SetCellValue("A$i", 'Site: ');
                                        $csv->getActiveSheet()->SetCellValue("B$i", $row['office_name']." (".$row['timezone']." UTC)");
                                        $csv->getActiveSheet()->mergeCells("B$i:F$i");
                                    }
                                    elseif($filterBy == _OFFICE_)
                                    {
                                        $i++;
                                        $csv->getActiveSheet()->SetCellValue("A$i", 'Name: ');
                                        $csv->getActiveSheet()->SetCellValue("B$i", $row['name']);
                                        $csv->getActiveSheet()->mergeCells("B$i:F$i");
                                        $i++;
                                        $csv->getActiveSheet()->SetCellValue("A$i", 'Employee Pin: ');
                                        $csv->getActiveSheet()->SetCellValue("B$i", $row['pin']);
                                        $csv->getActiveSheet()->mergeCells("B$i:F$i");
                                    }
                                    elseif($filterBy == _CLIENT_)
                                    {
                                        $i++;
                                        $csv->getActiveSheet()->SetCellValue("A$i", 'Employee Pin: ');
                                        $csv->getActiveSheet()->SetCellValue("B$i", $row['emp_id']);
                                        $csv->getActiveSheet()->mergeCells("B$i:F$i");
                                        $i++;
                                        $csv->getActiveSheet()->SetCellValue("A$i", 'Name: ');
                                        $csv->getActiveSheet()->SetCellValue("B$i", $row['name']);
                                        $csv->getActiveSheet()->mergeCells("B$i:F$i");
                                        $i++;
                                        $csv->getActiveSheet()->SetCellValue("A$i", 'Site: ');
                                        $csv->getActiveSheet()->SetCellValue("B$i", $row['office_name']);
                                        $csv->getActiveSheet()->mergeCells("B$i:F$i");
                                    }
                                    $i++;
                                    $csv->getActiveSheet()->SetCellValue("A$i", 'Check-in: ');
                                    $csv->getActiveSheet()->SetCellValue("B$i", date("H:i", $row['check_in_timestamp']));
                                    $csv->getActiveSheet()->mergeCells("B$i:F$i");
                                    $i++;
                                    $csv->getActiveSheet()->SetCellValue("A$i", 'Check-out: ');
                                    $csv->getActiveSheet()->SetCellValue("B$i", date("H:i", $row['check_out_timestamp']));
                                    $csv->getActiveSheet()->mergeCells("B$i:F$i");
                                    $i++;
                                    $csv->getActiveSheet()->SetCellValue("A$i", 'Break - Check-in: ');
                                    if($row['check_out_timestamp'] != $row['break_in_timestamp'])
                                    {
                                        $csv->getActiveSheet()->SetCellValue("B$i", date("H:i", $row['break_in_timestamp']));
                                    }
                                    else 
                                    {
                                        $csv->getActiveSheet()->SetCellValue("B$i", "-");
                                    }
                                    $csv->getActiveSheet()->mergeCells("B$i:F$i");
                                    $i++;
                                    $csv->getActiveSheet()->SetCellValue("A$i", 'Break - Check-out: ');
                                    if($row['check_out_timestamp'] != $row['break_out_timestamp'])
                                    {
                                        $csv->getActiveSheet()->SetCellValue("B$i", date("H:i", $row['break_out_timestamp']));
                                    }
                                    else 
                                    {
                                        $csv->getActiveSheet()->SetCellValue("B$i", "-");
                                    }
                                    $csv->getActiveSheet()->mergeCells("B$i:F$i");
                                    $i++;
                                    $csv->getActiveSheet()->SetCellValue("A$i", 'Rate/Hourly: ');
                                    $csv->getActiveSheet()->SetCellValue("B$i", "$".$row['pay_rate']);
                                    $csv->getActiveSheet()->mergeCells("B$i:C$i");

                                    $working = (($row['check_out_timestamp'] - $row['check_in_timestamp']) - ($row['break_out_timestamp'] - $row['break_in_timestamp']));
    //                                $break = $row['break_out_timestamp'] - $row['break_in_timestamp'];
    //
    //                                $hours = floor($working / 3600);
    //                                $minutes = floor(($working / 60) % 60);
    //                                $break = floor($break / 60);

                                    $totalCost = floor($working/60) * ($row['pay_rate']/60);

                                    $csv->getActiveSheet()->SetCellValue("D$i", 'Total Shift Cost: ');
                                    $csv->getActiveSheet()->SetCellValue("E$i", "$".round($totalCost, 2));
                                    $csv->getActiveSheet()->mergeCells("E$i:F$i");

//                                    $i++;
//                                    $csv->getActiveSheet()->SetCellValue("A$i", 'Shift ');
//                                    $csv->getActiveSheet()->mergeCells("A$i:F$i");
//
//                                    $i++;
//                                    $csv->getActiveSheet()->getRowDimension($i)->setRowHeight("120");
//                                    $image = is_file(_SHARED_UPLOADS_."/photos/".$row['cin_image'])? _SHARED_UPLOADS_."/photos/{$row['cin_image']}":_SHARED_UPLOADS_."/nopreview.png";
//                                    $objDrawing = '';
//                                    $objDrawing = new PHPExcel_Worksheet_Drawing();
//                                    $objDrawing->setName('Checked-in');
//                                    $objDrawing->setDescription('Checked-in');
//                                    $objDrawing->setPath($image);
//
//                                    $objDrawing->setWidthAndHeight(100,100);
//                                    $objDrawing->setResizeProportional(true);
//                                    $objDrawing->setCoordinates("A$i");
//                                    $objDrawing->setWorksheet($csv->getActiveSheet());
//
//                                    $image = is_file(_SHARED_UPLOADS_."/photos/".$row['cout_image'])? _SHARED_UPLOADS_."/photos/{$row['cout_image']}":_SHARED_UPLOADS_."/nopreview.png";
//                                    $objDrawing = '';
//                                    $objDrawing = new PHPExcel_Worksheet_Drawing();
//                                    $objDrawing->setName('Checked-out');
//                                    $objDrawing->setDescription('Checked-out');
//                                    $objDrawing->setPath($image);
//
//                                    $objDrawing->setWidthAndHeight(100,100);
//                                    $objDrawing->setResizeProportional(true);
//                                    $objDrawing->setCoordinates("B$i");
//                                    $objDrawing->setWorksheet($csv->getActiveSheet());
//                                    //$csv->getActiveSheet()->mergeCells("B$i:C$i");
//
//
//                                    $image = is_file(_SHARED_UPLOADS_."/photos/".$row['bin_image'])? _SHARED_UPLOADS_."/photos/{$row['bin_image']}":_SHARED_UPLOADS_."/nopreview.png";
//                                    $objDrawing = '';
//                                    $objDrawing = new PHPExcel_Worksheet_Drawing();
//                                    $objDrawing->setName('Start Break');
//                                    $objDrawing->setDescription('Start Break');
//                                    $objDrawing->setPath($image);
//
//                                    $objDrawing->setWidthAndHeight(100,100);
//                                    $objDrawing->setResizeProportional(true);
//                                    $objDrawing->setCoordinates("D$i");
//                                    $objDrawing->setWorksheet($csv->getActiveSheet());
//                                    //$csv->getActiveSheet()->mergeCells("D$i:E$i");
//
//                                    $image = is_file(_SHARED_UPLOADS_."/photos/".$row['bout_image'])? _SHARED_UPLOADS_."/photos/{$row['bout_image']}":_SHARED_UPLOADS_."/nopreview.png";
//                                    $objDrawing = '';
//                                    $objDrawing = new PHPExcel_Worksheet_Drawing();
//                                    $objDrawing->setName('End Break');
//                                    $objDrawing->setDescription('End Break');
//                                    $objDrawing->setPath($image);
//
//                                    $objDrawing->setWidthAndHeight(100,100);
//                                    $objDrawing->setResizeProportional(true);
//                                    $objDrawing->setCoordinates("F$i");
//                                    $objDrawing->setWorksheet($csv->getActiveSheet());
//
//                                    $csv->getActiveSheet()->mergeCells("A$i:F$i");
//
//                                    $i++;
//                                    $csv->getActiveSheet()->SetCellValue("A$i", 'Checked-in');
//                                    $csv->getActiveSheet()->SetCellValue("B$i", 'Checked-out');
//                                    $csv->getActiveSheet()->mergeCells("B$i:C$i");
//                                    $csv->getActiveSheet()->SetCellValue("D$i", 'Start Break');
//                                    $csv->getActiveSheet()->mergeCells("D$i:E$i");
//                                    $csv->getActiveSheet()->SetCellValue("F$i", 'End Break');
                                    $endBold = $i;

                                    $csv->getActiveSheet()->getStyle("A$startBold:A$endBold")->getFont()->setBold();
                                }
                                else 
                                {
                                    $working = (($row['check_out_timestamp'] - $row['check_in_timestamp']) - ($row['break_out_timestamp'] - $row['break_in_timestamp']));
                                    $totalCost = floor($working/60) * ($row['pay_rate']/60);
                                    
                                    if($filterBy == _EMPLOYEE_)
                                    {
                                        include _SHARED_UPLOADS_."/temp/reports/emp/detailed/main.php";
                                    }
                                    elseif($filterBy == _OFFICE_)
                                    {
                                        include _SHARED_UPLOADS_."/temp/reports/site/detailed/main.php";
                                    }
                                    elseif($filterBy == _CLIENT_)
                                    {
                                        include _SHARED_UPLOADS_."/temp/reports/client/detailed/main.php";
                                    }
                                }
                            }
                            
                            if($format != "pdf")
                            {
                                $csv->getActiveSheet()->setTitle("Detailed Report - ".date("Y-m-d"));

                                foreach(range('A','F') as $columnID) 
                                {
                                    $csv->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
                                }
                                $csv->getActiveSheet()->getColumnDimension("B")->setWidth("160");
                                $csv->getActiveSheet()->getColumnDimension("E")->setWidth("160");
                                $styleArray = array(
                                    'borders' => array(
                                        'allborders' => array(
                                            'style' => PHPExcel_Style_Border::BORDER_THIN
                                        )
                                    )
                                );
                                $csv->getActiveSheet()->getStyle(
                                    'A6:' . 
                                    $csv->getActiveSheet()->getHighestColumn() . 
                                    $csv->getActiveSheet()->getHighestRow()
                                )->applyFromArray($styleArray);
                                
                                $csv->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
                                $csv->getActiveSheet()->getColumnDimension('F')->setAutoSize(false);
                                $csv->getActiveSheet()->getColumnDimension('C')->setWidth("7");
                                $csv->getActiveSheet()->getColumnDimension('F')->setWidth("7");
                                if(!isset($_SESSION['cron_script']))
                                {
                                    $csvWriter->save("php://output");
                                }
                                else 
                                {
                                    $path = _SHARED_UPLOADS_."/temp/REPORT_".time().rand(999,9999).".".$format;
                                    $csvWriter->save($path);
                                    return $path;
                                }
                            }
                            else 
                            {
                                include_once _SHARED_UPLOADS_."/temp/reports/emp/detailed/footer.php";
                                $content = ob_get_contents();
                                ob_end_clean();
                                if(!isset($_SESSION['cron_script']))
                                {
                                    $obj->WriteHTML($content);
                                    if($filterBy == _EMPLOYEE_)
                                    {
                                        $obj->Output("Employee_Detailed_Report.pdf", "D");
                                    }
                                    elseif($filterBy == _OFFICE_)
                                    {
                                        $obj->Output("Site_Detailed_Report.pdf", "D");
                                    }
                                    elseif($filterBy == _CLIENT_)
                                    {
                                        $obj->Output("Company_Detailed_Report.pdf", "D");
                                    }
                                }
                                else 
                                {
                                    $path = _SHARED_UPLOADS_."/temp/REPORT_".time().rand(999,9999).".".$format;                                    
                                    $obj->Output($path, "F");
                                    return $path;
                                }
                                exit();
                            }
                        }
                    }
                    elseif($reportType == "checkin")
                    {
                        $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                        $dailyReports = $this->model->dailyCheckinReportBy($id, $filterBy, $fromTime, $toTime);
                        if(is_array($dailyReports))
                        { 
                            set_time_limit(0);
                            ini_set('memory_limit', '-1');
                            if($format != "pdf")
                            {
                                //Writing Data TO CSV
                                $csv = new PHPExcel();
                                if($format == "pdf")
                                {
        //                            $rendererName = PHPExcel_Settings::PDF_RENDERER_TCPDF;
        //                            $rendererLibrary = 'tcpdf';
                                    $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                                    $rendererLibrary = 'mpdf';
                                    $rendererLibraryPath = _LIBRARIES_PATH_."/".$rendererLibrary;
                                    PHPExcel_Settings::setPdfRenderer($rendererName,$rendererLibraryPath); 
                                    $csvWriter = PHPExcel_IOFactory::createWriter($csv, "PDF");
                                    $extenstion = "pdf";
                                }
                                else 
                                {
                                    $csvWriter = PHPExcel_IOFactory::createWriter($csv, "Excel5");
                                    $extenstion = "csv";
                                }

                                if(!isset($_SESSION['cron_script']))
                                {
                                    //Sending Headers
                                    if($format == "pdf")
                                    {
                                        header("Content-Type: application/pdf");
                                    }
                                    else 
                                    {
                                        header("Content-Type: application/csv");
                                    }
                                    header("Content-Disposition: attachment; filename=\"Daily_Check-in_Report.$extenstion\"");
                                    header("Cache-Control: max-age=0");
                                }
                                $csv->getProperties()->setLastModifiedBy("OTL");
                                $csv->getProperties()->setTitle("OTL - ".ucfirst($type)." Check-in Report");
                                $csv->getProperties()->setSubject("Daily Check-in Report");
                                $csv->getProperties()->setDescription("This Is Auto Daily Check-in Report Generated By OTL");

                                $csv->setActiveSheetIndex(0); 

                                //Adding Image TO Sheet
                                $objDrawing = new PHPExcel_Worksheet_Drawing();
                                $objDrawing->setName('OTL LOGO');
                                $objDrawing->setDescription('OTL LOGO');
                                $objDrawing->setPath($otlLogo);

                                $objDrawing->setWidthAndHeight(150,90);
                                $objDrawing->setResizeProportional(true);
                                $objDrawing->setCoordinates('A1');
                                $objDrawing->setWorksheet($csv->getActiveSheet());

                                $objDrawing = '';

                                //$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
                                $objDrawing = new PHPExcel_Worksheet_Drawing();
                                $objDrawing->setName('Company LOGO');
                                $objDrawing->setDescription('Company LOGO');
                                $objDrawing->setPath($companyLogo);

                                $objDrawing->setWidthAndHeight(150,90);
                                $objDrawing->setResizeProportional(true);
                                $objDrawing->setCoordinates('E1');
                                $objDrawing->setWorksheet($csv->getActiveSheet());

                                $csv->getActiveSheet()->getRowDimension(1)->setRowHeight("30");
                                //End

                                //Writing Heading
                                $csv->getActiveSheet()->SetCellValue('A2', 'Daily Check-in Report');
                                $csv->getActiveSheet()->mergeCells("A2:G2");
                                $csv->getActiveSheet()->getStyle('A2:G2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                $csv->getActiveSheet()->getStyle('A2:G2')->getFont()->setBold()->setSize(18);

                                $csv->getActiveSheet()->SetCellValue('G3', date("d/m/Y"));
                                $csv->getActiveSheet()->mergeCells("A3:G3");
                                $csv->getActiveSheet()->getStyle('A3:G3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                $i = 4;

                                //Writing Data
                                $csv->getActiveSheet()->SetCellValue("A$i", '#');
                                $csv->getActiveSheet()->SetCellValue("B$i", 'Site/Office');
                                $csv->getActiveSheet()->SetCellValue("C$i", 'Timezone');
                                $csv->getActiveSheet()->SetCellValue("D$i", 'Employee Pin');
                                $csv->getActiveSheet()->SetCellValue("E$i", 'Name');
                                $csv->getActiveSheet()->SetCellValue("F$i", 'Check-in');
                                $csv->getActiveSheet()->SetCellValue("G$i", 'Status');
                                $csv->getActiveSheet()->getStyle("A$i:G$i")->getFont()->setBold(true);
                            }
                            else 
                            {
                                require_once _LIBRARIES_PATH_."/mpdf/mpdf.php";
                                $obj = new mPDF('utf-8', 'A4-L');
                                ob_start();
                                include_once _SHARED_UPLOADS_."/temp/reports/checkin/header.php";
                            }
                            //LOOP HERE
                            $index = 1;
                            $timezone = '';
                            $set_offset = '';
                            $cur_offset = '';
                            foreach($dailyReports as $report)
                            {
                                if(!empty($report['timezone']))
                                {
                                    $cur_offset = $report['timezone'];
                                    if($cur_offset != $set_offset)
                                    {
                                        $set_offset = $cur_offset;
                                        list($hours, $minutes) = explode(':', $set_offset);
                                        $seconds = $hours * 60 * 60 + $minutes * 60;
                                        $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 1);
                                        // Workaround for bug #44780
                                        if($timezone === false) 
                                        {
                                            $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 0);
                                        }
                                        date_default_timezone_set($timezone);
                                    }
                                }
                                if($format != "pdf")
                                {
                                    $i++;
                                    $csv->getActiveSheet()->SetCellValue("A{$i}", $index++);
                                    if($filterBy == _CLIENT_) 
                                    {
                                        if($report['check_in_timestamp'] >= $fromTime && $report['check_in_timestamp'] <= $toTime)
                                        {
                                            $csv->getActiveSheet()->SetCellValue("B{$i}", $report['office_name']);
                                            $csv->getActiveSheet()->SetCellValue("C{$i}", $report['timezone']);
                                            $csv->getActiveSheet()->SetCellValue("D{$i}", $report['pin']);
                                            $csv->getActiveSheet()->SetCellValue("E{$i}", $report['emp_first_name'].' '.$report['emp_last_name']);
                                            $csv->getActiveSheet()->SetCellValue("F{$i}", date('H:i', $report['check_in_timestamp']));

                                            $myModel = $this->loadModel("employee");
                                            $graceTime = $myModel->getGraceTime($report['emp_id']);
                                            date_default_timezone_set($this->config['timezone']);
                                            $shiftStart = strtotime(date("H:i", ($report['timing_from']+$graceTime*60)));
//                                            date_default_timezone_set($timezone);
                                            $entry = strtotime(date("H:i", $report['check_in_timestamp'] + ($seconds)));
                                            if($shiftStart >= $entry)
                                            {
                                                $csv->getActiveSheet()->SetCellValue("G{$i}", "On Time");
                                            }
                                            else 
                                            {
                                                $objDrawing = '';
                                                $objDrawing = new PHPExcel_Worksheet_Drawing();
                                                $objDrawing->setName('Flag Logo');
                                                $objDrawing->setDescription('Flag Logo');
                                                $objDrawing->setPath($flagLogo);

                                                $objDrawing->setWidthAndHeight(30,20);
                                                $objDrawing->setResizeProportional(true);
                                                $objDrawing->setCoordinates("G$i");
                                                $objDrawing->setWorksheet($csv->getActiveSheet());
                                                $late = ceil(($entry - $shiftStart) / 60) - $report['emp_grace_time'];
                                                $csv->getActiveSheet()->SetCellValue("G{$i}", "Late - ".$late."mins");
                                            }
                                        }
                                        else
                                        {
                                            $csv->getActiveSheet()->SetCellValue("B{$i}", "-");
                                            $csv->getActiveSheet()->SetCellValue("C{$i}", "-");
                                            $csv->getActiveSheet()->SetCellValue("D{$i}", $report['pin']);
                                            $csv->getActiveSheet()->SetCellValue("E{$i}", $report['emp_first_name'].' '.$report['emp_last_name']);
                                            $csv->getActiveSheet()->SetCellValue("F{$i}", "-");
                                            $csv->getActiveSheet()->SetCellValue("G{$i}", "Not Checked-in");
                                            $csv->getActiveSheet()->getStyle("G{$i}")->applyFromArray(['color' => ['rgb' => 'B42C2D']]);
                                        }
                                        date_default_timezone_set($timezone);
                                    }
                                }
                                else 
                                {
                                    if(!($report['check_in_timestamp'] >= $fromTime && $report['check_in_timestamp'] <= $toTime))
                                    {
                                        $report['office_name'] = "-";
                                        $report['timezone'] = "-";
                                        $report['check_in_timestamp'] = "-";
                                        $status = "<span style='color:red'>Not Checked-in</span>";
                                    }
                                    else 
                                    {
                                        $myModel = $this->loadModel("employee");
                                        $graceTime = $myModel->getGraceTime($report['emp_id']);
                                        date_default_timezone_set($this->config['timezone']);
                                        $shiftStart = strtotime(date("H:i", ($report['timing_from']+$graceTime*60)));
//                                        date_default_timezone_set($timezone);
                                        $entry = strtotime(date("H:i", $report['check_in_timestamp'] + ($seconds)));
                                        if($shiftStart >= $entry)
                                        {
                                            $status =  "On Time";
                                        }
                                        else 
                                        {
                                            $late = ceil(($entry - $shiftStart) / 60) - $report['emp_grace_time'];
                                            $status = "<img src='".$flagLogo."'> Late - $late mins";
                                        }
                                    }
                                    date_default_timezone_set($timezone);
                                    include _SHARED_UPLOADS_."/temp/reports/checkin/main.php";
                                }
                            }
                            
                            if($format != "pdf")
                            {
                                $csv->getActiveSheet()->setTitle("Daily Check-in - ".date("Y-m-d"));

                                //PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
                                foreach(range('A','G') as $columnID) 
                                {
                                    $csv->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
                                }
                                $styleArray = array(
                                    'borders' => array(
                                        'allborders' => array(
                                            'style' => PHPExcel_Style_Border::BORDER_THIN
                                        )
                                    )
                                );
                                $csv->getActiveSheet()->getStyle(
                                    'A4:' . 
                                    $csv->getActiveSheet()->getHighestColumn() . 
                                    $csv->getActiveSheet()->getHighestRow()
                                )->applyFromArray($styleArray);
                                $csv->getActiveSheet()->getStyle('G5:G'.$csv->getActiveSheet()->getHighestRow())->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                                $csv->getActiveSheet()->getColumnDimension("G")->setAutoSize(false);
                                $csv->getActiveSheet()->getColumnDimension("G")->setWidth("15");
                                if(!isset($_SESSION['cron_script']))
                                {
                                    $csvWriter->save("php://output");
                                }
                                else 
                                {
                                    $path = _SHARED_UPLOADS_."/temp/REPORT_".time().rand(999,9999).".".$format;
                                    $csvWriter->save($path);
                                    return $path;
                                }
                            }
                            else 
                            {
                                include_once _SHARED_UPLOADS_."/temp/reports/checkin/footer.php";
                                $content = ob_get_contents();
                                ob_end_clean();
                                if(!isset($_SESSION['cron_script']))
                                {
                                    $obj->WriteHTML($content);
                                    $obj->Output("Daily_Check-in_Report.pdf", "D");
                                }
                                else 
                                {
                                    $path = _SHARED_UPLOADS_."/temp/REPORT_".time().rand(999,9999).".".$format;                                    
                                    $obj->Output($path, "F");
                                    return $path;
                                }
                                exit();
                            }
                        }
                    }
                    elseif($reportType == "attendance")
                    { 
                        $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                        $dailyReports = $this->model->dailyCheckinReportBy($id, $filterBy, $fromTime, $toTime);
                        if(is_array($dailyReports))
                        { 
                            set_time_limit(0);
                            ini_set('memory_limit', '-1');
                            if($format != "pdf")
                            {
                                //Writing Data TO CSV
                                $csv = new PHPExcel();
                                if($format == "pdf")
                                {
        //                            $rendererName = PHPExcel_Settings::PDF_RENDERER_TCPDF;
        //                            $rendererLibrary = 'tcpdf';
                                    $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                                    $rendererLibrary = 'mpdf';
                                    $rendererLibraryPath = _LIBRARIES_PATH_."/".$rendererLibrary;
                                    PHPExcel_Settings::setPdfRenderer($rendererName,$rendererLibraryPath); 
                                    $csvWriter = PHPExcel_IOFactory::createWriter($csv, "PDF");
                                    $extenstion = "pdf";
                                }
                                else 
                                {
                                    $csvWriter = PHPExcel_IOFactory::createWriter($csv, "Excel5");
                                    $extenstion = "csv";
                                }
                                if(!isset($_SESSION['cron_script']))
                                {
                                    //Sending Headers
                                    if($format == "pdf")
                                    {
                                        header("Content-Type: application/pdf");
                                    }
                                    else 
                                    {
                                        header("Content-Type: application/csv");
                                    }
                                    header("Content-Disposition: attachment; filename=\"Daily_Attendance_Report.$extenstion\"");
                                    header("Cache-Control: max-age=0");
                                }

                                $csv->getProperties()->setLastModifiedBy("OTL");
                                $csv->getProperties()->setTitle("OTL - ".ucfirst($type)." Attendance Report");
                                $csv->getProperties()->setSubject("Daily Check-in Report");
                                $csv->getProperties()->setDescription("This Is Auto Daily Attendance Report Generated By OTL");

                                $csv->setActiveSheetIndex(0); 

                                //Adding Image TO Sheet
                                $objDrawing = new PHPExcel_Worksheet_Drawing();
                                $objDrawing->setName('OTL LOGO');
                                $objDrawing->setDescription('OTL LOGO');
                                $objDrawing->setPath($otlLogo);

                                $objDrawing->setWidthAndHeight(150,90);
                                $objDrawing->setResizeProportional(true);
                                $objDrawing->setCoordinates('A1');
                                $objDrawing->setWorksheet($csv->getActiveSheet());

                                $objDrawing = '';

                                //$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
                                $objDrawing = new PHPExcel_Worksheet_Drawing();
                                $objDrawing->setName('Company LOGO');
                                $objDrawing->setDescription('Company LOGO');
                                $objDrawing->setPath($companyLogo);

                                $objDrawing->setWidthAndHeight(150,90);
                                $objDrawing->setResizeProportional(true);
                                $objDrawing->setCoordinates('E1');
                                $objDrawing->setWorksheet($csv->getActiveSheet());

                                $csv->getActiveSheet()->getRowDimension(1)->setRowHeight("30");
                                //End

                                //Writing Heading
                                $csv->getActiveSheet()->SetCellValue('A2', 'Daily Attendance Report');
                                $csv->getActiveSheet()->mergeCells("A2:H2");
                                $csv->getActiveSheet()->getStyle('A2:H2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                $csv->getActiveSheet()->getStyle('A2:H2')->getFont()->setBold()->setSize(18);

                                $csv->getActiveSheet()->SetCellValue('H3', date("d/m/Y"));
                                $csv->getActiveSheet()->mergeCells("A3:H3");
                                $csv->getActiveSheet()->getStyle('A3:H3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                $i = 4;

                                //Writing Data
                                $csv->getActiveSheet()->SetCellValue("A$i", '#');
                                $csv->getActiveSheet()->SetCellValue("B$i", 'Site/Office');
                                $csv->getActiveSheet()->SetCellValue("C$i", 'Timezone');
                                $csv->getActiveSheet()->SetCellValue("D$i", 'Employee Pin');
                                $csv->getActiveSheet()->SetCellValue("E$i", 'Name');
                                $csv->getActiveSheet()->SetCellValue("F$i", 'Check-in');
                                $csv->getActiveSheet()->SetCellValue("G$i", 'Check-out');
                                $csv->getActiveSheet()->SetCellValue("H$i", 'Status');
                                $csv->getActiveSheet()->getStyle("A$i:H$i")->getFont()->setBold(true);
                            }
                            else 
                            {
                                require_once _LIBRARIES_PATH_."/mpdf/mpdf.php";
                                $obj = new mPDF('utf-8', 'A4-L');
                                ob_start();
                                include_once _SHARED_UPLOADS_."/temp/reports/attendance/header.php";
                            }
                            //LOOP HERE
                            $index = 1;
                            $timezone = '';
                            $set_offset = '';
                            $cur_offset = '';
                            foreach($dailyReports as $report)
                            {
                                if(!empty($report['timezone']))
                                {
                                    $cur_offset = $report['timezone'];
                                    if($cur_offset != $set_offset)
                                    {
                                        $set_offset = $cur_offset;
                                        list($hours, $minutes) = explode(':', $set_offset);
                                        $seconds = $hours * 60 * 60 + $minutes * 60;
                                        $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 1);
                                        // Workaround for bug #44780
                                        if($timezone === false) 
                                        {
                                            $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 0);
                                        }
                                        date_default_timezone_set($timezone);
                                    }
                                }
                                
                                if($format != "pdf")
                                {
                                    $i++;
                                    $csv->getActiveSheet()->SetCellValue("A{$i}", $index++);
                                    if($filterBy == _CLIENT_) 
                                    {
                                        if($report['check_in_timestamp'] >= $fromTime && $report['check_in_timestamp'] <= $toTime)
                                        {
                                            $csv->getActiveSheet()->SetCellValue("B{$i}", $report['office_name']);
                                            $csv->getActiveSheet()->SetCellValue("C{$i}", $report['timezone']);
                                            $csv->getActiveSheet()->SetCellValue("D{$i}", $report['pin']);
                                            $csv->getActiveSheet()->SetCellValue("E{$i}", $report['emp_first_name'].' '.$report['emp_last_name']);
                                            $csv->getActiveSheet()->SetCellValue("F{$i}", date('H:i', $report['check_in_timestamp']));
                                            if(empty($report['check_out_timestamp']))
                                            {
                                                $csv->getActiveSheet()->SetCellValue("G{$i}", "-");
                                            }
                                            else
                                            {
                                                $csv->getActiveSheet()->SetCellValue("G{$i}", date('H:i', $report['check_out_timestamp']));
                                            }
                                            $myModel = $this->loadModel("employee");
                                            $graceTime = $myModel->getGraceTime($report['emp_id']);
                                            date_default_timezone_set($this->config['timezone']);
                                            $shiftStart = strtotime(date("H:i", ($report['timing_from']+$graceTime*60)));
//                                            date_default_timezone_set($timezone);
                                            $entry = strtotime(date("H:i", $report['check_in_timestamp'] + ($seconds)));
                                            if($shiftStart >= $entry)
                                            {
                                                $csv->getActiveSheet()->SetCellValue("H{$i}", "On Time");
                                            }
                                            else 
                                            {
                                                $objDrawing = '';
                                                $objDrawing = new PHPExcel_Worksheet_Drawing();
                                                $objDrawing->setName('Flag Logo');
                                                $objDrawing->setDescription('Flag Logo');
                                                $objDrawing->setPath($flagLogo);

                                                $objDrawing->setWidthAndHeight(30,20);
                                                $objDrawing->setResizeProportional(true);
                                                $objDrawing->setCoordinates("H$i");
                                                $objDrawing->setWorksheet($csv->getActiveSheet());
                                                $late = ceil(($entry - $shiftStart) / 60) - $report['emp_grace_time'];
                                                if($late > 0)
                                                {
                                                    $csv->getActiveSheet()->SetCellValue("H{$i}", "Late - ".$late."mins");
                                                }
                                                else 
                                                {
                                                    $csv->getActiveSheet()->SetCellValue("H{$i}", "On Time");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            $csv->getActiveSheet()->SetCellValue("B{$i}", "-");
                                            $csv->getActiveSheet()->SetCellValue("C{$i}", "-");
                                            $csv->getActiveSheet()->SetCellValue("D{$i}", $report['pin']);
                                            $csv->getActiveSheet()->SetCellValue("E{$i}", $report['emp_first_name'].' '.$report['emp_last_name']);
                                            $csv->getActiveSheet()->SetCellValue("F{$i}", "-");
                                            $csv->getActiveSheet()->SetCellValue("G{$i}", "-");
                                            $csv->getActiveSheet()->SetCellValue("H{$i}", "Not Checked-in");
                                            $csv->getActiveSheet()->getStyle("H{$i}")->applyFromArray(['color' => ['rgb' => 'B42C2D']]);
                                        }
                                        date_default_timezone_set($timezone);
                                    }
                                }
                                else 
                                {
                                    if(!($report['check_in_timestamp'] >= $fromTime && $report['check_in_timestamp'] <= $toTime))
                                    {
                                        $report['office_name'] = "-";
                                        $report['timezone'] = "-";
                                        $report['check_in_timestamp'] = "-";
                                        $report['check_out_timestamp'] = "-";
                                        $status = "<span style='color:red'>Not Checked-in</span>";
                                    }
                                    else 
                                    {
                                        if(empty($report['check_out_timestamp']))
                                        {
                                            $report['check_out_timestamp'] = "-";
                                        }
                                        $myModel = $this->loadModel("employee");
                                        $graceTime = $myModel->getGraceTime($report['emp_id']);
                                        date_default_timezone_set($this->config['timezone']);
                                        $shiftStart = strtotime(date("H:i", ($report['timing_from']+$graceTime*60)));
//                                        date_default_timezone_set($timezone);
                                        $entry = strtotime(date("H:i", $report['check_in_timestamp'] + ($seconds)));
                                        if($shiftStart >= $entry)
                                        {
                                            $status =  "On Time";
                                        }
                                        else 
                                        {
                                            $late = ceil(($entry - $shiftStart) / 60) - $report['emp_grace_time'];
                                            if($late > 0)
                                            {
                                                $status = "<img src='".$flagLogo."'> Late - $late mins";
                                            }
                                            else 
                                            {
                                                $status =  "On Time";
                                            }
                                        }
                                    }
                                    date_default_timezone_set($timezone);
                                    include _SHARED_UPLOADS_."/temp/reports/attendance/main.php";
                                }
                            }
                            
                            if($format != "pdf")
                            {
                                $csv->getActiveSheet()->setTitle("Daily Attendance - ".date("Y-m-d"));

                                //PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
                                foreach(range('A','G') as $columnID) 
                                {
                                    $csv->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
                                }
                                $styleArray = array(
                                    'borders' => array(
                                        'allborders' => array(
                                            'style' => PHPExcel_Style_Border::BORDER_THIN
                                        )
                                    )
                                );
                                $csv->getActiveSheet()->getStyle(
                                    'A4:' . 
                                    $csv->getActiveSheet()->getHighestColumn() . 
                                    $csv->getActiveSheet()->getHighestRow()
                                )->applyFromArray($styleArray);
                                $csv->getActiveSheet()->getStyle('H5:H'.$csv->getActiveSheet()->getHighestRow())->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                                $csv->getActiveSheet()->getColumnDimension("H")->setAutoSize(false);
                                $csv->getActiveSheet()->getColumnDimension("H")->setWidth("15");
                                if(!isset($_SESSION['cron_script']))
                                {
                                    $csvWriter->save("php://output");
                                }
                                else 
                                {
                                    $path = _SHARED_UPLOADS_."/temp/REPORT_".time().rand(999,9999).".".$format;
                                    $csvWriter->save($path);
                                    return $path;
                                }
                            }
                            else 
                            {
                                include_once _SHARED_UPLOADS_."/temp/reports/attendance/footer.php";
                                $content = ob_get_contents();
                                ob_end_clean();
                                if(!isset($_SESSION['cron_script']))
                                {
                                    $obj->WriteHTML($content);
                                    $obj->Output("Daily_Attendance_Report.pdf", "D");
                                }
                                else 
                                {
                                    $path = _SHARED_UPLOADS_."/temp/REPORT_".time().rand(999,9999).".".$format;                                    
                                    $obj->Output($path, "F");
                                    return $path;
                                }
                                exit();
                            }
                        }
                    }
                }
            }
       }
       else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
            die();
        }
   }
   
   public function export($type = 'daily', $format = 'csv') 
   {
       if(session::isLogin())
       {
            if(isset($_SESSION[__CLASS__."reports"]))
            {
                $admin = $_SESSION[__CLASS__."reports"];

                if($type == "weekly")
                { 
                    $dayStart = strtotime(date("Y-m-d")) - 86400*7;
                    $dayEnd = strtotime(date("Y-m-d")) + 86400;
                }
                elseif($type == "monthly")
                {
                    $dayStart = strtotime(date("Y-m-d")) - 86400*30;
                    $dayEnd = strtotime(date("Y-m-d")) + 86400;
                }
                elseif($type == "yearly")
                {
                    $dayStart = strtotime(date("Y-m-d")) - 86400*365;
                    $dayEnd = strtotime(date("Y-m-d")) + 86400;
                }
                elseif($type == "daily")
                {
                    $dayStart = strtotime(date("Y-m-d"));
                    $dayEnd = $dayStart + 86400;
                }
                elseif($type == "quarterly")
                {
                    $dayStart = strtotime(date("Y-m-d")) - 86400*90;
                    $dayEnd = strtotime(date("Y-m-d")) + 86400;
                }
                elseif($type == "semi")
                {
                    $type = "Semi_Annual";
                    $dayStart = strtotime(date("Y-m-d")) - 86400*180;
                    $dayEnd = strtotime(date("Y-m-d")) + 86400;
                }
                elseif($type == "custom")
                {
                    if(isset($_POST['export_submit']))
                    {
                        //date from for downlod pdf and csv
                        if(isset($_POST['date_from']) && !@empty($_POST['date_from']))
                        {
                            $_POST['date_from'] = str_replace("/", "-", $_POST['date_from']);
                            list($day, $month, $year) = explode("-",$_POST['date_from']);
                            if(@checkdate($month, $day, $year))
                            {
                                $dayStart = strtotime($_POST['date_from']);
                                $_SESSION[__CLASS__.__FUNCTION__."from"] = $dayStart;
                            }
                            else
                            {
                                $this->isError = true;
                                $this->errorMsgs['date_from_error'] = "Invalid Format of Date of From.";
                            }
                            $values['date_from'] = securestr::clean(str_replace("-", "/", $_POST['date_from']));
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['date_from_error'] = "Date of From is Required";
                        }
                        //date to for downlod pdf and csv
                        if(isset($_POST['date_to']) && !@empty($_POST['date_to']))
                        {
                            $_POST['date_to'] = str_replace("/", "-", $_POST['date_to']);
                            list($day, $month, $year) = explode("-",$_POST['date_to']);
                            if(@checkdate($month, $day, $year))
                            {
                                $dayEnd = strtotime($_POST['date_to']);
                                $_SESSION[__CLASS__.__FUNCTION__."to"] = $dayEnd;
                            }
                            else
                            {
                                $this->isError = true;
                                $this->errorMsgs['dob_error'] = "Invalid Format of Date of To.";
                            }
                            $values['date_to'] = securestr::clean(str_replace("-", "/", $_POST['date_to']));
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['date_to_error'] = "Date To is Required";
                        }
                        
                        if(isset($dayStart) && isset($dayEnd))
                        {
                            if($dayStart > $dayEnd)
                            {
                                $this->isError = true;
                                $this->errorMsgs['error'] = "Start Date Cannot Be Greater Then End Date";
                            }
                        }
                        //validate file format
                        if(isset($_POST['file']) && !@empty($_POST['file']))
                        {
                            if($_POST['file'] == "pdf" || $_POST['file'] == "csv")
                            {
                                $format = $_POST['file'];
                            }
                            else
                            {
                                $this->isError = true;
                                $this->errorMsgs['file_error'] = "Invalid File Format Selected";
                            }
                            $values['file'] = $_POST['file'];
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['file_error'] = "File Format is Required";
                        }
                        //validate Report Type
                        if(isset($_POST['report_type']) && !@empty($_POST['report_type']))
                        {
                            if($_POST['report_type'] == "flag" || $_POST['report_type'] == "summary" || $_POST['report_type'] == "detailed")
                            {
                                $reportType = $_POST['report_type'];
                            }
                            else
                            {
                                $this->isError = true;
                                $this->errorMsgs['report_type_error'] = "Invalid Report Type Selected";
                            }
                            $values['report_type'] = $_POST['report_type'];
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['report_type_error'] = "Report Type is Required";
                        }
                        
                        if($this->isError)
                        {
                            $error = $this->errorMsgs;
                            $this->data = compact("error", "values");
                            $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                            die();
                        }
                        else 
                        {
                            $this->exportit($format, $reportType, "custom", "company");
                            die();
                        }
                    }
                    else
                    {
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                        die();
                    }
                }
                 
                if(isset($dayStart) && isset($dayEnd) && !$this->isError)
                {
                    $this->model = $this->loadModel("admin");
                    $companyLogo = $this->model->getlogo($admin);
                    if(empty($companyLogo))
                    {
                        $companyLogo = _SHARED_UPLOADS_."/logos/default.png";
                    }
                    else 
                    {
                        $companyLogo = _SHARED_UPLOADS_."/logos/".$companyLogo;
                    }
                    $otlLogo = _SHARED_UPLOADS_."/logos/default.png";
                    
//                    $tempPNG = _SHARED_UPLOADS_."/TEMP_".time().  rand(999, 9999).".png";
//                    imagepng(imagecreatefromstring(file_get_contents($companyLogo)), $tempPNG);
//                    
//                    $companyLogoPNG = imagecreatefrompng($tempPNG);
//                    
//                    $otlLongPNG = imagecreatefrompng($otlLogo);
                    
                    $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                    $data = $this->model->viewReportBetween($admin, $dayStart, $dayEnd);
                    if(is_array($data))
                    { 
                        set_time_limit(0);
                        ini_set('memory_limit', '-1');
                        //Writing Data TO CSV
                        $csv = new PHPExcel();
                        if($format == "pdf")
                        {
//                            $rendererName = PHPExcel_Settings::PDF_RENDERER_TCPDF;
//                            $rendererLibrary = 'tcpdf';
                            $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                            $rendererLibrary = 'mpdf';
                            $rendererLibraryPath = _LIBRARIES_PATH_."/".$rendererLibrary;
                            PHPExcel_Settings::setPdfRenderer($rendererName,$rendererLibraryPath); 
                            $csvWriter = PHPExcel_IOFactory::createWriter($csv, "PDF");
                            $extenstion = "pdf";
                        }
                        else 
                        {
                            $csvWriter = PHPExcel_IOFactory::createWriter($csv, "Excel5");
                            $extenstion = "csv";
                        }

                        //Sending Headers
                        if($format == "pdf")
                        {
                            header("Content-Type: application/pdf");
                        }
                        else 
                        {
                            header("Content-Type: application/csv");
                        }
                        header("Content-Disposition: attachment; filename=\"".ucfirst($type)."Attendance_Report.$extenstion\"");
                        header("Cache-Control: max-age=0");

                        $csv->getProperties()->setLastModifiedBy("OTL");
                        $csv->getProperties()->setTitle("OTL - ".ucfirst($type)." Attendance");
                        $csv->getProperties()->setSubject("Attendance ");
                        $csv->getProperties()->setDescription("This Is Auto Attendance Generated By OTL");
                        
                        $csv->setActiveSheetIndex(0); 
                        
                        //Adding Image TO Sheet
                        //$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
                        $objDrawing = new PHPExcel_Worksheet_Drawing();
                        $objDrawing->setName('OTL LOGO');
                        $objDrawing->setDescription('OTL LOGO');
                        $objDrawing->setPath($otlLogo);
//                        $objDrawing->setImageResource($otlLongPNG);
//                        $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
//                        $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
                        $objDrawing->setWidthAndHeight(150,90);
                        $objDrawing->setResizeProportional(true);
                        $csv->getActiveSheet()->mergeCells("A1:G1");
                        $objDrawing->setCoordinates('A1');
                        $objDrawing->setWorksheet($csv->getActiveSheet());
                        
                        $objDrawing = '';
                        
                        //$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
                        $objDrawing = new PHPExcel_Worksheet_Drawing();
                        $objDrawing->setName('Company LOGO');
                        $objDrawing->setDescription('Company LOGO');
                        $objDrawing->setPath($companyLogo);
//                        $objDrawing->setImageResource($companyLogoPNG);
//                        $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
//                        $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
                        //$csv->getActiveSheet()->mergeCells("E1:G1");
                        $objDrawing->setWidthAndHeight(150,90);
                        $objDrawing->setResizeProportional(true);
                        $objDrawing->setCoordinates('E1');
                        $objDrawing->setWorksheet($csv->getActiveSheet());
                        
                        $csv->getActiveSheet()->getRowDimension(1)->setRowHeight("30");
                        //End
                        
                        //Writing Heading
                        $csv->getActiveSheet()->SetCellValue('A2', 'Summary Report');
                        $csv->getActiveSheet()->mergeCells("A2:G2");
                        $csv->getActiveSheet()->getStyle('A2:G2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        $csv->getActiveSheet()->getStyle('A2:G2')->getFont()->setBold()->setSize(18);
                        
                        //Writing Date
                        $csv->getActiveSheet()->SetCellValue('A3', 'From Date: '.date("d/m/Y", $dayStart));
                        $csv->getActiveSheet()->getStyle('D3:F3')->getFont()->setBold();
                        
                        $csv->getActiveSheet()->SetCellValue('D3', 'To Date: '.date("d/m/Y", $dayEnd));
                        $csv->getActiveSheet()->getStyle('D3:F3')->getFont()->setBold();
                        
                        //Total Cost
                        $csv->getActiveSheet()->SetCellValue('A4', 'Total Staff Cost: XXXXX');
                        $csv->getActiveSheet()->getStyle('A4:C4')->getFont()->setBold();
                                
                        //Writing Data
                        $csv->getActiveSheet()->SetCellValue('A5', 'Sr.');
                        $csv->getActiveSheet()->SetCellValue('B5', 'Employee Pin');
                        $csv->getActiveSheet()->SetCellValue('C5', 'Name');
                        $csv->getActiveSheet()->SetCellValue('D5', 'Total Hrs Worked');
                        $csv->getActiveSheet()->SetCellValue('E5', 'Total Break Duration');
                        $csv->getActiveSheet()->SetCellValue('F5', 'Rate/Hr');
                        $csv->getActiveSheet()->SetCellValue('G5', 'Total Cost');
                        $csv->getActiveSheet()->getStyle("A5:G5")->getFont()->setBold(true);

                        $i = 5;
                        $pre = 0;
                        $finalCost = 0;
                        $totalCost = 0;
                        $working = 0;
                        $break = 0;
                        foreach($data as $row)
                        {
                            if($pre != $row['pin'])
                            {
                                $pre = $row['pin'];
                                $i++;
                                $csv->getActiveSheet()->SetCellValue("A{$i}", $i-5);
                                $csv->getActiveSheet()->SetCellValue("B{$i}", $row['pin']);
                                $csv->getActiveSheet()->SetCellValue("C{$i}", $row['name']);
                                $csv->getActiveSheet()->SetCellValue("F{$i}", $row['pay_rate']);
                                $finalCost += $totalCost;
                                //echo "$totalCost<br>";
                                $totalCost = $working = $break = 0;
                            }
                            else 
                            {
                                $working += ($row['check_out_timestamp'] - $row['check_in_timestamp']) - ($row['break_out_timestamp'] - $row['break_in_timestamp']);
                                $break += $row['break_out_timestamp'] - $row['break_in_timestamp'];
                                
                                $hours = floor($working / 3600);
                                $minutes = floor(($break / 60) % 60);
                                
                                $totalCost = $hours * $row['pay_rate'];
                                //echo $row['name']." | {$row['pay_rate']} => $working | ".$hours." <> $break | ".$minutes." = ".$totalCost."<br>";
                                $csv->getActiveSheet()->SetCellValue("D{$i}", $hours);
                                $csv->getActiveSheet()->SetCellValue("E{$i}", $minutes);
                                $csv->getActiveSheet()->SetCellValue("G{$i}", $totalCost);
                            }
                        }
                        
                        //Total Cost
                        $csv->getActiveSheet()->SetCellValue('A4', "Total Staff Cost: $finalCost");
                        $csv->getActiveSheet()->getStyle('A4:C4')->getFont()->setBold();
                        
                        $csv->getActiveSheet()->setTitle("Attendance - ".date("Y-m-d"));
                        
                        //PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
                        foreach(range('A','G') as $columnID) 
                        {
                            $csv->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
                        }
                        $styleArray = array(
                            'borders' => array(
                                'allborders' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN
                                )
                            )
                        );
                        $csv->getActiveSheet()->getStyle(
                            'A1:' . 
                            $csv->getActiveSheet()->getHighestColumn() . 
                            $csv->getActiveSheet()->getHighestRow()
                        )->applyFromArray($styleArray);
                        
                        $csvWriter->save("php://output");
                     }
                }
                else 
                {
                    $error = $this->errorMsgs;
                    $this->data = compact("error", "values");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                }
            }
            else 
            {
                $error = $this->errorMsgs;
                $this->data = compact("error" , "values");
                $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
            }
       } 
       else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
            die();
        }
   }
   
   //Reports
   public function reports()
   {
       if(session::isLogin())
        {
           if(isset($_POST['submit']))
            {
                //validate admin
                if(isset($_POST['admin']))
                {
                    if(isset($_POST['csrf']))
                    {
                        if(!csrf::validateCsrf($_POST['csrf'], __FUNCTION__))
                        {
                            $this->errorMsgs;
                            $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                        }
                    }
                    else
                    {
                        $this->errorMsgs;
                        $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                    }
                    if(is_numeric($_POST['admin']))
                    {
                        $this->model = $this->loadModel("admin");
                        if($this->model)
                        {
                            if($this->model->adminExists($_POST['admin'], _CLIENT_,TRUE))
                            {
                                $admin = $_POST['admin'];
                                $adminName = $this->model->idToUsername($admin);
                                session::sessionStart();
                                $_SESSION[__CLASS__."reports"] = $admin;
                                $reports_bit = $this->model->getReportsBit($admin);
                                $this->data = compact("reports_bit");
                            }
                            else
                            {
                               $this->isError = true;
                               $this->errorMsgs['admin_error'] = "Admin Does Not Exists.";
                            }
                        }                    
                        else
                        {
                           $this->isError = true;
                           $this->errorMsgs['admin_error'] = "Admin does Not Exists";
                        }
                        $values['admin'] = $_POST['admin'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['admin_error'] = "Invalid Admin Selected";
                    }
               }
               else
               {
                   $this->isError = true;
                   $this->errorMsgs['admin_error'] = "Admin Name Required";
               }
               
               
               if($this->isError)
                {
                    unset($_POST['submit']);
                    $this->model = $this->loadModel("admin");
                    $admins = $this->model->viewAdmins();
                    $error = $this->errorMsgs;
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("admins", "csrf", "values", "error");
                }
            } 
            else 
            {
                $this->model = $this->loadModel("admin");
                $admins = $this->model->viewAdmins();
                $error = $this->errorMsgs;
                $csrf = csrf::generateCsrf(__FUNCTION__);
                $this->data = compact("admins", "csrf", "values");
            }
            
            $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
        } 
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
            die();
        }
   } 
   //Edit An Entry
   public function edit($id = '') 
   {       
        if(session::isLogin())
        {
            if(!empty($id) && is_numeric($id))
            {
                $admin = session::getAdminId();
                $this->model = $this->loadModel("check_in");
                $checkinId = $this->model->adminToCheckinRel($admin, $id);
                
                if(isset($_POST['submit'])) 
                {
                    //Start Transaction
                    $this->model->db->transaction();
                    
                    $this->model = $this->loadModel("check_in");
                    $office_id = $this->model->checkinToOfficeId($id);
                    $emp_id = $this->model->checkinToEmpId($id);
                    
                    $this->model = $this->loadModel("break_in");
                    $breakinTime = $this->model->breakinTimestamp($id);
                    
                    $this->model = $this->loadModel("office");
                    $offset = $this->model->getTimezone($office_id);
                    list($hours, $minutes) = explode(':', $offset);
                    $seconds = $hours * 60 * 60 + $minutes * 60;
                                        
                    $this->model = $this->loadModel("employee");
                    $graceTime = $this->model->getGraceTime($emp_id);
                    $fromTimestamp = $this->model->getFromTime($emp_id);
                    $toTimestamp = $this->model->getToTime($emp_id);
                    $breakDuration = $this->model->getBreakDuration($emp_id);
                    
                    if($graceTime == 0)
                    {
                        $this->model = $this->loadModel("admin");
                        $graceTime = $this->model->getGraceTime(session::getAdminId());
                    }

                    $a = date("H:i", $fromTimestamp);
                    $expectedEntry = strtotime($a);
                    $a1 = date("H:i", $toTimestamp);
                    $expectedOut = strtotime($a1);
                    
                    if($checkinId)
                    {
                        if(isset($_POST['checkin']))
                        {
                            if(preg_match("/^(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2})$/", $_POST['checkin']))
                            {
                                $checkin = strtotime(str_replace("/", "-", $_POST['checkin'])) - ($seconds);
                                $a = date("H:i", $checkin + ($seconds));
                                $entry = strtotime($a);
                                if($entry <= ($expectedEntry + $graceTime*60))
                                {
                                    $ciStatus = _IN_TIME_;
                                }
                                else
                                {
                                    $ciStatus = _LATE_;
                                }
                            }
                            else 
                            {
                                $this->isError = true;
                                $this->errorMsgs['checkin_error'] = "Invalid Date Time Format";
                                $values['checkin'] = securestr::clean($_POST['checkin']);
                            }
                        }
                        else 
                        {
                            $this->isError = true;
                            $this->errorMsgs['checkin_error'] = "Check-in Time Is Required.";
                        }

                        if(isset($_POST['checkout']))
                        {
                            if(preg_match("/^(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2})$/", $_POST['checkout']))
                            {
                                $checkout = strtotime(str_replace("/", "-", $_POST['checkout'])) - ($seconds);
                                $a = date("H:i", $checkout + ($seconds));
                                $out = strtotime($a);
                                if($out >= $expectedOut)
                                {
                                    $coStatus = _OVERTIME_;
                                }
                                else
                                {
                                    $coStatus = _EARLY_;
                                }
                                $this->model = $this->loadModel("check_out");
                                $checkoutExists = $this->model->checkoutExists($checkinId);
                                if(!$checkoutExists)
                                {
                                    $this->model->insertCheckOut($checkinId, $checkout, "nopreview.png");
                                }
                            }
                            else 
                            {
                                $this->isError = true;
                                $this->errorMsgs['checkout_error'] = "Invalid Date Time Format";
                                $values['checkout'] = securestr::clean($_POST['checkout']);
                            }
                        }
                        else 
                        {
                            $this->isError = true;
                            $this->errorMsgs['checkout_error'] = "Check-out Time Is Required.";
                        }
                        
                        if(isset($_POST['breakin']) && !$this->isError)
                        {
                            if(!empty($_POST['breakin']))
                            {
                                if(preg_match("/^(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2})$/", $_POST['breakin']))
                                {
                                    $breakin = strtotime(str_replace("/", "-", $_POST['breakin'])) - ($seconds);
                                    $biStatus = _IN_TIME_;
                                    $this->model = $this->loadModel("break_in");
                                    $breakinExists = $this->model->breakinExists($checkinId);
                                    if(!$breakinExists)
                                    {
                                        $this->model->insertBreakIn($checkinId, "nopreview.png", $breakin);
                                    }
                                }
                                else 
                                {
                                    $this->isError = true;
                                    $this->errorMsgs['breakin_error'] = "Invalid Date Time Format";
                                    $values['breakin'] = securestr::clean($_POST['breakin']);
                                }
                            }
                            elseif(!empty($_POST['breakout']))
                            {
                                $this->isError = true;
                                $this->errorMsgs['breakin_error'] = "Break Start Time Is Required.";
                            }
                            else 
                            {
                                $breakin = $checkout;
                                $biStatus = _IN_TIME_;
                                $this->model = $this->loadModel("break_in");
                                $breakinExists = $this->model->breakinExists($checkinId);
                                if(!$breakinExists)
                                {
                                    $this->model->insertBreakIn($checkinId, "nopreview.png", $breakin);
                                }
                            }
                        }
                        elseif(!empty($_POST['breakout']))
                        {
                            $this->isError = true;
                            $this->errorMsgs['breakin_error'] = "Break Start Time Is Required.";
                        }
                        else 
                        {
                            if(!$this->isError)
                            {
                                $breakin = $checkout;
                                $biStatus = _IN_TIME_;
                                $this->model = $this->loadModel("break_in");
                                $breakinExists = $this->model->breakinExists($checkinId);
                                if(!$breakinExists)
                                {
                                    $this->model->insertBreakIn($checkinId, "nopreview.png", $breakin);
                                }
                            }
                        }

                        if(isset($_POST['breakout']) && !$this->isError)
                        {
                            if(!empty($_POST['breakout']))
                            {
                                if(preg_match("/^(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2})$/", $_POST['breakout']))
                                {
                                    $breakout = strtotime(str_replace("/", "-", $_POST['breakout'])) - ($seconds);
                                    if(($breakout - $breakinTime)/60 > $breakDuration)
                                    {
                                        $boStatus = _LATE_;
                                    } 
                                    else 
                                    {
                                        $boStatus = _EARLY_;
                                    }
                                    $this->model = $this->loadModel("break_out");
                                    $breakoutExists = $this->model->breakoutExists($checkinId);
                                    if(!$breakoutExists)
                                    {
                                        $this->model->insertBreakOut($checkinId, "nopreview.png", $breakout);
                                    }
                                }
                                else 
                                {
                                    $this->isError = true;
                                    $this->errorMsgs['breakout_error'] = "Invalid Date Time Format";
                                    $values['breakout'] = securestr::clean($_POST['breakout']);
                                }
                            }
                            elseif(!empty($_POST['breakin']))
                            {
                                $this->isError = true;
                                $this->errorMsgs['breakout_error'] = "Break End Time Is Required.";
                            }
                            else 
                            {
                                $breakout = $checkout;
                                $boStatus = _EARLY_;
                                $this->model = $this->loadModel("break_out");
                                $breakoutExists = $this->model->breakoutExists($checkinId);
                                if(!$breakoutExists)
                                {
                                    $this->model->insertBreakOut($checkinId, "nopreview.png", $breakout);
                                }
                            }
                        }
                        elseif(!empty($_POST['breakin']))
                        {
                            $this->isError = true;
                            $this->errorMsgs['breakout_error'] = "Break End Time Is Required.";
                        }
                        else 
                        {
                            if(!$this->isError)
                            {
                                $breakout = $checkout;
                                $boStatus = _EARLY_;
                                $this->model = $this->loadModel("break_out");
                                $breakoutExists = $this->model->breakoutExists($checkinId);
                                if(!$breakoutExists)
                                {
                                    $this->model->insertBreakOut($checkinId, "nopreview.png", $breakout);
                                }
                            }
                        }

                        

                        if(!$this->isError)
                        {
                            if($checkin > $breakin || $checkin > $breakout || $checkin > $checkout)
                            {
                                $this->isError = true;
                                $this->errorMsgs['error'] = "Checkin Time Cannot Be Greater.";
                            } 
                            elseif($breakin > $breakout || $breakin > $checkout)
                            {
                                $this->isError = true;
                                $this->errorMsgs['error'] = "Breakin Time Cannot Be Greater Then Breakout or Checkout.";
                            }
                            elseif($breakout > $checkout)
                            {
                                $this->isError = true;
                                $this->errorMsgs['error'] = "Checkout Time Cannot Be Shorter Then Breakout.";
                            } 

                            if(!$this->isError)
                            {
                                $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                                $this->model->edit($checkinId, $checkin, $breakin, $breakout, $checkout, $ciStatus, $coStatus, $biStatus, $boStatus);
                                
                                if(!$this->model->db->commit())
                                {
                                    $this->isError = true;
                                    $this->errorMsgs['error'] = "Something Went Wrong.";
                                } 
                                else 
                                {
                                    $res['response'] = "Successfully Updated";
                                }
                            } 
                            else 
                            {
                                $this->model->db->rollback();
                                $this->isError = true;
                                if(!isset($this->errorMsgs['error']))
                                {
                                    $this->errorMsgs['error'] = "Something Went Wrong.";
                                }
                            }
                        }
                        else 
                        {
                            $this->model->db->rollback();
                            $this->isError = true;
                            if(!isset($this->errorMsgs['error']))
                            {
                                $this->errorMsgs['error'] = "Something Went Wrong.";
                            }
                        }
                    }
                    else 
                    {
                        $this->isError = true;
                        $this->errorMsgs['error'] = "Something Went Wrong.";
                    }
                }
            } 
            else 
            {
                $this->isError = TRUE;
                $this->errorMsgs['error'] = "Something Went Wrong."; 
            } 
            
            if($checkinId) 
            {
                $error = $this->errorMsgs;
                $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                $data = $this->model->viewByCheckinId($checkinId);
                
                if(sizeof($data) > 0)
                {                   
                    $data = $data[0];
                    list($hours, $minutes) = explode(':', $data['timezone']);
                    $seconds = $hours * 60 * 60 + $minutes * 60;
                    $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 1);
                    // Workaround for bug #44780
                    if($timezone === false) 
                    {
                        $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 0);
                    }
                    date_default_timezone_set($timezone);
                    $this->data = compact("values", "res", "error", "data", "id");                    
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data); 
                } 
                else 
                {                    
                    ob_get_clean();
                    header("Location: {$this->config['domain']}"._PUBLIC_PATH_."attendance/view");
                    die();
                }
            } 
            else 
            {
                ob_get_clean();
                header("Location: {$this->config['domain']}"._PUBLIC_PATH_."attendance/view");
                die();
            }
        }
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
            die();
        }
   }
   public function live($limit = 10, $offset = 0)
   {
        if(session::isLogin())
        {
            $type = "both";
            $filter = "today";
            if(isset($_POST['type']))
            {
                if($_POST['type'] == "both" || $_POST['type'] == "check_in" || $_POST['type'] == "check_out")
                {
                    $type = $_POST['type'];
                    $_SESSION[__CLASS__.__FUNCTION__.'filter'] = $type;
                }
            }
            elseif(isset($_SESSION[__CLASS__.__FUNCTION__.'filter']))
            {
                $type = $_SESSION[__CLASS__.__FUNCTION__.'filter'];
            } else{}
            
            if(isset($_POST['filter']))
            { 
                if($_POST['filter'] == "today" || $_POST['filter'] == "week")
                {
                    $filter = $_POST['filter'];
                    $_SESSION[__CLASS__.__FUNCTION__.'duration'] = $filter;
                }
				
				
			
            }
            elseif(isset($_SESSION[__CLASS__.__FUNCTION__.'duration']))
            {
                $filter = $_SESSION[__CLASS__.__FUNCTION__.'duration'];
            } else {}
            
            if($filter == "week")
            {
                $fromFilter = strtotime(date("d-m-Y")) - (86400 * 7);
            }
            else 
            {
                $fromFilter = strtotime(date("d-m-Y"));//time() - 86400;
				 
            }
            
            if(!is_numeric($limit) || !is_numeric($offset) || $limit <= 0 || $offset < 0)
            {
                $limit = 10;
                $offset = 0;
            }
            
			
            $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
            $attendance = $this->model->latestActivity($offset, $limit, $type, $fromFilter);            
            $total = $this->model->countAttendance(session::getAdminId(), session::getUserType(), $type, $fromFilter);
            $next = $offset + $limit;
            if($next > $total)
            {
                $next = $offset;
            }
            $previous = $offset - $limit;
            if($previous < 0)
            {
                $previous = 0;
            }
            $current = ceil($offset / $limit);
            $current++;
            if($current == 0)
            {
                $current = 1;
            }
            //Export
           // $exportId = md5(time().rand(0, 999));
            //exportCSV::setData($attendance, $exportId);
            $this->data = compact("attendance", "filter", "type", "exportId", "total", "next", "previous", "limit", "offset", "current");
            $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);  

        }
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
            die();
        }
   }
}
