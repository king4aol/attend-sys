<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of cron
 *
 * @author PSSLT2540p
 */
set_time_limit(0);
class cronController extends controller {
    private $isError = false;
    private $errorMsgs;
    private $model;
    private $controller;
    private $data;
    private $cronKey = '3310';
    
    public function reports($key = '') 
    {
        if($key == $this->cronKey)
        {
            $this->model = $this->loadModel("admin");
            $clients = $this->model->viewAdmins();
            if(is_array($clients))
            {
                $index = 0;
                $csv = FALSE;
                $this->controller = $this->loadController("attendance");
                session::sessionStart();
                $_SESSION['cron_script'] = TRUE;
                $this->model = $this->loadModel("membership_types");
                foreach($clients as $client)
                {
                    if($client['email_reports'] != 0)
                    {
                        $_SESSION["attendanceControllerreports"] = $client['admin_id'];
                        if($this->model->getTypeTitle($client['membership_type_id']) == "Enterprise")
                        {
                            $csv = TRUE;
                        }
                        else 
                        {
                            $csv = FALSE;
                        }
                        
                        if($client['email_reports'] & _DAILY_FLAG_)
                        {
                            if($csv)
                            {
                                $reportPath[0] = $this->controller->exportit("pdf", "flag", "daily", "company");
                                $reportPath[1] = $this->controller->exportit("csv", "flag", "daily", "company");
                            }
                            else 
                            {
                                $reportPath = $this->controller->exportit("pdf", "flag", "daily", "company");
                            }
                            if(sendmail::send()->dailyReport($client['email_address'], date("d/m/Y")." Flag Report", "<p>Please Check the Attachment File.</p>", $reportPath))
                            {
                                $this->data[$index][$client['username']]['type'] = "Flag Report";
                                $this->data[$index++][$client['username']]['sent'] = TRUE;
                            }
                            else 
                            {
                                $this->data[$index][$client['username']]['type'] = "Flag Report";
                                $this->data[$index++][$client['username']]['sent'] = FALSE;
                            }
                            
                            if($csv)
                            { 
                                foreach($reportPath as $r)
                                {
                                    unlink($r);
                                }
                            }
                            else 
                            {
                                unlink($reportPath);
                            }
                        }
                        if($client['email_reports'] & _DAILY_CHECKIN_)
                        {
                            if($csv)
                            {
                                $reportPath[0] = $this->controller->exportit("pdf", "checkin", "daily", "company");
                                $reportPath[1] = $this->controller->exportit("csv", "checkin", "daily", "company");
                            }
                            else 
                            {
                                $reportPath = $this->controller->exportit("pdf", "checkin", "daily", "company");
                            }
                            
                            if(sendmail::send()->dailyReport($client['email_address'], date("d/m/Y")." Check-in Report", "<p>Please Check the Attachment File.</p>", $reportPath))
                            {
                                $this->data[$index][$client['username']]['type'] = "Check-in Report";
                                $this->data[$index++][$client['username']]['sent'] = TRUE;
                            }
                            else 
                            {
                                $this->data[$index][$client['username']]['type'] = "Check-in Report";
                                $this->data[$index++][$client['username']]['sent'] = FALSE;
                            } 
                            
                            if($csv)
                            { 
                                foreach($reportPath as $r)
                                {
                                    unlink($r);
                                }
                            }
                            else 
                            {
                                unlink($reportPath);
                            }
                        }
                        if($client['email_reports'] & _DAILY_ATTND_)
                        {
                            if($csv)
                            {
                                $reportPath[0] = $this->controller->exportit("pdf", "attendance", "daily", "company");
                                $reportPath[1] = $this->controller->exportit("csv", "attendance", "daily", "company");
                            }
                            else 
                            {
                                $reportPath = $this->controller->exportit("pdf", "attendance", "daily", "company");
                            }
                            
                            if(sendmail::send()->dailyReport($client['email_address'], date("d/m/Y")." Attendance", "<p>Please Check the Attachment File.</p>", $reportPath))
                            {
                                $this->data[$index][$client['username']]['type'] = "Attendance";
                                $this->data[$index++][$client['username']]['sent'] = TRUE;
                            }
                            else 
                            {
                                $this->data[$index][$client['username']]['type'] = "Attendance";
                                $this->data[$index++][$client['username']]['sent'] = FALSE;
                            }
                            
                            if($csv)
                            { 
                                foreach($reportPath as $r)
                                {
                                    unlink($r);
                                }
                            }
                            else 
                            {
                                unlink($reportPath);
                            }
                        }
                    }
                }
            }
            else 
            {
                $this->data['isError'] = TRUE;
                $this->data['message'] = "Something Went Wrong";
            }
        }
        else 
        {
            $this->data['isError'] = TRUE;
            $this->data['message'] = "Invalid Cron Key";
        }
        echo json_encode($this->data);
    }
}
