<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clients
 *
 * @author Msajid
 */
class adminController extends controller
{
    private $data = [];
    private $isError = FALSE;
    private $errorMsgs = [];
    private $model;
    private $passwordLenght = 8;
    
    public function index()
    {
        
    }
    public function ajax()
    {
        if(session::isLogin())
        {
           if(isset($_POST['type']) && isset($_POST['action']) && isset($_POST['source']))
           {
                if($_POST['type'] == "companies" && $_POST['action'] == "select" && $_POST['source'] == "admin" && isset($_POST['input']))
                {
                    if(session::getUserType() == _ADMIN_)
                    {
                    $query = securestr::clean($_POST['input']);
                    $this->model = $this->loadModel("admin");
                    $clients = $this->model->searchAdmin($query);

                        if(sizeof($clients) > 0)
                        {
                            $response = $clients;
                        }
                        else 
                        {
                            $response = NULL;
                            $this->isError = TRUE;
                        }
                    }
                    else 
                    {
                        $response = NULL;
                        $this->isError = TRUE;
                    }
                }
                elseif($_POST['type'] == "offset" && $_POST['action'] == "timezone" && $_POST['source'] == "client" && isset($_POST['offset']))
                {
                    if(is_numeric($_POST['offset']))
                    {
                        $offset = $_POST['offset']*(-1);
                        $seconds = $offset * 60;
                        $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 1);
                        // Workaround for bug #44780
                        if($timezone === false) 
                        {
                            $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 0);
                        }
                        $_SESSION['client_timezone'] = $timezone;
                        $response = "Timezone $timezone: Offset ".$seconds/60 ." Abbr ".get_timezone_abbr($seconds);
                        $this->isError = FALSE;
                    }
                    else 
                    {
                        $response = "Invalid Timezone";
                        $this->isError = TRUE;
                    }
                }
                else 
                {
                    $response = "Nothing Found";
                    $this->isError = TRUE;
                }
           }
           else 
            {
                $response = "Nothing Found";
                $this->isError = TRUE;
            }
        }
        else 
        {
            $response = "You're Not Login";
            $this->isError = TRUE;
        }
        
        $this->data = "";
        $this->data['error'] = $this->isError;
        $this->data['response'] = $response;
        
        echo json_encode($this->data);
    }
   /* public function requests() 
    {
        if(session::isLogin())
        {
            $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
            $requests = [];

            if($this->model)
            {
                $requests = $this->mogdel->loadRequests();
            }
            $this->data = compact("requests");
            $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data); 
        }
        else 
        {
           header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login"); 
        }
    }*/
    
    public function view($limit = 9, $offset = 0) 
    {
        if(!is_numeric($limit) || !is_numeric($offset) || $limit <= 0 || $offset < 0)
        {
            $limit = 9;
            $offset = 0;
        }
        
        if(session::isLogin())
        {
            $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
            $requests = [];

            if($this->model)
            {
                $total = $this->model->totalAdmins();
                $requests = $this->model->viewAdmins(TRUE, $offset, $limit);
                $next = $offset + $limit;
                if($next > $total)
                {
                    $next = $offset;
                }
                $previous = $offset - $limit;
                if($previous < 0)
                {
                    $previous = 0;
                }
                $current = ceil($offset / $limit);
                $current++;
                if($current == 0)
                {
                    $current = 1;
                }
            }
            $this->data = compact("requests", "total", "limit", "offset", "next", "previous", "current");
            $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data); 
        }
        else 
        {
           header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login"); 
        }
    }
    
    public function action($user = '', $action = '')
    {
        if(session::isLogin())
        {
            $action = strtolower($action);
            if($action == "activate" || $action == "deactivate" || $action == "delete")
            {
                $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                
                if($this->model)
                {
                    if($this->model->adminExists($user, _CLIENT_))
                    {
                        $performed = $this->model->actions($user ,$action);
                        if($performed)
                        {
                            ob_get_clean();
                            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."admin/view"); 
                            die();
                        }
                        else
                        {
                            $controller = $this->loadController("error");
                            if($controller)
                            {
                                $controller->_custom("Something Went Wrong.", "Something Went Wrong. Please Try Again Later.");
                            }
                        }
                    }
                    else
                    {
                        $controller = $this->loadController("error");
                        if($controller)
                        {
                            $controller->_custom("Client Does Not Exists.", "Client Does Not Exists. Please Check The Spellings.");
                        }
                    }
                }
            }
            elseif($action == "deviceremove")
            {
                if(isset($_SESSION[__CLASS__.'adminId']))
                {
                    $admin = $_SESSION[__CLASS__.'adminId'];
                }
                else 
                {
                    $admin = session::getAdminId();
                }
                if(is_numeric($user))
                {
                    $this->model = $this->loadModel("api_logs");
                    $uuid = $this->model->getUUIDId($user);
                    if($this->model->uuidExists($admin, $uuid))
                    {
                        if($this->model->delete($user))
                        {
                            ob_get_clean();
                            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."admin/devices/success"); 
                            die();
                        }
                        else
                        {
                            $controller = $this->loadController("error");
                            if($controller)
                            {
                                $controller->_custom("Unable to Perform Action.", "Something Went Wrong. While Removing UUID.");
                            }
                        }
                    }
                    else
                    {
                        $controller = $this->loadController("error");
                        if($controller)
                        {
                            $controller->_custom("UUID Does Not Belongs To You.", "UUID Does Not Belongs To You.");
                        }
                    }
                }
                else
                {
                    $controller = $this->loadController("error");
                    if($controller)
                    {
                        $controller->_custom("Client Does Not Exists.", "Client Does Not Exists. Please Check The Spellings.");
                    }
                }
            }
        }
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login"); 
            die();
        }
    }
    
    public function details($user = '') 
    {
        if(session::isLogin())
        {
            if(preg_match("/^[\w-$@._]+$/", $user))
            {
                $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                if($this->model)
                {
                    if($this->model->adminExists($user, _CLIENT_))
                    {
                        $details = $this->model->loadDetails($user);
                        $error = $this->isError;
                        $this->data = compact("details", "error");
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs = "Company Does Not Exists.";
                        $error = $this->isError;
                        $errorMsg = $this->errorMsgs;
                        $this->data = compact("error", "errorMsg");
                    }
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['error'] = "Something Went Wrong. Please Try Again.";
                    $error = $this->errorMsgs;
                    $this->data = compact("error");
                }
            }
            else
            {
                $this->isError = true;
                $this->errorMsgs['error'] = "Something Went Wrong. Please Try Again.";
                $error = $this->errorMsgs;
                $this->data = compact("error");
            }

            $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
        }
        else 
        {
           header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login"); 
        }
    }
       
    //edit admin load view
    
    public function edit($company = '')
    {
        if(session::isLogin())
        {
            if(!empty($company))
            {
                if(preg_match("/^[\w-$@._]+$/", $company))
                {
                    if(session::getUserType() == _ADMIN_)
                    {
                        $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                        $id = $this->model->usernameToId($company);
                        $userId = $this->model->idToUserId($id);
                    }
                }
            }
            
            if(!isset($_POST['update']))
            {
                if(!isset($id))
                {
                    $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                    $id = session::getAdminId();
                    $userId = session::getUserId();
                }
                if($this->model)
                {
                    $user = $this->model->idToUsername($id);
                    if($this->model->adminExists($user))
                    {
                        $details = $this->model->loadDetails($user);
                        $this->model = $this->loadModel("countries");
                        $countries = $this->model->loadCountries();
                        $country = $this->model->idToIso($details[0]['country_id']);
                        $this->model = $this->loadModel("membership_types");
                        $memberships = $this->model->loadTypes();
                        $this->data = compact("details", "countries", "memberships", "country", "company", "id");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs = "Client Does Not Exists.";
                        $error = $this->isError;
                        $errorMsg = $this->errorMsgs;
                        $this->data = compact("error", "errorMsg", "id");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                    }
                }
            }
            else
            {
                $country = '';
                //get user name
                if(!isset($id))
                {
                    $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                    $id = session::getAdminId();
                    $userId = session::getUserId();
                }
                $user = $this->model->idToUsername($id);
                //Checking First Name
                if(isset($_POST['fname']) && !@empty($_POST['fname']))
                {
                    if(ctype_alpha($_POST['fname']))
                    {
                        $firstName = $_POST['fname'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['fname_error'] = "Name Can Only Consist of Alphabets";
                    }
                    $values['fname'] = securestr::clean($_POST['fname']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['fname_error'] = "Firstname is Required";
                }
                
                //Checking Last Name
                if(isset($_POST['lname']) && !@empty($_POST['lname']))
                {
                    if(ctype_alpha($_POST['lname']))
                    {
                        $lastName = $_POST['lname'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['lname_error'] = "Name Can Only Consist of Alphabets";
                    }
                    $values['lname'] = securestr::clean($_POST['lname']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['lname_error'] = "Lastname is Required";
                }
                
                //office grace time
                
                /*if(isset($_POST['grace_time']) && !@empty($_POST['grace_time']))
                {
                    if(is_numeric($_POST['grace_time']))
                    {
                        $office_grace_time = $_POST['grace_time'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['grace_time_error'] = "office grace time is only in minutes";
                    }
                    $values['grace_time'] = securestr::clean($_POST['grace_time']);
                }
                else
                {
                    $office_grace_time = '';
                }
                
                */
                
                //validate company name
               
                if(isset($_POST['company_name']) && !@empty($_POST['company_name']))
                {
                    if(preg_match("/^[ \w#-:,]+$/", $_POST['company_name']))
                    {
                        $company_name = $_POST['company_name'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['company_name_error'] = "Name Can Only have alphabets,numeric,-,_,#,:, ,";
                    }
                    $values['company_name'] = securestr::clean($_POST['company_name']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['company_name_error'] = "company name is required";
                }
                
                //Validate and Cleaning Address
                if(isset($_POST['address']) && !@empty($_POST['address']))
                {
                    $address = securestr::clean($_POST['address']);
                    $values['address'] = $address;
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['address_error'] = "Address is Required";
                }
                
                //Checking Country
                if(isset($_POST['country']) && !@empty($_POST['country']))
                {
                    if(ctype_alpha($_POST['country']))
                    {
                        $this->model = $this->loadModel("countries");
                        if($this->model->countryExists($_POST['country']))
                        {
                            $country = $this->model->isoToId($_POST['country']);
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['country_error'] = "Invalid Country Selected.";
                        }
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['country_error'] = "Invalid Country Selected";
                    }
                    $values['country'] = $_POST['country'];
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['country_error'] = "Country is Required";
                }

                 //validating Date of Birth
                if(isset($_POST['dob']) && !@empty($_POST['dob']))
                {
                    $_POST['dob'] = str_replace("/", "-", $_POST['dob']);
                    list($day, $month, $year) = explode("-",$_POST['dob']);
                    if(@checkdate($month, $day, $year))
                    {
                        $dob = $_POST['dob'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['dob_error'] = "Invalid Format of Date of Birth.";
                    }
                    $values['dob'] = securestr::clean($_POST['dob']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['dob_error'] = "Date of Birth is Required";
                }

                //Validating Phone Number With Respect to Country
                if(isset($_POST['phone']) && !@empty($_POST['phone']))
                {
                    $this->model = $this->loadModel("contact_numbers");

                    if($this->model->validate($_POST['phone'], $country))
                    {
                        if(!$this->model->numberExistsExceptThis($_POST['phone'], $userId))
                        {
                            $phone = $_POST['phone'];
                        }
                        else
                        {
                           $this->isError = true;
                           $this->errorMsgs['phone_error'] = "Phone Number Already Exists"; 
                        }
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['phone_error'] = "Invalid Phone Number";
                    }
                    $values['phone'] = securestr::clean($_POST['phone']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['phone_error'] = "Phone Number is Required";
                }
                
//                //Checking Username
//                if(isset($_POST['user']) && !@empty($_POST['user']))
//                {
//                    if(ctype_alnum($_POST['user']))
//                    {
//                        $username = $_POST['user'];
//                        $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
//
//                        if($this->model->adminExistsExceptThis($username, $id))
//                        {
//                            $this->isError = true;
//                            $this->errorMsgs['user_error'] = "Username Already Exists";
//                        }
//                    }
//                    else
//                    {
//                        $this->isError = true;
//                        $this->errorMsgs['user_error'] = "Invalid Username";
//                    }
//
//                    $values['user'] = securestr::clean($_POST['user']);
//                }
//                else
//                {
//                    $this->isError = true;
//                    $this->errorMsgs['user_error'] = "Username is Required";
//                }
                //Checking Email
                if(isset($_POST['email']) && !@empty($_POST['email']))
                {
                    if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
                    {
                        $this->model = $this->loadModel("email");
                        if(!$this->model->emailExistsExceptThis($_POST['email'], $userId))
                        {
                            $email = $_POST['email'];
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['email_error'] = "Email Already Exists";
                        }
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['email_error'] = "Invalid Email Address";
                    }
                    $values['email'] = securestr::clean($_POST['email']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['email_error'] = "Email is Required";
                }
                
                //Validating Password
                
                if(isset($_POST['pass']) && !@empty($_POST['pass']) && isset($_POST['confirmpass']) && !@empty($_POST['confirmpass'] ))
                {
                    if(strlen($_POST['pass']) >= password::$passwordLenght)
                    {
                        if($_POST['pass'] == $_POST['confirmpass'])
                        {
                            $password = $_POST['pass'];
                            $ecnryptedPassword= password::encryptPassword($password);
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['pass_error'] = "Password does not Match";
                        }
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['pass_error'] = "Minimum Length of password is 8!";
                    }
                }
                else
                {
                    $this->model = $this->loadModel("admin");
                    $user = $this->model->idToUsername($id);
                    $detail = $this->model->loadDetails($user);
                    $detail = $detail[0];
                    $ecnryptedPassword = $detail['password'];
                }
                
                if(session::getUserType() == _ADMIN_ && session::getAdminId() !== $id )
                {
                    if(isset($_POST['memtype']) && !@empty($_POST['memtype']))
                    {
                        if(is_numeric($_POST['memtype']))
                        {
                            $this->model = $this->loadModel("membership_types");
                            if($this->model->membershipExists($_POST['memtype']))
                            {
                                $memType = $_POST['memtype'];
                            }
                            else
                            {
                                $this->isError = true;
                                $this->errorMsgs['memtype_error'] = "Invalid Membership Selected";
                            }
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['memtype_error'] = "Invalid Membership Selected";
                        }
                        $values['memtype'] = $_POST['memtype'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['memtype_error'] = "Membership Type is Required";
                    }
                }
                else 
                {
                    $this->model = $this->loadModel("admin");
                    $memType = $this->model->getMembershipType($id);
                }
                
                if(!$this->isError)
                {
                    $username = $email;
                    $this->model = $this->loadModel(str_replace("Controller", "",__CLASS__));
                    $details = $this->model->edit($user, $firstName, $lastName , $company_name, $address, $country, $dob, $phone, $username, $email,$ecnryptedPassword, $memType);
                    if($company != session::getUsername())
                    {
                        $company = $user;
                    }
                    $res['response'] = "Successfully Updated.";
                    $this->model = $this->loadModel("countries");
                    $countries = $this->model->loadCountries();
                    $country = $this->model->idToIso($details[0]['country_id']);
                    $this->model = $this->loadModel("membership_types");
                    $memberships = $this->model->loadTypes();
                    $this->data = compact("details", "countries", "memberships", "country", "res", "company", "id");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
                }
                else
                {
                    if(!isset($id))
                    {
                        $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                        $id = session::getAdminId();
                    }
                    $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                    $user = $this->model->idToUsername($id);
                    if($this->model)
                    {
                        if($this->model->adminExists($user))
                        {
                            $error = $this->errorMsgs;
                            $details = $this->model->loadDetails($user);
                            $this->model = $this->loadModel("countries");
                            $countries = $this->model->loadCountries();
                            $country = $this->model->idToIso($details[0]['country_id']);
                            $this->model = $this->loadModel("membership_types");
                            $memberships = $this->model->loadTypes();
                            $this->data = compact("details", "countries", "memberships", "error", "country", "company", "id");
                            $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs = "Client Does Not Exists.";
                            $error = $this->errorMsgs;
                            $this->data = compact("error", "id");
                            $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                        }
                    }
                }
            }
            $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
        }
        else 
        {
           header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login"); 
        }
    }
    
    //extend date of Membership 
    
    public function extendDate($username = '')
    {
      if(session::isLogin())
        {
            if(!isset($_POST['update']))
            {
                $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                 if($this->model)
                {
                    if($this->model->adminExists($username, _CLIENT_))
                    {
                        $details = $this->model->loadDetails($username, TRUE);
                        $this->data = compact("details");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs = "Client Does Not Exists.";
                        $error = $this->isError;
                        $errorMsg = $this->errorMsgs;
                        $details = $this->model->loadDetails($username, TRUE);
                        $this->data = compact("details","error");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                    }
                }
            }
            else
            {
                $memtype_end_time ='';
                $memtype_start_time ='';
                
                //Validate MemberShip Start Time
                
                if(isset($_POST['memtype_start_time']) && !@empty($_POST['memtype_start_time']))
                {
                    $_POST['memtype_start_time'] = str_replace("/", "-", $_POST['memtype_start_time']);
                    @list($year, $month, $day) = explode("-",$_POST['memtype_start_time']);
                    if(@checkdate($month, $day, $year))
                    {
                        $memtype_start_time = $_POST['memtype_start_time'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['memtype_start_error'] = "Invalid Format of Date of Membership Start Date.";
                    }
                    $values['memtype_start_time'] = securestr::clean($_POST['memtype_start_time']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['memtype_start_error'] = "Membership Start Date is required.";
                }
               
                //validate Date Of MemberShip End Time
                
                if(isset($_POST['memtype_end_time']) && !@empty($_POST['memtype_end_time']))
                {
                    $_POST['memtype_end_time'] = str_replace("/", "-", $_POST['memtype_end_time']);
                    @list($year, $month, $day) = explode("-",$_POST['memtype_end_time']);
                    if(@checkdate($month, $day, $year))
                    {
                        $memtype_end_time = $_POST['memtype_end_time'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['memtype_end_error'] = "Invalid Format of Date of Membership End Date.";
                    }
                    $values['memtype_end_time'] = securestr::clean($_POST['memtype_end_time']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['memtype_end_error'] = "Membership End Date is required.";
                }
                $date1 = strtotime($memtype_start_time);
                $date2 = strtotime($memtype_end_time);
                if($date2 < $date1)
                {
                    $this->isError = TRUE;
                    $this->errorMsgs['error'] = "End Time Is Shorter Then Start Time.";
                }
                if(!$this->isError)
                {
                   $this->model = $this->loadModel(str_replace("Controller", "",__CLASS__));
                   $details = $this->model->editMembershipDates($username, $memtype_start_time, $memtype_end_time);
                   $this->data = compact("details");
                   $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
                }
                else
                {
                   $this->model = $this->loadModel(str_replace("Controller", "",__CLASS__));
                   $details = $this->model->loadDetails($username ,TRUE);
                   $error = $this->errorMsgs;
                   $this->data = compact("details", "error");
                   $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                }
            }
            $this->model = $this->loadModel(str_replace("Controller", "",__CLASS__));
            $details = $this->model->loadDetails($username ,TRUE);
            $error = $this->errorMsgs;
            $this->data = compact("details", "error");
            $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
        }
        else 
        {
           header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login"); 
        }
    }
    //Api Logins 
    public function api($action = '')
    { 
        if(session::isLogin())
        {
            if(isset($_POST['submit']) && !isset($_POST['api_submit']) && $action != "edit")
            {   //validate admin
                if(isset($_POST['admin']))
                {
                    if(is_numeric($_POST['admin']))
                    {
                        $this->model = $this->loadModel("admin");
                        if($this->model)
                        {
                            if($this->model->adminExists($_POST['admin'], _CLIENT_,TRUE))
                            {
                                $admin = $_POST['admin'];
                                session::sessionStart();
                                $_SESSION[__CLASS__.'api_adminId'] = $admin;
                            }
                            else
                            {
                               $this->isError = true;
                               $this->errorMsgs['admin_error'] = "Admin Does Not Exists.";
                            }
                        }                    
                        else
                        {
                           $this->isError = true;
                           $this->errorMsgs['admin_error'] = "Admin does Not Exists";
                        }
                        $values['admin'] = $_POST['admin'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['admin_error'] = "Invalid Admin Selected";
                    }
               }
               else
               {
                   $this->isError = true;
                   $this->errorMsgs['admin_error'] = "Admin Name Required";
               }
               
               if($this->isError)
               {
                   unset($_POST['submit']);
                   $isForm = FALSE;
                   $adminModel = $this->loadModel("admin");
                    if($adminModel)
                    {
                        $admins = $adminModel->viewAdmins();
                    }
                   $error = $this->errorMsgs;
                   $csrf = csrf::generateCsrf(__FUNCTION__);
                   $this->data = compact("admins", "office",  "csrf", "error", "values", "isForm");
                   $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
               }
               else 
               {
                    $this->model = $this->loadModel("api");
                    if(isset($_SESSION[__CLASS__.'api_adminId']))
                    {
                        $admin_id = $_SESSION[__CLASS__.'api_adminId'];
                    }
                    else 
                    {
                        $admin_id = session::getAdminId();
                    }
                    
                    if($this->model->userExists($admin_id))
                    {
                        $isForm = FALSE;
                        $username = $this->model->getUsername($admin_id);
                    }
                    else 
                    {
                        $isForm = TRUE;
                    }
                    $_POST['submit'] = "";
                    $this->data = compact("isForm", "username");
                    $this->loadView(str_replace("Controller", "", __CLASS__),  __FUNCTION__, $this->data);
               }
            }
            elseif(isset($_POST['api_submit']))
            { 
                $this->model = $this->loadModel("api");
                if(isset($_SESSION[__CLASS__.'api_adminId']))
                {
                    $admin_id = $_SESSION[__CLASS__.'api_adminId'];
                }
                else 
                {
                    $admin_id = session::getAdminId();
                }
                
                $hasApi = FALSE;
                if($this->model->userExists($admin_id))
                {
                    $hasApi = TRUE;
                }
                
                //Checking Username
                if(isset($_POST['username']) && !@empty($_POST['username']))
                {
                    if(preg_match("/^[\w-$@._]+$/", $_POST['username']))
                    {   
                        $this->model = $this->loadModel("api");
                        if(!$this->model->usernameExistsExceptthis($_POST['username'], $admin_id))
                        {
                            $username = $_POST['username'];
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['error'] = "Username Already Exists";
                        }
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['username_error'] = "Invalid Username";
                    }

                    $values['username'] = securestr::clean($_POST['username']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['username_error'] = "Username is Required";
                }
                //validate Password And ConfirmPassword
                if(isset($_POST['pass']) && !@empty($_POST['pass']) && isset($_POST['confirmpass']) && !@empty($_POST['confirmpass'] ))
                {
                    if(strlen($_POST['pass']) >= password::$passwordLenght)
                    {
                        if($_POST['pass'] == $_POST['confirmpass'])
                        {
                            $password = $_POST['pass'];
                            $ecnryptedPassword = password::encryptPassword($password);
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['pass_error'] = "Password does not Match";
                        }
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['pass_error'] = "Minimum Length of password is 8";
                    }
                }
                else
                {
                    if($hasApi)
                    {
                        $details = $this->model->view($admin_id);
                        $ecnryptedPassword = $details['password'];
                    }
                    else 
                    {
                        $this->isError = true;
                        $this->errorMsgs['pass_error'] = "Password Is Required";
                    }
                }
                 //Inserting Data
                if(!$this->isError)
                {
                    $isForm = FALSE;
                    $_POST['submit'] = "";
                    //Inserting Admin
                    $this->model = $this->loadModel("api");
                    if($hasApi)
                    {
                        $this->model->update($admin_id, $username, $ecnryptedPassword);
                    }
                    else 
                    {
                        $this->model->insertApiLogins($admin_id, $username, $ecnryptedPassword);
                    }
                    
                    $username = $this->model->getUsername($admin_id);
                    $res['response'] = "Updated Successfully.";
                    $this->data = compact("username", "isForm", "res");
                    $this->loadView(str_replace("Controller", "",__CLASS__),__FUNCTION__,  $this->data);
                }
                else
                {
                    $isForm = TRUE;
                    $_POST['submit'] = "";
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $error = $this->errorMsgs;
                    $this->data = compact("error", "values", "csrf", "isForm");
                    $this->loadView(str_replace("Controller", "", __CLASS__),  "api", $this->data);
                }
            }
            elseif($action == "edit")
            { 
                $this->model = $this->loadModel("api");
                if(isset($_SESSION[__CLASS__.'api_adminId']))
                {
                    $admin_id = $_SESSION[__CLASS__.'api_adminId'];
                }
                else 
                {
                    $admin_id = session::getAdminId();
                }
                $this->model = $this->loadModel("api");
                if(isset($_SESSION[__CLASS__.'api_adminId']))
                {
                    $admin_id = $_SESSION[__CLASS__.'api_adminId'];
                }
                else 
                {
                    $admin_id = session::getAdminId();
                }
                if($this->model->userExists($admin_id))
                {
                    $isForm = TRUE;
                    $values['username'] = $this->model->getUsername($admin_id);
                }
                else 
                {
                    $isForm = TRUE;
                }
                $_POST['submit'] = "";
                $this->data = compact("isForm", "values");
                $this->loadView(str_replace("Controller", "", __CLASS__),  __FUNCTION__, $this->data);
            }
            else 
            {
                $isForm = FALSE;
                $adminModel = $this->loadModel("admin");
                if($adminModel)
                {
                    $admins = $adminModel->viewAdmins();
                }
                $csrf = csrf::generateCsrf(__FUNCTION__);
                $this->data = compact("admins","csrf");
                $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
            }
        }
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login"); 
            die();
        }
    }
    //Set Messages
    public function message() 
    {
        if(session::isLogin())
        {
            if(!isset($_POST['submit']) && !isset($_POST['msg_submit']))
            {
                $this->model = $this->loadModel("admin");
                $admins = $this->model->viewAdmins();
                $error = $this->errorMsgs;
                $csrf = csrf::generateCsrf(__FUNCTION__);
                $this->data = compact("admins");
                $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
            }
            elseif(isset($_POST['submit']) && !isset($_POST['msg_submit']))
            {
                //checking admin
                if(isset($_POST['admin']))
                {
                    if(is_numeric($_POST['admin']))
                    {
                        $adminModel = $this->loadModel("admin");
                        if($adminModel)
                        {
                            if($adminModel->adminExists($_POST['admin'], _CLIENT_, TRUE))
                            {
                                $admin = $_POST['admin'];
                                $_SESSION[__CLASS__.__FUNCTION__] = $admin;
                            }
                            else
                            {
                               $this->isError = true;
                               $this->errorMsgs['admin_error'] = "Admin Does Not Exists.";
                            }
                        }                    
                        else
                        {
                           $this->isError = true;
                           $this->errorMsgs['admin_error'] = "Admin does Not Exists";
                        }
                        $values['admin'] = $_POST['admin'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['admin_error'] = "Invalid Admin Selected";
                    }
               }
               else
               {
                   
               }
               
               if($this->isError)
               {
                    unset($_POST['submit']);
                    $this->model = $this->loadModel("admin");
                    $admins = $this->model->viewAdmins();
                    $error = $this->errorMsgs;
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("admins", "csrf", "error", "values");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
               }
               else
               {
                   if(isset($_SESSION[__CLASS__.__FUNCTION__]))
                   {
                       $admin = $_SESSION[__CLASS__.__FUNCTION__];
                   }
                   else 
                   {
                       $admin = session::getAdminId();
                   }
                   
                   $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                   $messages = $this->model->viewMessage($admin);
                   $this->data = compact("messages");
                   $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
               }
            }
            elseif(isset($_POST['msg_submit']))
            {
                if(isset($_SESSION[__CLASS__.__FUNCTION__]))
                {
                    $admin = $_SESSION[__CLASS__.__FUNCTION__];
                }
                else 
                {
                    $admin = session::getAdminId();
                }
                
                $cin_msg = '';
                $cout_msg = '';
                $bin_msg = '';
                $bout_msg = '';
                
                if(isset($_POST['cin_message']))
                {
                    $cin_msg = htmlentities($_POST['cin_message'], ENT_QUOTES);
                }
                if(isset($_POST['cout_message']))
                {
                    $cout_msg = htmlentities($_POST['cout_message'], ENT_QUOTES);
                }
                if(isset($_POST['bin_message']))
                {
                    $bin_msg = htmlentities($_POST['bin_message'], ENT_QUOTES);
                }
                if(isset($_POST['bout_message']))
                {
                    $bout_msg = htmlentities($_POST['bout_message'], ENT_QUOTES);
                }
                
                $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                $this->model->updateMessages($admin, $cin_msg, $cout_msg, $bin_msg, $bout_msg);
                $res['response'] = "Messages Updated Successfully";
                $messages = $this->model->viewMessage($admin);
                $this->data = compact("messages", "res");
                $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
            }
        }
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login"); 
            die();
        }
    }
    //Global Gracetime
    public function gracetime() 
    {
        if(session::isLogin())
        {
            if(isset($_POST['submit']) && !isset($_POST['grace_submit']))
            {
                //validate admin
                if(isset($_POST['admin']))
                {
                    if(is_numeric($_POST['admin']))
                    {
                        $this->model = $this->loadModel("admin");
                        if($this->model)
                        {
                            if($this->model->adminExists($_POST['admin'], _CLIENT_,TRUE))
                            {
                                $admin = $_POST['admin'];
                                session::sessionStart();
                                $_SESSION[__CLASS__.'settings_adminId'] = $admin;
                            }
                            else
                            {
                               $this->isError = true;
                               $this->errorMsgs['admin_error'] = "Admin Does Not Exists.";
                            }
                        }                    
                        else
                        {
                           $this->isError = true;
                           $this->errorMsgs['admin_error'] = "Admin does Not Exists";
                        }
                        $values['admin'] = $_POST['admin'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['admin_error'] = "Invalid Admin Selected";
                    }
               }
               else
               {
                   $this->isError = true;
                   $this->errorMsgs['admin_error'] = "Admin Name Required";
               }
               
               if($this->isError)
               {
                    unset($_POST['submit']);
                    $this->model = $this->loadModel("admin");
                    $admins = $this->model->viewAdmins();
                    $error = $this->errorMsgs;
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("admins", "office",  "csrf", "error", "values");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
               }
               else 
               {
                    $this->model = $this->loadModel("admin");
                    $values['grace_time'] = $this->model->getGracetime($admin);
                    $this->data = compact("values");
                    $this->loadView(str_replace("Controller", "", __CLASS__),__FUNCTION__,  $this->data);
               }
            }
            elseif(isset($_POST['grace_submit']))
            {
                if(isset($_SESSION[__CLASS__.'settings_adminId']))
                {
                    $admin = $_SESSION[__CLASS__.'settings_adminId'];
                }
                else 
                {
                    $admin = session::getAdminId();
                }
                //officegrace time
                if(isset($_POST['grace_time']) && !@empty($_POST['grace_time']))
                {
                    if(is_numeric($_POST['grace_time']))
                    {
                        $office_grace_time = $_POST['grace_time'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['grace_time_error'] = "office grace time is only in minutes";
                    }
                    $values['grace_time'] = securestr::clean($_POST['grace_time']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['grace_time_error'] = "Grace Time Is Required";
                }
                
                if(!$this->isError)
                {
                    unset($_POST['grace_submit']);
                    $_POST['submit'] = "";
                    $this->model = $this->loadModel("admin");
                    $this->model->setGracetime($admin ,$office_grace_time);
                    $res['response'] = "Global Time Is updated Successfully.";
                    $this->data = compact("values", "res");
                    $this->loadView(str_replace("Controller", "", __CLASS__),__FUNCTION__,  $this->data);
                }
                else 
                {
                    unset($_POST['grace_submit']);
                    $_POST['submit'] = "";
                    $this->errorMsgs['error'] = "Something Went Wrong. Please Try Again.";
                    $error = $this->errorMsgs;
                    $this->data = compact("error", "values");
                    $this->loadView(str_replace("Controller", "", __CLASS__),__FUNCTION__,  $this->data);
                }
            }
            /*elseif(!$this->isError && isset($_POST['submit']))
            {
                $_POST['submit'] = "";
                unset($_POST['grace_submit']);
                $this->model = $this->loadModel("admin");
                $values = $this->model->getGracetime($admin);
                $this->data = compact("values");
                $this->loadView(str_replace("Controller", "", __CLASS__),__FUNCTION__,  $this->data);
            }*/
            else 
            {
                $this->model = $this->loadModel("admin");
                $admins = $this->model->viewAdmins();
                $error = $this->errorMsgs;
                $csrf = csrf::generateCsrf(__FUNCTION__);
                $this->data = compact("admins", "office",  "csrf", "error", "values");
                $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
            }
        }
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login"); 
            die();
        }
    }
    //Set Logo
    public function logo() 
    {
        if(session::isLogin())
        {
            if(isset($_POST['submit']) && !isset($_POST['update_logo']))
            {
                //validate admin
                if(isset($_POST['admin']))
                {
                    if(is_numeric($_POST['admin']))
                    {
                        $this->model = $this->loadModel("admin");
                        if($this->model)
                        {
                            if($this->model->adminExists($_POST['admin'], _CLIENT_,TRUE))
                            {
                                $admin = $_POST['admin'];
                                session::sessionStart();
                                $_SESSION[__CLASS__.'settings_adminId'] = $admin;
                            }
                            else
                            {
                               $this->isError = true;
                               $this->errorMsgs['admin_error'] = "Admin Does Not Exists.";
                            }
                        }                    
                        else
                        {
                           $this->isError = true;
                           $this->errorMsgs['admin_error'] = "Admin does Not Exists";
                        }
                        $values['admin'] = $_POST['admin'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['admin_error'] = "Invalid Admin Selected";
                    }
               }
               else
               {
                   $this->isError = true;
                   $this->errorMsgs['admin_error'] = "Admin Name Required";
               }
               
               if($this->isError)
               {
                    unset($_POST['submit']);
                    $this->model = $this->loadModel("admin");
                    $admins = $this->model->viewAdmins();
                    $error = $this->errorMsgs;
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("admins", "csrf", "error");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
               }
               else 
               {
                    $this->model = $this->loadModel("admin");
                    $values['logo_path'] = $this->model->getLogoPath($admin);
                    if(empty($values['logo_path']))
                    {
                        $values['logo_path'] = "logos/default.png";
                    }
                    else 
                    {
                        $values['logo_path'] = "logos/".$values['logo_path'];
                    }
                    $this->data = compact("values");
                    $this->loadView(str_replace("Controller", "", __CLASS__),__FUNCTION__,  $this->data);
               }
            }
            elseif(isset($_POST['update_logo'])) 
            { 
                $image = new Bulletproof\Image($_FILES);
                $image->setSize(1, 1048576); 
                //$image->setDimension(420, 110);
                $image->setMime(array("jpg", "png", "jpeg", "gif"));
                $image->setLocation("../shared/logos", 0777);  
                $image->setName("OTL_logo".time().  rand(0, 1000));
               
                if($image["logo"])
                { 
                    if($image->getWidth() == "420" && $image->getHeight() == "110")
                    {
                        if($image->upload())
                        { 
                            if(isset($_SESSION[__CLASS__.'settings_adminId']))
                            {
                                $admin = $_SESSION[__CLASS__.'settings_adminId'];
                            }
                            else 
                            {
                                $admin = session::getAdminId();
                            }
                            $this->model = $this->loadModel("admin");
                            $values['logo_path'] = $this->model->setLogoPath($admin, $image->getName().".".$image->getMime());
                            unset($_POST['update_logo']);
                            $_POST['submit'] = '';
                            $res['response'] = "Logo Uploaded Successfully.";
                            $values['logo_path'] = $image->getFullPath();
                            $this->data = compact("values", "res");
                            $this->loadView(str_replace("Controller", "", __CLASS__),__FUNCTION__,  $this->data);
                        }
                        else 
                        { 
                            if(isset($_SESSION[__CLASS__.'settings_adminId']))
                            {
                                $admin = $_SESSION[__CLASS__.'settings_adminId'];
                            }
                            else 
                            {
                                $admin = session::getAdminId();
                            }
                            unset($_POST['update_logo']);
                            $_POST['submit'] = '';
                            $error['error'] = $image['error'];//"Something Went Wrong.";
                            $this->model = $this->loadModel("admin");
                            $values['logo_path'] = $this->model->getLogoPath($admin);
                            if(empty($values['logo_path']))
                            {
                                $values['logo_path'] = "logos/default.png";
                            }
                            else 
                            {
                                $values['logo_path'] = "logos/".$values['logo_path'];
                            }
                            $this->data = compact("error", "values");
                            $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                        }
                    }
                    else 
                    { 
                        if(isset($_SESSION[__CLASS__.'settings_adminId']))
                        {
                            $admin = $_SESSION[__CLASS__.'settings_adminId'];
                        }
                        else 
                        {
                            $admin = session::getAdminId();
                        }
                        unset($_POST['update_logo']);
                        $_POST['submit'] = '';
                        $error['error'] = "You need to resize your graphic. The required dimensions are 420 w x 110 h.";
                        $this->model = $this->loadModel("admin");
                        $values['logo_path'] = $this->model->getLogoPath($admin);
                        if(empty($values['logo_path']))
                        {
                            $values['logo_path'] = "logos/default.png";
                        }
                        else 
                        {
                            $values['logo_path'] = "logos/".$values['logo_path'];
                        }
                        $this->data = compact("error", "values");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                    }
                }
                else 
                { 
                    if(isset($_SESSION[__CLASS__.'settings_adminId']))
                    {
                        $admin = $_SESSION[__CLASS__.'settings_adminId'];
                    }
                    else 
                    {
                        $admin = session::getAdminId();
                    }
                    unset($_POST['update_logo']);
                    $_POST['submit'] = '';
                    $error['error'] = "Something Went Wrong.";
                    $this->model = $this->loadModel("admin");
                    $values['logo_path'] = $this->model->getLogoPath($admin);
                    if(empty($values['logo_path']))
                    {
                        $values['logo_path'] = "logos/default.png";
                    }
                    else 
                    {
                        $values['logo_path'] = "logos/".$values['logo_path'];
                    }
                    $this->data = compact("error", "values");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                }
            }
            else 
            {
                $this->model = $this->loadModel("admin");
                $admins = $this->model->viewAdmins();
                $error = $this->errorMsgs;
                $csrf = csrf::generateCsrf(__FUNCTION__);
                $this->data = compact("admins", "office",  "csrf", "error", "values");
                $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
            }
        }
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login"); 
            die();
        }
    }
    //settings
//    
//    public function settings($action = '')
//    {
//        if(session::isLogin())
//        {
//            $admin_id = session::getAdminId();
//            $isForm = false;
//            if($action == "gracetime")
//            {
//                $isForm = TRUE;
//                $this->model = $this->loadModel("api");
//                if($this->model)
//                { 
//                    if($this->model->userExists($admin_id))
//                    {
//                        if(isset($_POST['submit']))
//                        {
//                            //office grace time
//                            if(isset($_POST['grace_time']) && !@empty($_POST['grace_time']))
//                            {
//                                if(is_numeric($_POST['grace_time']))
//                                {
//                                    $office_grace_time = $_POST['grace_time'];
//                                }
//                                else
//                                {
//                                    $this->isError = true;
//                                    $this->errorMsgs['grace_time_error'] = "office grace time is only in minutes";
//                                }
//                                $values['grace_time'] = securestr::clean($_POST['grace_time']);
//                            }
//                            else
//                            {
//                                $this->isError = true;
//                                $this->errorMsgs['grace_time_error'] = "Grace Time Is Required";
//                            }
//                            if(!$this->isError)
//                            {
//                               $this->model = $this->loadModel("access");
//                               $values = $this->model->gracetime($admin_id, $office_grace_time);
//                               $this->data = compact("values", "isForm", "action");
//                               $this->loadView(str_replace("Controller", "", __CLASS__),__FUNCTION__, $this->data);
//                            }
//                            else
//                            {
//                                $csrf = csrf::generateCsrf(__FUNCTION__);
//                                $error = $this->errorMsgs;
//                                $this->data = compact("error", "values", "csrf", "isForm", "action");
//                                $this->loadView(str_replace("Controller", "", __CLASS__),__FUNCTION__, $this->data);
//                            }
//                        }
//                        else
//                        {
//                            $isForm =TRUE;
//                            $this->data = compact("isForm", "action");
//                            $this->loadView(str_replace("Controller", "", __CLASS__),__FUNCTION__, $this->data);
//                        }
//                    }
//                    else
//                    {
//                        $this->isError = true;
//                        $this->errorMsgs = "Client Admin Does Not Exists";
//                    }
//                }
//            }
//            elseif($action == "logo")
//            {
//                if (isset($_POST['submit']))
//                {
//                    if(!$this->isError)
//                    {      
//                       $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
//                       $user = $this->model->idToUsername($admin_id);
//                       $values = $this->model->insertLogo($user, $new_image_name);
//                       $logo = $this->model->getlogo($admin_id);
//                       $this->data = compact("values", "isForm", "logo", "new_image_name", "action");
//                       $this->loadView(str_replace("Controller", "", __CLASS__),__FUNCTION__, $this->data);
//                    }
//                    else
//                    {
//                        $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
//                        $logo = $this->model->getlogo($admin_id);
//                        $csrf = csrf::generateCsrf(__FUNCTION__);
//                        $error = $this->errorMsgs;
//                        $this->data = compact("error", "values", "csrf", "isForm", "logo", "new_image_name", "action");
//                        $this->loadView(str_replace("Controller", "", __CLASS__),__FUNCTION__, $this->data);
//                    }
//                }
//                else 
//                {
//                    $isForm == TRUE;
//                    $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
//                    $logo = $this->model->getlogo($admin_id);
//                    $this->data = compact("isForm", "logo", "new_image_name", "action");
//                    $this->loadView(str_replace("Controller", "", __CLASS__),__FUNCTION__,  $this->data);
//                }
//            }
//        }
//        else
//        {
//            ob_get_clean();
//            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
//            die();
//        }
//    }
    public function devices($perform = '', $limit = 10, $offset = 0) 
    {
        if(session::isLogin())
        {
           if($perform == 'success' && isset($_SESSION[__CLASS__.'adminId']))
           {
               $_POST['submit'] = '';
               $_POST['admin'] = $_SESSION[__CLASS__.'adminId'];
           }
           if(!is_numeric($limit) || !is_numeric($offset) || $limit <= 0 || $offset < 0)
            {
                $limit = 10;
                $offset = 0;
            }
           
           
           if(isset($_POST['submit']))
            {
                //validate admin
                if(isset($_POST['admin']))
                {
                    if(is_numeric($_POST['admin']))
                    {
                        $this->model = $this->loadModel("admin");
                        if($this->model)
                        {
                            if($this->model->adminExists($_POST['admin'], _CLIENT_,TRUE))
                            {
                                $admin = $_POST['admin'];
                                session::sessionStart();
                                $_SESSION[__CLASS__.'adminId'] = $admin;
                            }
                            else
                            {
                               $this->isError = true;
                               $this->errorMsgs['admin_error'] = "Admin Does Not Exists.";
                            }
                        }                    
                        else
                        {
                           $this->isError = true;
                           $this->errorMsgs['admin_error'] = "Admin does Not Exists";
                        }
                        $values['admin'] = $_POST['admin'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['admin_error'] = "Invalid Admin Selected";
                    }
               }
               else
               {
                   $this->isError = true;
                   $this->errorMsgs['admin_error'] = "Admin Name Required";
               }
               
               if($this->isError)
               {
                   unset($_POST['submit']);
                   $adminModel = $this->loadModel("admin");
                    if($adminModel)
                    {
                        $admins = $adminModel->viewAdmins();
                    }
                   $error = $this->errorMsgs;
                   $csrf = csrf::generateCsrf(__FUNCTION__);
                   $this->data = compact("admins", "office",  "csrf", "error", "values");
                   $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
               }
               else
               {
                    $this->model = $this->loadModel("api_logs");
                    $uuids = $this->model->view($admin, TRUE, $offset, $limit);
                    $total = $this->model->countUUID($admin);
                    $next = $offset + $limit;
                    if($next > $total)
                    {
                        $next = $offset;
                    }
                    $previous = $offset - $limit;
                    if($previous < 0)
                    {
                        $previous = 0;
                    }
                    $current = ceil($offset / $limit);
                    $current++;
                    if($current == 0)
                    {
                        $current = 1;
                    }
                    $this->model = $this->loadModel("admin");
                    $username = $this->model->idToUsername($admin);
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("csrf","office", "uuids", "username","total", "next", "previous", "current", "limit", "offset");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                }
               
                $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,$this->data);
            }
            else
            {
               $this->javascripts[] = "{$this->config['domain']}"._PUBLIC_PATH_."js/jquery.validate.min.js";
                //loading countries model
                $adminModel = $this->loadModel("admin");
                $admins = [];

                if($adminModel)
                {
                    $admins = $adminModel->viewAdmins();
                }
                $error = $this->errorMsgs;
                $csrf = csrf::generateCsrf(__FUNCTION__);
                $this->data = compact("admins", "csrf", "error", "values");
                $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
            }
        }
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
            die();
        }
    }
}
