<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of api
 *
 * @author Umar
 */
class apiController extends controller 
{
    private $apiKey = "12345";
    private $data = [];
    private $isError = FALSE;
    private $errorMsgs = [];
    private $errorCodes = [];
    private $model;
    private $index = 0;
    
    public function __construct() 
    {
        $this->errorCodes = array(
            "authentication_failed" => "100",
            "session_not_started" => "101",
            "session_expired" => "102",
            "incomplete_parameters" => "103",
            "successfull" => "104",
            "unknown_error" => "105",
            "invalid_input" => "106"
        );
        $this->data['error'] = false;
        $this->data['session_initiated'] = false;
        $this->data['session_expiration'] = -1;
        $this->data['response'] = [];
    }
    
    public function index()
    {
        
    }
    
    public function initiate($key = '', $duration = 0)
    {
        session::sessionStart();
        if(!empty($key))
        {
            if($key == $this->apiKey)
            {
                if(!is_numeric($duration))
                    $duration = 0;

                $csrf = csrf::generateCsrf(__CLASS__, $duration);

                $this->data['error'] = false;
                $this->data['session_initiated'] = true;
                $this->data['session_expiration'] = csrf::expiration($csrf, __CLASS__);
                $this->data['response'][$this->index]['csrf'] = $csrf;
                $this->data['response'][$this->index++]['phpssid'] = session::getSession();
            }
            else
            {
                $this->isError = true;
                $this->data['error'] = true;
                $this->data['session_initiated'] = false;
                $this->data['response'][$this->index]['error_msg'] = "Invalid Api Key";
                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['authentication_failed'];
            }
        }
        else
        {
            $this->isError = true;
            $this->data['error'] = true;
            $this->data['session_initiated'] = false;
            $this->data['response'][$this->index]['error_msg'] = "Api Key Is Missing";
            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
        }
        //print after json encode
        echo json_encode($this->data);
        die();
    }

    public function login($phpSSId = '',$csrf = '', $user = '', $password = '')
    {
        if(!empty($phpSSId))
        {
            if(session::setSession($phpSSId))
            {
                //echo session::getSession();
                //print_r($_SESSION);
                //Setup basic indexes
                $this->data['session_initiated'] = true;
                
                if(!empty($user))
                {
                    if(ctype_alnum($user))
                    {
                        $username = $user;
                    }
                    else
                    {
                        $this->isError = true;
                        $this->data['response'][$this->index]['error_msg'] = "Invalid Username";
                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['invalid_input'];
                    }
                }
                else
                {
                    $this->isError = true;
                    $this->data['response'][$this->index]['error_msg'] = "Username is Required";
                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                }

                if(!empty($password))
                {
                    $password = password::encryptPassword($password);
                }
                else
                {
                    $this->isError = true;
                    $this->data['response'][$this->index]['error_msg'] = "Password is Required";
                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                }

                if(!empty($csrf))
                {
                    if(csrf::validateCsrf($csrf, __CLASS__))
                    {
                        $this->data['session_expiration'] = csrf::expiration($csrf, __CLASS__);
                    }
                    else
                    {
                        $this->isError = true;
                        $this->data['response'][$this->index]['error_msg'] = "Invalid or Expired CSRF Token";
                        $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['authentication_failed'];
                    }
                }
                else
                {
                    $this->isError = true;
                    $this->data['response'][$this->index]['error_msg'] = "CSRF Token is Required";
                    $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['incomplete_parameters'];
                }
            }
            else
            {
                $this->isError = true;
                $this->data['response'][$this->index]['error_msg'] = "Session ID is Required";
                $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['authentication_failed'];
            }
        }
        else
        {
            $this->isError = true;
            $this->data['response'][$this->index]['error_msg'] = "Session ID is Required";
            $this->data['response'][$this->index++]['error_code'] = $this->errorCodes['session_not_started'];
        }
        
        if(!$this->isError)
        {
            $this->model =  $this->loadModel("access");
            if($this->model->login($username , $password))
            {
                $this->model = $this->loadModel("logs");
                //$_SESSION["{$this->config['admin_session_prefix']}log_id"] = $this->model->insertLog($_SESSION["{$this->config['admin_session_prefix']}user_id"]);

                $this->data['response'][$this->index]['basic_info']['name'] = $_SESSION['otl_admin_name'];
                $this->data['response'][$this->index]['basic_info']['admin_id'] = $_SESSION['otl_admin_admin_id'];
                
                $country = $this->loadModel("countries");
                $this->data['response'][$this->index]['basic_info']['country'] = $country->idToName($_SESSION['otl_admin_country_id']);
            }
            else
            {
                $this->isError = true;
                $this->data['response'][$this->index]['error_msg'] = "Invalid Username or Password";
                $this->data['response'][$this->index]['error_code'] = $this->errorCodes['authentication_failed']; 
            }
        }
        
        $this->data['error'] = $this->isError;
        
        //print json string
        echo json_encode($this->data);
        die();
    }
    
}
