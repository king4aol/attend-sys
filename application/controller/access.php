<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of access
 *
 * @author Msajid
 */

class accessController extends controller {
    private $data = [];
    private $isError = FALSE;
    private $errorMsgs = [];
    private $model;
    private $passwordLenght = 8;
    private $enterpriseId = 3;
    
    public function register() 
    {
        if(session::isLogin())
        {
            $this->javascripts[] = "{$this->config['domain']}"._PUBLIC_PATH_."js/jquery.validate.min.js";
            //loading countries model
            $countriesModel = $this->loadModel("countries");
            $countries = [];

            if($countriesModel)
            {
                $countries = $countriesModel->loadCountries();
            }
            //Loading Membership Types
            $membershipModel = $this->loadModel("membership_types");
            $memberships = [];
            
            if($membershipModel)
            {
                $memberships = $membershipModel->loadTypes();
            }
            
            $csrf = csrf::generateCsrf(__FUNCTION__);

            $this->data = compact("countries", "memberships", "csrf");
            $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data); 
        }
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
            die();
        }
    }
    
    public function freeregister() 
    {
        if(!session::isLogin())
        {
            $this->javascripts[] = "{$this->config['domain']}"._PUBLIC_PATH_."js/jquery.validate.min.js";
            //loading countries model
            $countriesModel = $this->loadModel("countries");
            $countries = [];

            if($countriesModel)
            {
                $countries = $countriesModel->loadCountries();
            }
            $csrf = csrf::generateCsrf(__FUNCTION__);

            $this->data = compact("countries", "csrf");
            $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data); 
        }
        else
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."home/index");
            die();
        }
        
    }
    
    public function registered()
    {
        if(session::isLogin())
        {
            if(isset($_POST['fname']))
            {
                $country = "";
                $values = [];
                //required Fields
                //Validating CSRF Token
                if(isset($_POST['csrf']))
                {
                    if(!csrf::validateCsrf($_POST['csrf'], "register"))
                    {
                        $this->isError = true;;
                        $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                    }
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                }
                //Checking Username
                /*if(isset($_POST['user']) && !@empty($_POST['user']))
                {
                    if(ctype_alnum($_POST['user']))
                    {
                        $username = $_POST['user'];
                        $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));

                        if($this->model->userExists($username))
                        {
                            $this->isError = true;
                            $this->errorMsgs['user_error'] = "Username Already Exists";
                        }
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['user_error'] = "Invalid Username";
                    }

                    $values['user'] = securestr::clean($_POST['user']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['user_error'] = "Username is Required";
                }*/
                //Checking Email
                if(isset($_POST['email']) && !@empty($_POST['email']))
                {
                    if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
                    {
                        $this->model = $this->loadModel("email");
                        if(!$this->model->emailExists($_POST['email']))
                        {
                            $email = $_POST['email'];
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['email_error'] = "Email Already Exists";
                        }
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['email_error'] = "Invalid Email Address";
                    }
                    $values['email'] = securestr::clean($_POST['email']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['email_error'] = "Email is Required";
                }

                //Checking Country
                if(isset($_POST['country']) && !@empty($_POST['country']))
                {
                    if(ctype_alpha($_POST['country']))
                    {
                        $this->model = $this->loadModel("countries");
                        if($this->model->countryExists($_POST['country']))
                        {
                            $country = $this->model->isoToId($_POST['country']);
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['country_error'] = "Invalid Country Selected.";
                        }
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['country_error'] = "Invalid Country Selected";
                    }
                    $values['country'] = $_POST['country'];
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['country_error'] = "Country is Required";
                }

                //Validating Phone Number With Respect to Country
                if(isset($_POST['phone']) && !@empty($_POST['phone']))
                {
                    $this->model = $this->loadModel("contact_numbers");

                    if($this->model->validate($_POST['phone'], $country))
                    {
                        if(!$this->model->numberExists($_POST['phone']))
                        {
                            $phone = $_POST['phone'];
                        }
                        else
                        {
                           $this->isError = true;
                           $this->errorMsgs['phone_error'] = "Phone Number Already Exists"; 
                        }
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['phone_error'] = "Invalid Phone Number";
                    }
                    $values['phone'] = securestr::clean($_POST['phone']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['phone_error'] = "Phone Number is Required";
                }

                //Validating Password
                if(isset($_POST['pass']) && !@empty($_POST['pass']) && isset($_POST['confirmpass']) && !@empty($_POST['confirmpass'] ))
                {
                    if(strlen($_POST['pass']) >= password::$passwordLenght)
                    {
                        if($_POST['pass'] == $_POST['confirmpass'])
                        {
                            $password = $_POST['pass'];
                            $ecnryptedPassword= password::encryptPassword($password);
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['pass_error'] = "Password does not Match";
                        }
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['pass_error'] = "Minimum Length of password is 8!";
                    }
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['pass_error'] = "Password and Confirm Password is Required";
                }

                //Checking First Name
                if(isset($_POST['fname']) && !@empty($_POST['fname']))
                {
                    if(ctype_alpha($_POST['fname']))
                    {
                        $firstName = $_POST['fname'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['fname_error'] = "Name Can Only Consist of Alphabets";
                    }
                    $values['fname'] = $_POST['fname'];
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['fname_error'] = "Firstname is Required";
                }
                //Checking Last Name
                if(isset($_POST['lname']) && !@empty($_POST['lname']))
                {
                    if(ctype_alpha($_POST['lname']))
                    {
                        $lastName = $_POST['lname'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['lname_error'] = "Name Can Only Consist of Alphabets";
                    }
                    $values['lname'] = securestr::clean($_POST['lname']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['lname_error'] = "Lastname is Required";
                }
                
               /* //office grace time
                
                if(isset($_POST['grace_time']) && !@empty($_POST['grace_time']))
                {
                    if(is_numeric($_POST['grace_time']))
                    {
                        $office_grace_time = $_POST['grace_time'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['grace_time_error'] = "office grace time is only in minutes";
                    }
                    $values['grace_time'] = securestr::clean($_POST['grace_time']);
                }
                else
                {
                    $office_grace_time = '';
                }
                */
                
                //validate company name
               
                if(isset($_POST['company_name']) && !@empty($_POST['company_name']))
                {
                    if(preg_match("/^[ \w#-:,]+$/", $_POST['company_name']))
                    {
                        $company_name = $_POST['company_name'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['company_name_error'] = "Name Can Only have alphabets,numeric,-,_,#,:, ,";
                    }
                    $values['company_name'] = securestr::clean($_POST['company_name']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['company_name_error'] = "company name is required";
                }
                //validating Date of Birth
                if(isset($_POST['dob']) && !@empty($_POST['dob']))
                {
                    $_POST['dob'] = str_replace("/", "-", $_POST['dob']);
                    list($day, $month, $year) = explode("-",$_POST['dob']);
                    if(@checkdate($month, $day, $year))
                    {
                        $dob = $_POST['dob'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['dob_error'] = "Invalid Format of Date of Birth.";
                    }
                    $values['dob'] = securestr::clean($_POST['dob']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['dob_error'] = "Date of Birth is Required";
                }
                
                
                //Validate MemberShip Start Time
                
//                if(isset($_POST['memtype_start_time']) && !@empty($_POST['memtype_start_time']))
//                {
//                    $_POST['memtype_start_time'] = str_replace("/", "-", $_POST['memtype_start_time']);
//                    @list($year, $month, $day) = explode("-",$_POST['memtype_start_time']);
//                    if(@checkdate($month, $day, $year))
//                    {
//                        $memtype_start_time = $_POST['memtype_start_time'];
//                    }
//                    else
//                    {
//                        $this->isError = true;
//                        $this->errorMsgs['memtype_start_error'] = "Invalid Format of Date of Membership Start Date.";
//                    }
//                    $values['memtype_start_time'] = securestr::clean($_POST['memtype_start_time']);
//                }
//                else
//                {
//                    $memtype_start_time = 0;
//                }
//               
//                //validate Date Of MemberShip End Time
//                
//                if(isset($_POST['memtype_end_time']) && !@empty($_POST['memtype_end_time']))
//                {
//                    $_POST['memtype_end_time'] = str_replace("/", "-", $_POST['memtype_end_time']);
//                    @list($year, $month, $day) = explode("-",$_POST['memtype_end_time']);
//                    if(@checkdate($month, $day, $year))
//                    {
//                        $memtype_end_time = $_POST['memtype_end_time'];
//                    }
//                    else
//                    {
//                        $this->isError = true;
//                        $this->errorMsgs['memtype_end_error'] = "Invalid Format of Date of Membership End Date.";
//                    }
//                    $values['memtype_end_time'] = securestr::clean($_POST['memtype_end_time']);
//                }
//                else
//                {
//                    
//                    $memtype_end_time = 0;
//                }
                $memtype_start_time = date("Y-m-d");
                $memtype_end_time = date("Y-m-d");
                //Validating and Cleaning Address
                if(isset($_POST['address']) && !@empty($_POST['address']))
                {
                    $address = securestr::clean($_POST['address']);
                    $values['address'] = $address;
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['address_error'] = "Address is Required";
                }
                //Getting Admin Type
                if(isset($_POST['admintype']) && !@empty($_POST['admintype']))
                {
                    if($_POST['admintype'] == "admin" || $_POST['admintype'] == "client")
                    {
                        if($_POST['admintype'] == "admin")
                        {
                            $adminType = _ADMIN_;
                            $memType = $this->enterpriseId;
                            $bitMask = roles::getInstance()->adminMask();
                        }
                        else
                        {
                            $adminType = _CLIENT_;
                            $bitMask = roles::getInstance()->clientMask();
                        }
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['admintype_error'] = "Invalid Admin Type.";
                    }
                    $values['admintype'] = securestr::clean($_POST['admintype']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['admintype_error'] = "Admin Type is Required.";
                }
                //Checking Membership Type
                if(isset($_POST['memtype']) && !@empty($_POST['memtype']))
                    {
                        if(is_numeric($_POST['memtype']))
                        {
                            $this->model = $this->loadModel("membership_types");
                            if($this->model->membershipExists($_POST['memtype']))
                            {
                                $memType = $_POST['memtype'];
                            }
                            else
                            {
                                $this->isError = true;
                                $this->errorMsgs['memtype_error'] = "Invalid Membership Selected";
                            }
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['memtype_error'] = "Invalid Membership Selected";
                        }
                        $values['memtype'] = $_POST['memtype'];
                    }
                    else if(!isset($memType))
                    {
                        $this->isError = true;
                        $this->errorMsgs['memtype_error'] = "Membership is Required";
                    }
                    
                $date1 = strtotime($memtype_start_time);
                $date2 = strtotime($memtype_end_time);
                if($date2 < $date1)
                {
                    $this->isError = TRUE;
                    $this->errorMsgs['error'] = "End Time Is Shorter Then Start Time.";
                }
                
                //Inserting Data
                if(!$this->isError)
                {
                    //Inserting Admin
                    $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                    //Start Transaction
                    $this->model->db->transaction();
                    $adminId = $this->model->insertAdmin($email, $ecnryptedPassword, $firstName, $lastName, $company_name,  $dob, $country, $adminType, $memType, $memtype_start_time, $memtype_end_time, $bitMask);
                    //Inserting Into Users
                    $this->model = $this->loadModel("users");
                    $userId = $this->model->insertUsers($adminId, _ADMIN_);
                    //Inserting Email Address
                    $this->model = $this->loadModel("email");
                    $this->model->insertEmail($userId, $email);
                    //Inserting Phone Number
                    $this->model = $this->loadModel("contact_numbers");
                    $this->model->insertNumber($userId, $phone);
                    //Inserting Address
                    $this->model = $this->loadModel("addresses");
                    $this->model->insertAddress($userId, $address);
                    //Commiting Transaction
                    if($this->model->db->commit())
                    {
                        $this->isError = FALSE;
                        $res['response'] = "Company created successfully";
                        $_POST['success'] = $res['response'];
                        $controller = $this->loadController("admin");
                        $controller->view();
                        die();
                    }
                    else
                    {
                        $this->isError = TRUE;
                        $this->errorMsgs['error'] = "Something Went Wrong. Please Try Again.";
                    }
                    $this->javascripts[] = "{$this->config['domain']}"._PUBLIC_PATH_."js/jquery.validate.min.js";
                    //loading countries model
                    $countriesModel = $this->loadModel("countries");
                    $countries = [];

                    if($countriesModel)
                    {
                        $countries = $countriesModel->loadCountries();
                    }
                    //Loading Membership Types
                    $membershipModel = $this->loadModel("membership_types");
                    $memberships = [];

                    if($membershipModel)
                    {
                        $memberships = $membershipModel->loadTypes();
                    }
            
                    $csrf = csrf::generateCsrf("register");
                    $error = $this->errorMsgs;
                    if($this->isError)
                    {
                        $this->data = compact("error", "values", "countries", "memberships", "csrf");
                    }
                    else 
                    {
                        $this->data = compact("error", "countries", "memberships", "csrf", "res");
                    }
                    $this->loadView(str_replace("Controller", "", __CLASS__), "register", $this->data);
                }
                else
                {
                    $this->javascripts[] = "{$this->config['domain']}"._PUBLIC_PATH_."js/jquery.validate.min.js";
                    //loading countries model
                    $countriesModel = $this->loadModel("countries");
                    $countries = [];

                    if($countriesModel)
                    {
                        $countries = $countriesModel->loadCountries();
                    }
                    //Loading Membership Types
                    $membershipModel = $this->loadModel("membership_types");
                    $memberships = [];

                    if($membershipModel)
                    {
                        $memberships = $membershipModel->loadTypes();
                    }
            
                    $csrf = csrf::generateCsrf("register");
                    $error = $this->errorMsgs;
                    $this->data = compact("error", "values", "countries", "memberships", "csrf");
                    $this->loadView(str_replace("Controller", "", __CLASS__), "register", $this->data);
                }
            }
            else
            {
                ob_get_clean();
                header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/register");
                die();
            }
        }
        else
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
            die();
        }
    }
    public function login($action = '')
    {
        if(!session::isLogin())
        {
            $values = [];
            if(isset($_POST['user']) && isset($_POST['pass']) && isset($_POST['csrf']))
            {
                if(!empty($_POST['user']))
                {
                    $username = securestr::clean($_POST['user']);
                    $values['user'] = $username;
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['user_error'] = "Please Enter Username";
                }
                if(!empty($_POST['pass']))
                {
                    $password = password::encryptPassword($_POST['pass']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['pass_error'] = "Please Enter Password";
                }
//                if(!empty($_POST['csrf']))
//                {
//                    if(!csrf::validateCsrf($_POST['csrf'], __FUNCTION__))
//                    {
//                        $this->isError = TRUE;
//                        $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token.";
//                    }
//                }
//                else
//                {
//                    $this->isError = TRUE;
//                    $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token.";
//                }
                
                if(!$this->isError)
                {  
                    $this->model =  $this->loadModel(str_replace("Controller", "", __CLASS__));
                    if($this->model->login($username , $password))
                    { 
                        $this->model = $this->loadModel("logs");
                        session::setLogId($this->model->insertLog(session::getAdminId()));
                        ob_get_clean();
                        header("Location: {$this->config['domain']}"._PUBLIC_PATH_."home/index");
                        die();
                    }
                    else
                    {
                        $this->errorMsgs['error'] = "Username or Password is Incorrect.";
                        $error = $this->errorMsgs;
                        $this->data = compact("error");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
                    }
                }
                else
                {
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $error = $this->errorMsgs;
                    $this->data = compact("error","values", "csrf");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
                }
            }
            else
            {
                $csrf = csrf::generateCsrf(__FUNCTION__);
                $this->data = compact("csrf", "action");
                $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
            }
        }
        else
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."home/index");
            die();
        }
    }
    //Logout
    public function logout() 
    {
        if(session::isLogin())
        {
            $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__), __FUNCTION__);
            $logId = $this->model->logout();
            $this->model = $this->loadModel("logs");
            $this->model->expireLog($logId);
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login/logout");
            die();
        }
        else
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
            die();
        }
    }
    //index
    public function index() 
    {
        ob_get_clean();
        header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
        die();
    }
    
    public function freeregistered()
    {
        if(!session::isLogin())
        {
            if(isset($_POST['fname']))
            {
                $country = "";
                $values = [];
                //required Fields
                //Validating CSRF Token
                if(isset($_POST['csrf']))
                {
                    if(!csrf::validateCsrf($_POST['csrf'], "register"))
                    {
                        $this->errorMsgs;
                        $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                    }
                }
                else
                {
                    $this->errorMsgs;
                    $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                }
                //Checking Username
                if(isset($_POST['user']) && !@empty($_POST['user']))
                {
                    if(preg_match("/^[\w-$@._]+$/", $_POST['user']))
                    {
                        $username = $_POST['user'];
                        $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));

                        if($this->model->userExists($username))
                        {
                            $this->isError = true;
                            $this->errorMsgs['user_error'] = "Username Already Exists";
                        }
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['user_error'] = "Invalid Username";
                    }

                    $values['user'] = securestr::clean($_POST['user']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['user_error'] = "Username is Required";
                }
                //Checking Email
                if(isset($_POST['email']) && !@empty($_POST['email']))
                {
                    if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
                    {
                        $this->model = $this->loadModel("email");
                        if(!$this->model->emailExists($_POST['email']))
                        {
                            $email = $_POST['email'];
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['email_error'] = "Email Already Exists";
                        }
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['email_error'] = "Invalid Email Address";
                    }
                    $values['email'] = securestr::clean($_POST['email']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['email_error'] = "Email is Required";
                }

                //Checking Country
                if(isset($_POST['country']) && !@empty($_POST['country']))
                {
                    if(ctype_alpha($_POST['country']))
                    {
                        $this->model = $this->loadModel("countries");
                        if($this->model->countryExists($_POST['country']))
                        {
                            $country = $this->model->isoToId($_POST['country']);
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['country_error'] = "Invalid Country Selected.";
                        }
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['country_error'] = "Invalid Country Selected";
                    }
                    $values['country'] = $_POST['country'];
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['country_error'] = "Country is Required";
                }

                //Validating Phone Number With Respect to Country
                if(isset($_POST['phone']) && !@empty($_POST['phone']))
                {
                    $this->model = $this->loadModel("contact_numbers");

                    if($this->model->validate($_POST['phone'], $country))
                    {
                        if(!$this->model->numberExists($_POST['phone']))
                        {
                            $phone = $_POST['phone'];
                        }
                        else
                        {
                           $this->isError = true;
                           $this->errorMsgs['phone_error'] = "Phone Number Already Exists"; 
                        }
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['phone_error'] = "Invalid Phone Number";
                    }
                    $values['phone'] = securestr::clean($_POST['phone']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['phone_error'] = "Phone Number is Required";
                }

                //Validating Password
                if(isset($_POST['pass']) && !@empty($_POST['pass']) && isset($_POST['confirmpass']) && !@empty($_POST['confirmpass'] ))
                {
                    if(strlen($_POST['pass']) >= $this->passwordLenght)
                    {
                        if($_POST['pass'] == $_POST['confirmpass'])
                        {
                            $password = $_POST['pass'];
                            $ecnryptedPassword= password::encryptPassword($password);
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['pass_error'] = "Password does not Match";
                        }
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['pass_error'] = "Minimum Length of password is 8!";
                    }
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['pass_error'] = "Password and Confirm Password is Required";
                }

                //Checking First Name
                if(isset($_POST['fname']) && !@empty($_POST['fname']))
                {
                    if(ctype_alpha($_POST['fname']))
                    {
                        $firstName = $_POST['fname'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['fname_error'] = "Name Can Only Consist of Alphabets";
                    }
                    $values['fname'] = $_POST['fname'];
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['fname_error'] = "Firstname is Required";
                }
                //Checking Last Name
                if(isset($_POST['lname']) && !@empty($_POST['lname']))
                {
                    if(ctype_alpha($_POST['lname']))
                    {
                        $lastName = $_POST['lname'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['lname_error'] = "Name Can Only Consist of Alphabets";
                    }
                    $values['lname'] = securestr::clean($_POST['lname']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['lname_error'] = "Lastname is Required";
                }
                
                 
                /*//office grace time
                
                if(isset($_POST['grace_time']) && !@empty($_POST['grace_time']))
                {
                    if(is_numeric($_POST['grace_time']))
                    {
                        $office_grace_time = $_POST['grace_time'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['grace_time_error'] = "office grace time is only in minutes";
                    }
                    $values['grace_time'] = securestr::clean($_POST['grace_time']);
                }
                else
                {
                    $office_grace_time = '';
                }
                */
                
                //validating Date of Birth
                if(isset($_POST['dob']) && !@empty($_POST['dob']))
                {
                    $_POST['dob'] = str_replace("/", "-", $_POST['dob']);
                    list($day, $month, $year) = explode("-",$_POST['dob']);
                    if(checkdate($month, $day, $year))
                    {
                        $dob = $_POST['dob'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['dob_error'] = "Date of Birth is Required";
                    }
                    $values['dob'] = securestr::clean(str_replace("-", "/", $_POST['dob']));
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['dob_error'] = "Date of Birth is Required";
                }

                //Validating and Cleaning Address
                if(isset($_POST['address']) && !@empty($_POST['address']))
                {
                    $address = securestr::clean($_POST['address']);
                    $values['address'] = $address;
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['address_error'] = "Address is Required";
                }
                
                
                //Inserting Data
                if(!$this->isError)
                {
                    //setting admin Type
                    $adminType = _CLIENT_;
                    $bitMask = roles::getInstance()->clientMask();
                    $companyName = "Basic User";
                    $membershipStart = 0;
                    $membershipEnd = 0;
                    //setting Membership Type
                    $this->model = $this->loadModel("membership_types");
                    $memType = $this->model->getTypeId("Free/Basic");
                    //Inserting Admin
                    $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                    //Start Transaction
                    $this->model->db->transaction();
                    $adminId = $this->model->insertAdmin($username, $ecnryptedPassword, $firstName, $lastName, $companyName, $dob ,$country, $adminType, $memType, $membershipStart, $membershipEnd, $bitMask);
                    //Inserting Into Users
                    $this->model = $this->loadModel("users");
                    $userId = $this->model->insertUsers($adminId, _ADMIN_);
                    //Inserting Email Address
                    $this->model = $this->loadModel("email");
                    $this->model->insertEmail($userId, $email);
                    //Inserting Phone Number
                    $this->model = $this->loadModel("contact_numbers");
                    $this->model->insertNumber($userId, $phone);
                    //Inserting Address
                    $this->model = $this->loadModel("addresses");
                    $this->model->insertAddress($userId, $address);
                    /*
                    //Insert Office
                    $this->model = $this->loadModel("office");
                    $officeId = $this->model->insertOffice($adminId, $username);
                    //insert into users for office
                    $this->model = $this->loadModel("users");
                    $oUserId = $this->model->insertUsers($officeId, _OFFICE_);
                    //insert into addresses for office
                    $this->model = $this->loadModel("addresses");
                    $this->model->insertAddress($oUserId, $username);
                    
                    */
                    //Inserting Employee
                    //$this->model = $this->loadModel("employee");
                    //$this->model->insertEmployee($adminId, $firstName, $lastName, 0, 0, 0, 0,  0);
                    //Commiting Transaction
                    if($this->model->db->commit())
                    {
                        $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                        if($this->model->login($username, $ecnryptedPassword))
                        {
                            $this->model = $this->loadModel("logs");
                            session::setLogId($this->model->insertLog(session::getAdminId()));
                            ob_get_clean();
                            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."admin/api");
                            die();
                        }
                        else 
                        {
							$res['response'] = "User Registered.";
                            $this->data = compact("res");
                            $this->loadView(str_replace("Controller", "", __CLASS__), "freeregister", $this->data);
                            
                        }
                    }
                    else
                    {
                        echo "Registeration Failed.".$this->model->db->error." -> ".$this->model->db->failed;
						$this->errorMsgs['error'] = "Registeration Failed.";
						$error = $this->errorMsgs;
						$this->data = compact("error");
						$this->loadView(str_replace("Controller", "", __CLASS__), "freeregister", $this->data);
                    }
                }
                else
                {
                    $this->javascripts[] = "{$this->config['domain']}"._PUBLIC_PATH_."js/jquery.validate.min.js";
                    //loading countries model
                    $countriesModel = $this->loadModel("countries");
                    $countries = [];

                    if($countriesModel)
                    {
                        $countries = $countriesModel->loadCountries();
                    }
                    $csrf = csrf::generateCsrf("register");
                    $error = $this->errorMsgs;
                    $this->data = compact("error", "values", "countries", "csrf");
                    $this->loadView(str_replace("Controller", "", __CLASS__), "freeregister", $this->data);
                }
            }
            else
            {
                ob_get_clean();
                header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/freeregister");
                die();
            }
        }
        else
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."home/index");
            die();
        }
    }
    
    //forget password
    public function forgetpassword($type = '', $username = '', $hash = '') 
    {
        if(!session::isLogin())
        {
            if($type == "reset")
            {
                if(!empty($username) && strlen($hash) == 32)
                {
                    if(preg_match("/^[\w-$@._]+$/", $username) && ctype_alnum($hash))
                    {
                        $this->model = $this->loadModel("access");
                        if($this->model->matchHash($username, $hash, time()))
                        {
                            session::sessionStart();
                            $_SESSION[__CLASS__.__FUNCTION__."Username"] = $username;
                            $_SESSION[__CLASS__.__FUNCTION__."Hash"] = $hash;
                        }
                        else 
                        {
                            $this->isError = TRUE;
                            $this->errorMsgs['error'] = "Invalid Username or Token";
                        }
                    }
                    else 
                    {
                        $this->isError = TRUE;
                        $this->errorMsgs['error'] = "Something Went Wrong";
                    }
                }
                else 
                {
                    $this->isError = TRUE;
                }
                
                if(!$this->isError)
                {
                    $this->data = compact("type");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                }
                else 
                {
                    $type = '';
                    $error = $this->errorMsgs;
                    $this->data = compact("type", "error");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                }
            }
            else 
            {
                if(isset($_POST['submit']))
                {
                    if(isset($_POST['username']))
                    {
                        if(!empty($_POST['username']))
                        {
                            if(preg_match("/^[\w-$@._]+$/", $_POST['username']))
                            {
                                $username = $_POST['username'];
                                $this->model = $this->loadModel("admin");
                                if(!$this->model->usernameToId($username))
                                {
                                    $this->isError = TRUE;
                                    $this->errorMsgs['error'] = "Username Does Not Exists";
                                }
                            }
                            else 
                            {
                                $this->isError = TRUE;
                                $this->errorMsgs['error'] = "Invalid Username";
                                $values['username'] = securestr::clean($_POST['username']);
                            }
                        }
                        else 
                        {
                            $this->isError = TRUE;
                            $this->errorMsgs['error'] = "Username Is Required";
                        }
                    }
                    else 
                    {
                        $this->isError = TRUE;
                        $this->errorMsgs['error'] = "Username Is Required";
                    }
                    
                    if($this->isError)
                    {
                        $error = $this->errorMsgs;
                        $this->data = compact("type", "error", "values");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                    }
                    else 
                    { 
                        $this->model = $this->loadModel("admin");
                        $details = $this->model->loadDetails($username);
                        $detail = $details[0];
                        
                        $newHash = password::Hash();
                        
                        $this->model = $this->loadModel("access");
                        if($this->model->insertHash($username, $newHash, time()+password::$hashExpiry))
                        {
                            sendmail::send()->forgetPassword($detail['email_address'], $detail['first_name'], $username, $newHash);
//                            $mail = new PHPMailer\PHPMailer;    
//                            $mail->fromAccounts(); //Setting From Email
//                            $mail->addAddress($detail['email_address']);
//                            $mail->isHTML(true);   
//                            
//                            $mail->Subject = 'OTL Password Recovery';
//                            $mail->Body    = "<b>Dear {$detail['first_name']},</b><br>"
//                            . "<p>Please Follow This Url To Reset Your Password<br>"
//                            . "<a href='{$this->config['domain']}"._PUBLIC_PATH_."access/forgetpassword/reset/{$username}/{$newHash}'>"
//                            . "{$this->config['domain']}"._PUBLIC_PATH_."access/forgetpassword/reset/{$username}/{$newHash}</a></p>";
                            
                            if(sendmail::send()->forgetPassword($detail['email_address'], $detail['first_name'], $username, $newHash))
                            {
                                $res['response'] = "Please Check Your Inbox";
                            }
                            else
                            {
                                $error['error'] = "Something Went Wrong. Please Try Again.";
                            }
                        }
                        else 
                        {
                            $error['error'] = "Something Went Wrong. Please Try Again";
                        }
                        
                        $this->data = compact("res", "type", "error");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                    }
                } 
                elseif(isset($_POST['p_submit']))
                {
                    //Validating Password
                    if(isset($_POST['password']) && !@empty($_POST['password']) && isset($_POST['c_password']) && !@empty($_POST['c_password'] ))
                    {
                        if(strlen($_POST['password']) >= $this->passwordLenght)
                        {
                            if($_POST['password'] == $_POST['c_password'])
                            {
                                $password = $_POST['password'];
                                $ecnryptedPassword= password::encryptPassword($password);
                            }
                            else
                            {
                                $this->isError = true;
                                $this->errorMsgs['error'] = "Password does not Match";
                            }
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['error'] = "Minimum Length of password is 8!";
                        }
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['error'] = "Password and Confirm Password is Required";
                    }
                    
                    //Getting Username & Hash
                    if(isset($_SESSION[__CLASS__.__FUNCTION__."Username"]) && isset($_SESSION[__CLASS__.__FUNCTION__."Hash"]))
                    {
                        $username = $_SESSION[__CLASS__.__FUNCTION__."Username"];
                        $hash = $_SESSION[__CLASS__.__FUNCTION__."Hash"];
                    }
                    else 
                    {
                        $this->isError = true;
                        $this->errorMsgs['error'] = "Something Went Wrong";
                    }
                    
                    if($this->isError)
                    {
                        $error = $this->errorMsgs;
                        $type = "reset";
                        $this->data = compact("type", "error");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data); 
                    }
                    else 
                    {
                        $this->model = $this->loadModel("admin");
                        if($this->model->updatePassword($username, $ecnryptedPassword))
                        {
                            $res['response'] = "Password Updated Successfully";
                            session::sessionDestroy();
                        }
                        else 
                        {
                            $error['error'] = "Something Went Wrong. Please Try Again";
                        }
                        
                        $this->data = compact("type", "error", "res");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                    }
                }
                else 
                {
                    $this->data = compact("type");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                }
            }
        }
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."home/index");
            die();
        }
    }
}
