<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of office
 *
 * @author Msajid
 */
class departmentController extends controller
{
   private $requiredBit = 0;
   private $isError = false;
   private $errorMsgs;
   private $model;
   
   public function index()
   {
       
   }
   
   public function add()
   {
       if(session::isLogin())
        {
           if(isset($_POST['adminsubmit']) && !isset($_POST['office']))
            {
               //checking admin
               if(is_numeric($_POST['admin']))
                {
                    $adminModel = $this->loadModel("admin");
                    if($adminModel)
                    {
                        if($adminModel->adminExists($_POST['admin'], _CLIENT_, TRUE))
                        {
                            $admins = $_POST['admin'];
                            $_SESSION[__CLASS__.__FUNCTION__] = $admins;
                        }
                        else
                        {
                           $this->isError = true;
                           $this->errorMsgs['client_error'] = "Client Does Not Exists.";
                        }
                    }                    
                    else
                    {
                       $this->isError = true;
                       $this->errorMsgs['client_error'] = "Client does Not Exists";
                    }
                    $values['admin'] = $_POST['admin'];
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['client_error'] = "Invalid Client Selected";
                }
                if($this->isError)
                {
                    unset($_POST['adminsubmit']);
                    $adminModel = $this->loadModel("admin");
                    if($adminModel)
                    {
                        $admins = $adminModel->viewAdmins();
                    }
                    $error = $this->errorMsgs;
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("admins", "csrf", "error", "values");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
               }
               else
               {
                   $csrf = csrf::generateCsrf(__FUNCTION__);
                   $this->data = compact("csrf", "values");
                   $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
               }
                
            } 
            elseif(isset($_POST['dept']))
            {
                if(isset($_POST['csrf']))
                {
                    if(!csrf::validateCsrf($_POST['csrf'], __FUNCTION__))
                    {
                        $this->errorMsgs;
                        $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                    }
                }
                else
                {
                    $this->errorMsgs;
                    $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                }
                //validating office name
                if(isset($_POST['name']) && !@empty($_POST['name']))
                {
                    $deptName = $values['name'] = securestr::clean($_POST['name']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['name_error'] = "Department name is required";
                }
                
                //validatin address
                if(isset($_POST['description']) && !@empty($_POST['description']))
                {
                    $description = $values['description'] = securestr::clean($_POST['description']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['address_error'] = "Description is required";
                }
                
                
                if(!$this->isError)
                {
                    if(isset($_SESSION[__CLASS__.__FUNCTION__]))
                    {
                        $admin_id = $_SESSION[__CLASS__.__FUNCTION__];
                    }
                    else 
                    {
                        $admin_id = session::getAdminId();
                    }
                }
                else 
                {
                    $_POST['adminsubmit'] = '';
                    unset($_POST['dept']);
                    $this->errorMsgs['error'] = "Something Went Wrong";
                    $error = $this->errorMsgs;
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("admins", "csrf", "error", "values" ,"res");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
                    die();
                }
                if(!$this->isError)
                {
                    //insert into office
                    if(isset($_SESSION[__CLASS__.__FUNCTION__]))
                    {
                        $admin_id = $_SESSION[__CLASS__.__FUNCTION__];
                    }
                    else 
                    {
                        $admin_id = session::getAdminId();
                    }
                    $this->model = $this->loadModel("department");
                    $this->model->db->transaction();
                    $deptId = $this->model->insert($admin_id, $deptName, $description);
                    //insert into users
                    $this->model = $this->loadModel("users");
                    $userId = $this->model->insertUsers($deptId, _DEPARTMENT_);
                    
                    if($this->model->db->commit())
                    {
                        $_POST['adminsubmit'] = '';
                        unset($_POST['dept']);
                        $res['response'] = "Department Added Successfully.";
                        $error = $this->errorMsgs;
                        $csrf = csrf::generateCsrf(__FUNCTION__);
                        $this->data = compact("admins", "csrf", "error", "values" ,"res");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
                    }
                    else
                    {
                        $_POST['adminsubmit'] = '';
                        unset($_POST['dept']);
                        $this->errorMsgs['error'] = "Something Went Wrong".$this->model->db->error;
                        $error = $this->errorMsgs;
                        $csrf = csrf::generateCsrf(__FUNCTION__);
                        $this->data = compact("admins", "csrf", "error", "values" ,"res");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
					}
                }
                else
                {
                    unset($_POST['dept']);
                    $_POST['adminsubmit'] = '';
                    $this->javascripts[] = "{$this->config['domain']}"._PUBLIC_PATH_."js/jquery.validate.min.js";
                    //loading countries model
                    $adminModel = $this->loadModel("admin");
                    $admins = [];
                    if($adminModel)
                    {
                        $admins = $adminModel->viewAdmins();
                    }
                    $error = $this->errorMsgs;
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("admins", "csrf", "error", "values", "res");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data); 
                }
            }
            else
            {
                $this->javascripts[] = "{$this->config['domain']}"._PUBLIC_PATH_."js/jquery.validate.min.js";
                //loading admin model
                $adminModel = $this->loadModel("admin");
                $admins = [];

                if($adminModel)
                {
                    $admins = $adminModel->viewAdmins();
                }
                $csrf = csrf::generateCsrf(__FUNCTION__);
                $this->data = compact("admins", "csrf", "error", "values", "res");
                $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data); 
            }
        }
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
            die();
        }
   }
   
   public function view($perform = '', $limit = 10, $offset = 0)
   {
        if(session::isLogin())
        {
            if(!is_numeric($limit) || !is_numeric($offset) || $limit <= 0 || $offset < 0)
            {
                $limit = 10;
                $offset = 0;
            }
            if($perform == 'success' || $perform == 'load' && isset($_SESSION[__CLASS__.'adminUser']))
            {
                $_POST['submit'] = '';
                $_POST['client'] = $_SESSION[__CLASS__.'adminUser'];
            }
            if(!isset($_POST['submit']))
            {
                $this->javascripts[] = "{$this->config['domain']}"._PUBLIC_PATH_."js/jquery.validate.min.js";
                //loading countries model
                $adminModel = $this->loadModel("admin");
                $admins = [];

                if($adminModel)
                {
                    $admins = $adminModel->viewAdmins();
                }
                $error = $this->errorMsgs;
                $this->data = compact("admins", "error", "values");
                $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data); 
            }
            else
            {
                if(isset($_POST['client']) && !@empty($_POST['client']))
                {
                    $user = $_POST['client'];
                    if(preg_match("/^[\w-$@._]+$/", $user))
                    {
                        $_SESSION[__CLASS__.'adminUser'] = $_POST['client'];
                        $this->model = $this->loadModel("admin");
                        $adminId = $this->model->usernameToId($user);
                        session::sessionStart();
                        $_SESSION[__CLASS__.'adminId'] = $adminId;
                        if($this->model)
                        {
                            if($this->model->adminExists($user, _CLIENT_))
                            {
                                $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                                $departments = $this->model->view($user, TRUE, $offset, $limit);
                                $total = $this->model->countDept($user);
                                $next = $offset + $limit;
                                if($next > $total)
                                {
                                    $next = $offset;
                                }
                                $previous = $offset - $limit;
                                if($previous < 0)
                                {
                                    $previous = 0;
                                }
                                $current = ceil($offset / $limit);
                                $current++;
                                if($current == 0)
                                {
                                    $current = 1;
                                }
								
                                $error = $this->errorMsgs;
								
                                $this->data = compact("departments", "error", "limit", "offset", "total", "next", "previous", "current");

                                $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                            }
                            else
                            {
                                $controller = $this->loadController("error");
                                if($controller)
                                {
                                    $controller->_custom("Some thing went wrong.", "Please Check The Spellings.");
                                }
                            }
                        }
                        else
                        {
                            $controller = $this->loadController("error");
                            if($controller)
                            {
                                $controller->_custom("Some thing went wrong.", "Please Check The Spellings.");
                            }
                        }
                    }
                    else
                    {
                        $controller = $this->loadController("error");
                        if($controller)
                        {
                            $controller->_custom("Some thing went wrong.", "Please Check The Spellings.");
                        }
                    }
                }
                else
                {
                    $controller = $this->loadController("error");
                    if($controller)
                    {
                        $controller->_custom("Some thing went wrong.", "Please Check The Spellings.");
                    }
                }
                
            }
        }
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login"); 
            die();
        }
   }
   //
    public function action($depId = '', $action = '')
    {
        if(session::isLogin())
        {
            $action = strtolower($action);
            if($action == "delete")
            {
                $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                
                if($this->model)
                {
                    //if($this->model->adminExists($oofice, _OFFICE_))
                    //{
                    if($this->model->adminToDeptRel(session::getAdminId(), $depId))
                    {
                        $performed = $this->model->actions($depId ,$action);
                        if($performed)
                        {
                            ob_get_clean();
                            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."department/view/success"); 
                            die();
                        }
                        else
                        {
                            $controller = $this->loadController("error");
                            if($controller)
                            {
                                $controller->_custom("Something Went Wrong.", "Something Went Wrong. Please Try Again Later.");
                            }
                        }
                    }
                    else
                    {
                        $controller = $this->loadController("error");
                            if($controller)
                            {
                                $controller->_custom("Something Went Wrong.", "Something Went Wrong. Please Try Again Later.");
                            }
                    }
                    /*}
                    else
                    {
                        $controller = $this->loadController("error");
                        if($controller)
                        {
                            $controller->_custom("Office Does Not Exists.", "Office Does Not Exists. Please Check The Spellings.");
                        }
                    }*/
                }
            }
        }
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login"); 
            die();
        }
    }
    
    //edit offices
    
    public function edit($dept = '')
    {
        if(session::isLogin())
        {
            if(isset($_POST['submit']))
            {
                if(isset($_POST['csrf']))
                {
                    if(!csrf::validateCsrf($_POST['csrf'], __FUNCTION__))
                    {
                        $this->errorMsgs;
                        $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                    }
                }
                else
                {
                    $this->errorMsgs;
                    $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                }
                //validating office name
                if(isset($_POST['name']) && !@empty($_POST['name']))
                {
                    $deptName = $values['name'] = securestr::clean($_POST['name']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['name_error'] = "Department name is required";
                }
                
                //validatin address
                if(isset($_POST['description']) && !@empty($_POST['description']))
                {
                    $description = $values['description'] = securestr::clean($_POST['description']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['description_error'] = "Description is required";
                }
               
                if(!$this->isError)
                {
                    $this->model = $this->loadModel(str_replace("Controller", "",__CLASS__));
                    if($this->model->adminToDeptRel(session::getAdminId(), $dept))
                    {
                        $departments = $this->model->update($dept, $deptName, $description);
                        $departments = $departments[0];
                        $csrf = csrf::generateCsrf(__FUNCTION__);
                        $res['response'] = "Office Updated Successfully.";
                        $this->data = compact("departments", "dept", "values", "csrf", "res");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
                    }
                    else 
                    {
                        $controller = $this->loadController("error");
                        if($controller)
                        {
                            $controller->_custom("Something Went Wrong.", "Something Went Wrong. Please Try Again Later.");
                        }
                    }
                }
                else
                {
                    $this->model = $this->loadModel(str_replace("Controller", "",__CLASS__));
                    if($this->model->adminToDeptRel(session::getAdminId(), $dept))
                    {
                        $departments = $this->model->getDept($dept);                    
                        $departments = $departments[0];
                        $csrf = csrf::generateCsrf(__FUNCTION__);
                        $this->errorMsgs['error'] = "Something went wrong.";
                        $error = $this->errorMsgs;
                        $this->data = compact("departments", "dept", "values" , "error", "csrf");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
                    }
                    else 
                    {
                        $controller = $this->loadController("error");
                        if($controller)
                        {
                            $controller->_custom("Something Went Wrong.", "Something Went Wrong. Please Try Again Later.");
                        }
                    }
                }
            }
            else
            {
                $this->model = $this->loadModel(str_replace("Controller", "",__CLASS__));
                if($this->model->adminToDeptRel(session::getAdminId(), $dept))
                {
                    $departments = $this->model->getDept($dept);
                    $departments = $departments[0];
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("departments", "csrf");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
                }
                else 
                {
                    $controller = $this->loadController("error");
                    if($controller)
                    {
                        $controller->_custom("Something Went Wrong.", "Something Went Wrong. Please Try Again Later.");
                    }
                }
            }
        }
        else
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
            die();
        }
    }
   
    //For Ajax Calls
   public function ajax()
   {
        if(session::isLogin())
        {
           if(isset($_POST['type']) && isset($_POST['action']) && isset($_POST['source']))
           {
               if($_POST['type'] == "office" && $_POST['action'] == "select" && $_POST['source'] == "admin" && isset($_POST['input']))
               {
                   if(!empty($_POST['input']))
                   {
                        if(isset($_SESSION['attendanceControlleradminId']))
                        {
                            $admin = $_SESSION['attendanceControlleradminId'];
                        }
                        else 
                        {
                            $admin = session::getAdminId();
                        }
                        
                        $query = securestr::clean($_POST['input']);
                        
                        $this->model = $this->loadModel("office");
                        $offices = $this->model->searchOffice($admin, $query);
                        
                        if(sizeof($offices) > 0)
                        {
                            $response = $offices;
                        }
                        else 
                        {
                            $response = NULL;
                            $this->isError = TRUE;
                        }
                   }
               }
               else 
               {
                    $this->isError = TRUE;
                    $response = "Nothing Found";
               }
           }
           else 
            {
                 $this->isError = TRUE;
                 $response = "Nothing Found";
            }
        }
        else 
        {
            $this->isError = TRUE;
            $response = "You're Not Login";
        }
        
        $this->data = "";
        $this->data['error'] = $this->isError;
        $this->data['response'] = $response;
        
        echo json_encode($this->data);
   }
}
