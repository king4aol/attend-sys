<section class="wrapper">
        <div id="divLargerImage"></div>

        <div id="divOverlay"></div>
      <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;">Live Statistics</span>
         </div>
     <!-- top menue bar end -->          
       
<div class="col-lg-12">   
    <div class="wrapper">
    <div class="col-lg-12"> 
 
        <form class="form-inline" action="<?=$this->config['domain'].""._PUBLIC_PATH_."attendance/live"?>" method="post" role="form" style="width:80%">
    <div class="dropdown" style="margin-left:-16px; ">
    <div class="form-group">
    <select class="form-control" name="type">
        <option value="all" <?php if($type == "all") echo "selected";?>>All</option>
        <option value="check_in" <?php if($type == "check_in") echo "selected";?>>Check-in</option>
        <option value="check_out" <?php if($type == "check_out") echo "selected";?>>Check-out</option>
<!--        <option value="both" <?php if($type == "both") echo "selected";?>>Both</option>-->
    </select>
    </div>
    <div class="form-group">
    <select class="form-control" name="filter">
        <option value="today" <?php if($filter == "today") echo "selected";?>>Today</option>
        <option value="week" <?php if($filter == "week") echo "selected";?>>Last Week</option>
    </select>
    </div>
 
<input type="submit" class="btn btn-default" style="background:#071d24;border:none;color:#fff;" value="Filter"/>
    </div>
</form>
<a href="<?=$this->config['domain'].""._PUBLIC_PATH_."attendance/reports/"?>"><button type="button" class="btn btn-default pull-right" style="background:#071d24;border:none;color:#fff;margin-right: -14px;margin-top:-33px;">Download Report</button></a>

<!-- Trigger the modal with a button -->
<br><br>
</div>
    <table class="table">
    <thead class="otl_list_employes_color">
        <tr>
            <th>SN</th>
            <th>Employee Name</th>
            <th>Action</th>
            <th>Datetime</th>
            <th>Time zone</th>
            <th>Site/Office</th>     
        </tr>
    </thead>
    <tbody style="background:#ccc">
        <?php 
        $i = 1;
        $timezone = '';
        $set_offset = '';
        $cur_offset = '';
        foreach ($attendance as $attnd)
        {
            $cur_offset = $attnd['timezone'];
            if($cur_offset != $set_offset)
            {
                $set_offset = $cur_offset;
                list($hours, $minutes) = explode(':', $set_offset);
                $seconds = $hours * 60 * 60 + $minutes * 60;
                $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 1);
                // Workaround for bug #44780
                if($timezone === false) 
                {
                    $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 0);
                }
                date_default_timezone_set($timezone);
            }
        ?> 
        <tr>
            <td class="border_color"><?=$offset + $i++?></td>
            <td class="border_color"><?=$attnd['emp_first_name'].' '.$attnd['emp_last_name'];?></td>
            <td class="border_color"><?=$attnd['Action']?></td>
            <td class="border_color"><?=date("d/m/Y H:i:s", $attnd['Time'])?></td>
            <td class="border_color"><?=$attnd['timezone']?> UTC</td>
            <td class="border_color"><?=$attnd['office_name']?></td>
        </tr>
        <?php }
        date_default_timezone_set($this->config['timezone']);
        if($i == 1)
        {?>
        <tr><td colspan="6">No record found</td></tr>
        <?php }?>
    </tbody>
  </table>
                  
    </div>
     <!--pagination start -->    
    <div class="row">
        <ul class="pagination pull-right" style="margin-top:-15px">
            <li><a href="" id="first_page" style="background:#071d24;border:#071d24;color:#fff"><<</a></li>
            <li><a href="" id="previous_page" style="background:#071d24;border:#071d24;color:#fff"><</a></li>
            <li><a style="padding:1px !important;background:#071d24;"><input id="current_page" type="text" data-current="<?=$current?>" data-total="<?php echo(ceil($total/$limit))?ceil($total/$limit):"1"?>" data-limit="<?=$limit?>" value="<?=$current?>" style="color:#000;width:30px;height:27px;border:none;text-align:center"></a></li>
            <li><a style="padding:1px !important;height: 32px;width: 46px;text-align: center;line-height: 30px;text-decoration: none;background: none;border: none;text-align:left;color:#000"><span style="text-align:left"> &nbsp;of</span> &nbsp; <?php echo(ceil($total/$limit))?ceil($total/$limit):"1"?></a></li>
            <li><a href="" id="next_page" style="background:#071d24;border:#071d24;color:#fff">></a></li>
            <li><a href="" id="last_page" style="background:#071d24;border:#071d24;color:#fff;margin-right:10px;">>></a></li>
        </ul>
        <script type="text/javascript">
            $("#current_page").on("keyup", function(e){
                if($(this).val() != "")
                {
                    var input = parseInt($(this).val());
                    var current = $("#current_page").attr("data-current");
                    var total = $("#current_page").attr("data-total");
                    var limit = $("#current_page").attr("data-limit");
                    if(!isNaN(input))
                    {
                        if(input > total)
                        {
                            input = current;
                        }
                        else if(input <= 0)
                        {
                            input = current;
                        }
                    }
                    else 
                    {
                        input = current;
                    }
                    $(this).val(input);
                    
                    if(e.which == 13)
                    {
                        window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."attendance/live/"?>"+limit+"/"+(input*limit-limit);
                    }
                }
            });
            $("#first_page, #previous_page, #next_page, #last_page").click(function(e){
                e.preventDefault();
                console.log($(this).attr("id"));
                if($(this).attr("id") == "first_page")
                {
                    window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."attendance/live/{$limit}/0"?>";
                }
                else if($(this).attr("id") == "last_page")
                {
                    window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."attendance/live/{$limit}/".($total-$limit)?>";
                }
                else if($(this).attr("id") == "previous_page")
                {
                    <?php 
                        if($offset - $limit <= 0)
                        {
                            echo "var previous = 0;";
                        }
                        else 
                        {
                            echo "var previous = ".($offset-$limit).";";
                        }
                    ?>
                    window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."attendance/live/{$limit}/";?>" + previous;
                }
                else if($(this).attr("id") == "next_page")
                {
                    if($("#current_page").val() == $("#current_page").attr("data-current"))
                    {
                        <?php
                            if(ceil($total/$limit) == ceil(($offset+$limit)/$limit))
                            {
                                echo "var next = $offset;";
                            }
                            elseif(ceil($total/$limit) == 0 || ceil($total/$limit) == 1)
                            {
                                echo "var next = 0;";
                            }
                            else 
                            {
                                echo "var next = ".($offset+$limit).";";
                            }
                        ?>
                    window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."attendance/live/{$limit}/";?>"+next;
                    }
                    else 
                    {
                        var input = $("#current_page").val();
                        var total = $("#current_page").attr("data-total");
                        var limit = $("#current_page").attr("data-limit");
                        window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."attendance/live/"?>"+limit+"/"+(input*limit-limit);
                    }
                }
            });
        </script>       
    </div>
       <!--pagination end -->   
      </div>
</section>

  