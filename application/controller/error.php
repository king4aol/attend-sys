<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of error
 *
 * @author Msajid
 */
class errorController extends controller {
    
    public function _404()
    {
        http_response_code(404);
        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__); 
    }
    public function _403()
    {
        http_response_code(403);
        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__); 
    }
    public function _custom($heading = '', $detail = '')
    {
        $data = compact("heading", "detail");
        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $data);
    }
}
