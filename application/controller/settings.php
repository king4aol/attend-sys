<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of setting
 *
 * @author Msajid
 */
class settingsController extends controller
{
    private $data = [];
    private $isError = FALSE;
    private $errorMsgs = [];
    private $model;
    
    public function gracetime()
    {
        if(session::isLogin())
        {
            if(isset($_POST['adminsubmit']) || isset($_POST['submit']))
            {
                if(isset($_POST['admin']) && isset($_POST['adminsubmit']))
                {
                    if(is_numeric($_POST['admin']))
                    {
                        $this->model = $this->loadModel("admin");
                        if($this->model)
                        {
                            if($this->model->adminExists($_POST['admin'], _CLIENT_,TRUE))
                            {
                                $admin = $_POST['admin'];
                                session::sessionStart();
                                $_SESSION[__CLASS__.'adminId'] = $admin;
                            }
                            else
                            {
                               $this->isError = true;
                               $this->errorMsgs['admin_error'] = "Admin Does Not Exists.";
                            }
                        }                    
                        else
                        {
                           $this->isError = true;
                           $this->errorMsgs['admin_error'] = "Admin does Not Exists";
                        }
                        $values['admin'] = $_POST['admin'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['admin_error'] = "Invalid Admin Selected";
                    }
                    
                    if(!$this->isError)
                    {
                        if(isset($_SESSION[__CLASS__.'adminId']))
                        {
                            $admin = $_SESSION[__CLASS__.'adminId'];
                        }
                        else 
                        {
                            $admin = session::getAdminId();
                        }
                        $csrf = csrf::generateCsrf(__FUNCTION__);
                        $this->model = $this->loadModel(str_replace("Controller", "",__CLASS__));
                        $values['grace_time'] = $this->model->graceTime($admin); 
                        $this->data = compact("csrf", "values");
                        $this->loadView(str_replace("Controller", "", __CLASS__),__FUNCTION__, $this->data);
                    }
                    else
                    {
                        unset($_POST['adminsubmit']);
                        $adminModel = $this->loadModel("admin");
                        $admins = [];

                        if($adminModel)
                        {
                            $admins = $adminModel->viewAdmins();
                        }
                        $csrf = csrf::generateCsrf(__FUNCTION__);
                        $error = $this->errorMsgs;
                        $this->data = compact("admins", "csrf", "values", "error");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
                    }
               }
               elseif(isset($_POST['submit']))
               {
                    if(isset($_SESSION[__CLASS__.'adminId']))
                    {
                        $admin = $_SESSION[__CLASS__.'adminId'];
                    }
                    else 
                    {
                        $admin = session::getAdminId();
                    }
                    //office grace time
                    if(isset($_POST['csrf']))
                    {
                        if(!csrf::validateCsrf($_POST['csrf'], __FUNCTION__))
                        {
                            $this->errorMsgs;
                            $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                        }
                    }
                    else
                    {
                        $this->errorMsgs;
                        $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                    }

                    if(isset($_POST['grace_time']) && !@empty($_POST['grace_time']))
                    {
                        if(is_numeric($_POST['grace_time']))
                        {
                            $office_grace_time = $_POST['grace_time'];
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['grace_time_error'] = "office grace time is only in minutes";
                        }
                        $values['grace_time'] = securestr::clean($_POST['grace_time']);
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['grace_time_error'] = "Grace Time Is Required";
                    }
                    if(!$this->isError)
                    {
                        $_POST['adminsubmit'] = "";
                        $_POST['admin'] = $admin;
                        $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                        $csrf = csrf::generateCsrf(__FUNCTION__);
                        $values['grace_time'] = $this->model->updateGraceTime($admin, $office_grace_time);
                        $this->data = compact("values", "csrf");
                        $this->loadView(str_replace("Controller", "", __CLASS__),__FUNCTION__, $this->data);
                    }
                    else
                    {
                        $_POST['adminsubmit'] = "";
                        $_POST['admin'] = $admin;
                        $csrf = csrf::generateCsrf(__FUNCTION__);
                        $error = $this->errorMsgs;
                        $this->data = compact("error", "values", "csrf");
                        $this->loadView(str_replace("Controller", "", __CLASS__),__FUNCTION__, $this->data);
                    }
               }
            }
            else
            {
                $adminModel = $this->loadModel("admin");
                $admins = [];

                if($adminModel)
                {
                    $admins = $adminModel->viewAdmins();
                }
                $csrf = csrf::generateCsrf(__FUNCTION__);
                $this->data = compact("admins", "csrf", "values");
                $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
            }
        }
        else
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
            die();
        }
    }
}
