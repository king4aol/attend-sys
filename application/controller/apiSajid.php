<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of apiSajid
 *
 * @author Msajid
 */
class apiSajidController extends controller
{
    private $apiKey = "1234";
    private $data = [];
    private $isError = FALSE;
    private $errorMsgs = [];
    private $model;
    
    public function __construct() 
    {
        parent::__construct();
        $this->data['error'] = false;
        $this->data['is_login'] = false;
    }
    public function index()
    {
        
    }
    public function login($api = '')
    {
        $username = '';
        $password = '';
        if(!empty($api))
        {
            if($api != $this->apiKey)
            {
                $this->isError = true;
                $this->data['error_msg'] = "Invalid API";
            }
        }
        else
        {
            $this->isError = true;
            $this->data['error_msg'] = "API Key is Missing";
        }
        if(isset($_POST['name']))
        {
            $username = $_POST['name'];
        }
        else
        {
            $this->isError = TRUE;
            $this->data['error_message'] = "Name Is Required";

        }
        if(isset($_POST['password']))
        {
            $pass = password::encryptPassword($_POST['password']);
        }
        else
        {
            $this->isError = TRUE;
            $this->data['error_message'] = "Password Is Required";

        }
        if(!$this->isError)
        {
            session::sessionStart();
            $this->model =  $this->loadModel("access");
            if($this->model->login($username , $pass))
            {
                $this->data['is_login'] = true;
                $this->data['admin_id'] = $_SESSION['otl_admin_admin_id'];
                $this->data['full_name'] = $_SESSION['otl_admin_name'];
            }
            else
            {
                $this->isError = true;
                $this->data['error'] = "Invalid Username or Password";
            }
        }
        else
        {
            $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
        }
        $this->data['error'] = $this->isError;
        echo json_encode($this->data);
    }
    
}
