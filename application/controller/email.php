<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of email
 *
 * @author Msajid
 */
class email extends model{
    private $dbTable = __CLASS__;
    private $created = false;
    
    private function dbcheck()
    { return TRUE;
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (
                    `email_id` int(11) NOT NULL AUTO_INCREMENT,
                    `user_id` int(11) NOT NULL,
                    `email_address` varchar(50) NOT NULL UNIQUE,
                    `added_date` datetime NOT NULL,
                    `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`email_id`),
                    FOREIGN KEY (`user_id`) REFERENCES `{$this->config['db_table_prefix']}users`(`user_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return true;
    }
    public function firstCreate()
    {
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (
                    `email_id` int(11) NOT NULL AUTO_INCREMENT,
                    `user_id` int(11) NOT NULL,
                    `email_address` varchar(50) NOT NULL UNIQUE,
                    `added_date` datetime NOT NULL,
                    `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`email_id`),
                    FOREIGN KEY (`user_id`) REFERENCES `{$this->config['db_table_prefix']}users`(`user_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return true;
    }
    
    public function insertEmail($userId, $email) 
    {
        if($this->validate($email))
        {
            $sql = "INSERT INTO `{$this->config['db_table_prefix']}{$this->dbTable}`(`user_id`, `email_address`, `added_date`) VALUES ($userId, '$email', now())";

            if($this->dbcheck())
            {
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return $this->db->last_id;
                }
                return false;
            }
            return false;
        }
        return false;
    }
    
    private function validate($email)
    {
        if(!filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            return false;
        }
        return true;
    }
    
    public function emailExists($email = '')
    {
        $sql = "SELECT `email_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `email_address` = '$email'";
        
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                return true;
            }
            return false;
        }
        return false;
    }
    //Check if email already Exists except me
    public function emailExistsExceptThis($email = '', $userId = '')
    {
        $sql = "SELECT `email_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `email_address` = '$email' AND `user_id` <> $userId";
       
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found && $this->db->is_true)
            {
                return true;
            }
            return false;
        }
        return false;
    }
}
