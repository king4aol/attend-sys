<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of home
 *
 * @author Msajid
 */
class homeController extends controller
{
    private $data = [];
    private $model;
    
    public function index($limit = 10, $offset = 0) 
    {
        if(session::isLogin())
        {
            if(!is_numeric($limit) || !is_numeric($offset) || $limit <= 0 || $offset < 0)
            {
                $limit = 10;
                $offset = 0;
            }
            
            $this->model = $this->loadModel("office");           
            $offices = $this->model->viewById(session::getAdminId(), false, session::getUserType());
            if(!isset($_SESSION[__CLASS__.__FUNCTION__."selected_office"]))
            {
                $_SESSION[__CLASS__.__FUNCTION__."selected_office"] = $offices[0]['office_id'];
                $_SESSION[__CLASS__.__FUNCTION__."selected_office_name"] = $offices[0]['office_name'];
                $selectedOfficeName = $_SESSION[__CLASS__.__FUNCTION__."selected_office_name"];
                $selectedOffice = $_SESSION[__CLASS__.__FUNCTION__."selected_office"];
            }
            
            if(isset($_POST['office']))
            {
                if(is_numeric($_POST['office']))
                {
                    $office = $_POST['office'];
                    $this->model = $this->loadModel("office");
                    if($this->model->adminToOfficeRel(session::getAdminId(), $office))
                    {
                        $selectedOffice = $office;
                        $_SESSION[__CLASS__.__FUNCTION__."selected_office"] = $selectedOffice;
                        $_SESSION[__CLASS__.__FUNCTION__."selected_office_name"] = $this->model->idToName($selectedOffice);
                        $selectedOfficeName = $_SESSION[__CLASS__.__FUNCTION__."selected_office_name"];
                    }
                }
                else 
                {
                    $selectedOffice = $_SESSION[__CLASS__.__FUNCTION__."selected_office"];
                    $selectedOfficeName = $_SESSION[__CLASS__.__FUNCTION__."selected_office_name"];
                }
            }
            elseif(isset($_SESSION[__CLASS__.__FUNCTION__."selected_office"]))
            {
                $selectedOffice = $_SESSION[__CLASS__.__FUNCTION__."selected_office"];
                $selectedOfficeName = $_SESSION[__CLASS__.__FUNCTION__."selected_office_name"];
            }
            
            if(isset($_POST['filter']))
            {
                if($_POST['filter'] == "today")
                {
                    $fromTime = "today";
                    $selectedFilter = "today";
                }
                elseif($_POST['filter'] == "yesterday")
                {
                    $fromTime = "yesterday";
                    $selectedFilter = "yesterday";
                }
                else 
                {
                    $fromTime = "today";
                    $selectedFilter = "today";
                }
                $_SESSION[__CLASS__.__FUNCTION__."selected_filter"] = $selectedFilter;
            }
            elseif(isset($_SESSION[__CLASS__.__FUNCTION__."selected_filter"]))
            {
                $selectedFilter = $_SESSION[__CLASS__.__FUNCTION__."selected_filter"];
            }
            else 
            {
                $selectedFilter = "today";
            }
            
            $detail = [];
            $bar = [];
            $charts['pie'] = [];
            $charts['bars'] = [];
            $raw = [];
            $admin = session::getAdminId();
            if(session::getUserType() == _ADMIN_)
            {
                $this->model = $this->loadModel("admin");
                $detail['clients'] = $this->model->totalAdmins();
                $bar['clients'] = 100;
                $this->model = $this->loadModel("office");
                $detail['offices'] = $this->model->totalOffices();
                $bar['offices'] = 100;
                $this->model = $this->loadModel("employee");
                $detail['employees'] = $this->model->totalEmployees();
                $bar['employees'] = 100;
                $this->model = $this->loadModel("check_in");
//                $raw = $this->model->countCheckInByStatus();
//                if(is_array($raw))
//                { 
//                    $detail['check_in'] = $raw[_IN_TIME_] + $raw[_LATE_];
//                    $bar['check_in'] = round($raw[_IN_TIME_]/$detail['employees'] * 100);
//                }
//                else 
//                {
//                    $detail['check_in'] = 0;
//                    $bar['check_in'] = 0;
//                }
                $detail['check_in'] = $this->model->countCheckIn(session::getAdminId(), session::getUserType(), TRUE);
                $bar['check_in'] = 100;
                $fromTimestamp = strtotime(date("d-m-Y")) - (86400 * 30);
                $charts['pie'] = $this->model->countAllCheckinFromTime($fromTimestamp);
                $this->model = $this->loadModel("attendance");
                if($selectedFilter == "week")
                {
                    $fromTimestamp = strtotime(date("d-m-Y")) - (86400 * 7);
                }
                elseif($selectedFilter == "yesterday")
                {
                    $fromTimestamp = strtotime(date("d-m-Y")) - 86400 ;//* 2);
                }
                else
                {
                    $fromTimestamp = strtotime(date("d-m-Y")) ;//- 86400;
                }
                $charts['bars'] = $this->model->countAllAttendanceFromTime($fromTimestamp, $selectedOffice);
                $this->model = $this->loadModel("check_out");
//                $raw = $this->model->countCheckOutByStatus();
//                if(is_array($raw))
//                { 
//                    $detail['check_out'] = $raw[_OVERTIME_] + $raw[_EARLY_];
//                    $bar['check_out'] = round($raw[_OVERTIME_]/$detail['employees'] * 100);
//                }
//                else 
//                {
//                    $detail['check_out'] = 0;
//                    $bar['check_out'] = 0;
//                }
                $detail['check_out'] = $this->model->countCheckOut(session::getAdminId(), session::getUserType(), TRUE);
                $bar['check_out'] = 100;
            }
            else 
            {
                $this->model = $this->loadModel("office");
                $detail['offices'] = $this->model->countOffices($admin);
                $bar['offices'] = 100;
                $this->model = $this->loadModel("employee");
                $detail['employees'] = $this->model->countEmployees($admin, TRUE);
                $bar['employees'] = 100;
                $this->model = $this->loadModel("check_in");
//                $raw = $this->model->countCheckInByStatus($admin, _CLIENT_);
//                if(is_array($raw))
//                { 
//                    $detail['check_in'] = $raw[_IN_TIME_] + $raw[_LATE_];
//                    $bar['check_in'] = round($raw[_IN_TIME_]/$detail['employees'] * 100);
//                }
//                else 
//                {
//                    $detail['check_in'] = 0;
//                    $bar['check_in'] = 0;
//                }
                $detail['check_in'] = $this->model->countCheckIn(session::getAdminId(), session::getUserType(), TRUE);
                $bar['check_in'] = 100;
                $fromTimestamp = strtotime(date("d-m-Y")) - (86400 * 30);
                $charts['pie'] = $this->model->countCheckinFromTime($admin, $fromTimestamp);
                $this->model = $this->loadModel("attendance");
                if($selectedFilter == "week")
                {
                    $fromTimestamp = strtotime(date("d-m-Y")) - (86400 * 7);
                }
                elseif($selectedFilter == "yesterday")
                {
                    $fromTimestamp = strtotime(date("d-m-Y")) - 86400; //* 2);
                }
                else
                {
                    $fromTimestamp = strtotime(date("d-m-Y"));// - 86400;
                }
                $charts['bars'] = $this->model->countAttendanceFromTime($admin, $fromTimestamp, $selectedOffice);
                $this->model = $this->loadModel("check_out");
//                $raw = $this->model->countCheckOutByStatus($admin, _CLIENT_);                
//                if(is_array($raw))
//                { 
//                    $detail['check_out'] = $raw[_OVERTIME_] + $raw[_EARLY_];
//                    $raw = $this->model->countCheckOutByStatus($selectedOffice, _OFFICE_, $selectedFilter);
//                    $bar['check_out'] = round($raw[_OVERTIME_]/$detail['employees'] * 100);
//                }
//                else 
//                {
//                    $detail['check_out'] = 0;
//                    $bar['check_out'] = 0;
//                }
                $detail['check_out'] = $this->model->countCheckOut(session::getAdminId(), session::getUserType(), TRUE);
                $bar['check_out'] = 100;
            }
            
            $this->model = $this->loadModel("check_in");
            $from = strtotime(date("d-m-Y"));
            $checkIns = $this->model->lastestCheckin(session::getAdminId(), session::getUserType(), $offset, $limit, $from);
            $total = $this->model->countCheckIn(session::getAdminId(), session::getUserType(), TRUE);
            $next = $offset + $limit;
            if($next > $total)
            {
                $next = $offset;
            }
            $previous = $offset - $limit;
            if($previous < 0)
            {
                $previous = 0;
            }
            $current = ceil($offset / $limit);
            $current++;
            if($current == 0)
            {
                $current = 1;
            }
            //For Google Charts
            $this->javascripts[] = "https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['corechart']}]}";
            $this->javascripts[] = "https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['bar']}]}";
            $this->data = compact("offices", "selectedFilter", "selectedOffice", "selectedOfficeName", "detail", "bar", "charts", "checkIns", "total", "next", "previous", "limit", "offset", "current");
            $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data); 
        }
        else
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."home/commingsoon");
            die();
        }
    }
    
    public function support()
    {
        if(session::isLogin())
        {
            $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data); 
        }
        else
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
            die();
        }
    }

    public function ajax($type = "activity") 
    {
        if(session::isLogin())
        {
            if($type == "activity")
            {
                $admin = session::getAdminId();
                $detail = [];
                $response = "";
//                $this->model = $this->loadModel("check_in");
//                $detail['check_in'] = $this->model->lastestCheckin($admin, session::getUserType());
//                $this->model = $this->loadModel("break_in");
//                $detail['break_in'] = $this->model->lastestBreakin($admin, session::getUserType());
//                $this->model = $this->loadModel("break_out");
//                $detail['break_out'] = $this->model->lastestBreakout($admin, session::getUserType());
//                $this->model = $this->loadModel("check_out");
//                $detail['check_out'] = $this->model->lastestCheckout($admin, session::getUserType());
                $this->model = $this->loadModel("attendance");
                $detail = $this->model->latestActivity(0,2,"all");
                $timezone = '';
                $set_offset = '';
                $cur_offset = '';
                if(is_array($detail))
                {
                    foreach($detail as $v)
                    {
                        $cur_offset = $v['timezone'];
                        if($cur_offset != $set_offset)
                        {
                            $set_offset = $cur_offset;
                            list($hours, $minutes) = explode(':', $set_offset);
                            $seconds = $hours * 60 * 60 + $minutes * 60;
                            $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 1);
                            // Workaround for bug #44780
                            if($timezone === false) 
                            {
                                $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 0);
                            }
                            date_default_timezone_set($timezone);
                        }
                        $response .= '<div class="desc" style="';
                        if($v['status'] == _IN_TIME_ || $v['status'] == _OVERTIME_ || $v['status'] == _EARLY_)
                        {
                            $response.= 'background-color:#E8F8F7';
                        }
                        $response .= '">
                        <div class="thumb">
                            <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
                        </div>
                        <div class="details">
                            <p><a href="'.$this->config['domain'].'/'._PUBLIC_PATH_.'/attendance/view/'.$v['emp_id'].'"><strong>'.$v['emp_first_name'].' '.$v['emp_last_name'].'</strong></a><br>
                                <strong>Last Action: </strong>'.$v['Action'].'<br>
                                <strong>Date: </strong> '.date("d/m/Y H:i", $v['Time']).'<br>
                                <strong>Site: </strong>'.$v['office_name'].'
                            </p>
                        </div>
                    </div>';
                    }
                    date_default_timezone_set($this->config['timezone']);
                }
//                if(!@empty($detail['check_in']['emp_last_name']))
//                {
//                    $response .= '<div class="desc" style="';
//                    if($detail['check_in']['status'] == _IN_TIME_)
//                    {
//                        $response.= 'background-color:#E8F8F7';
//                    }
//                    $response .= '">
//                        <div class="thumb">
//                            <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
//                        </div>
//                        <div class="details">
//                            <p><a href="'.$this->config['domain'].'/'._PUBLIC_PATH_.'/attendance/view/'.$detail['check_in']['emp_id'].'"><strong>'.$detail['check_in']['emp_first_name'].' '.$detail['check_in']['emp_last_name'].'</strong></a><br>
//                                <strong>Last Action: </strong>Check In<br>
//                                <strong>Date: </strong> '.date("d/m/Y H:i", $detail['check_in']['check_in_timestamp']).'<br>
//                                <strong>Site: </strong>'.$detail['check_in']['office_name'].'
//                            </p>
//                        </div>
//                    </div>';
//                }
////                $response .= '<div class="desc">
////                    <div class="thumb">
////                        <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
////                    </div>
////                    <div class="details">
////                        <p><strong>'.$detail['break_in']['emp_first_name'].' '.$detail['break_in']['emp_last_name'].'</strong><br>
////                            <strong>Date: </strong> '.date("d/m/Y H:i", $detail['break_in']['break_in_timestamp']).'<br>
////                            <strong>Last Action: </strong>Break In
////                        </p>
////                    </div>
////                </div>';
////                $response .= '<div class="desc">
////                    <div class="thumb">
////                        <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
////                    </div>
////                    <div class="details">
////                        <p><strong>'.$detail['break_out']['emp_first_name'].' '.$detail['break_out']['emp_last_name'].'</strong><br>
////                            <strong>Date: </strong> '.date("d/m/Y H:i", $detail['break_out']['break_out_timestamp']).'<br>
////                            <strong>Last Action: </strong>Break Out
////                        </p>
////                    </div>
////                </div>';
//                if(!@empty($detail['check_out']['emp_last_name']))
//                {
//                    $response .= '<div class="desc" style="';
//                    if($detail['check_in']['status'] == _OVERTIME_)
//                    {
//                        $response.= 'background-color:#E8F8F7';
//                    }
//                    $response .= '">
//                        <div class="thumb">
//                            <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
//                        </div>
//                        <div class="details">
//                            <p><a href="'.$this->config['domain'].'/'._PUBLIC_PATH_.'/attendance/view/'.$detail['check_in']['emp_id'].'"><strong>'.$detail['check_out']['emp_first_name'].' '.$detail['check_out']['emp_last_name'].'</strong></a><br>
//                                <strong>Last Action: </strong>Check Out<br>
//                                <strong>Date: </strong> '.date("d/m/Y H:i", $detail['check_out']['check_out_timestamp']).'<br>
//                                <strong>Site: </strong>'.$detail['check_in']['office_name'].'
//                            </p>
//                        </div>
//                    </div>';
//                }
                echo $response;
            }
        }
    }
    public function comingsoon() 
    {
        if(!session::isLogin())
        {
            $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
        }
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."home/index");
            die();
        }
    }
}
