<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of employee
 *
 * @author Msajid
 */
class employeeController extends controller  
{
   private $isError = false;
   private $errorMsgs;
   private $model;
   private $data;
   
   public function add()
   { 
       if(session::isLogin())
        {
            if(isset($_POST['submit']) && !isset($_POST['employee']))
            {
                //checking admin
                if(isset($_POST['admin']))
                {
                    if(is_numeric($_POST['admin']))
                    {
                        $adminModel = $this->loadModel("admin");
                        if($adminModel)
                        {
                            if($adminModel->adminExists($_POST['admin'], _CLIENT_, TRUE))
                            {
                                $admins = $_POST['admin'];
                                $_SESSION[__CLASS__.__FUNCTION__] = $admins;
                            }
                            else
                            {
                               $this->isError = true;
                               $this->errorMsgs['admin_error'] = "Company Does Not Exists.";
                            }
                        }                    
                        else
                        {
                           $this->isError = true;
                           $this->errorMsgs['admin_error'] = "Company does Not Exists";
                        }
                        $values['admin'] = $_POST['admin'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['admin_error'] = "Invalid Company Selected";
                    }
               }
               else
               {
                   
               }
               //validate office
               /*if(isset($_POST['office']))
                {
                    if(is_numeric($_POST['office']))
                    {
                        $officeModel = $this->loadModel("office");
                        if($officeModel)
                        {
                            if($officeModel->officeExists($_POST['office']))
                            {
                                $office = $_POST['office'];
                            }
                            else
                            {
                               $this->isError = true;
                               $this->errorMsgs['office_error'] = "office Does Not Exists.";
                            }
                        }                    
                        else
                        {
                           $this->isError = true;
                           $this->errorMsgs['office_error'] = "office does Not Exists";
                        }
                        $values['office'] = $_POST['office'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['office_error'] = "Invalid office Selected";
                    }
               }*/
               
               if($this->isError)
               {
                    unset($_POST['submit']);
                    $this->model = $this->loadModel("admin");
                    $admins = $this->model->viewAdmins();
                    $error = $this->errorMsgs;
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("admins", "csrf", "error", "values");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
               }
               else
               {
                   $this->model = $this->loadModel("admin");
                   $globalGracetime = $this->model->getGraceTime($admins);
                   $username = $this->model->idToUsername($admins);
                   $this->model = $this->loadModel("department");
                   $departments = $this->model->view($username);
                   $this->model = $this->loadModel("employee_types");
                   $types = $this->model->loadTypes();
                   $csrf = csrf::generateCsrf(__FUNCTION__);
                   $this->data = compact("csrf", "values", "globalGracetime", "departments", "types");
                   $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
               }
            }
           elseif(isset($_POST['employee']))
           {
               /*
                if(isset($_POST['csrf']))
                {
                    if(!csrf::validateCsrf($_POST['csrf'], __FUNCTION__))
                    {
                        $this->errorMsgs;
                        $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                    }
                }
                else
                {
                    $this->errorMsgs;
                    $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                }
                
                */
                //validating first name
                if(isset($_POST['firstname']) && !@empty($_POST['firstname']))
                {
                    if(ctype_alpha($_POST['firstname']))
                    {
                        $firstName = $_POST['firstname'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['firstname_error'] = "first name is only alpha";
                    }
                    $values['firstname'] = securestr::clean($_POST['firstname']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['firstname_error'] = "first name is required";
                }

                //validating last name

                if(isset($_POST['lastname']) && !@empty($_POST['lastname']))
                {
                    if(ctype_alpha($_POST['lastname']))
                    {
                        $lastName = $_POST['lastname'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['lastname_error'] = "Last Name is Only Alpha";
                    }
                    $values['lastname'] = securestr::clean($_POST['lastname']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['lastname_error'] = "Last Name is Required";
                }

                //office grace time

                if(isset($_POST['emp_grace_time']) && !@empty($_POST['emp_grace_time']))
                {
                    if(is_numeric($_POST['emp_grace_time']))
                    {
                        $emp_grace_time = $_POST['emp_grace_time'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['grace_time_error'] = "Employee Grace Time is Only in Minutes";
                    }
                    $values['emp_grace_time'] = securestr::clean($_POST['emp_grace_time']);
                }
                else
                {
                   $emp_grace_time = 0;
                }
                //break grace time

               if(isset($_POST['break_time']))
                {
                    if(is_numeric($_POST['break_time']) && $_POST['break_time'] > 0)
                    {
                        if($_POST['break_time'] <= 60)
                        {
                            $break_time = $_POST['break_time'];
                        }
                        else 
                        {
                            $this->isError = true;
                            $this->errorMsgs['break_time_error'] = "Break Duration Should Between 0-60.";
                        }
                    }
                    else
                    {
                        $break_time = 0;
                    }
                    $values['break_time'] = securestr::clean($_POST['break_time']);
                }
                else 
                {
                        $break_time = 0;
                }
                //valedate timing from
                
                if(isset($_POST['timing_from']) && !@empty($_POST['timing_from']))
                {
                    //if(is_numeric($_POST['timing_from']))
                    //{
                        if(preg_match('#^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$#', $_POST['timing_from']))
                        {
                            $timing_from = strtotime($_POST['timing_from']);
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['timing_from_error'] = "Invalid Time Format";
                        }
//                    }
//                    else
//                    {
//                        $this->isError = true;
//                        $this->errorMsgs['timing_from_error'] = "Time  is Only in Numaric Values";
//                    }
                    $values['timing_from'] = securestr::clean($_POST['timing_from']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['timing_from_error'] = " Time  is Required";
                }
                
                // validate timing to
                
                if(isset($_POST['timing_to']) && !@empty($_POST['timing_to']))
                {
                    if(preg_match('#^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$#', $_POST['timing_to']))
                    {
                        $timing_to = strtotime($_POST['timing_to']);
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['timing_to_error'] = "Invalid Time Format";
                    }
                    $values['timing_to'] = securestr::clean($_POST['timing_to']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['timing_to_error'] = "Time is required";
                }
                
                if(isset($timing_from) && isset($timing_to))
                {
                    if($timing_from > $timing_to)
                    {
                        $this->isError = true;
                        $this->errorMsgs['error'] = "Shift Start Time Cannot Be Greater Then Shift Ending Time";
                    }
                }
                //validate pay rate
                
                if(isset($_POST['pay_rate']) && !@empty($_POST['pay_rate']))
                {
                    if(is_numeric($_POST['pay_rate']))
                    {
                        $pay_rate = $_POST['pay_rate'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['pay_rate_error'] = "pay rate should only be in numaric values";
                    }
                    $values['pay_rate'] = securestr::clean($_POST['pay_rate']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['pay_rate_error'] = "pay rate is required";
                }
                
				$department = 1;
				$type = 1;
				/*
                if(isset($_POST['dept']) && !@empty($_POST['dept']))
                {
                    if(is_numeric($_POST['dept']))
                    {
                        $this->model = $this->loadModel("department");
                        if($this->model->adminToDeptRel(session::getAdminId(), $_POST['dept']))
                        {
                            $department = $_POST['dept'];
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['dept_error'] = "Invalid Department Selected";
                        }
                    }
                    elseif(isset($_POST['csv_upload_check']) && @$_POST['csv_upload_check'] == "AppRoveD")
                    {
                        $this->model = $this->loadModel("department");
                        $department = $this->model->nameToId($_POST['dept']);
                        if(!$department)
                        {
                            $this->isError = true;
                            $this->errorMsgs['dept_error'] = "Invalid Department Selected";
                        }
                        else 
                        {
                            $this->model = $this->loadModel("department");
                            if(!$this->model->adminToDeptRel(session::getAdminId(), $_POST['dept']))
                            {
                                $this->isError = true;
                                $this->errorMsgs['dept_error'] = "Invalid Department Selected";
                            }
                        }
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['dept_error'] = "Invalid Department Selected";
                    }
                    $values['dept'] = securestr::clean($_POST['dept']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['dept_error'] = "Department is required";
                }
                
                if(isset($_POST['type']) && !@empty($_POST['type']))
                {
                    if(is_numeric($_POST['type']))
                    {
                        $this->model = $this->loadModel("employee_types");
                        if($this->model->typeExists($_POST['type']))
                        {
                            $type = $_POST['type'];
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['type_error'] = "Invalid Employee Type Selected";
                        }
                    }
                    elseif(isset($_POST['csv_upload_check']) && @$_POST['csv_upload_check'] == "AppRoveD")
                    {
                        $this->model = $this->loadModel("employee_types");
                        $type = $this->model->getTypeId($_POST['type']);
                        if(!$type)
                        {
                            $this->isError = true;
                            $this->errorMsgs['type_error'] = "Invalid Employee Type Selected";
                        }
                        else 
                        {
                            $this->model = $this->loadModel("employee_types");
                            if(!$this->model->typeExists($type))
                            {
                                $this->isError = true;
                                $this->errorMsgs['type_error'] = "Invalid Employee Type Selected";
                            }
                        }
                    }
                    $values['type'] = securestr::clean($_POST['type']);
                }
                else
                {
                    $this->isError = true;
                    $this->errorMsgs['type_error'] = "Employee Type is required";
                }
				*/
                //validate over time pay rate
                
//                if(isset($_POST['overtime_pay_rate']) && !@empty($_POST['overtime_pay_rate']))
//                {
//                    if(is_numeric($_POST['overtime_pay_rate']))
//                    {
//                        $overtime_pay_rate = $_POST['overtime_pay_rate'];
//                    }
//                    else
//                    {
//                        $this->isError = true;
//                        $this->errorMsgs['overtime_pay_rate_error'] = "overtime pay rate is only in numaric values";
//                    }
//                    $values['overtime_pay_rate'] = securestr::clean($_POST['overtime_pay_rate']);
//                }
//                else
//                {
//                    $this->isError = true;
//                    $this->errorMsgs['overtime_pay_rate_error'] = "overtime pay rate is required";
//                }
                $overtime_pay_rate = 0;
                //Checking Email
                if(isset($_POST['email']) && !@empty($_POST['email']))
                {
                    if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
                    {
                        $this->model = $this->loadModel("email");
                        if(!$this->model->emailExists($_POST['email']))
                        {
                            $email = $_POST['email'];
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['email_error'] = "Email Already Exists";
                        }
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['email_error'] = "Invalid Email Address";
                    }
                    $values['email'] = securestr::clean($_POST['email']);
                }
//                else
//                {
//                    $this->isError = true;
//                    $this->errorMsgs['email_error'] = "Email is Required";
//                }
                
                if(!$this->isError)
                {
                    set_time_limit(0);
                    
                    if(isset($_SESSION[__CLASS__.__FUNCTION__]))
                    {
                        $admin_id = $_SESSION[__CLASS__.__FUNCTION__];
                    }
                    else 
                    {
                        $admin_id = session::getAdminId();
                    }
                    
                    $this->model = $this->loadModel("admin");
                    $membershipType = $this->model->getMembershipType($admin_id);
                    
                    $this->model = $this->loadModel("membership_types");
                    $employeesAllowed = $this->model->getEmployeesAllow($membershipType, FALSE);
                    $totalEmployees = 0;
                    
                    if($employeesAllowed == -1)
                    {
                        $employeesAllowed = 9999999999;
                    }
                    else 
                    {
                        $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                        $totalEmployees = $this->model->countEmployees($admin_id);
                    }
                    
                    if($employeesAllowed <= $totalEmployees)
                    {
                        $this->isError = true;
                        $this->errorMsgs['emp_limit'] = "You're Not Allowed To Add More Then $employeesAllowed Employees";
                    }
                }
                
                
                if(!$this->isError)
                {   
                    $adminId = '';
                    if(isset($_SESSION[__CLASS__.__FUNCTION__]))
                    {
                        $adminId = $_SESSION[__CLASS__.__FUNCTION__];
                    }
                    //insert into employee
                    $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                    $this->model->db->transaction();
                    $empId = $this->model->insertEmployee($adminId, $firstName, $lastName, $timing_from, $timing_to , $pay_rate, $overtime_pay_rate,  $emp_grace_time, $break_time, $department, $type);
                    //Inserting Into Users
                    $this->model = $this->loadModel("users");
                    $userId = $this->model->insertUsers($empId, _EMPLOYEE_);
                    if(isset($email))
                    {
                        //Inserting Email Address
                        $this->model = $this->loadModel("email");
                        $this->model->insertEmail($userId, $email);
                    }
                    
                    if(isset($_POST['csv_upload_check']) && @$_POST['csv_upload_check'] == "AppRoveD")
                    {
                        if($this->model->db->commit())
                        {
                            $this->isError = FALSE;
                        }
                        else
                        {
                            $this->isError = TRUE;
                        }
                        return $this->isError;
                    }
                    else 
                    {
                        if($this->model->db->commit())
                        {
                            $this->isError = FALSE;
                            $res['response'] = "Employee Added Successfully";
                            //Science
                            $_POST['success'] = $res['response'];
                            $_SESSION[__CLASS__.'adminId'] = $_SESSION[__CLASS__.__FUNCTION__];
                            $controller = $this->loadController("employee");
                            $controller->view('success');
                            die();
                            //Science End
                        }
                        else
                        {
                            $this->isError = TRUE;
                            $this->errorMsgs['error'] = "Something Went Wrong. Please Try Again. ";
                        }

                        unset($_POST['employee']);
                        $_POST['submit'] = '';
                        $this->javascripts[] = "{$this->config['domain']}"._PUBLIC_PATH_."js/jquery.validate.min.js";
                        //loading countries model
                        $adminModel = $this->loadModel("admin");
                        $admins = [];

                        if($adminModel)
                        {
                            $admins = $adminModel->viewAdmins();
                        }
                        $this->model = $this->loadModel("admin");
                        $globalGracetime = $this->model->getGraceTime($adminId);
                        $username = $this->model->idToUsername($adminId);
                        $this->model = $this->loadModel("department");
                        $departments = $this->model->view($username);
                        $this->model = $this->loadModel("employee_types");
                        $types = $this->model->loadTypes();
                        $error = $this->errorMsgs;
                        $csrf = csrf::generateCsrf(__FUNCTION__);
                        $this->data = compact("admins", "csrf", "error", "values", "res", "globalGracetime", "departments", "types");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
                        /*//insert into emp_to_office
                        $this->model = $this->loadModel("emp_to_office");
                        if($this->model)
                        {
                            $this->model->insert($empId, $officeId);
                        }
                        if($empId)//$this->model->db->commit())
                        {
                            echo "employee Added Successfully.";
                        }
                        else
                        {
                            echo "employee Added Failed. ".$this->model->db->failed;
                        }*/
                    }
                }
                else
                {
                    if(isset($_POST['csv_upload_check']) && @$_POST['csv_upload_check'] == "AppRoveD")
                    {
                        return $this->isError;
                    }
                    else 
                    {
                        if(isset($_SESSION[__CLASS__.__FUNCTION__]))
                        {
                            $admin_id = $_SESSION[__CLASS__.__FUNCTION__];
                        }
                        else 
                        {
                            $admin_id = session::getAdminId();
                        }
                        unset($_POST['employee']);
                        $_POST['submit'] = '';
                        $this->javascripts[] = "{$this->config['domain']}"._PUBLIC_PATH_."js/jquery.validate.min.js";
                        //loading countries model
                        $adminModel = $this->loadModel("admin");
                        $admins = [];

                        if($adminModel)
                        {
                            $admins = $adminModel->viewAdmins();
                        }
                        $this->model = $this->loadModel("admin");
                        $globalGracetime = $this->model->getGraceTime($admin_id);
                        $username = $this->model->idToUsername($admin_id);
                        $this->model = $this->loadModel("department");
                        $departments = $this->model->view($username);
                        $this->model = $this->loadModel("employee_types");
                        $types = $this->model->loadTypes();
                        $error = $this->errorMsgs;
                        $csrf = csrf::generateCsrf(__FUNCTION__);
                        $this->data = compact("admins", "csrf", "error", "values", "globalGracetime", "departments", "types");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data); 
                    }
                }
           }
           else
            {
                $this->javascripts[] = "{$this->config['domain']}"._PUBLIC_PATH_."js/jquery.validate.min.js";
                //loading countries model
                $adminModel = $this->loadModel("admin");
                $admins = [];

                if($adminModel)
                {
                    $admins = $adminModel->viewAdmins();
                }
                $error = $this->errorMsgs;
                $csrf = csrf::generateCsrf(__FUNCTION__);
                $this->data = compact("admins", "csrf", "error", "values");
                $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data); 
            }
        }
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
            die();
        }
   }
   //For Ajax Calls
   public function ajax()
   {
       /*
       if(isset($_POST['user']) && isset($_POST['type']) && isset($_POST['source']))
       {
           if($_POST['type'] == "office" && ctype_alnum($_POST['user']))
           {
               $response = "<option value=''>Select Office</option>";
               $this->model = $this->loadModel("office");
               $offices = $this->model->view($_POST['user']);
               foreach($offices as $office)
               {
                    $response .= "<option value='{$office['office_id']}'>{$office['office_name']}</option>";
               }
               echo $response;
           }
       }
        */
        if(session::isLogin())
        {
           if(isset($_POST['type']) && isset($_POST['action']) && isset($_POST['source']))
           {
               if($_POST['type'] == "employee" && $_POST['action'] == "search" && $_POST['source'] == "admin" )
               {
                   if(isset($_SESSION[__CLASS__.'adminId']))
                   {
                       $admin = $_SESSION[__CLASS__.'adminId'];
                   }
                   else 
                   {
                       $admin = session::getAdminId();
                   }
                   
                   $query = securestr::clean($_POST['query']);
                   if(!empty($query))
                    {
                        $_POST['submit'] = '';
                        $_POST['admin'] = $admin;
                        
                        $this->isError = FALSE;
                        $this->model = $this->loadModel("employee");
                        $employees = $this->model->searchEmployee($admin, $query);
                        if(sizeof($employees) > 0)
                        {
                            $total = $this->model->countEmployees($admin, TRUE);
                            $offset = 0;
                            $limit = $total;
                            $next = 0;
                            $previous = 0;
                            $current = 1;
                            $this->data = compact("employees", "values", "limit", "offset", "total", "query", "next", "previous", "current");
                            ob_start();
                            $this->loadView(str_replace("Controller", "", __CLASS__), "view", $this->data);
                            $content = ob_get_clean();
                            /*var_dump($content);
                            $dochtml = new DOMDocument();
                            $dochtml->loadHTML($content);
                            $element = $dochtml->getElementById('employees');
                            $response = $element->nodeValue;*/
                            $response = $content;
                        }
                        else 
                        {
                            $this->isError = TRUE;
                            $response = "Noting Found";
                        }
                    } 
                    else 
                    {
                        $this->isError = TRUE;
                        $response = "Noting Found";
                    }
               } 
               elseif($_POST['type'] == "employee" && $_POST['action'] == "select" && $_POST['source'] == "admin" && isset($_POST['input']))
               { 
                    if(isset($_SESSION['attendanceControlleradminId']))
                    {
                        $admin = $_SESSION['attendanceControlleradminId'];
                    }
                    else 
                    {
                        $admin = session::getAdminId();
                    }

                    $query = securestr::clean($_POST['input']);

                    $this->model = $this->loadModel("employee");
                    $employees = $this->model->searchEmployee($admin, $query);

                    if(sizeof($employees) > 0)
                    {
                        $response = $employees;
                    }
                    else 
                    {
                        $response = NULL;
                        $this->isError = TRUE;
                    }
               }
               else 
               {
                    $this->isError = TRUE;
                    $response = "Nothing Found";
               }
           }
           else 
            {
                 $this->isError = TRUE;
                 $response = "Nothing Found";
            }
        }
        else 
        {
            $this->isError = TRUE;
            $response = "You're Not Login";
        }
        
        $this->data = "";
        $this->data['error'] = $this->isError;
        $this->data['response'] = $response;
        
        echo json_encode($this->data);
   }
   
   public function view($perform = '', $limit = 10, $offset = 0)
   {
       if(session::isLogin())
        {
           if($perform == 'success' || $perform == 'load' && isset($_SESSION[__CLASS__.'adminId']))
           {
               $_POST['submit'] = '';
               $_POST['admin'] = $_SESSION[__CLASS__.'adminId'];
           }
           
            if(!is_numeric($limit) || !is_numeric($offset) || $limit <= 0 || $offset < 0)
            {
                $limit = 10;
                $offset = 0;
            }
           
           if(isset($_POST['submit']))
            {
                //validate admin
                if(isset($_POST['admin']))
                {
                    if(is_numeric($_POST['admin']))
                    {
                        $this->model = $this->loadModel("admin");
                        if($this->model)
                        {
                            if($this->model->adminExists($_POST['admin'], _CLIENT_,TRUE))
                            {
                                $admin = $_POST['admin'];
                                session::sessionStart();
                                $_SESSION[__CLASS__.'adminId'] = $admin;
                            }
                            else
                            {
                               $this->isError = true;
                               $this->errorMsgs['admin_error'] = "Company Does Not Exists.";
                            }
                        }                    
                        else
                        {
                           $this->isError = true;
                           $this->errorMsgs['admin_error'] = "Company does Not Exists";
                        }
                        $values['admin'] = $_POST['admin'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['admin_error'] = "Invalid Company Selected";
                    }
               }
               else
               {
                   $this->isError = true;
                   $this->errorMsgs['admin_error'] = "Company Name Required";
               }
               
              /* //validate office
               
                if(isset($_POST['office']))
                {
                    if(is_numeric($_POST['office']))
                    {
                        $this->model = $this->loadModel("office");
                        if($this->model)
                        {
                            if($this->model->officeExists($_POST['office']))
                            {
                                
                               $office =  $this->model->officeToName($_POST['office']);
                               $officeId = $_POST['office'];
                            }
                            else
                            {
                               $this->isError = true;
                               $this->errorMsgs['office_error'] = "office Does Not Exists.";
                            }
                        }                    
                        else
                        {
                           $this->isError = true;
                           $this->errorMsgs['office_error'] = "office does Not Exists";
                        }
                        $values['office'] = $_POST['office'];
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['office_error'] = "Invalid office Selected";
                    }
               }*/
               
               if($this->isError)
               {
                    unset($_POST['submit']);
                    $adminModel = $this->loadModel("admin");
                    if($adminModel)
                    {
                        $admins = $adminModel->viewAdmins();
                    }
                   $error = $this->errorMsgs;
                   $csrf = csrf::generateCsrf(__FUNCTION__);
                   $this->data = compact("admins", "office",  "csrf", "error", "values");
                   $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
               }
               else
               {
                    $this->model = $this->loadModel(str_replace("Controller", "",__CLASS__));
                    $employees = $this->model->viewEmployee($admin, TRUE, $offset, $limit);
                    $total = $this->model->countEmployees($admin, TRUE);
                    $next = $offset + $limit;
                    if($next > $total)
                    {
                        $next = $offset;
                    }
                    $previous = $offset - $limit;
                    if($previous < 0)
                    {
                        $previous = 0;
                    }
                    $current = ceil($offset / $limit);
                    $current++;
                    if($current == 0)
                    {
                        $current = 1;
                    }
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("csrf","office", "employees", "values", "limit", "offset", "total", "next", "previous", "current");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                }
            }
            else
            {
               $this->javascripts[] = "{$this->config['domain']}"._PUBLIC_PATH_."js/jquery.validate.min.js";
                //loading countries model
                $adminModel = $this->loadModel("admin");
                $admins = [];

                if($adminModel)
                {
                    $admins = $adminModel->viewAdmins();
                }
                $error = $this->errorMsgs;
                $csrf = csrf::generateCsrf(__FUNCTION__);
                $this->data = compact("admins", "csrf", "error", "values");
                $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
            }
        }
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
            die();
        }
   }
   //employee edit
   
   public function edit($employe = '')
   { 
        if(session::isLogin())
        {
            if(is_numeric($employe))
            {
                if(isset($_POST['submit']))
                {
                    if(isset($_POST['csrf']))
                    {
                        if(!csrf::validateCsrf($_POST['csrf'], "register"))
                        {
                            $this->errorMsgs;
                            $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                        }
                    }
                    else
                    {
                        $this->errorMsgs;
                        $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                    }
                    //validating first name
                    if(isset($_POST['firstname']) && !@empty($_POST['firstname']))
                    {
                        if(ctype_alpha($_POST['firstname']))
                        {
                            $firstName = $_POST['firstname'];
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['firstname_error'] = "first name is only alpha";
                        }
                        $values['firstname'] = securestr::clean($_POST['firstname']);
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['firstname_error'] = "first name is required";
                    }

                    //validating last name

                    if(isset($_POST['lastname']) && !@empty($_POST['lastname']))
                    {
                        if(ctype_alpha($_POST['lastname']))
                        {
                            $lastName = $_POST['lastname'];
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['lastname_error'] = "last name is only alpha";
                        }
                        $values['lastname'] = securestr::clean($_POST['lastname']);
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['lastname_error'] = "last name is required";
                    }

                    //office grace time

                    if(isset($_POST['emp_grace_time']))
                    {
                        if(is_numeric($_POST['emp_grace_time']))
                        {
                            $emp_grace_time = $_POST['emp_grace_time'];
                        }
                        else
                        {
                            $emp_grace_time = 0;
                        }
                        $values['emp_grace_time'] = securestr::clean($_POST['emp_grace_time']);
                    }
                    //break grace time

                    if(isset($_POST['break_time']))
                    {
                        if(is_numeric($_POST['break_time']) && $_POST['break_time'] > 0)
                        {
                            if($_POST['break_time'] <= 60)
                            {
                                $break_time = $_POST['break_time'];
                            }
                            else 
                            {
                                $this->isError = true;
                                $this->errorMsgs['break_time_error'] = "Break Duration Should Between 0-60.";
                            }
                        }
                        else
                        {
                            $break_time = 0;
                        }
                        $values['break_time'] = securestr::clean($_POST['break_time']);
                    }
                    //valedate timing from

                    if(isset($_POST['timing_from']) && !@empty($_POST['timing_from']))
                    {
                        if(preg_match('#^([01][0-9])|(2[0-3])(:[0-5][0-9]){1,2}$#', $_POST['timing_from']))
                        {
                            $timing_from = strtotime($_POST['timing_from']);
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['timing_from_error'] = "time  is only in numaric values";
                        }
                        $values['timing_from'] = securestr::clean($_POST['timing_from']);
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['timing_from_error'] = " time  is required";
                    }

                    // validate timing to

                    if(isset($_POST['timingto']) && !@empty($_POST['timingto']))
                    {
                        if(preg_match('#^([01][0-9])|(2[0-3])(:[0-5][0-9]){1,2}$#',$_POST['timingto']))
                        {
                            $timing_to = strtotime($_POST['timingto']);
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['timing_to_error'] = "time is only in numaric values";
                        }
                        $values['timingto'] = securestr::clean($_POST['timingto']);
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['timing_to_error'] = "time to is required";
                    }
                    
                    if(isset($timing_from) && isset($timing_to))
                    {
                        if($timing_from > $timing_to)
                        {
                            $this->isError = true;
                            $this->errorMsgs['error'] = "Shift Start Time Cannot Be Greater Then Shift Ending Time";
                        }
                    }
                    //validate pay rate

                    if(isset($_POST['pay_rate']) && !@empty($_POST['pay_rate']))
                    {
                        if(is_numeric($_POST['pay_rate']))
                        {
                            $pay_rate = $_POST['pay_rate'];
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['pay_rate_error'] = "pay rate is onli in numaric values";
                        }
                        $values['pay_rate'] = securestr::clean($_POST['pay_rate']);
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['pay_rate_error'] = "pay rate is required";
                    }
                    
//                    if(isset($_POST['dept']) && !@empty($_POST['dept']))
//                    {
//                        if(is_numeric($_POST['dept']))
//                        {
//                            $this->model = $this->loadModel("department");
//                            if($this->model->adminToDeptRel(session::getAdminId(), $_POST['dept']))
//                            {
//                                $department = $_POST['dept'];
//                            }
//                            else
//                            {
//                                $this->isError = true;
//                                $this->errorMsgs['dept_error'] = "Invalid Department Selected";
//                            }
//                        }
//                        else
//                        {
//                            $this->isError = true;
//                            $this->errorMsgs['dept_error'] = "Invalid Department Selected";
//                        }
//                        $values['dept'] = securestr::clean($_POST['dept']);
//                    }
//                    else
//                    {
//                        $this->isError = true;
//                        $this->errorMsgs['dept_error'] = "Department is required";
//                    }
//                    
//                    if(isset($_POST['type']) && !@empty($_POST['type']))
//                    {
//                        if(is_numeric($_POST['type']))
//                        {
//                            $this->model = $this->loadModel("employee_types");
//                            if($this->model->typeExists($_POST['type']))
//                            {
//                                $type = $_POST['type'];
//                            }
//                            else
//                            {
//                                $this->isError = true;
//                                $this->errorMsgs['type_error'] = "Invalid Employee Type Selected";
//                            }
//                        }
//                        else
//                        {
//                            $this->isError = true;
//                            $this->errorMsgs['type_error'] = "Invalid Employee Type Selected";
//                        }
//                        $values['type'] = securestr::clean($_POST['type']);
//                    }
//                    else
//                    {
//                        $this->isError = true;
//                        $this->errorMsgs['type_error'] = "Employee Type is required";
//                    }
                    $department = 1;
                    $type = 1;
                    //validate over time pay rate
                    
                    /*
                    if(isset($_POST['overtime_pay_rate']) && !@empty($_POST['overtime_pay_rate']))
                    {
                        if(is_numeric($_POST['overtime_pay_rate']))
                        {
                            $overtime_pay_rate = $_POST['overtime_pay_rate'];
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['overtime_pay_rate_error'] = "overtime pay rate is only in numaric values";
                        }
                        $values['overtime_pay_rate'] = securestr::clean($_POST['overtime_pay_rate']);
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['overtime_pay_rate_error'] = "overtime pay rate is required";
                    }
                    */$overtime_pay_rate = 0;
                    //validate pin 

                    if(isset($_POST['pin']) && !@empty($_POST['pin']))
                    {
                        if(is_numeric($_POST['pin']))
                        {
                            if(strlen($_POST['pin']) == 4)
                            {
                                $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                                if(!$this->model->pinExistsExceptThis($this->model->employeeToAdmin($employe), $employe, $_POST['pin'] ))
                                {
                                    $pin = $_POST['pin'];
                                }
                                else
                                {
                                    $this->isError = true;
                                    $this->errorMsgs['pin_error'] = "pin is already exists";
                                }
                            }
                            else
                            {
                                $this->isError = true;
                                $this->errorMsgs['pin_error'] = "pin length must be 4";
                            }
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['pin_error'] = "pin is only in numeric values";
                        }
                        $values['pin'] = securestr::clean($_POST['pin']);
                    }
                    else
                    {
                        $this->isError = true;
                        $this->errorMsgs['pin_error'] = "pin is required";
                    }
                    
                    //Checking Email
                    $email = '';
                    if(isset($_POST['email']) && !@empty($_POST['email']))
                    {
                        if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
                        { 
                            $this->model = $this->loadModel("employee");
                            $userId = $this->model->idToUserId($employe);
                            $this->model = $this->loadModel("email");
                            if(!$this->model->emailExistsExceptThis($_POST['email'], $userId))
                            {
                                $email = $_POST['email'];
                                if(!$this->model->useridToEmail($userId))
                                {
                                    $this->model->insertEmail($userId, $email);
                                }
                            }
                            else
                            {
                                $this->isError = true;
                                $this->errorMsgs['email_error'] = "Email Already Exists";
                            }
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['email_error'] = "Invalid Email Address";
                        }
                        $values['email'] = securestr::clean($_POST['email']);
                    }
//                    else
//                    {
//                        $this->isError = true;
//                        $this->errorMsgs['email_error'] = "Email is Required";
//                    }
                
                    if(!$this->isError)
                    {
                        $this->model = $this->loadModel(str_replace("Controller", "",__CLASS__));
                        if($this->model->adminToEmployeeRel(session::getAdminId(), $employe))
                        {
                            $res['response'] .= "Successfully Updated";
                            $employee = $this->model->update($employe, $firstName, $lastName, $timing_from, $timing_to, $pay_rate, $overtime_pay_rate, $emp_grace_time, $pin, $break_time, $email, $department, $type);
                            $employee = $employee[0];
                            
                            //Science
                            if($employee)
                            {
                                $_POST['success'] = $res['response'];
                                if(!isset($_SESSION[__CLASS__.'adminId']))
                                {
                                    $_SESSION[__CLASS__.'adminId'] = $employee['admin_id'];
                                }
                                $controller = $this->loadController("employee");
                                $controller->view('success');
                                die();
                            }
                            else 
                            {
                                $this->errorMsgs['error'] = "Something Went Wrong. Please Try Again.";
                                $error = $this->errorMsgs;
                            }
                            //Science End
                            $admin_id = $this->model->employeeToAdmin($employe);
                            $this->model = $this->loadModel("admin");
                            $username = $this->model->idtoUsername($admin_id);
                            $globalGracetime = $this->model->getGraceTime($admin_id);
                            $this->model = $this->loadModel("department");
                            $departments = $this->model->view($username);
                            $this->model = $this->loadModel("employee_types");
                            $types = $this->model->loadTypes();
                            $this->data = compact("employee", "employe", "values", "res", "globalGracetime", "departments", "types");
                            $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
                        }
                        else 
                        {
                            $controller = $this->loadController("error");
                            if($controller)
                            {
                                $controller->_custom("Something Went Wrong.", "Something Went Wrong. Please Try Again Later.");
                            }
                        }
                    }
                    else
                    { 
                        $error = $this->errorMsgs;
                        $this->model = $this->loadModel(str_replace("Controller", "",__CLASS__));
                        $admin_id = $this->model->employeeToAdmin($employe);
                        $employee = $this->model->getEmployee($employe);
                        $employee = $employee[0];
                        $this->model = $this->loadModel("admin");
                        $username = $this->model->idtoUsername($admin_id);
                        $globalGracetime = $this->model->getGraceTime($admin_id);
                        $this->model = $this->loadModel("department");
                        $departments = $this->model->view($username);
                        $this->model = $this->loadModel("employee_types");
                        $types = $this->model->loadTypes();
                        $this->data = compact("employee", "employe", "values" , "error", "globalGracetime", "departments", "types");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
                    }
                }
                else
                {
                    $this->model = $this->loadModel(str_replace("Controller", "",__CLASS__));
                    if($this->model->adminToEmployeeRel(session::getAdminId(), $employe))
                    {
                        $admin_id = $this->model->employeeToAdmin($employe);
                        $employee = $this->model->getEmployee($employe);
                        $employee = $employee[0];
                        $this->model = $this->loadModel("admin");
                        $username = $this->model->idtoUsername($admin_id);
                        $globalGracetime = $this->model->getGraceTime($admin_id);
                        $this->model = $this->loadModel("department");
                        $departments = $this->model->view($username);
                        $this->model = $this->loadModel("employee_types");
                        $types = $this->model->loadTypes();
                        $csrf = csrf::generateCsrf(__FUNCTION__);
                        $this->data = compact("employee", "values", "error", "globalGracetime", "departments", "types");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
                    }
                    else 
                    {
                        $controller = $this->loadController("error");
                        if($controller)
                        {
                            $controller->_custom("Something Went Wrong.", "Something Went Wrong. Please Try Again Later.");
                        }
                    }
                }
            }
            else 
            {
                $controller = $this->loadController("error");
                if($controller)
                {
                    $controller->_custom("Something Went Wrong.", "Something Went Wrong. Please Try Again Later.");
                }
            }
        }
        else
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
            die();
        }
   }
   
   //actions 
    public function action($empId = '', $action = '')
    {
        if(session::isLogin())
        {
            $action = strtolower($action);
            if($action == "delete")
            {
                if(is_numeric($empId))
                {
                    $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));

                    if($this->model)
                    {
                        //if($this->model->adminExists($oofice, _OFFICE_))
                        //{
                       $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                       if($this->model->adminToEmployeeRel(session::getAdminId(), $empId))
                       {
                            $performed = $this->model->actions($empId ,$action);
                            if($performed)
                            {
                                ob_get_clean();
                                header("Location: {$this->config['domain']}"._PUBLIC_PATH_."employee/view/success"); 
                                die();
                            }
                            else
                            {
                                $controller = $this->loadController("error");
                                if($controller)
                                {
                                    $controller->_custom("Something Went Wrong.", "Something Went Wrong. Please Try Again Later.");
                                }
                            }
                       }
                       else
                       {
                            $controller = $this->loadController("error");
                            if($controller)
                            {
                                $controller->_custom("Something Went Wrong.", "Something Went Wrong. Please Try Again Later.");
                            }
                       }
                        /*}
                        else
                        {
                            $controller = $this->loadController("error");
                            if($controller)
                            {
                                $controller->_custom("Office Does Not Exists.", "Office Does Not Exists. Please Check The Spellings.");
                            }
                        }*/
                    }
                }
                else
                {
                     $controller = $this->loadController("error");
                     if($controller)
                     {
                         $controller->_custom("Something Went Wrong.", "Something Went Wrong. Please Try Again Later.");
                     }
                }
            }
            else if($action == "activate" || $action == "deactivate")
            {
                if(is_numeric($empId))
                {
                    $this->model = $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                    if($this->model->adminToEmployeeRel(session::getAdminId(), $empId))
                    {
                        if($this->model)
                        {
                            if($action == "activate")
                            {
                                if(!$this->model->isActive($empId))
                                {
                                    $this->model = $this->loadModel("check_in");
                                    $last = $this->model->lastCheckinWithoutCheckout($empId);
                                    $unCheckedouts = $this->model->checkinWithoutCheckout($empId);
                                    $checkinTime = $this->model->checkinTimestamp($last['check_in_id']);
                                    //if(is_array($last))
                                    if(is_array($unCheckedouts))
                                    {
                                        //$this->model = $this->loadModel("check_out");
                                        //$checkedOut = $this->model->checkoutDone($last['check_in_id']);
                                        $checkedOut = FALSE;
                                    }
                                    else 
                                    {
                                        $checkedOut = TRUE;
                                    }
                                    if($checkedOut)
                                    {
                                        $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                                        $performed = $this->model->activateEmp($empId);
                                        if($performed)
                                        {
                                            ob_get_clean();
                                            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."employee/view/success"); 
                                            die();
                                        }
                                        else 
                                        {
                                            $controller = $this->loadController("error");
                                            if($controller)
                                            {
                                                $controller->_custom("Something Went Wrong.", "Something Went Wrong. Please Try Again Later.");
                                            }
                                        }
                                    }
                                    else 
                                    {
                                        $values['url'] = $empId."/".$action;
                                        if(isset($_POST['activate']))
                                        {
                                            if(isset($_POST['csrf']))
                                            {
                                                if(!csrf::validateCsrf($_POST['csrf'], __FUNCTION__))
                                                {
                                                    $this->errorMsgs;
                                                    $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                                                }
                                            }
                                            else
                                            {
                                                $this->errorMsgs;
                                                $this->errorMsgs['csrf_error'] = "Invalid or Expired CSRF token";
                                            }
                                            if(isset($_POST['checkout']))
                                            {
                                                $checkoutTime = '';
                                                if(!is_array($_POST['checkout']))
                                                {
                                                    if(preg_match("/^(\d{2})\/(\d{2})\/(\d{4}) (\d{2}|\d{1}):(\d{2}|\d{1})$/", $_POST['checkout']))
                                                    {
                                                        $checkoutTime = strtotime(str_replace("/", "-", $current));
                                                    }
                                                    else 
                                                    {
                                                        $this->isError = true;
                                                        $this->errorMsgs['checkout_error'] = "Invalid Date Time Format";
                                                        $values['checkout'] = securestr::clean($_POST['checkout']);
                                                    }
                                                }
                                                else 
                                                {
                                                    foreach($_POST['checkout'] as $current)
                                                    {
                                                        if(preg_match("/^(\d{2})\/(\d{2})\/(\d{4}) (\d{2}|\d{1}):(\d{2}|\d{1})$/", $current))
                                                        {
                                                            $checkoutTime[] = strtotime(str_replace("/", "-", $current));
                                                        }
                                                        else 
                                                        {
                                                            $this->isError = true;
                                                            $this->errorMsgs['checkout_error'][] = "Invalid Date Time Format";
                                                            $this->errorMsgs['error'] = "Invalid Date Time Format";
                                                            $values['checkout'][] = securestr::clean($current);
                                                        }
                                                    }
                                                }
                                            }
                                            else 
                                            {
                                                $this->isError = true;
                                                $this->errorMsgs['checkout_error'] = "Checkout Time Is Required.";
                                            }
                                            
                                            if(!$this->isError)
                                            {
                                                $missings = sizeof($unCheckedouts);
                                                if($missings != (sizeof($checkoutTime)+1))
                                                {
                                                    $this->isError = true;
                                                    $this->errorMsgs['error'] = "Something Went Wrong. Please Try Again.";
                                                }
                                                elseif(!is_array($_POST['checkout']))
                                                {
                                                    if($checkinTime < $checkoutTime)
                                                    {
                                                        $offset = $unCheckedouts['timezone'];
                                                        list($hours, $minutes) = explode(':', $offset);
                                                        $seconds = $hours * 60 * 60 + $minutes * 60;
                                                        $this->model = $this->loadModel("check_out");
                                                        if($this->model->insertCheckOut($last['check_in_id'], $checkoutTime - $seconds))
                                                        {
                                                            $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                                                            if($this->model->activateEmp($empId))
                                                            {
                                                                $res['response'] = "Employee is Activated Successfully.";
                                                                $this->model = $this->loadModel(str_replace("Controller", "",__CLASS__));
                                                                $employee = $this->model->getEmployee($empId);
                                                                $this->data = compact("res", "employee");
                                                                $this->loadView(str_replace("Controller", "", __CLASS__), "details", $this->data);
                                                                die();
                                                            }
                                                            else 
                                                            {
                                                                $this->isError = true;
                                                                $this->errorMsgs['error'] = "Something Went Wrong. Please Try Again.";
                                                            }
                                                        }
                                                        else 
                                                        {
                                                            $this->isError = true;
                                                            $this->errorMsgs['error'] = "Something Went Wrong. Please Try Again.";
                                                        } 
                                                    }
                                                    else 
                                                    {
                                                        $this->isError = true;
                                                        $this->errorMsgs['error'] = "Check-out Time Cannot Be Shorter Then Check-in Time.";
                                                    } 
                                                }
                                                else 
                                                {
                                                    $this->model = $this->loadModel("check_out");
                                                    $this->model->db->transaction();
                                                    for($i = 0; $i < $missings-1; $i++)
                                                    {
                                                        $this->model = $this->loadModel("check_in");
                                                        if($checkoutTime[$i] > $this->model->checkinTimestamp($unCheckedouts[$i]['check_in_id']))
                                                        { 
                                                            $offset = $unCheckedouts[$i]['timezone'];
                                                            list($hours, $minutes) = explode(':', $offset);
                                                            $seconds = $hours * 60 * 60 + $minutes * 60;
                                                            $this->model = $this->loadModel("check_out");
                                                            $this->model->insertCheckOut($unCheckedouts[$i]['check_in_id'], $checkoutTime[$i] - $seconds);
                                                            if($i == ($missings-2))
                                                            {
                                                                if($this->model->db->commit())
                                                                {
                                                                    $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                                                                    if($this->model->activateEmp($empId))
                                                                    {
                                                                        $res['response'] = "Employee is Activated Successfully.";
                                                                        $this->model = $this->loadModel(str_replace("Controller", "",__CLASS__));
                                                                        $employee = $this->model->getEmployee($empId);
                                                                        $this->data = compact("res", "employee");
                                                                        $this->loadView(str_replace("Controller", "", __CLASS__), "details", $this->data);
                                                                        die();
                                                                    }
                                                                    else 
                                                                    {
                                                                        $this->isError = true;
                                                                        $this->errorMsgs['error'] = "Something Went Wrong. Please Try Again.";
                                                                    }
                                                                }
                                                                else 
                                                                {
                                                                    $this->isError = true;
                                                                    $this->errorMsgs['error'] = "Something Went Wrong. Please Try Again.";
                                                                } 
                                                            }
                                                        }
                                                        else 
                                                        {
                                                            $this->isError = true;
                                                            $this->errorMsgs['error'] = "Check-out Time Cannot Be Shorter Then Check-in Time.";
                                                        }
                                                    }
                                                }
                                            }
                                        } 
                                        else 
                                        {
                                            
                                        }
                                        
                                        $this->model = $this->loadModel(str_replace("Controller", "",__CLASS__));
                                        $csrf = csrf::generateCsrf(__FUNCTION__);
                                        $error = $this->errorMsgs;
                                        $this->data = compact("csrf", "action", "error", "values", "unCheckedouts");
                                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                                    }
                                }
                                else 
                                {
                                    $error['error'] = "Employee Is Already Acticve.";
                                    $this->model = $this->loadModel(str_replace("Controller", "",__CLASS__));
                                    $employee = $this->model->getEmployee($empId);
                                    $this->data = compact("error", "employee");
                                    $this->loadView(str_replace("Controller", "", __CLASS__), "details", $this->data);
                                }
                            }
                            else 
                            {
                                if($this->model->isActive($empId))
                                {
                                    
                                    $performed = $this->model->deactivateEmp($empId);
                                    if($performed)
                                    {
                                        ob_get_clean();
                                        header("Location: {$this->config['domain']}"._PUBLIC_PATH_."employee/view/success"); 
                                        die();
                                    }
                                    else 
                                    {
                                        $controller = $this->loadController("error");
                                        if($controller)
                                        {
                                            $controller->_custom("Something Went Wrong.", "Something Went Wrong. Please Try Again Later.");
                                        }
                                    }
                                }
                                else 
                                {
                                    
                                }
                            }
                            
                            $this->data = compact("error", "values");
                            $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                        }
                    }
                }
                else
                {
                     $controller = $this->loadController("error");
                     if($controller)
                     {
                         $controller->_custom("Something Went Wrong.", "Something Went Wrong. Please Try Again Later.");
                     }
                }
            }
        }
         else 
        {
            ob_get_clean();     
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login"); 
            die();
        }
    }
    public function details($employee_id = '')
    {
       if(session::isLogin())
       {
            if(is_numeric($employee_id))
            {
                $this->model = $this->loadModel(str_replace("Controller", "", __CLASS__));
                if($this->model->adminToEmployeeRel(session::getAdminId(), $employee_id))
                {
                    $employee = $this->model->getEmployee($employee_id);
                    $this->data = compact("employee");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                }
                else
                {
                    ob_get_clean();
                    header("location: {$this->config['domain']}"._PUBLIC_PATH_."employee/view");
                    die();
                    $this->model = $this->loadController("error");
                    $this->model->_custom("Something Went Wrong.", "Something Went Wrong. Please Try Again Later.");
                }
            }
      }
       else
       { 
           ob_get_clean();
           header("location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
           die();
       }
    }
    
    public function import($upload = '', $name = '')
   {
       if(session::isLogin())
        {
           if(empty($upload))
           {
                if(isset($_POST['submit']))
                {
                    //validate admin
                    if(isset($_POST['admin']))
                    {
                        if(is_numeric($_POST['admin']))
                        {
                            $this->model = $this->loadModel("admin");
                            if($this->model)
                            {
                                if($this->model->adminExists($_POST['admin'], _CLIENT_,TRUE))
                                {
                                    session::sessionStart();
                                    $admins = $_POST['admin'];
                                    $_SESSION[__CLASS__."add"] = $admins;
                                }
                                else
                                {
                                   $this->isError = true;
                                   $this->errorMsgs['admin_error'] = "Company Does Not Exists.";
                                }
                            }                    
                            else
                            {
                               $this->isError = true;
                               $this->errorMsgs['admin_error'] = "Company does Not Exists";
                            }
                            $values['admin'] = $_POST['admin'];
                        }
                        else
                        {
                            $this->isError = true;
                            $this->errorMsgs['admin_error'] = "Invalid Company Selected";
                        }
                   }
                   else
                   {
                       $this->isError = true;
                       $this->errorMsgs['admin_error'] = "Company Name Required";
                   }

                   if($this->isError)
                   {
                        unset($_POST['submit']);
                        $this->model = $this->loadModel("admin");
                        $admins = $this->model->viewAdmins();
                        $error = $this->errorMsgs;
                        $csrf = csrf::generateCsrf(__FUNCTION__);
                        $this->data = compact("admins", "office",  "csrf", "error", "values");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                   }
                   else
                   {
                        $this->javascripts[] = "{$this->config['domain']}"._PUBLIC_PATH_."assets/js/jquery.ui.widget.js";
                        $this->javascripts[] = "{$this->config['domain']}"._PUBLIC_PATH_."assets/js/jquery.iframe-transport.js";
                        $this->javascripts[] = "{$this->config['domain']}"._PUBLIC_PATH_."assets/js/jquery.fileupload.js";
                        $this->stylesheets[] = "{$this->config['domain']}"._PUBLIC_PATH_."assets/css/jquery.fileupload.css";
                        
                        $this->model = $this->loadModel(str_replace("Controller", "",__CLASS__));
                        $this->data = compact("upload");
                        $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__, $this->data);
                    }
                }
                else
                {
                    $this->model = $this->loadModel("admin");
                    $admins = $this->model->viewAdmins();
                    $error = $this->errorMsgs;
                    $csrf = csrf::generateCsrf(__FUNCTION__);
                    $this->data = compact("admins", "csrf", "error", "values");
                    $this->loadView(str_replace("Controller", "", __CLASS__), __FUNCTION__,  $this->data);
                }
           }
           elseif($upload == "upload")
           {
               if(isset($_FILES['files']))
               {
                    $extension = ['csv'];
                    $fileExt = pathinfo(basename($_FILES['files']['name']), PATHINFO_EXTENSION);
                    $_FILES['files']['name'] = "CSV_".time()."_".rand(999,9999).".".$fileExt; //Random Name
                    if(in_array(strtolower($fileExt), $extension))
                    {
                        $upload_handler = new UploadHandler();
                    }
                    else 
                    {
                        $res['files'][0]['name'] = "Invalid Extension Only CSV Allowed.";
                        $this->data = $res;
                        echo json_encode($this->data);
                        die();
                    }
               }
           }
           elseif($upload == "import" && !empty ($name))
           {
               $file = _SHARED_UPLOADS_."/temp/".$name;
               if(is_file($file))
               {
                    $this->model = $this->loadModel("employee");
                    $res['response'] = array();
                    $index = 0;
                    $csv = new PHPExcel();
                    $objReader = PHPExcel_IOFactory::createReader("CSV");
                    $objPHPExcel = $objReader->load($file);
                    //$worksheet = $objPHPExcel->getActiveSheet();
                    
                    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet)
                    {
                        $highestRow = $worksheet->getHighestRow(); // e.g. 10
                        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
                        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                        break;
                        //$res[] = PHPExcel_Cell::columnIndexFromString($highestColumn);
                    }
//                    echo $highestRow." => ".$highestColumn." => ".$highestColumnIndex." => $totalColumns";
//                    var_dump($worksheet);
                    if($highestColumnIndex == 9)
                    {
                        set_time_limit(0);
                        if($highestRow > 1)
                        {
                            $res['response']['is_error'] = FALSE;
                            $res['response']['response'] = $highestRow;
                                    
                            for($row = 2; $row <= $highestRow; $row++)
                            {
                                $col = 1;
                                $cell = $worksheet->getCellByColumnAndRow($col++, $row);
                                $_POST['firstname'] = $cell->getValue();
                                $cell = $worksheet->getCellByColumnAndRow($col++, $row);
                                $_POST['lastname'] = $cell->getValue();
//                                $cell = $worksheet->getCellByColumnAndRow($col++, $row);
//                                $_POST['dept'] = $cell->getValue();
//                                $cell = $worksheet->getCellByColumnAndRow($col++, $row);
//                                $_POST['type'] = $cell->getValue();
                                $cell = $worksheet->getCellByColumnAndRow($col++, $row);
                                $_POST['timing_from'] = $cell->getValue();
                                $cell = $worksheet->getCellByColumnAndRow($col++, $row);
                                $_POST['timing_to'] = $cell->getValue();
                                $cell = $worksheet->getCellByColumnAndRow($col++, $row);
                                $_POST['pay_rate'] = $cell->getValue();
                                $cell = $worksheet->getCellByColumnAndRow($col++, $row);
                                $_POST['emp_grace_time'] = $cell->getValue();
                                $cell = $worksheet->getCellByColumnAndRow($col++, $row);
                                $_POST['break_time'] = $cell->getValue();
                                $cell = $worksheet->getCellByColumnAndRow($col++, $row);
                                $_POST['email'] = $cell->getValue();
                                
                                $_POST['csv_upload_check'] = "AppRoveD";
                                $_POST['employee'] = "";
                                
                                $this->isError = FALSE;
                                $this->errorMsgs = [];
                                
                                $res[$row]['is_error'] = $this->add();
                                if($res[$row]['is_error'])
                                {
                                    $res[$row]['errors'] = $this->errorMsgs;
                                }
                                else 
                                {
                                    $res[$row]['response'] = "Employee Added Successfully.";
                                }
                                
                                $this->model->db->__destruct();
                                $this->model->db->__construct();
//                                $this->model->db->is_true = TRUE;
//                                $this->model->db->is_found = FALSE;
//                                for($col = 0; $col < $highestColumnIndex; ++$col)
//                                {
//                                    $cell = $worksheet->getCellByColumnAndRow($col, $row);
//                                    $val = $cell->getValue();
//                                }
                            }
                        }
                        else
                        {
                            $res['response']['is_error'] = TRUE;
                            $res['response']['response'] = "CSV is Empty.";
                            //empty file
                        }
                    }
                    else 
                    {
                        $res['response']['is_error'] = TRUE;
                        $res['response']['response'] = "Invalid CSV Format. Please Download Sample.";
                        //invalid format
                    }
                    
//                    foreach ($worksheet->getRowIterator() as $row)
//                    {
//                        $res[] = serialize($row);
//                    }
                    @unlink($file);
                    echo json_encode($res);
               }
               else 
               {
                    $res['response']['is_error'] = TRUE;
                    $res['response']['response'] = "Something Went Wrong. Please Try Again.";
                    echo json_encode($res);
               }
           }
           elseif($upload == "download")
           {
               $file = _SHARED_UPLOADS_."/temp/Sample_Employee_Import.csv";
               if (is_file($file)) {
                    header('Content-Description: File Transfer');
                    header('Content-Type: application/csv');
                    header('Content-Disposition: attachment; filename='.basename($file));
                    header('Content-Transfer-Encoding: binary');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($file));
                    readfile($file);
                    exit;
                }
           }
        }
        else 
        {
            ob_get_clean();
            header("Location: {$this->config['domain']}"._PUBLIC_PATH_."access/login");
            die();
        }
   }
}
