      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
                  <li class="sub-menu">
                      <a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>home/index" data-pg="home/index">
                          <i class="sidebar_li"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/dashbord_icon_1.png"> </i>
                          <span class="color_error_fix_sidebar">Dashboard</span>
                          <i class="sidebar_li pull-right"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/2419down_arrow.png"> </i>
                      </a>
                      <ul class="sub">
                            <li><a  href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>home/index">Home</a></li>
                            <li><a  href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>attendance/live">Live Statistics</a></li>
                      </ul>
                  </li>
                  <?php if(session::getUserType() == _ADMIN_){?>
                  <li class="sub-menu">
                      <a href="javascript:;">
                          <i class="sidebar_li"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/employs_icon.png"> </i>
                          <span class="color_error_fix_sidebar">Companies</span>
                          <i class="sidebar_li pull-right"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/2419down_arrow.png"> </i>
                      </a>
                      <ul class="sub">
                          <li><a  href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>access/register" data-pg="access/register">Add Company</a></li>
                          <li><a  href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>admin/view" data-pg="admin/view">View Companies</a></li>
                      </ul>
                  </li>
                  <?php } ?>
                  <li class="sub-menu">
                      <a href="javascript:;" data-pg="office">
                          <i class="sidebar_li"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/sites_icon.png" > </i>
                          <span class="color_error_fix_sidebar">Sites/Offices</span>
                          <i class="sidebar_li pull-right"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/2419down_arrow.png"> </i>
                      </a>
                      <ul class="sub">
                          <li><a  href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>office/add" data-pg="office/add">Add Sites/Offices</a></li>
                          <li><a  href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>office/view" data-pg="office/view">View Sites/Offices</a></li>
                      </ul>
                  </li>
<!--                  <li class="sub-menu">
                      <a href="javascript:;" data-pg="department">
                          <i class="sidebar_li"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/dept_icon.png" > </i>
                          <span class="color_error_fix_sidebar">Departments</span>
                          <i class="sidebar_li pull-right"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/2419down_arrow.png"> </i>
                      </a>
                      <ul class="sub">
                          <li><a  href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>department/add" data-pg="department/add">Add Departments</a></li>
                          <li><a  href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>department/view" data-pg="department/view">View Departments</a></li>
                      </ul>
                  </li>-->
                  <li class="sub-menu">
                      <a href="javascript:;" data-pg="employee">
                         <i class="sidebar_li"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/clients.png"> </i>
                          <span class="color_error_fix_sidebar">Employees</span>
                          <i class="sidebar_li pull-right"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/2419down_arrow.png"> </i>
                      </a>
                      <ul class="sub">
                          <li><a  href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>employee/add" data-pg="employee/add">Add Employee</a></li>
                          <li><a  href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>employee/view" data-pg="employee/view">View Employees</a></li>
                          <li><a  href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>employee/import" data-pg="employee/view">Import CSV</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" data-pg="attendance">
                          <i class="sidebar_li"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/reports_icon.png"> </i>
                          <span class="color_error_fix_sidebar">Reports</span>
                          <i class="sidebar_li pull-right"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/2419down_arrow.png"> </i>
                      </a>
                      <ul class="sub">
                          <li><a  href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>attendance/view" data-pg="attendance/view">Employees Attendance</a></li>
                          <li><a  href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>attendance/offices" data-pg="attendance/view">Site/Office Attendance</a></li>
                          <li><a  href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>attendance/reports" data-pg="attendance/reports">Download Reports</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>home/support" data-pg="home/support">
                           <i class="sidebar_li"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/support_icon.png"> </i>
                          <span class="color_error_fix_sidebar">Support</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;">
                          <i class="sidebar_li"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/setting_icon.png" > </i>
                          <span class="color_error_fix_sidebar">Setting</span>
                          <i class="sidebar_li pull-right"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/2419down_arrow.png"> </i>
                      </a>
                    <ul class="sub">
                        <li><a  href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>admin/edit" data-pg="admin/edit">Profile</a></li>
                        <li><a  href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>admin/api" data-pg="admin/api">Device Login</a></li>
                        <li><a  href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>admin/devices" data-pg="admin/devices">Devices</a></li>
                        <li><a  href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>admin/logo" data-pg="admin/logo">Company Logo</a></li>
                        <li><a  href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>admin/gracetime" data-pg="settings/gracetime">Check-in Buffer Time</a></li>
                        <li id="myChild"> <a href="javascript:;" class="childs-parent">Broadcast Notifications</a>
                            <ul class="sub">
                                <li><a  href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>admin/message" style="margin-right:5px" data-pg="admin/message"><span class="dropdown_sidebar_color">Entire Company</span></a></li>                          
                                <li><a  href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>office/message" style="margin-right:5px" data-pg="office/message"><span class="dropdown_sidebar_color">Specific Sites/Offices</span></a></li>
                            </ul>
                        </li>
                    </ul>
                  </li>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
<!--main content start-->
<section id="main-content" style="margin-bottom: 50px">
      
	