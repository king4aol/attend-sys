<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of access
 *
 * @author Msajid
 */
class access extends model {
    private $dbTable = "adminusers";
    private $created = false;
    
    //Just To Check Database ,Create If Not Exists
    private function dbcheck()
    { return TRUE;
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}`(`admin_id` int(11) NOT NULL AUTO_INCREMENT,
                    `username` varchar(255) NOT NULL UNIQUE,
                    `password` varchar(32) NOT NULL,
                    `first_name` varchar(50) NOT NULL,
                    `last_name` varchar(50) NOT NULL,
                    `office_grace_time` int(11) NOT NULL,
                    `break_time` int(11) NOT NULL,
                    `break_grace_time_per` float NOT NULL,
                    `company_name` varchar(255) NOT NULL,
                    `date_of_birth` varchar(255) NOT NULL,
                    `country_id` int(11) NOT NULL,
                    `admin_type` int(11) NOT NULL,
                    `membership_type_id` int(11) NOT NULL,
                    `membership_start_time` date NOT NULL,
                    `membership_end_time` date NOT NULL,
                    `permissions_mask` int(11) NOT NULL,
                    `logo_path` varchar(255) NOT NULL,
                    `a_cin_message` LONGTEXT NOT NULL,
                    `a_cout_message` LONGTEXT NOT NULL,
                    `a_bin_message` LONGTEXT NOT NULL,
                    `a_bout_message` LONGTEXT NOT NULL,
                    `p_hash` VARCHAR(32) NOT NULL,
                    `p_hash_expire` INT(11) NOT NULL,
                    `email_reports` INT(11) NOT NULL,
                    `status` TINYINT NOT NULL,
                    `added_date` datetime NOT NULL,
                    `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`admin_id`),
                    FOREIGN KEY (`country_id`) REFERENCES `{$this->config['db_table_prefix']}countries`(`country_id`),
                    FOREIGN KEY (`membership_type_id`) REFERENCES `{$this->config['db_table_prefix']}membership_types` (`type_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return TRUE;
    }
    
    //Create First Time
    public function firstCreate()
    {
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}`(`admin_id` int(11) NOT NULL AUTO_INCREMENT,
                    `username` varchar(255) NOT NULL UNIQUE,
                    `password` varchar(32) NOT NULL,
                    `first_name` varchar(50) NOT NULL,
                    `last_name` varchar(50) NOT NULL,
                    `office_grace_time` int(11) NOT NULL,
                    `break_time` int(11) NOT NULL,
                    `break_grace_time_per` float NOT NULL,
                    `company_name` varchar(255) NOT NULL,
                    `date_of_birth` date NOT NULL,
                    `country_id` int(11) NOT NULL,
                    `admin_type` int(11) NOT NULL,
                    `membership_type_id` int(11) NOT NULL,
                    `membership_start_time` date NOT NULL,
                    `membership_end_time` date NOT NULL,
                    `permissions_mask` int(11) NOT NULL,
                    `logo_path` varchar(255) NOT NULL,
                    `a_cin_message` LONGTEXT NOT NULL,
                    `a_cout_message` LONGTEXT NOT NULL,
                    `a_bin_message` LONGTEXT NOT NULL,
                    `a_bout_message` LONGTEXT NOT NULL,
                    `p_hash` VARCHAR(32) NOT NULL,
                    `p_hash_expire` INT(11) NOT NULL,
                    `email_reports` INT(11) NOT NULL,
                    `status` TINYINT NOT NULL,
                    `added_date` datetime NOT NULL,
                    `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`admin_id`),
                    FOREIGN KEY (`country_id`) REFERENCES `{$this->config['db_table_prefix']}countries`(`country_id`),
                    FOREIGN KEY (`membership_type_id`) REFERENCES `{$this->config['db_table_prefix']}membership_types` (`type_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return TRUE;
    }
    
    public function makeAdmin() 
    {
        $username = 'admin';
        $email = 'admin@otl.com';
        $phone = '+1234567890';
        $address = 'Admin User';
        $password = password::encryptPassword("OtlTekLabs123");
        $firstname= 'Tek';
        $lastname = 'Labs';
        $company = 'Tek Labs';
        $date = date("d-m-Y");
        require_once dirname(__FILE__)."/../model/countries.php";
        $model = new countries;
        $country = $model->isoToId("AT"); //Austrailia
        $admin = _ADMIN_;
        $type = 3; //Enterprise ID
        $bitMask = roles::getInstance()->adminMask();
        
        
        $this->db->transaction();
        
        require_once dirname(__FILE__)."/../model/membership_types.php";
        $model = new membership_types;
        
        require_once dirname(__FILE__)."/../model/access.php";
        $model = new access;
        $adminId = $model->insertAdmin($username, $password, $firstname, $lastname, $company, $date, $country, $admin, $type,  0, 0, $bitMask);
        
        require_once dirname(__FILE__)."/../model/users.php";
        //Inserting Into Users
        $model = new users;
        $userId = $model->insertUsers($adminId, _ADMIN_);
        
        require_once dirname(__FILE__)."/../model/email.php";
        //Inserting Email Address
        $model = new email;
        $model->insertEmail($userId, $email);
        
        require_once dirname(__FILE__)."/../model/contact_numbers.php";
        //Inserting Phone Number
        $model = new contact_numbers;
        $model->insertNumber($userId, $phone);
        
        require_once dirname(__FILE__)."/../model/addresses.php";
        //Inserting Address
        $model = new addresses;
        $model->insertAddress($userId, $address);
        
        //Commiting Transaction
        if(!$this->db->commit())
        {
            die("Unable To Create Admin. Try Again".$this->db->error);
        }
        
        
    }
    //Insert Data
    public function insertAdmin($username = '', $password = '', $firstname = '', $lastname = '', $company_name = '', $dob = '', $country = '',$adminType = 0, $memType = 0,  $memtype_start_time = 0, $memtype_end_time = 0, $permissionsMask = 0, $status = _DEFAULT_STATUS_)
    {
        $sql = "INSERT INTO `{$this->config['db_table_prefix']}{$this->dbTable}`(`username`, `password`, `first_name`, `last_name`, `company_name`, `date_of_birth`, `country_id`, `admin_type`, `membership_type_id`, `membership_start_time`, `membership_end_time`, `permissions_mask`, `status`, `added_date`) VALUES ('$username', '$password', '$firstname', '$lastname',  '$company_name' , '$dob', $country, $adminType, $memType, $memtype_start_time, $memtype_end_time, $permissionsMask, $status, now())";
        
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return $this->db->last_id;
            }
            return FALSE;
        }
        return FALSE;
    }
    
    //check username if exists
    public function userExists($username) 
    {
        $sql = "SELECT `admin_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `username` = '$username'";
        
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                return true;
            }
            return false;
        }
        return false;
    }
    //login
    public function login($userName = '', $password = '')
    {
        if($userName != "" && $password != "")
        {
            $sql="SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` INNER JOIN `{$this->config['db_table_prefix']}users` ON `admin_id` = `uid` WHERE BINARY `username` = '$userName' AND `password` = '$password' AND `user_type` = "._ADMIN_." AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` <> "._DELETED_." LIMIT 1";
            
            if($this->dbcheck())
            {
                $this->db->query($sql);
                if($this->db->is_found)
                {
                    $data = $this->db->result()->fetch_assoc();
                    session::sessionStart();
                    session::loginUser($data['admin_id'], $data['user_id'], $data['first_name']." ".$data['last_name'], $data['username'], $data['admin_type'], $data['membership_type_id'], $data['permissions_mask']);
                    /*$_SESSION["{$this->config['admin_session_prefix']}admin_id"] = $data['admin_id'];
                    $_SESSION["{$this->config['admin_session_prefix']}user_id"] = $data['user_id'];
                    $_SESSION["{$this->config['admin_session_prefix']}name"] = $data['first_name']." ".$data['last_name'];*/
                    $_SESSION["{$this->config['admin_session_prefix']}country_id"] = $data['country_id'];
                    return TRUE;
                }
                return false;
            }
            return FALSE;
        }			
    }
    //logout
    public function logout() 
    {
        session::sessionStart();
        if(isset($_SESSION["{$this->config['admin_session_prefix']}log_id"]))
        {
            $logId = $_SESSION["{$this->config['admin_session_prefix']}log_id"];
            session::sessionDestroy();
            return $logId;
        }
        session::sessionDestroy();
        return NULL;
    }
    //add grace time 
    
    public function gracetime($admin_id, $grace_time)
    {
        $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `office_grace_time` = '$grace_time' WHERE `admin_id` = $admin_id ";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return TRUE;
            }
            return FALSE;
        }
        return FALSE;
    }
   //Insert Hash
   public function insertHash($username = '', $hash = '', $expire = '' ) 
   {
       $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `p_hash` = '$hash', `p_hash_expire` = $expire WHERE `username` = '$username'";
        
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return TRUE;
            }
            return FALSE;
        }
        return FALSE;
   }
   //Match Hash
   public function matchHash($username = '', $hash = '', $cTime = '')
   {
       $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `username` = '$username' AND `p_hash` = '$hash' AND `p_hash_expire` > $cTime";
        
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                return TRUE;
            }
            return FALSE;
        }
        return FALSE;
   }
}
