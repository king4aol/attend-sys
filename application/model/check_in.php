<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of check_in
 *
 * @author PSSLT2540p
 */
class check_in extends model{
    private $dbTable = "check_in";
    private $created = false;
    
    public function dbcheck()
    { return TRUE;
        if(!$this->created)
        {
            
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (`check_in_id` int(11) NOT NULL AUTO_INCREMENT,
                 `office_id` int(11) NOT NULL,
                 `emp_id` int(11) NOT NULL,
                 `device_check_in_id` int(11) NOT NULL,
                 `check_in_timestamp` int(11) NOT NULL,
                 `image_path` varchar(255) NOT NULL,
                 `status` tinyint(4) NOT NULL,
                 PRIMARY KEY (`check_in_id`),
                 FOREIGN KEY (`office_id`) REFERENCES `{$this->config['db_table_prefix']}office`(`office_id`),
                 FOREIGN KEY (`emp_id`) REFERENCES `{$this->config['db_table_prefix']}employee`(`emp_id`)
               ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return TRUE;
            }
            else
            {
                return false;
            }
       }
       return TRUE;
    }
    
    public function firstCreate()
    {
        if(!$this->created)
        {
            
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (`check_in_id` int(11) NOT NULL AUTO_INCREMENT,
                 `office_id` int(11) NOT NULL,
                 `emp_id` int(11) NOT NULL,
                 `device_check_in_id` int(11) NOT NULL,
                 `check_in_timestamp` int(11) NOT NULL,
                 `image_path` varchar(255) NOT NULL,
                 `status` tinyint(4) NOT NULL,
                 PRIMARY KEY (`check_in_id`),
                 FOREIGN KEY (`office_id`) REFERENCES `{$this->config['db_table_prefix']}office`(`office_id`),
                 FOREIGN KEY (`emp_id`) REFERENCES `{$this->config['db_table_prefix']}employee`(`emp_id`)
               ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return TRUE;
            }
            else
            {
                return false;
            }
       }
       return TRUE;
    }
    //Insert Data
    public function insertCheckIn($office_id = '', $emp_id = '', $device_check_in_id = '', $check_in_timestamp = '', $image_path = '', $status = _DEFAULT_STATUS_)
    {
        $sql = "INSERT INTO `{$this->config['db_table_prefix']}{$this->dbTable}`(`office_id`, `emp_id`, `device_check_in_id`, `check_in_timestamp`, `image_path`, `status`) VALUES ('$office_id' , '$emp_id' , '$device_check_in_id' , '$check_in_timestamp' , '$image_path', $status)";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return $this->db->last_id;
            }
            return FALSE;
        }
        return FALSE;
    }
    
    //CheckIn Id From Device Id
    public function checkInId($device_id = '', $emp_id = '') 
    {
        $sql = "SELECT `check_in_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE  `device_check_in_id` = '$device_id' AND `emp_id` = '$emp_id' ";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $user = $this->db->result()->fetch_assoc();
                return $user['check_in_id'];
            }
            return false;
        }
        return false;
    }
    //Last Checkin Details
    public function lastCheckin($emp_id = '')
    {
        $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `emp_id` = $emp_id ORDER BY `check_in_timestamp` DESC LIMIT 1";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                return $this->db->result()->fetch_assoc();
            }
            return false;
        }
        return false;
    }
    //First Check In
    public function firstCheckIn($id = NULL, $type = _EMPLOYEE_) 
    {
        $sql = "SELECT `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_timestamp` "
        . " FROM `{$this->config['db_table_prefix']}{$this->dbTable}` ";
        if($type == _EMPLOYEE_)
        {
            $sql = $sql." WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id` = $id";
        }
        elseif($type == _OFFICE_)
        {
            $sql = $sql." WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`office_id` = $id";
        }
        elseif($type == _CLIENT_)
        {
            $sql = $sql." INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}office`.`admin_id` = `{$this->config['db_table_prefix']}adminusers`.`admin_id` "
            . " WHERE `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $id";
        }
        $sql = $sql." ORDER BY `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_id` ASC LIMIT 1 ";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $data = $this->db->result()->fetch_assoc();
                return $data['check_in_timestamp'];
            }
            return false;
        }
        return false;
    }
    //CheckIn's Without Checkouts
    public function checkinWithoutCheckout($empId = '') 
    {
        $sql = "SELECT `{$this->config['db_table_prefix']}{$this->dbTable}`.*, `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp`, `{$this->config['db_table_prefix']}office`.* FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "		. " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}office`.`office_id` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`office_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}check_out` ON "
        . " `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
        . " WHERE `{$this->config['db_table_prefix']}check_out`.`check_out_id` IS NULL AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id` = $empId"
        . " ORDER BY `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_id` ASC";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $obj = $this->db->result();
                $object = [];
                while($row = $obj->fetch_assoc())
                {
                    $object[] = $row;
                }
                return $object;
            }
            return false;
        }
        return false;
    }
    //Last CheckIn Without Checkout
    public function lastCheckinWithoutCheckout($empId = '') 
    {
        $sql = "SELECT `{$this->config['db_table_prefix']}{$this->dbTable}`.* FROM `{$this->config['db_table_prefix']}{$this->dbTable}`"
        . " LEFT JOIN `{$this->config['db_table_prefix']}check_out` ON "
        . " `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id`"
        . " WHERE `{$this->config['db_table_prefix']}check_out`.`check_out_id` IS NULL AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id` = $empId "
        . " ORDER BY `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_id` DESC LIMIT 1";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                return $this->db->result()->fetch_assoc();
            }
            return false;
        }
        return false;
    }
    //From Device CheckIn to Employee ID
    public function empIdToDeviceId($deviceId = '')
    {
        $sql = "SELECT `emp_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `device_check_in_id` = $deviceId";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $data = $this->db->result()->fetch_assoc();
                return $data['emp_id'];
            }
            return false;
        }
        return false;
    }
    //From CheckIn Id to Employee ID
    public function checkinToEmpId($checkinId = '')
    {
        $sql = "SELECT `emp_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `check_in_id` = $checkinId";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $data = $this->db->result()->fetch_assoc();
                return $data['emp_id'];
            }
            return false;
        }
        return false;
    }	
	//From CheckIn Id to Office ID	    
	
	public function checkinToOfficeId($checkinId = '')    {        $sql = "SELECT `office_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `check_in_id` = $checkinId";        if($this->dbcheck())        {            $this->db->query($sql);            if($this->db->is_found)            {                $data = $this->db->result()->fetch_assoc();                return $data['office_id'];            }            return false;        }        return false;    }
    //Checkin timestamp
    public function checkinTimestamp($checkinId = '')
    {
        $sql = "SELECT `check_in_timestamp` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `check_in_id` = $checkinId";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $data = $this->db->result()->fetch_assoc();
                return $data['check_in_timestamp'];
            } 
            return false;
        }
        return false;
    } 
    
    public function adminToCheckinRel($adminId = '', $id = '')
    {
        if(session::getUserType() == _ADMIN_)
        {
            return $id;
        }
        else 
        {
            $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            ." INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id` "
            ." INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id` "
            ." WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_id` = $id AND `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $adminId ";
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $data = $this->db->result()->fetch_assoc();
                return $data['check_in_id'];
            }
            return FALSE;
        }
    }
    
    //Count Today CheckIn
    public function countCheckIn($id = '', $by = _ADMIN_, $today = TRUE) 
    { 
        if($today)
        {
            $todayTime = strtotime(date("Y-m-d"));
        }
        else 
        {
            $todayTime = 0;
        }
        if($by == _EMPLOYEE_)
        {
            $sql = "SELECT COUNT(*) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_timestamp` > $todayTime"
            . " AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id` = $id";
        }
        elseif($by == _CLIENT_)
        {
            $sql = "SELECT COUNT(*) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id` = `{$this->config['db_table_prefix']}employee`.`emp_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_timestamp` > $todayTime"
            . " AND `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $id";
        }
        elseif($by == _OFFICE_)
        {
            $sql = "SELECT COUNT(*) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_timestamp` > $todayTime"
            . " AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`office_id` = $id";
        }
        elseif($by == _ADMIN_)
        {
            $sql = "SELECT COUNT(*) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_timestamp` > $todayTime";
        }
        $this->db->query($sql);
        if($this->db->is_found)
        {
            $data = $this->db->result()->fetch_assoc();
            return $data['total'];
        }
        return FALSE;
    } 
    //Count Today CheckIn By Status
    public function countCheckInByStatus($id = '', $by = _ADMIN_, $today = TRUE) 
    {
        $data = [];
        if($today || $today == "today")
        {
            $todayTime = strtotime(date("Y-m-d"));
        }
        elseif($today == "week")
        {
            $todayTime = strtotime(date("Y-m-d")) - (86400*7);
        }
        else 
        {
            $todayTime = 0;
        }
        if($by == _EMPLOYEE_)
        {
            $sql = "SELECT COUNT(DISTINCT `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id`) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_timestamp` > $todayTime"
            . " AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id` = $id AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` = "._IN_TIME_." "
            . " GROUP BY `{$this->config['db_table_prefix']}{$this->dbTable}`.`status`, `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id`";
           
            $sql1 = "SELECT COUNT(DISTINCT `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id`) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_timestamp` > $todayTime"
            . " AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id` = $id AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` = "._LATE_." "
            . " GROUP BY `{$this->config['db_table_prefix']}{$this->dbTable}`.`status`, `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id`";
            
        }
        elseif($by == _CLIENT_)
        {
            $sql = "SELECT COUNT(DISTINCT `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id`) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id` = `{$this->config['db_table_prefix']}employee`.`emp_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_timestamp` > $todayTime"
            . " AND `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $id AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` = "._IN_TIME_." "
            . "GROUP BY `{$this->config['db_table_prefix']}{$this->dbTable}`.`status`, `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id`";
            $sql1 = "SELECT COUNT(DISTINCT `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id`) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id` = `{$this->config['db_table_prefix']}employee`.`emp_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_timestamp` > $todayTime"
            . " AND `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $id AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` = "._LATE_." "
            . " GROUP BY `{$this->config['db_table_prefix']}{$this->dbTable}`.`status`, `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id`";
        
            
        }
        elseif($by == _OFFICE_)
        {
            $sql = "SELECT COUNT(DISTINCT `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id`) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_timestamp` > $todayTime"
            . " AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`office_id` = $id AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` = "._IN_TIME_." "
            . " GROUP BY `{$this->config['db_table_prefix']}{$this->dbTable}`.`status`, `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id`";
            $sql1 = "SELECT COUNT(*) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_timestamp` > $todayTime"
            . " AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`office_id` = $id AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` = "._LATE_." "
            . " GROUP BY `{$this->config['db_table_prefix']}{$this->dbTable}`.`status`, `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id`";
            
        }
        elseif($by == _ADMIN_)
        {
            $sql = "SELECT COUNT(DISTINCT `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id`) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_timestamp` > $todayTime "
            . " AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` = "._IN_TIME_
            . " GROUP BY `{$this->config['db_table_prefix']}{$this->dbTable}`.`status`, `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id`";
            $sql1 = "SELECT COUNT(*) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_timestamp` > $todayTime AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` = "._LATE_
            . " GROUP BY `{$this->config['db_table_prefix']}{$this->dbTable}`.`status`, `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id`";
        
            
        }
        $this->db->query($sql);
        if($this->db->is_found)
        {
            $row = $this->db->result()->fetch_assoc();
            $data[_IN_TIME_] = $row['total'];
            
            $this->db->query($sql1);
            $row = $this->db->result()->fetch_assoc();
            $data[_LATE_] = $row['total'];
            
            return $data;
        }
        return FALSE;
    }
    //Latest Checkin Details
    public function lastestCheckin($adminId = '', $type = '', $offset = 0, $limit = 1, $fromTime = 0)
    {
        if($type == _ADMIN_)
        {
            $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}office`.`office_id` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`office_id` "
            . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_timestamp` >= $fromTime "
            . " ORDER BY `check_in_id` DESC LIMIT $offset, $limit";
        }
        else 
        {
         $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
         ." INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id` "
         . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}office`.`office_id` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`office_id` "
         ." INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id` "
         ." WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_timestamp` >= $fromTime "
         ." AND `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $adminId "
         ." ORDER BY `check_in_id` DESC LIMIT $offset, $limit";
        } 
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $obj = $this->db->result();
                $object = [];
                while($row = $obj->fetch_assoc())
                {
                    $object[] = $row;
                }
                return $object;
            }
            return false;
        }
        return false;
    }
    //Office Based Check In From Given Timestamp For A Single Admin
    public function countCheckinFromTime($adminId = '', $from = 0) 
    {
        $sql = "SELECT COUNT(`{$this->config['db_table_prefix']}check_in`.`check_in_id`) AS total, `{$this->config['db_table_prefix']}office`.`office_name` FROM `{$this->config['db_table_prefix']}check_in` "
        . " INNER JOIN`{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
        . " WHERE `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $adminId AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` >= $from GROUP BY `{$this->config['db_table_prefix']}check_in`.`office_id` ORDER BY `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` DESC";
        
        $this->db->query($sql);
        if($this->db->is_true)
        {
            $obj = $this->db->result();
            $object = [];
            while($row = $obj->fetch_assoc())
            {
                $object[] = $row;
            }
            return $object;
        }
        return FALSE;
    } 
    //Client Admin Based Check In From Given Timestamp
    public function countAllCheckinFromTime($from = 0) 
    {
        $sql = "SELECT COUNT(`{$this->config['db_table_prefix']}check_in`.`check_in_id`) AS total, CONCAT(`{$this->config['db_table_prefix']}adminusers`.`first_name`, ' ', `{$this->config['db_table_prefix']}adminusers`.`last_name`) AS client_name FROM `{$this->config['db_table_prefix']}check_in` "
        . " INNER JOIN`{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
        . " WHERE `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` >= $from  GROUP BY `{$this->config['db_table_prefix']}adminusers`.`admin_id` ORDER BY `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` DESC";
        
        $this->db->query($sql);
        if($this->db->is_true)
        {
            $obj = $this->db->result();
            $object = [];
            while($row = $obj->fetch_assoc())
            {
                $object[] = $row;
            }
            return $object;
        }
        return FALSE;
    } 
}
