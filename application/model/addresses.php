<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of addresses
 *
 * @author Msajid
 */
class addresses extends model{
    private $dbTable = __CLASS__;
    private $created = false;
    
    private function dbcheck()
    { return TRUE;
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (
                    `address_id` int(11) NOT NULL AUTO_INCREMENT,
                    `user_id` int(11) NOT NULL,
                    `type` varchar(20) NOT NULL,
                    `address` varchar(255) NOT NULL,
                    `added_date` datetime NOT NULL,
                    `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`address_id`),
                    FOREIGN KEY (`user_id`) REFERENCES `{$this->config['db_table_prefix']}users`(`user_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return true;
    }
    //First TIme Create
    public function firstCreate()
    {
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (
                    `address_id` int(11) NOT NULL AUTO_INCREMENT,
                    `user_id` int(11) NOT NULL,
                    `type` varchar(20) NOT NULL,
                    `address` varchar(255) NOT NULL,
                    `added_date` datetime NOT NULL,
                    `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`address_id`),
                    FOREIGN KEY (`user_id`) REFERENCES `{$this->config['db_table_prefix']}users`(`user_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return true;
    }
    
    public function insertAddress($userId, $address, $type = 'business') 
    {
        $sql = "INSERT INTO `{$this->config['db_table_prefix']}{$this->dbTable}`(`user_id`, `address`, `type`, `added_date`) VALUES ($userId, '$address', '$type', 'now()')";

        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return $this->db->last_id;
            }
            return false;
        }
        return false;
    }
}
