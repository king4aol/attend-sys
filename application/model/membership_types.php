<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of membership_types
 *
 * @author Msajid
 */
class membership_types extends model{
    private $dbTable = "membership_types";
    private $created = false;
    
    private function dbcheck()
    { return TRUE;
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (
                    `type_id` int(11) NOT NULL AUTO_INCREMENT,
                    `membership_title` varchar(30) NOT NULL UNIQUE,
                    `roles` int(11) NOT NULL,
                    `devices_allowed` int(11) NOT NULL,
                    `offices_allowed` int(11) NOT NULL,
                    `employees_allowed` int(11) NOT NULL,
                    PRIMARY KEY (`type_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                $sql = "SELECT `membership_title` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` LIMIT 1";
                $this->db->query($sql);
                if(!$this->db->is_found)
                {
                    $this->defaultTypes();
                }
                return TRUE;
            }
            else
            {
                return false;
            }
       }
       return TRUE;
    }
    public function firstCreate()
    {
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (
                    `type_id` int(11) NOT NULL AUTO_INCREMENT,
                    `membership_title` varchar(30) NOT NULL UNIQUE,
                    `roles` int(11) NOT NULL,
                    `devices_allowed` int(11) NOT NULL,
                    `offices_allowed` int(11) NOT NULL,
                    `employees_allowed` int(11) NOT NULL,
                    PRIMARY KEY (`type_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                $sql = "SELECT `membership_title` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` LIMIT 1";
                $this->db->query($sql);
                if(!$this->db->is_found)
                {
                    $this->defaultTypes();
                }
                return TRUE;
            }
            else
            {
                return false;
            }
       }
       return TRUE;
    }
    //Get Membership Types
    public function loadTypes() 
    {
        $cacheObject = cache::readCache(__CLASS__);
        
        if(!$cacheObject)
        {
            $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}`";
            if($this->dbcheck())
            {
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return cache::saveCache($this->db->result(), __CLASS__, "database");
                }
                return FALSE;
            }
            return false;
        }
        return $cacheObject;
    }
    //Default Types
    private function defaultTypes() 
    {
        $sql = "INSERT INTO `{$this->config['db_table_prefix']}{$this->dbTable}`(`membership_title`, `roles`, `devices_allowed`, `offices_allowed`, `employees_allowed`) VALUES ('Free/Basic', 0, 1, 1, 1)";
        $this->db->query($sql);
        $sql = "INSERT INTO `{$this->config['db_table_prefix']}{$this->dbTable}`(`membership_title`, `roles`, `devices_allowed`, `offices_allowed`, `employees_allowed`) VALUES ('Professional', 0, -2, 1, -1)";
        $this->db->query($sql);
        $sql = "INSERT INTO `{$this->config['db_table_prefix']}{$this->dbTable}`(`membership_title`, `roles`, `devices_allowed`, `offices_allowed`, `employees_allowed`) VALUES ('Enterprise', 0, -1, -1, -1)";
        $this->db->query($sql);
        
    }
    public function membershipExists($type_id)
    {
        $sql = "SELECT `type_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `type_id` = $type_id";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                return TRUE;
            }
            return FALSE;
        }
        return FALSE;
    }
    
    public function getTypeId($title = '')
    {
        $sql = "SELECT `type_id` FROM `{$this->config['db_table_prefix']}membership_types` WHERE `membership_title` = '$title'";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->num_rows > 0)
            { 
                $id = $this->db->result()->fetch_assoc();
                return $id['type_id'];
            }
            return FALSE;
        }
        return FALSE;
    }
    
    public function getTypeTitle($typeId = '')
    {
        $sql = "SELECT `membership_title` FROM `{$this->config['db_table_prefix']}membership_types` WHERE `type_id` = '$typeId'";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->num_rows > 0)
            { 
                $title = $this->db->result()->fetch_assoc();
                return $title['membership_title'];
            }
            return FALSE;
        }
        return FALSE;
    }
    
    public function getDevicesAllow($input = '', $by_title = TRUE)
    {
        if($by_title)
        {
            $sql = "SELECT `devices_allowed` FROM `{$this->config['db_table_prefix']}membership_types` WHERE `membership_title` = '$input'";
        }
        else 
        {
            $sql = "SELECT `devices_allowed` FROM `{$this->config['db_table_prefix']}membership_types` WHERE `type_id` = '$input'";
        }
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->num_rows > 0 && $this->db->is_true)
            { 
                $id = $this->db->result()->fetch_assoc();
                return $id['devices_allowed'];
            }
            return FALSE;
        }
        return FALSE;
    }
    
    public function getOfficesAllow($input = '', $by_title = TRUE)
    {
        if($by_title)
        {
            $sql = "SELECT `offices_allowed` FROM `{$this->config['db_table_prefix']}membership_types` WHERE `membership_title` = '$input'";
        }
        else 
        {
            $sql = "SELECT `offices_allowed` FROM `{$this->config['db_table_prefix']}membership_types` WHERE `type_id` = '$input'";
        }
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->num_rows > 0 && $this->db->is_true)
            { 
                $id = $this->db->result()->fetch_assoc();
                return $id['offices_allowed'];
            }
            return FALSE;
        }
        return FALSE;
    }
    
    public function getEmployeesAllow($input = '', $by_title = TRUE)
    {
        if($by_title)
        {
            $sql = "SELECT `employees_allowed` FROM `{$this->config['db_table_prefix']}membership_types` WHERE `membership_title` = '$input'";
        }
        else 
        {
            $sql = "SELECT `employees_allowed` FROM `{$this->config['db_table_prefix']}membership_types` WHERE `type_id` = '$input'";
        }
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->num_rows > 0 && $this->db->is_true)
            { 
                $id = $this->db->result()->fetch_assoc();
                return $id['employees_allowed'];
            }
            return FALSE;
        }
        return FALSE;
    }
    
}
