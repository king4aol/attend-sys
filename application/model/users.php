<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of users
 *
 * @author Msajid
 */
class users extends model{
    private $dbTable = __CLASS__;
    private $created = false;
    
    private function dbcheck() 
    { return TRUE;
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (
                    `user_id` int(11) NOT NULL AUTO_INCREMENT,
                    `uid` int(11) NOT NULL,
                    `user_type` int(11) NOT NULL,
                    PRIMARY KEY (`user_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return true;
    }
    public function firstCreate() 
    {
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (
                    `user_id` int(11) NOT NULL AUTO_INCREMENT,
                    `uid` int(11) NOT NULL,
                    `user_type` int(11) NOT NULL,
                    PRIMARY KEY (`user_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return true;
    }
    public function insertUsers($userId, $userType) 
    {
        $sql = "INSERT INTO `{$this->config['db_table_prefix']}{$this->dbTable}`(`uid`, `user_type`) VALUES ($userId, $userType)";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return $this->db->last_id;
            }
            return false;
        }
        return false;
    }
    //Create Table
    public function createTable() 
    {
        return $this->dbcheck();
    }
}
