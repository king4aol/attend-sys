<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of check_out
 *
 * @author PSSLT2540p
 */
class check_out extends model{
    private $dbTable = "check_out";
    private $created = false;
    
    public function dbcheck()
    { return TRUE;
        if(!$this->created)
        {
            
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (`check_out_id` int(11) NOT NULL AUTO_INCREMENT,
                 `check_in_id` int(11) NOT NULL UNIQUE,
                 `check_out_timestamp` int(11) NOT NULL,
                 `image_path` varchar(255) NOT NULL,
                 `status` tinyint(4) NOT NULL,
                 PRIMARY KEY (`check_out_id`),
                 FOREIGN KEY (`check_in_id`) REFERENCES `{$this->config['db_table_prefix']}check_in`(`check_in_id`)
               ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return TRUE;
            }
            else
            {
                return false;
            }
       }
       return TRUE;
    }
    //First Create
    public function firstCreate()
    {
        if(!$this->created)
        {
            
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (`check_out_id` int(11) NOT NULL AUTO_INCREMENT,
                 `check_in_id` int(11) NOT NULL UNIQUE,
                 `check_out_timestamp` int(11) NOT NULL,
                 `image_path` varchar(255) NOT NULL,
                 `status` tinyint(4) NOT NULL,
                 PRIMARY KEY (`check_out_id`),
                 FOREIGN KEY (`check_in_id`) REFERENCES `{$this->config['db_table_prefix']}check_in`(`check_in_id`)
               ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return TRUE;
            }
            else
            {
                return false;
            }
       }
       return TRUE;
    }
     //Insert Data
    public function insertCheckOut($check_in_id = '', $check_out_timestamp = '', $image_path = '', $status = _DEFAULT_STATUS_)
    {
        $sql = "INSERT INTO `{$this->config['db_table_prefix']}{$this->dbTable}`(`check_in_id`, `check_out_timestamp`, `image_path`, `status`) VALUES ($check_in_id , $check_out_timestamp , '$image_path', '$status')";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return $this->db->last_id;
            }
            return FALSE;
        }
        return FALSE;
    }
    //Check If Last Checkout Was Checked
    public function checkoutDone($checkin_id) 
    {
        $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `check_in_id` = $checkin_id";
        
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                return TRUE;
            }
            return FALSE;
        }
        return FALSE;
    } 
    //Checkout Exists
    public function checkoutExists($checkinId = '') 
    {
        $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `check_in_id` = $checkinId";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                return TRUE;
            }
            return false;
        }
        return false;
    }
    //Count Today CheckOut
    public function countCheckOut($id = '', $by = _ADMIN_, $today = TRUE) 
    {
        if($today)
        {
            $todayTime = strtotime(date("Y-m-d"));
        }
        else 
        {
            $todayTime = 0;
        }
        if($by == _EMPLOYEE_)
        {
            $sql = "SELECT COUNT(*) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " INNER JOIN `{$this->config['db_table_prefix']}check_in` ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_id` = `{$this->config['db_table_prefix']}check_in`.`check_in_id`"
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_out_timestamp` > $todayTime"
            . " AND `{$this->config['db_table_prefix']}check_in`.`emp_id` = $id";
        }
        elseif($by == _CLIENT_)
        {
            $sql = "SELECT COUNT(*) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " INNER JOIN `{$this->config['db_table_prefix']}check_in` ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_id` = `{$this->config['db_table_prefix']}check_in`.`check_in_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}check_in`.`emp_id` = `{$this->config['db_table_prefix']}employee`.`emp_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_out_timestamp` > $todayTime"
            . " AND `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $id";
        }
        elseif($by == _OFFICE_)
        {
            $sql = "SELECT COUNT(*) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " INNER JOIN `{$this->config['db_table_prefix']}check_in` ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_id` = `{$this->config['db_table_prefix']}check_in`.`check_in_id` "
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_out_timestamp` > $todayTime"
            . " AND `{$this->config['db_table_prefix']}check_in`.`office_id` = $id";
        }
        elseif($by == _ADMIN_)
        {
            $sql = "SELECT COUNT(*) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_out_timestamp` > $todayTime";
        }
        $this->db->query($sql);
        if($this->db->is_found)
        {
            $data = $this->db->result()->fetch_assoc();
            return $data['total'];
        }
        return FALSE;
    }
    //Count Today CheckOut By Status
    public function countCheckOutByStatus($id = '', $by = _ADMIN_, $today = TRUE) 
    {
        $data = [];
        if($today || $today == "today")
        {
            $todayTime = strtotime(date("Y-m-d"));
        }
        elseif($today == "week")
        {
            $todayTime = strtotime(date("Y-m-d")) - (86400*7);
        }
        else 
        {
            $todayTime = 0;
        }
        if($by == _EMPLOYEE_)
        {
            $sql = "SELECT COUNT(DISTINCT `{$this->config['db_table_prefix']}check_in`.`emp_id`) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " INNER JOIN `{$this->config['db_table_prefix']}check_in` ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_id` = `{$this->config['db_table_prefix']}check_in`.`check_in_id`"
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_out_timestamp` > $todayTime "
            . " AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` = "._OVERTIME_
            . " AND `{$this->config['db_table_prefix']}check_in`.`emp_id` = $id "
            . " GROUP BY `{$this->config['db_table_prefix']}{$this->dbTable}`.`status`";
            $sql1 = "SELECT COUNT(DISTINCT `{$this->config['db_table_prefix']}check_in`.`emp_id`) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " INNER JOIN `{$this->config['db_table_prefix']}check_in` ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_id` = `{$this->config['db_table_prefix']}check_in`.`check_in_id`"
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_out_timestamp` > $todayTime "
            . " AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` = "._EARLY_
            . " AND `{$this->config['db_table_prefix']}check_in`.`emp_id` = $id "
            . " GROUP BY `{$this->config['db_table_prefix']}{$this->dbTable}`.`status`";
        }
        elseif($by == _CLIENT_)
        {
            $sql = "SELECT COUNT(DISTINCT `{$this->config['db_table_prefix']}check_in`.`emp_id`) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " INNER JOIN `{$this->config['db_table_prefix']}check_in` ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_id` = `{$this->config['db_table_prefix']}check_in`.`check_in_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}check_in`.`emp_id` = `{$this->config['db_table_prefix']}employee`.`emp_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_out_timestamp` > $todayTime "
            . " AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` = "._OVERTIME_
            . " AND `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $id "
            . "  GROUP BY `{$this->config['db_table_prefix']}{$this->dbTable}`.`status`";
            $sql1 = "SELECT COUNT(DISTINCT `{$this->config['db_table_prefix']}check_in`.`emp_id`) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " INNER JOIN `{$this->config['db_table_prefix']}check_in` ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_id` = `{$this->config['db_table_prefix']}check_in`.`check_in_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}check_in`.`emp_id` = `{$this->config['db_table_prefix']}employee`.`emp_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_out_timestamp` > $todayTime "
            . " AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` = "._EARLY_
            . " AND `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $id "
            . " GROUP BY `{$this->config['db_table_prefix']}{$this->dbTable}`.`status`";
        }
        elseif($by == _OFFICE_)
        {
            $sql = "SELECT COUNT(DISTINCT `{$this->config['db_table_prefix']}check_in`.`emp_id`) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " INNER JOIN `{$this->config['db_table_prefix']}check_in` ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_id` = `{$this->config['db_table_prefix']}check_in`.`check_in_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}check_in`.`emp_id` = `{$this->config['db_table_prefix']}employee`.`emp_id` "
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_out_timestamp` > $todayTime "
            . " AND `{$this->config['db_table_prefix']}check_in`.`office_id` = $id "
            . " AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` = "._OVERTIME_
            . " GROUP BY `{$this->config['db_table_prefix']}{$this->dbTable}`.`status`";
            $sql1 = "SELECT COUNT(DISTINCT `{$this->config['db_table_prefix']}check_in`.`emp_id`) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " INNER JOIN `{$this->config['db_table_prefix']}check_in` ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_id` = `{$this->config['db_table_prefix']}check_in`.`check_in_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}check_in`.`emp_id` = `{$this->config['db_table_prefix']}employee`.`emp_id` "
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_out_timestamp` > $todayTime "
            . " AND `{$this->config['db_table_prefix']}check_in`.`office_id` = $id "
            . " AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` = "._EARLY_
            . " GROUP BY `{$this->config['db_table_prefix']}{$this->dbTable}`.`status`";
        
            
        }
        elseif($by == _ADMIN_)
        {
            $sql = "SELECT COUNT(DISTINCT `{$this->config['db_table_prefix']}check_in`.`emp_id`) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " INNER JOIN `{$this->config['db_table_prefix']}check_in` ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_id` = `{$this->config['db_table_prefix']}check_in`.`check_in_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}check_in`.`emp_id` = `{$this->config['db_table_prefix']}employee`.`emp_id` "
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_out_timestamp` > $todayTime "
            . " AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` = "._OVERTIME_
            . " GROUP BY `{$this->config['db_table_prefix']}{$this->dbTable}`.`status`";
            $sql1 = "SELECT COUNT(DISTINCT `{$this->config['db_table_prefix']}check_in`.`emp_id`) AS total "
            . "  FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " INNER JOIN `{$this->config['db_table_prefix']}check_in` ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_id` = `{$this->config['db_table_prefix']}check_in`.`check_in_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}check_in`.`emp_id` = `{$this->config['db_table_prefix']}employee`.`emp_id` "
            . " WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_out_timestamp` > $todayTime "
            . " AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` = "._EARLY_
            . " GROUP BY `{$this->config['db_table_prefix']}{$this->dbTable}`.`status`";
         }
        $this->db->query($sql);
        if($this->db->is_found)
        {
            $row = $this->db->result()->fetch_assoc();
            $data[_OVERTIME_] = $row['total'];
            
            $this->db->query($sql1);
            $row = $this->db->result()->fetch_assoc();
            $data[_EARLY_] = $row['total'];
            
            return $data;
        }
        return FALSE;
    }
    //Latest Checkout Details
    public function lastestCheckout($adminId = '', $type = '')
    {
        if($type == _ADMIN_)
        {
            $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " INNER JOIN `{$this->config['db_table_prefix']}check_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
            . "ORDER BY `check_out_id` DESC LIMIT 1";
        }
        else 
        {
         $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
         . "  INNER JOIN `{$this->config['db_table_prefix']}check_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_id` "
         ." INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
         ." INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id` "
         ." WHERE `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $adminId "
         ." ORDER BY `check_out_id` DESC LIMIT 1";
        }
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                return $this->db->result()->fetch_assoc();
            }
            return false;
        }
        return false;
    }
}
