<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of attendence
 *
 * @author Msajid
 */
class attendance  extends model
{
    public function createTables() 
    {
        require_once dirname(__FILE__)."/../model/check_in.php";
        require_once dirname(__FILE__)."/../model/check_out.php";
        require_once dirname(__FILE__)."/../model/break_in.php";
        require_once dirname(__FILE__)."/../model/break_out.php";
        $model = new check_in;
        $model->dbcheck();
        $model = new check_out;
        $model->dbcheck();
        $model = new break_in;
        $model->dbcheck();
        $model = new break_out;
        $model->dbcheck();
    }
    /*private $dbTable = "attendence";
    private $created = false;
    
    //Just To Check Database ,Create If Not Exists
    private function dbcheck()
    {
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (
                    `attendence_id` int(11) NOT NULL AUTO_INCREMENT,
                    `emp_id` int(11) NOT NULL,
                    `office_id` int(11) NOT NULL,
                    `checkin_time` time NOT NULL,
                    `checkout_time` time NOT NULL,
                    `checkin_snap` varchar(255) NOT NULL,
                    `checkout_snap` varchar(255) NOT NULL,
                    `status` tinyint(4) NOT NULL,
                    `added_date` datetime NOT NULL,
                    `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`attendence_id`),
                    FOREIGN KEY (`emp_id`) REFERENCES `{$this->config['db_table_prefix']}employee` (`emp_id`),
                    FOREIGN KEY (`office_id`) REFERENCES `{$this->config['db_table_prefix']}office` (`office_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return TRUE;
    }
    
    //loadDeatails
    
    public function loadDetails($emp_id = '', $office_id = '', $createCache = false)
    {
        $cacheObject = cache::readCache($emp_id."_attendence");
        if(!$cacheObject || $createCache)
        {
            $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `emp_id` = '$emp_id' AND `office_id` ='$office_id' LIMIT 1";         
            
            if($this->dbcheck())
            {  
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return cache::saveCache($this->db->result(), $emp_id."_attendence", "database");
                }
                return FALSE;
            }
            return false;
        }
        return $cacheObject;
    }
    
    //Checkin Time 
    public function checkin($emp_id = '', $office_id = '', $chechin_snap = '', $status = '')
    {
        $sql = "INSERT INTO `otl_attendence` (`emp_id`, `office_id`, `checkin_time`, `checkin_snap`, `status`, `added_date`) VALUES ('$emp_id', '$office_id', now(), '$chechin_snap', '$status', now())";
        
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return $this->db->last_id;
            }
            return FALSE;
        }   
        return FALSE;
    }
    
    //Checkout Time
    public function checkout($emp_id = '', $office_id = '', $checkout_snap = '')
    {
         $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `checkout_time` = now(), `checkout_snap` = '$checkout_snap'  WHERE `emp_id` = '$emp_id' AND `office_id` = '$office_id' ORDER BY `attendence_id` DESC LIMIT 1";
         if($this->dbcheck())
            {
                      
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return $this->loadDetails($emp_id, $office_id , TRUE);
                }
                return FALSE;
            }
            return FALSE;
    }
    
    //view 
    
    public function view($admin_id = '', $from = '', $to = '', $createCache = false )
    {
        $cacheObject = cache::readCache($admin_id."_".__CLASS__);
        
        if(!$cacheObject || $createCache)
        {
            $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` 
                    INNER JOIN `{$this->config['db_table_prefix']}employee`
                    ON  `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id` 
                    = `{$this->config['db_table_prefix']}employee`.`emp_id`
                    INNER JOIN `{$this->config['db_table_prefix']}adminusers`
                    ON  `{$this->config['db_table_prefix']}employee`.`admin_id` 
                    = `{$this->config['db_table_prefix']}adminusers`.`admin_id`
                    INNER JOIN `{$this->config['db_table_prefix']}office`
                    ON  `{$this->config['db_table_prefix']}{$this->dbTable}`.`office_id` 
                    = `{$this->config['db_table_prefix']}office`.`office_id`
                    WHERE `{$this->config['db_table_prefix']}adminusers`.`admin_id` = '$admin_id'
                    AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`last_updated` > '$from'
                    AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`last_updated` < '$to'
                    AND `{$this->config['db_table_prefix']}office`.`admin_id` = '$admin_id'					
                    AND `{$this->config['db_table_prefix']}adminusers`.`status` <> "._DELETED_." 
                    AND `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_."
                    AND `{$this->config['db_table_prefix']}office`.`status` <> "._DELETED_;
            if($this->dbcheck())
            {
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return cache::saveCache($this->db->result(), $admin_id."_".__CLASS__, "database");
                }
                return FALSE;
            }
            return FALSE;
        }
        return $cacheObject;
    }*/
    
    public function view($empId = '', $offset = 0, $limit = 10, $fromTime = 0, $toTime = 999999999999) 
    {
        $sql = "SELECT *, `{$this->config['db_table_prefix']}check_in`.`check_in_id` AS id, `{$this->config['db_table_prefix']}check_in`.`image_path` AS cin_image, `{$this->config['db_table_prefix']}check_out`.`image_path` AS cout_image, `{$this->config['db_table_prefix']}break_in`.`image_path` AS bin_image, `{$this->config['db_table_prefix']}break_out`.`image_path` AS bout_image "
        . " FROM `{$this->config['db_table_prefix']}check_in` "
        . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
        . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}check_in`.`emp_id` = $empId "
        . " AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` >= $fromTime AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` <= $toTime"
        . " ORDER BY `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` DESC LIMIT $limit OFFSET $offset";
        
        $this->db->query($sql);
        if($this->db->is_true)
        {
            return cache::saveCache($this->db->result(), $empId."_".__CLASS__, "database");
        }
        return FALSE;
    }
    //View By Offices
    public function viewByOffice($officeId = '', $offset = 0, $limit = 10, $fromTime = 0, $toTime = 99999999999) 
    {
        $sql = "SELECT *, `{$this->config['db_table_prefix']}check_in`.`check_in_id` AS id, `{$this->config['db_table_prefix']}check_in`.`image_path` AS cin_image, `{$this->config['db_table_prefix']}check_out`.`image_path` AS cout_image, `{$this->config['db_table_prefix']}break_in`.`image_path` AS bin_image, `{$this->config['db_table_prefix']}break_out`.`image_path` AS bout_image "
        . "  FROM `{$this->config['db_table_prefix']}check_in` "
        . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}check_in`.`emp_id` = `{$this->config['db_table_prefix']}employee`.`emp_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
        . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}check_in`.`office_id` = $officeId "
        . " AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` >= $fromTime AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` <= $toTime "
        . " ORDER BY `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` DESC LIMIT $limit OFFSET $offset";
        
        $this->db->query($sql);
        if($this->db->is_true)
        {
            return cache::saveCache($this->db->result(), $officeId."_".__CLASS__, "database");
        }
        return FALSE;
    }
    
    public function viewAll($adminId = '') 
    {
        $sql = "SELECT *, `{$this->config['db_table_prefix']}check_in`.`check_in_id` AS id FROM `{$this->config['db_table_prefix']}check_in` "
        . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
        . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $adminId ORDER BY `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` DESC";
        
        $this->db->query($sql);
        if($this->db->is_true)
        {
            return cache::saveCache($this->db->result(), "all_".__CLASS__, "database");
        }
        return FALSE;
    }
    //Attendance Between Given Timestamp
    public function viewBetween($adminId = '', $start = 0, $end = 0) 
    {
        $sql = "SELECT `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp`, "
        . " `{$this->config['db_table_prefix']}break_in`.`break_in_timestamp`, "
        . "`{$this->config['db_table_prefix']}break_out`.`break_out_timestamp`, "
        . " `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp`, "
        . " `{$this->config['db_table_prefix']}office`.`office_name`, `{$this->config['db_table_prefix']}office`.`timezone`, "
        . " `{$this->config['db_table_prefix']}employee`.`emp_first_name`, `{$this->config['db_table_prefix']}employee`.`emp_last_name` "
        . " FROM `{$this->config['db_table_prefix']}check_in` "
        . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
        . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $adminId AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` BETWEEN $start AND $end ORDER BY `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp`, `{$this->config['db_table_prefix']}adminusers`.`first_name` DESC";
        
        $this->db->query($sql);
        if($this->db->is_true)
        {
            return cache::saveCache($this->db->result(), "all_".__CLASS__, "database");
        }
        return FALSE;
    } 
    
    //Report Between Given Timestamp
    public function viewReportBetween($adminId = '', $start = 0, $end = 0) 
    {
        $sql = "SELECT `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp`, "
        . " `{$this->config['db_table_prefix']}break_in`.`break_in_timestamp`, "
        . "`{$this->config['db_table_prefix']}break_out`.`break_out_timestamp`, "
        . " `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp`, "
        . " `{$this->config['db_table_prefix']}office`.`office_name`, `{$this->config['db_table_prefix']}office`.`timezone`, "
        . " CONCAT(`{$this->config['db_table_prefix']}employee`.`emp_first_name`, ' ', `{$this->config['db_table_prefix']}employee`.`emp_last_name`) AS name, "
        . " `{$this->config['db_table_prefix']}employee`.`pin`, `{$this->config['db_table_prefix']}employee`.`pay_rate` "
        . " FROM `{$this->config['db_table_prefix']}check_in` "
        . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
        . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $adminId AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` BETWEEN $start AND $end "
        . " ORDER BY `{$this->config['db_table_prefix']}check_in`.`emp_id` ASC";
        
        $this->db->query($sql);
        if($this->db->is_true)
        {
            return cache::saveCache($this->db->result(), "all_".__CLASS__, "database");
        }
        return FALSE;
    } 
    //Report Between Given Timestamp And By Whom
    public function viewReportBetweenBy($id = '', $by = _EMPLOYEE_, $start = 0, $end = 0, $order = 'by_date') 
    {
        if($by == _EMPLOYEE_)
        {
            $sql = "SELECT DATE_FORMAT( FROM_UNIXTIME( `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` ),'%d/%m/%Y' ) AS `date`, "
            . " `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp`, "
            . " `{$this->config['db_table_prefix']}break_in`.`break_in_timestamp`, "
            . " `{$this->config['db_table_prefix']}break_out`.`break_out_timestamp`, "
            . " `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp`, "
            . " `{$this->config['db_table_prefix']}office`.`office_name`, `{$this->config['db_table_prefix']}office`.`timezone`, "
            . " CONCAT(`{$this->config['db_table_prefix']}employee`.`emp_first_name`, ' ', `{$this->config['db_table_prefix']}employee`.`emp_last_name`) AS name, "
            . " `{$this->config['db_table_prefix']}employee`.`emp_id`, `{$this->config['db_table_prefix']}employee`.`pin`, `{$this->config['db_table_prefix']}employee`.`pay_rate` "
            . " FROM `{$this->config['db_table_prefix']}check_in` "
            . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
            . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}employee`.`emp_id` = $id AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` BETWEEN $start AND $end "
            
            . " ORDER BY `date` ASC";
			
			// . " GROUP BY `date` "
        }
        elseif($by == _OFFICE_)
        {
            $sql = "SELECT DATE_FORMAT( FROM_UNIXTIME( `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` ),'%d/%m/%Y' ) AS `date`, "
            . " `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp`, "
            . " `{$this->config['db_table_prefix']}break_in`.`break_in_timestamp`, "
            . " `{$this->config['db_table_prefix']}break_out`.`break_out_timestamp`, "
            . " `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp`, "
            . " `{$this->config['db_table_prefix']}office`.`office_name`, `{$this->config['db_table_prefix']}office`.`timezone`, "
            . " CONCAT(`{$this->config['db_table_prefix']}employee`.`emp_first_name`, ' ', `{$this->config['db_table_prefix']}employee`.`emp_last_name`) AS name, "
            . " `{$this->config['db_table_prefix']}employee`.`emp_id`, `{$this->config['db_table_prefix']}employee`.`pin`, `{$this->config['db_table_prefix']}employee`.`pay_rate` "
            . " FROM `{$this->config['db_table_prefix']}check_in` "
            . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
            . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}office`.`office_id` = $id AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` BETWEEN $start AND $end ";
            
//            $sql = "SELECT DATE_FORMAT( FROM_UNIXTIME( `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` ),'%d/%m/%Y' ) AS `date`, SUM(`{$this->config['db_table_prefix']}check_in`.`check_in_timestamp`) AS check_in_timestamp, "
//            . " SUM(`{$this->config['db_table_prefix']}break_in`.`break_in_timestamp`) AS break_in_timestamp, "
//            . " SUM(`{$this->config['db_table_prefix']}break_out`.`break_out_timestamp`) AS break_out_timestamp, "
//            . " SUM(`{$this->config['db_table_prefix']}check_out`.`check_out_timestamp`) AS check_out_timestamp, "
//            . " `{$this->config['db_table_prefix']}office`.`office_name`, "
//            . " COUNT(*) AS total, "
//            . " CONCAT(`{$this->config['db_table_prefix']}employee`.`emp_first_name`, ' ', `{$this->config['db_table_prefix']}employee`.`emp_last_name`) AS name, "
//            . " `{$this->config['db_table_prefix']}employee`.`emp_id`, `{$this->config['db_table_prefix']}employee`.`pin`, `{$this->config['db_table_prefix']}employee`.`pay_rate` "
//            . " FROM `{$this->config['db_table_prefix']}check_in` "
//            . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
//            . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
//            . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
//            . " INNER JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
//            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
//            . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
//            . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}office`.`office_id` = $id AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` BETWEEN $start AND $end ";
            if($order == "by_emp")
            {
            $sql.= " ORDER BY `{$this->config['db_table_prefix']}employee`.`emp_first_name` ASC";
            }
            else 
            {
                $sql.= " ORDER BY `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` DESC";
            }
        }
        elseif($by == _CLIENT_)
        {
            $sql = "SELECT 
			SUM( `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp`) as check_in_timestamp , 
			SUM(`{$this->config['db_table_prefix']}check_out`.`check_out_timestamp`) as check_out_timestamp ,
			SUM( `{$this->config['db_table_prefix']}break_in`.`break_in_timestamp`) as break_in_timestamp , 
			SUM(`{$this->config['db_table_prefix']}break_out`.`break_out_timestamp`) as break_out_timestamp ,
			DATE_FORMAT( FROM_UNIXTIME( `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` ),'%d/%m/%Y' ) AS `date`, "
            
            . " `{$this->config['db_table_prefix']}office`.`office_name`, "
            . " CONCAT(`{$this->config['db_table_prefix']}employee`.`emp_first_name`, ' ', `{$this->config['db_table_prefix']}employee`.`emp_last_name`) AS name, "
            . " `{$this->config['db_table_prefix']}employee`.`emp_id`, `{$this->config['db_table_prefix']}employee`.`pin`, `{$this->config['db_table_prefix']}employee`.`pay_rate` "
            . " FROM `{$this->config['db_table_prefix']}check_in` "
            . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
            . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $id AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` BETWEEN $start AND $end "
            . "  GROUP By `{$this->config['db_table_prefix']}employee`.`pin` ORDER BY `check_in_timestamp` ASC";
        }
		
		
		
        $this->db->query($sql);
        if($this->db->is_true)
        {
            return cache::saveCache($this->db->result(), "all_".__CLASS__, "database");
        }
        return FALSE;
    } 
    //Detailed Report
    public function viewDetailedReportBetweenBy($id = '', $by = _EMPLOYEE_, $start = 0, $end = 0) 
    {
        if($by == _EMPLOYEE_)
        {
            $sql = "SELECT DATE_FORMAT( FROM_UNIXTIME( `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` ),'%d/%m/%Y' ) AS `date`, "
            . " `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp`, `{$this->config['db_table_prefix']}check_in`.`image_path` AS cin_image, "
            . " `{$this->config['db_table_prefix']}break_in`.`break_in_timestamp`, `{$this->config['db_table_prefix']}break_in`.`image_path` AS bin_image, "
            . " `{$this->config['db_table_prefix']}break_out`.`break_out_timestamp`, `{$this->config['db_table_prefix']}break_out`.`image_path` AS bout_image, "
            . " `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp`, `{$this->config['db_table_prefix']}check_out`.`image_path` AS cout_image, "
            . " `{$this->config['db_table_prefix']}office`.`office_name`, `{$this->config['db_table_prefix']}office`.`timezone`, `{$this->config['db_table_prefix']}office`.`timezone`, "
            . " CONCAT(`{$this->config['db_table_prefix']}employee`.`emp_first_name`, ' ', `{$this->config['db_table_prefix']}employee`.`emp_last_name`) AS name, "
            . " `{$this->config['db_table_prefix']}employee`.`emp_id`, `{$this->config['db_table_prefix']}employee`.`pay_rate`, `{$this->config['db_table_prefix']}employee`.`pin` "
            . " FROM `{$this->config['db_table_prefix']}check_in` "
            . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
            . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}employee`.`emp_id` = $id AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` BETWEEN $start AND $end "
            . " ORDER BY `date` ASC";
        }
        elseif($by == _OFFICE_)
        {
            $sql = "SELECT DATE_FORMAT( FROM_UNIXTIME( `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` ),'%d/%m/%Y' ) AS `date`, "
            . " `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp`, `{$this->config['db_table_prefix']}check_in`.`image_path` AS cin_image, "
            . " `{$this->config['db_table_prefix']}break_in`.`break_in_timestamp`, `{$this->config['db_table_prefix']}break_in`.`image_path` AS bin_image, "
            . " `{$this->config['db_table_prefix']}break_out`.`break_out_timestamp`, `{$this->config['db_table_prefix']}break_out`.`image_path` AS bout_image, "
            . " `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp`, `{$this->config['db_table_prefix']}check_out`.`image_path` AS cout_image, "
            . " `{$this->config['db_table_prefix']}office`.`office_name`, `{$this->config['db_table_prefix']}office`.`timezone`, "
            . " CONCAT(`{$this->config['db_table_prefix']}employee`.`emp_first_name`, ' ', `{$this->config['db_table_prefix']}employee`.`emp_last_name`) AS name, "
            . " `{$this->config['db_table_prefix']}employee`.`emp_id`, `{$this->config['db_table_prefix']}employee`.`pay_rate`, `{$this->config['db_table_prefix']}employee`.`pin` "
            . " FROM `{$this->config['db_table_prefix']}check_in` "
            . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
            . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}office`.`office_id` = $id AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` BETWEEN $start AND $end "
            . " ORDER BY `date` ASC";
        }
        elseif($by == _CLIENT_)
        {
            $sql = "SELECT 
			DATE_FORMAT( FROM_UNIXTIME( `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` ),'%d/%m/%Y' ) AS `date`, "
            . " `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp`, `{$this->config['db_table_prefix']}check_in`.`image_path` AS cin_image, "
            . " `{$this->config['db_table_prefix']}break_in`.`break_in_timestamp`, `{$this->config['db_table_prefix']}break_in`.`image_path` AS bin_image, "
            . " `{$this->config['db_table_prefix']}break_out`.`break_out_timestamp`, `{$this->config['db_table_prefix']}break_out`.`image_path` AS bout_image, "
            . " `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp`, `{$this->config['db_table_prefix']}check_out`.`image_path` AS cout_image, "
            . " `{$this->config['db_table_prefix']}office`.`office_name`, `{$this->config['db_table_prefix']}office`.`timezone`, "
            . " CONCAT(`{$this->config['db_table_prefix']}employee`.`emp_first_name`, ' ', `{$this->config['db_table_prefix']}employee`.`emp_last_name`) AS name, "
            . " `{$this->config['db_table_prefix']}employee`.`emp_id`, `{$this->config['db_table_prefix']}employee`.`pay_rate`, `{$this->config['db_table_prefix']}employee`.`pin` "
            . " FROM `{$this->config['db_table_prefix']}check_in` "
            . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
            . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $id AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` BETWEEN $start AND $end "
            . " ORDER BY `date` ASC";
        }
		
		
        $this->db->query($sql);
        if($this->db->is_true)
        {
            return cache::saveCache($this->db->result(), "all_".__CLASS__, "database");
        }
        return FALSE;
    }
    //update attendance
    public function edit($id = '', $checkin = 0, $breakin = 0, $breakout = 0, $checkout = 0, $ciStatus = _IN_TIME_, $coStatus = _OVERTIME_, $biStatus = _IN_TIME_, $boStatus = _IN_TIME_) 
    {
        $sql = "UPDATE `{$this->config['db_table_prefix']}check_in` "
        . " INNER JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
        . " SET `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` = $checkin, `{$this->config['db_table_prefix']}break_in`.`break_in_timestamp` = $breakin, "
        . " `{$this->config['db_table_prefix']}break_out`.`break_out_timestamp` = $breakout, `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp` = $checkout, "
        . " `{$this->config['db_table_prefix']}check_in`.`status` = $ciStatus, `{$this->config['db_table_prefix']}check_out`.`status` = $coStatus, "
        . " `{$this->config['db_table_prefix']}break_in`.`status` = $biStatus, `{$this->config['db_table_prefix']}break_out`.`status` = $boStatus"
        . " WHERE `{$this->config['db_table_prefix']}check_in`.`check_in_id` = $id";
        
        $this->db->query($sql);
        if($this->db->is_true)
        {
            return TRUE;
        }
        return FALSE;
    } 
    //View By Checkin ID
    public function viewByCheckinId($checkinId = '') 
    {
        $sql = "SELECT * FROM `{$this->config['db_table_prefix']}check_in` "
        . "INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
        . "LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
        . "LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
        . "LEFT JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
        . "WHERE `{$this->config['db_table_prefix']}check_in`.`check_in_id` = $checkinId LIMIT 1";
        
        $this->db->query($sql);
        if($this->db->is_true)
        {
            return cache::saveCache($this->db->result(), $checkinId."_".__CLASS__, "database");
        }
        return FALSE;
    } 
    //Count Attendance
    public function countAttendance($id = '', $by = _EMPLOYEE_, $type = "all", $fromTime = 0, $toTime = 999999999999) 
    {
        if($by == _EMPLOYEE_)
        {
            $sql = "SELECT COUNT(*) AS total "
            . " FROM `{$this->config['db_table_prefix']}check_in` "
            . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}check_in`.`emp_id` = `{$this->config['db_table_prefix']}employee`.`emp_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
            . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}check_in`.`emp_id` = $id ";
            if($type == "check_out")
            {
                $sql = $sql." AND `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp` IS NOT NULL "
                . " AND `{$this->config['db_table_prefix']}break_in`.`break_in_timestamp` IS NULL ";
            }
            elseif($type == "both")
            {
                $sql = $sql." AND `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp` IS NOT NULL "
                . " AND `{$this->config['db_table_prefix']}break_in`.`break_in_timestamp` IS NULL ";
            }
            $sql = $sql." AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` >= $fromTime AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` <= $toTime "
            . " ORDER BY `{$this->config['db_table_prefix']}check_in`.`check_in_id` ASC";
        }
        elseif($by == _CLIENT_)
        {
            $sql = "SELECT COUNT(*) AS total "
            . " FROM `{$this->config['db_table_prefix']}check_in` "
            . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}check_in`.`emp_id` = `{$this->config['db_table_prefix']}employee`.`emp_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
            . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}office`.`admin_id` = $id ";
            if($type == "check_out")
            {
                $sql = $sql." AND `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp` IS NOT NULL "
                . " AND `{$this->config['db_table_prefix']}break_in`.`break_in_timestamp` IS NULL ";
            }
            elseif($type == "both")
            {
                $sql = $sql." AND `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp` IS NOT NULL "
                . " AND `{$this->config['db_table_prefix']}break_in`.`break_in_timestamp` IS NULL ";
            }
            $sql = $sql." AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` >= $fromTime AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` <= $toTime "
            . " ORDER BY `{$this->config['db_table_prefix']}check_in`.`check_in_id` ASC";
        }
        elseif($by == _OFFICE_)
        {
        $sql = "SELECT COUNT(*) AS total "
            . " FROM `{$this->config['db_table_prefix']}check_in` "
            . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}check_in`.`emp_id` = `{$this->config['db_table_prefix']}employee`.`emp_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
            . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}check_in`.`office_id` = $id ";
            if($type == "check_out")
            {
                $sql = $sql." AND `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp` IS NOT NULL ";
            }
//            elseif($type == "both")
//            {
//                $sql = $sql." AND `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp` IS NOT NULL ";
//            }
            $sql = $sql." AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` >= $fromTime AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` <= $toTime "
            . " ORDER BY `{$this->config['db_table_prefix']}check_in`.`check_in_id` ASC";
        }
        elseif($by == _ADMIN_)
        {
            $sql = "SELECT COUNT(*) AS total "
            . "  FROM `{$this->config['db_table_prefix']}check_in` "
            . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}check_in`.`emp_id` = `{$this->config['db_table_prefix']}employee`.`emp_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
            . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_;
            if($type == "check_out")
            {
                $sql = $sql." AND `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp` IS NOT NULL AND ";
            }
//            elseif($type == "both")
//            {
//                $sql = $sql." WHERE `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp` IS NOT NULL AND ";
//            }
            $sql = $sql." AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` >= $fromTime AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` <= $toTime "
            . " ORDER BY `{$this->config['db_table_prefix']}check_in`.`check_in_id` ASC";
        }
        $this->db->query($sql);
        if($this->db->is_found)
        {
            $data = $this->db->result()->fetch_assoc();
            return $data['total'];
        }
        return FALSE;
    } 
    
    //Office Based Attendance From Given Timestamp For A Single Admin
    public function countAttendanceFromTime($adminId = '', $from = 0, $office = NULL) 
    {
        if($office != NULL)
        {
            $site = $office;
        }
        $row = '';
        $sql = "SELECT COUNT(`{$this->config['db_table_prefix']}check_in`.`check_in_id`) AS cin_total, "
        . " `{$this->config['db_table_prefix']}office`.`office_name` "
        . " FROM `{$this->config['db_table_prefix']}check_in` "
        . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
        . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $adminId AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` >= $from ";
        if(isset($site))
        {
            $sql = $sql." AND `{$this->config['db_table_prefix']}check_in`.`office_id` = $site";
        }
        $this->db->query($sql);
        $res = $this->db->result()->fetch_assoc();
        if($res['cin_total'] == NULL)
        {
            $row['cin_total'] = 0;
            $row['office_name'] = "Site";
        }
        else 
        {
            $row['cin_total'] = $res['cin_total'];
            $row['office_name'] = $res['office_name'];
        }

        $sql = "SELECT "
        . " COUNT(`{$this->config['db_table_prefix']}check_out`.`check_out_id`) AS cout_total, "
        . " `{$this->config['db_table_prefix']}office`.`office_name` "
        . " FROM `{$this->config['db_table_prefix']}check_in` "
        . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
        . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $adminId AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` >= $from ";
        if(isset($site))
        {
            $sql = $sql." AND `{$this->config['db_table_prefix']}check_in`.`office_id` = $site";
        }
        $this->db->query($sql);
        $res = $this->db->result()->fetch_assoc();
        if($res['cout_total'] == NULL)
        {
            $row['cout_total'] = 0;
        }
        else 
        {
            $row['cout_total'] = $res['cout_total'];
        }

        $sql = "SELECT "
        . " COUNT(`{$this->config['db_table_prefix']}break_in`.`break_in_id`) AS bin_total, "
        . " `{$this->config['db_table_prefix']}office`.`office_name` "
        . " FROM `{$this->config['db_table_prefix']}check_in` "
        . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
        . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $adminId AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` >= $from AND `{$this->config['db_table_prefix']}break_in`.`break_in_timestamp` <> `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp`";
        if(isset($site))
        {
            $sql = $sql." AND `{$this->config['db_table_prefix']}check_in`.`office_id` = $site";
        }
        $this->db->query($sql);
        $res = $this->db->result()->fetch_assoc();
        if($res['bin_total'] == NULL)
        {
            $row['bin_total'] = 0;
        }
        else 
        {
            $row['bin_total'] = $res['bin_total'];
        }

        $sql = "SELECT "
        . " COUNT(`{$this->config['db_table_prefix']}break_out`.`break_out_id`) AS bout_total, "
        . " `{$this->config['db_table_prefix']}office`.`office_name` "
        . " FROM `{$this->config['db_table_prefix']}check_in` "
        . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
        . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $adminId AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` >= $from AND `{$this->config['db_table_prefix']}break_out`.`break_out_timestamp` <> `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp`";
        if(isset($site))
        {
            $sql = $sql." AND `{$this->config['db_table_prefix']}check_in`.`office_id` = $site";
        }
        $this->db->query($sql);
        $res = $this->db->result()->fetch_assoc();
        if($res['bout_total'] == NULL)
        {
            $row['bout_total'] = 0;
        }
        else 
        {
            $row['bout_total'] = $res['bout_total'];
        }

        return $row;
        
//        if($this->db->is_true)
//        {
//            $obj = $this->db->result();
//            $object = [];
//            while($row = $obj->fetch_assoc())
//            {
//                $object[] = $row;
//            }
//            return $object;
//        }
//        return FALSE;
    } 
    //Client Admin Based Attendance From Given Timestamp
    public function countAllAttendanceFromTime($from = 0, $office = NULL) 
    {
        if($office != NULL)
        {
            $site = $office;
        }
        $row = '';
        $sql = "SELECT COUNT(`{$this->config['db_table_prefix']}check_in`.`check_in_id`) AS cin_total, "
        . " `{$this->config['db_table_prefix']}office`.`office_name` "
        . " FROM `{$this->config['db_table_prefix']}check_in` "
        . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
        . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` >= $from ";
        if(isset($site))
        {
            $sql = $sql." AND `{$this->config['db_table_prefix']}check_in`.`office_id` = $site ";
        }
        $sql = $sql." GROUP BY `{$this->config['db_table_prefix']}adminusers`.`admin_id` ";
        $this->db->query($sql);
        $res = $this->db->result()->fetch_assoc();
        if($res['cin_total'] == NULL)
        {
            $row['cin_total'] = 0;
            $row['office_name'] = "Site";
        }
        else 
        {
            $row['cin_total'] = $res['cin_total'];
            $row['office_name'] = $res['office_name'];
        }

        $sql = "SELECT "
        . " COUNT(`{$this->config['db_table_prefix']}check_out`.`check_out_id`) AS cout_total, "
        . " `{$this->config['db_table_prefix']}office`.`office_name` "
        . " FROM `{$this->config['db_table_prefix']}check_in` "
        . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
        . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` >= $from ";
        if(isset($site))
        {
            $sql = $sql." AND `{$this->config['db_table_prefix']}check_in`.`office_id` = $site ";
        }
        $sql = $sql." GROUP BY `{$this->config['db_table_prefix']}adminusers`.`admin_id` ";
        $this->db->query($sql);
        $res = $this->db->result()->fetch_assoc();
        if($res['cout_total'] == NULL)
        {
            $row['cout_total'] = 0;
        }
        else 
        {
            $row['cout_total'] = $res['cout_total'];
        }
				
        $sql = "SELECT "
        . " COUNT(`{$this->config['db_table_prefix']}break_in`.`break_in_id`) AS bin_total, "
        . " `{$this->config['db_table_prefix']}office`.`office_name` "
        . " FROM `{$this->config['db_table_prefix']}check_in` "
        . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
        . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` >= $from AND CASE WHEN `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp` IS NULL THEN TRUE ELSE `{$this->config['db_table_prefix']}break_in`.`break_in_timestamp` <> `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp` END ";//CASE `{$this->config['db_table_prefix']}break_in`.`break_in_timestamp` WHEN (`{$this->config['db_table_prefix']}break_in`.`break_in_timestamp` = `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp`) THEN 1<>1 ELSE 1=1 END ";
        if(isset($site))
        {
            $sql = $sql." AND `{$this->config['db_table_prefix']}check_in`.`office_id` = $site ";
        }
        $sql = $sql." GROUP BY `{$this->config['db_table_prefix']}adminusers`.`admin_id` ";
        $this->db->query($sql);
        $res = $this->db->result()->fetch_assoc();
        if($res['bin_total'] == NULL)
        {
            $row['bin_total'] = 0;
        }
        else 
        {
            $row['bin_total'] = $res['bin_total'];
        }
				
        $sql = "SELECT "
        . " COUNT(`{$this->config['db_table_prefix']}break_out`.`break_out_id`) AS bout_total, "
        . " `{$this->config['db_table_prefix']}office`.`office_name` "
        . " FROM `{$this->config['db_table_prefix']}check_in` "
        . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
        . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` >= $from AND CASE WHEN `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp` IS NULL THEN TRUE ELSE `{$this->config['db_table_prefix']}break_out`.`break_out_timestamp` <> `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp` END ";
        if(isset($site))
        {
            $sql = $sql." AND `{$this->config['db_table_prefix']}check_in`.`office_id` = $site ";
        }
        $sql = $sql." GROUP BY `{$this->config['db_table_prefix']}adminusers`.`admin_id` ";
        $this->db->query($sql);
        $res = $this->db->result()->fetch_assoc();
        if($res['bout_total'] == NULL)
        {
            $row['bout_total'] = 0;
        }
        else 
        {
            $row['bout_total'] = $res['bout_total'];
        }
				
        return $row;
				
        $sql = "SELECT COUNT(`{$this->config['db_table_prefix']}check_in`.`check_in_id`) AS cin_total, "
        . " COUNT(`{$this->config['db_table_prefix']}break_in`.`break_in_id`) AS bin_total, "
        . " COUNT(`{$this->config['db_table_prefix']}break_out`.`break_out_id`) AS bout_total, "
        . " COUNT(`{$this->config['db_table_prefix']}check_out`.`check_out_id`) AS cout_total, "
        . " CONCAT(`{$this->config['db_table_prefix']}adminusers`.`first_name`, ' ', `{$this->config['db_table_prefix']}adminusers`.`last_name`) AS client_name "
        . " FROM `{$this->config['db_table_prefix']}check_in` "
        . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_in`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}break_out`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id`"
        . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` >= $from ";
        if(isset($site))
        {
            $sql = $sql." AND `{$this->config['db_table_prefix']}office`.`office_id` = $site ";
        }
        $sql = $sql." GROUP BY `{$this->config['db_table_prefix']}adminusers`.`admin_id` ";
//        . " `{$this->config['db_table_prefix']}check_in`.`check_in_timestamp`, "
//        . " `{$this->config['db_table_prefix']}break_in`.`break_in_timestamp`, "
//        . " `{$this->config['db_table_prefix']}break_out`.`break_out_timestamp`, "
//        . " `{$this->config['db_table_prefix']}check_out`.`check_out_timestamp` ";
        
        $this->db->query($sql);
        if($this->db->is_true)
        {
            $obj = $this->db->result();
            $object = [];
            while($row = $obj->fetch_assoc())
            {
                $object[] = $row;
            }
            return $object;
        }
        return FALSE;
    } 
    //Daily Report
    public function dailyCheckinReportBy($id = NULL, $type = _CLIENT_, $fromTime = 0, $toTime = 999999999999)
    {
        $sql = '';
        if($type == _CLIENT_)
        {
            $sql = "SELECT * "
            . " FROM `{$this->config['db_table_prefix']}employee` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}check_in` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` AND`{$this->config['db_table_prefix']}check_in`.`check_in_timestamp` BETWEEN $fromTime AND $toTime "
            . " LEFT JOIN `{$this->config['db_table_prefix']}check_out` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}check_out`.`check_in_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}check_in`.`office_id` = `{$this->config['db_table_prefix']}office`.`office_id` "
            . " WHERE `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." AND `{$this->config['db_table_prefix']}employee`.`admin_id` = $id";
        }
        $this->db->query($sql);
        if($this->db->is_true)
        {
            $obj = $this->db->result();
            $object = [];
            while($row = $obj->fetch_assoc())
            {
                $object[] = $row;
            }
            return $object;
        }
        return FALSE;
    }
    //Flag Report By Umar
    public function flagReportOmi($id = NULL, $type = _EMPLOYEE_, $fromTime = 0, $toTime = 999999999999) 
    {
        $sql = "SELECT DATE_FORMAT( FROM_UNIXTIME( `check_in`.`check_in_timestamp` ),'%d/%m/%Y' ) AS `date`, `emp`.`emp_id`, `emp`.`pin`, CONCAT(`emp`.`emp_first_name`, ' ', `emp`.`emp_last_name`) AS emp_name, `off`.`office_name`, `off`.`timezone`, "
        . " `emp`.`timing_from`, `emp`.`timing_to`, `emp`.`emp_break_time`, `emp`.`emp_grace_time`, "
        . " `check_in`.`check_in_timestamp`, `check_in`.`status` AS `ciStatus`, "
        . " `break_in`.`break_in_timestamp`, `break_out`.`break_out_timestamp`, `break_out`.`status` AS `boStatus` "
        . " FROM `{$this->config['db_table_prefix']}employee` AS `emp` "
        . " INNER JOIN `{$this->config['db_table_prefix']}check_in` AS `check_in` ON `check_in`.`emp_id` = `emp`.`emp_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` AS `break_in` ON `check_in`.`check_in_id` = `break_in`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` AS `break_out` ON `check_in`.`check_in_id` = `break_out`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}check_out` AS `check_out` ON `check_in`.`check_in_id` = `check_out`.`check_in_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}office` AS `off` ON `off`.`office_id` = `check_in`.`office_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` AS `com` ON `com`.`admin_id` = `emp`.`admin_id` "
        . " WHERE `emp`.`status` <> "._DELETED_." AND `check_in`.`check_in_timestamp` >= $fromTime AND `check_in`.`check_in_timestamp` <= $toTime ";
        //. " AND (`check_in`.`status` = "._LATE_." OR `break_out`.`status` = "._LATE_.") ";
        if($type == _EMPLOYEE_)
        {
            $sql = $sql." AND `emp`.`emp_id` = $id ";
        }
        elseif($type == _OFFICE_)
        {
            $sql = $sql." AND `off`.`office_id` = $id ";
        }
        elseif($type == _CLIENT_)
        {
            $sql = $sql." AND `com`.`admin_id` = $id ";
        }
        $sql = $sql." GROUP BY `emp`.`pin`, `date` ORDER BY `check_in`.`check_in_timestamp` ASC";
        $this->db->query($sql);
        if($this->db->is_true)
        {
            $obj = $this->db->result();
            $object = [];
            while($row = $obj->fetch_assoc())
            {
                $object[] = $row;
            }
            return $object;
        }
        return FALSE;
    }
    //Flag Report
    public function flagReport($id = NULL, $type = _EMPLOYEE_, $fromTime = 0, $toTime = 999999999999) 
    {
        $sql = "SELECT `emp`.`emp_id`, `emp`.`pin`, CONCAT(`emp`.`emp_first_name`, ' ', `emp`.`emp_last_name`) AS emp_name, `off`.`office_name`, `off`.`timezone`, "
        . " DATE_FORMAT( FROM_UNIXTIME( `checkin`.`check_in_timestamp` ),'%d/%m/%Y' ) AS `attendance_date`, "
        . " DATE_FORMAT( FROM_UNIXTIME( `emp`.`timing_from` ),'%H:%i:%s' ) AS `shift_start_time`, "
        . " DATE_FORMAT( FROM_UNIXTIME( `checkin`.`check_in_timestamp` ),'%H:%i:%s' ) AS `attendance_time`, "
        . " TIMEDIFF(FROM_UNIXTIME( `checkin`.`check_in_timestamp` ), "
        . " CONCAT(DATE_FORMAT( FROM_UNIXTIME( `checkin`.`check_in_timestamp` ),'%Y-%m-%d' ),' ', DATE_FORMAT( FROM_UNIXTIME( `emp`.`timing_from` ),'%H:%i:%s' ))) AS `late_check_in_time`, "
        . " TIME_TO_SEC(TIMEDIFF(FROM_UNIXTIME( `checkin`.`check_in_timestamp` ), "
        . " CONCAT(DATE_FORMAT( FROM_UNIXTIME( `checkin`.`check_in_timestamp` ),'%Y-%m-%d' ),' ', DATE_FORMAT( FROM_UNIXTIME( `emp`.`timing_from` ),'%H:%i:%s' )))) / 60 AS `late_check-in_min`, "
        . " (CASE WHEN (`emp`.`emp_grace_time` = 0 AND `com`.`office_grace_time` = 0) THEN 0 WHEN `emp`.`emp_grace_time` = 0 THEN `com`.`office_grace_time` ELSE `emp`.`emp_grace_time` END) AS `grace_time_min`, "
        . " `emp`.`emp_grace_time`, `com`.`office_grace_time`, `emp`.`emp_break_time`, "
        . " DATE_FORMAT( FROM_UNIXTIME( `breakin`.`break_in_timestamp` ),'%H:%i:%s' ) AS `break_in`, "
        . " DATE_FORMAT( FROM_UNIXTIME( `breakout`.`break_out_timestamp` ),'%H:%i:%s' ) AS `break_out`, "
        . " TIME_TO_SEC(TIMEDIFF( FROM_UNIXTIME( `breakout`.`break_out_timestamp` ), "
        . " FROM_UNIXTIME( `breakin`.`break_in_timestamp`)))/60 AS `late_break_in`, "
        . " ((TIME_TO_SEC(TIMEDIFF(FROM_UNIXTIME( `checkin`.`check_in_timestamp` ), "
        . " CONCAT(DATE_FORMAT( FROM_UNIXTIME( `checkin`.`check_in_timestamp` ),'%Y-%m-%d' ),' ', DATE_FORMAT( FROM_UNIXTIME( `emp`.`timing_from` ),'%H:%i:%s' )))) / 60) "
        . " - (CASE WHEN (`emp`.`emp_grace_time` = 0 AND `com`.`office_grace_time` = 0) THEN 0 WHEN `emp`.`emp_grace_time` = 0 THEN `com`.`office_grace_time` ELSE `emp`.`emp_grace_time` END)) AS `late_check_in` "
        . " FROM `{$this->config['db_table_prefix']}employee` AS `emp` "
        . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` AS `com` ON `com`.`admin_id` = `emp`.`admin_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}check_in` AS `checkin` ON `checkin`.`emp_id` = `emp`.`emp_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}office` AS `off` ON `off`.`office_id` = `checkin`.`office_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_in` AS `breakin` ON `breakin`.`check_in_id` = `checkin`.`check_in_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}break_out` AS `breakout` ON `breakout`.`check_in_id` = `checkin`.`check_in_id` "
        . " WHERE `emp`.`status` <> "._DELETED_." AND ((TIME_TO_SEC(TIMEDIFF(FROM_UNIXTIME( `checkin`.`check_in_timestamp` ), "
        . " CONCAT(DATE_FORMAT( FROM_UNIXTIME( `checkin`.`check_in_timestamp` ),'%Y-%m-%d' ),' ', DATE_FORMAT( FROM_UNIXTIME( `emp`.`timing_from` ),'%H:%i:%s' )))) / 60) - (CASE WHEN (`emp`.`emp_grace_time` = 0 AND `com`.`office_grace_time` = 0) THEN 0 WHEN `emp`.`emp_grace_time` = 0 THEN `com`.`office_grace_time`  ELSE `emp`.`emp_grace_time` END)) > 0 "
        . " AND `checkin`.`check_in_timestamp` >= $fromTime AND `checkin`.`check_in_timestamp` <= $toTime ";
        if($type == _EMPLOYEE_)
        {
            $sql = $sql." AND `emp`.`emp_id` = $id ";
        }
        elseif($type == _OFFICE_)
        {
            $sql = $sql." AND `off`.`office_id` = $id ";
        }
        elseif($type == _CLIENT_)
        {
            $sql = $sql." AND `com`.`admin_id` = $id ";
        }
        $sql = $sql." ORDER BY late_check_in";
        
        $this->db->query($sql);
        if($this->db->is_true)
        {
            $obj = $this->db->result();
            $object = [];
            while($row = $obj->fetch_assoc())
            {
                $object[] = $row;
            }
            return $object;
        }
        return FALSE;
    }
    //Latest Activity 
    public function latestActivity($offset = 0, $limit = 10, $type = "both", $fromTime = 0) 
    {
        if($type == "check_in")
        {
            $action = "Checked-in";
        }
        elseif($type == "check_out")
        {
            $action = "Checked-out";
        }
        elseif($type == "both")
        {
            $action = "Checked-in' OR `Val`.`Action` = 'Checked-out";
        } 
        if(session::getUserType() == _ADMIN_)
        {
            $sql = "SELECT `{$this->config['db_table_prefix']}employee`.`emp_first_name`, `{$this->config['db_table_prefix']}employee`.`emp_last_name`, "
            . " `{$this->config['db_table_prefix']}employee`.`emp_id`, `{$this->config['db_table_prefix']}office`.`office_name`, `{$this->config['db_table_prefix']}office`.`timezone`, "
            . " (CASE WHEN COUNT(`Val`.`Time`) > 1 THEN 'Checked-out' ELSE `Val`.`Action` END) AS `Action` "
            . " , `Val`.`Time`, `Val`.`status`, `{$this->config['db_table_prefix']}office`.`timezone` "
            . " FROM ( (SELECT check_in_id, 'Checked-in' AS Action, check_in_timestamp AS Time, `status` FROM `{$this->config['db_table_prefix']}check_in`) "
            . " UNION ALL (SELECT check_in_id, 'Checked-out' AS Action, check_out_timestamp AS Time, `status` FROM `{$this->config['db_table_prefix']}check_out`) "
            . " UNION ALL (SELECT check_in_id, 'Start Break' AS Action, break_in_timestamp AS Time, `status` FROM `{$this->config['db_table_prefix']}break_in`) "
            . " UNION ALL (SELECT check_in_id, 'End Break' AS Action, break_out_timestamp AS Time, `status` FROM `{$this->config['db_table_prefix']}break_out` ) ) AS `Val` "
            . " INNER JOIN `{$this->config['db_table_prefix']}check_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `Val`.`check_in_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}office`.`office_id` = `{$this->config['db_table_prefix']}check_in`.`office_id` ";
            if($type == "both")
            {
                    $sql = $sql." WHERE (`Val`.`Action` = 'Checked-in' OR `Val`.`Action` = 'Checked-out') AND `Val`.`Time` > $fromTime";
            }
            elseif(isset($action))
            {
                $sql = $sql." WHERE `Val`.`Action` = '$action' AND `Val`.`Time` > $fromTime ";
            }
            else 
            {
                $sql = $sql." WHERE `Val`.`Time` > $fromTime ";
            }
            $sql = $sql." AND `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." GROUP BY `{$this->config['db_table_prefix']}employee`.`emp_first_name`, `{$this->config['db_table_prefix']}employee`.`emp_last_name`,  `{$this->config['db_table_prefix']}employee`.`emp_id`, `{$this->config['db_table_prefix']}office`.`office_name`, `Val`.`Time` "
            . " ORDER BY `Val`.`Time` DESC LIMIT $offset,$limit";
			
        }
        else
        {
            $sql = "SELECT `{$this->config['db_table_prefix']}employee`.`emp_first_name`, `{$this->config['db_table_prefix']}employee`.`emp_last_name`, "
            . " `{$this->config['db_table_prefix']}employee`.`emp_id`, `{$this->config['db_table_prefix']}office`.`office_name`, `{$this->config['db_table_prefix']}office`.`office_name`, `Val`.`Action`, `Val`.`Time`, `Val`.`status`, `{$this->config['db_table_prefix']}office`.`timezone` "
            . " FROM ( (SELECT check_in_id, 'Checked-in' AS Action, check_in_timestamp AS Time, `status` FROM `{$this->config['db_table_prefix']}check_in`) "
            . " UNION ALL (SELECT check_in_id, 'Checked-out' AS Action, check_out_timestamp AS Time, `status` FROM `{$this->config['db_table_prefix']}check_out`) "
            . " UNION ALL (SELECT check_in_id, 'Start Break' AS Action, break_in_timestamp AS Time, `status` FROM `{$this->config['db_table_prefix']}break_in`) "
            . " UNION ALL (SELECT check_in_id, 'End Break' AS Action, break_out_timestamp AS Time, `status` FROM `{$this->config['db_table_prefix']}break_out` ) ) AS `Val` "
            . " INNER JOIN `{$this->config['db_table_prefix']}check_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `Val`.`check_in_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}office`.`office_id` = `{$this->config['db_table_prefix']}check_in`.`office_id` "
            . " WHERE `{$this->config['db_table_prefix']}employee`.`admin_id` = ".session::getAdminId()." ";
            if($type == "both")
            {
                    $sql = $sql." AND (`Val`.`Action` = 'Checked-in' OR `Val`.`Action` = 'Checked-out') AND `Val`.`Time` > $fromTime";
            }
            elseif(isset($action))
            {
                $sql = $sql." AND `Val`.`Action` = '$action' AND `Val`.`Time` > $fromTime ";
            }
            else 
            {
                $sql = $sql." AND `Val`.`Time` > $fromTime ";
            }
            $sql = $sql." AND `{$this->config['db_table_prefix']}employee`.`status` <> "._DELETED_." ORDER BY `Val`.`Time` DESC LIMIT $offset,$limit";
			
			
			
        }
		
		
        $this->db->query($sql);
        if($this->db->is_true)
        {
            $obj = $this->db->result();
            $object = [];
            while($row = $obj->fetch_assoc())
            {
                $object[] = $row;
            }
            return $object;
        }
        return FALSE;
    } 
}
