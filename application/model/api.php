<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of api_logins
 *
 * @author Msajid
 */
class api extends model
{
    private $dbTable = "api_logins";
    private $created = false;
    
    //Just To Check Database ,Create If Not Exists
    private function dbcheck()
    { return TRUE;
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (
                    `api_logins_id` int(11) NOT NULL AUTO_INCREMENT,
                    `admin_id` int(11) NOT NULL UNIQUE,
                    `username` varchar(255) NOT NULL UNIQUE,
                    `password` varchar(255) NOT NULL,
                    `added_date` datetime NOT NULL,
                    `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`api_logins_id`),
                    FOREIGN KEY (`admin_id`) REFERENCES `{$this->config['db_table_prefix']}adminusers` (`admin_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return TRUE;
    }
    //First Time Create
    public function firstCreate()
    {
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (
                    `api_logins_id` int(11) NOT NULL AUTO_INCREMENT,
                    `admin_id` int(11) NOT NULL UNIQUE,
                    `username` varchar(255) NOT NULL UNIQUE,
                    `password` varchar(255) NOT NULL,
                    `added_date` datetime NOT NULL,
                    `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`api_logins_id`),
                    FOREIGN KEY (`admin_id`) REFERENCES `{$this->config['db_table_prefix']}adminusers` (`admin_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return TRUE;
    }
    //Insert Data Of Api Logins
    public function insertApiLogins($admin_id = '', $username = '', $password = '')
    {
        $sql = "INSERT INTO `{$this->config['db_table_prefix']}{$this->dbTable}`(`admin_id`, `username`, `password`, `added_date`) VALUES ('$admin_id' , '$username' , '$password' , now())";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return $this->db->last_id;
            }
            return FALSE;
        }
        return FALSE;
    }
    //update details
    public function update($admin_id = '', $username = '', $password = '')
    {
        $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `username` = '$username' , `password` = '$password' WHERE `admin_id` = $admin_id";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return true;
            }
            return FALSE;
        }
        return FALSE;
    }
    //userExists
    
    public function userExists($admin_id)
    {
        $sql = "SELECT `admin_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `admin_id` = '$admin_id' ";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->num_rows > 0)
            {
                return true;
            }
            return false;
        }
        return false;
    }
	//Get Username
    
    public function getUsername($adminId = '')
    {
        $sql = "SELECT `username` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `admin_id` = '$adminId' LIMIT 1";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $user = $this->db->result()->fetch_assoc();
                return $user['username'];
            }
            return false;
        }
        return false;
    }
    //username Exists
    
    public function usernameExists($user = '')
    {
        $sql = "SELECT `username` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `username` = '$user' LIMIT 1";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $user = $this->db->result()->fetch_assoc();
                return $user['username'];
            }
            return false;
        }
        return false;
    }
    //username exists except me
    public function usernameExistsExceptthis($user = '', $admin_id = '')
    {
        $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `username` = '$user' AND `admin_id` <> {$admin_id} LIMIT 1";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                return true;
            }
            return false;
        }
        return false;
    }
    //view User Name
    
    public function view($admin_id = '')
    {
        $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `admin_id` = $admin_id ";
        if($this->dbcheck())
        {          
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $user = $this->db->result()->fetch_assoc();
                return $user;
            }
            return false;
        }
        return false;
    }
    //api login
   
    public function apiLogin($username = '', $password = '')
    {
        $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE BINARY `username` = '$username' AND `password` = '$password'";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $data = $this->db->result()->fetch_assoc();
                $adminId = $data['admin_id'];
                return $this->loginAdmin($adminId);
            }
            return false;
        }
        return false;
    }
    //Access Login
    public function loginAdmin($adminId)
    {
        $sql="SELECT * FROM `{$this->config['db_table_prefix']}adminusers` INNER JOIN `{$this->config['db_table_prefix']}users` ON `admin_id` = `uid` WHERE `admin_id` = $adminId AND `user_type` = "._ADMIN_." AND `{$this->config['db_table_prefix']}adminusers`.`status` <> "._DELETED_." LIMIT 1";

        if($this->dbcheck())
        { 
            $this->db->query($sql);
            if($this->db->is_found)
            { 
                $data = $this->db->result()->fetch_assoc();
                session::sessionStart();
                session::loginUser($data['admin_id'], $data['user_id'], $data['first_name']." ".$data['last_name'], $data['username'], $data['admin_type'], $data['membership_type_id'], $data['permissions_mask']);
                /*$_SESSION["{$this->config['admin_session_prefix']}admin_id"] = $data['admin_id'];
                $_SESSION["{$this->config['admin_session_prefix']}user_id"] = $data['user_id'];
                $_SESSION["{$this->config['admin_session_prefix']}name"] = $data['first_name']." ".$data['last_name'];*/
                $_SESSION["{$this->config['admin_session_prefix']}country_id"] = $data['country_id'];
                return TRUE;
            }
            return false;
        }
        return FALSE;		
    }
}
