<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of access
 *
 * @author Msajid
 */
class admin extends model {
    
    private $dbTable = "adminusers";
    private $created = false;
    
    //Just To Check Database ,Create If Not Exists
    private function dbcheck()
    { return TRUE;
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}`(`admin_id` int(11) NOT NULL AUTO_INCREMENT,
                    `username` varchar(255) NOT NULL UNIQUE,
                    `password` varchar(32) NOT NULL,
                    `first_name` varchar(50) NOT NULL,
                    `last_name` varchar(50) NOT NULL,
                    `office_grace_time` int(11) NOT NULL,
                    `break_time` int(11) NOT NULL,
                    `break_grace_time_per` float NOT NULL,
                    `company_name` varchar(255) NOT NULL,
                    `date_of_birth` varchar(255) NOT NULL,
                    `country_id` int(11) NOT NULL,
                    `admin_type` int(11) NOT NULL,
                    `membership_type_id` int(11) NOT NULL,
                    `membership_start_time` date NOT NULL,
                    `membership_end_time` date NOT NULL,
                    `permissions_mask` int(11) NOT NULL,
                    `logo_path` varchar(255) NOT NULL,
                    `a_cin_message` LONGTEXT NOT NULL,
                    `a_cout_message` LONGTEXT NOT NULL,
                    `a_bin_message` LONGTEXT NOT NULL,
                    `a_bout_message` LONGTEXT NOT NULL,
                    `p_hash` VARCHAR(32) NOT NULL,
                    `p_hash_expire` INT(11) NOT NULL,
                    `email_reports` INT(11) NOT NULL,
                    `status` TINYINT NOT NULL,
                    `added_date` datetime NOT NULL,
                    `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`admin_id`),
                    FOREIGN KEY (`country_id`) REFERENCES `{$this->config['db_table_prefix']}countries`(`country_id`),
                    FOREIGN KEY (`membership_type_id`) REFERENCES `{$this->config['db_table_prefix']}membership_types` (`type_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return TRUE;
    }
    //Basic Actions
    public function actions($user, $action)
    {
        $sql = "";
        if($action == "activate")
        {
            $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `status` = "._ACTIVE_." WHERE `username` = '$user' ";
        }
        elseif($action == "deactivate")
        {
            $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `status` = "._DEACTIVE_." WHERE `username` = '$user' ";
        }
        elseif($action == "delete")
        {
            $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `status` = "._DELETED_." WHERE `username` = '$user' ";
        }
        if($this->dbcheck())
            {
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    //Just to Refresh the Cache
                    $this->viewAdmins(TRUE);
                    return TRUE;
                }
                return FALSE;
            }
            return FALSE;
        
    }
    //View All Admins
    public function viewAdmins($createCache = false, $offset = 0, $limit = 9999999999999)
    {
        $cacheObject = cache::readCache(__CLASS__);
        
        if(!$cacheObject || $createCache)
        {
            $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " INNER JOIN `{$this->config['db_table_prefix']}membership_types` ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`membership_type_id` = `{$this->config['db_table_prefix']}membership_types`.`type_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}users` ON `{$this->config['db_table_prefix']}users`.`uid` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}contact_numbers` ON `{$this->config['db_table_prefix']}contact_numbers`.`user_id` = `{$this->config['db_table_prefix']}users`.`user_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}email` ON `{$this->config['db_table_prefix']}email`.`user_id` = `{$this->config['db_table_prefix']}users`.`user_id` "
            . " WHERE `{$this->config['db_table_prefix']}users`.`user_type` = "._ADMIN_." AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` <> "._DELETED_."  AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_type` = "._CLIENT_." ORDER BY `{$this->config['db_table_prefix']}{$this->dbTable}`.`added_date` DESC LIMIT $limit OFFSET $offset" ;
            
            if($this->dbcheck())
            {
                $this->db->query($sql);                
                if($this->db->is_true)
                {
                    return cache::saveCache($this->db->result(), __CLASS__, "database");
                }
                return FALSE;
            }
            return FALSE;
        }
        return $cacheObject;
    }
    //Search Admin
    public function searchAdmin($query)
    {
        $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
        . " WHERE (`{$this->config['db_table_prefix']}{$this->dbTable}`.`first_name` LIKE '$query%' OR `{$this->config['db_table_prefix']}{$this->dbTable}`.`last_name` LIKE '$query%' ) AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` <> "._DELETED_." "
        . " AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_type` = "._CLIENT_." ORDER BY `{$this->config['db_table_prefix']}{$this->dbTable}`.`first_name`";

        if($this->dbcheck())
        {
            $this->db->query($sql);                
            if($this->db->is_true)
            {
                $obj = $this->db->result();
                $object = [];
                while($row = $obj->fetch_assoc())
                {
                    $object[] = $row;
                }
                return $object;
            }
            return FALSE;
        }
        return FALSE;
    }
    //Total No Of Admins
    public function totalAdmins() 
    {
        $sql = "SELECT COUNT(*) AS total FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` <> "._DELETED_."  AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_type` = "._CLIENT_ ;
        
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $data = $this->db->result()->fetch_assoc();
                return $data['total'];
            }
            return FALSE;
        }
        return FALSE;
    }
    //Load Single Admin Details
    public function loadDetails($user = '', $createCache = false)
    {
        $cacheObject = cache::readCache($user."_admin");
        
        if(!$cacheObject || $createCache)
        {
            $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` 
                    INNER JOIN `{$this->config['db_table_prefix']}membership_types` ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`membership_type_id` = `{$this->config['db_table_prefix']}membership_types`.`type_id`
                    INNER JOIN `{$this->config['db_table_prefix']}users` ON `{$this->config['db_table_prefix']}users`.`uid` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_id` 
                    INNER JOIN `{$this->config['db_table_prefix']}contact_numbers` ON `{$this->config['db_table_prefix']}contact_numbers`.`user_id` = `{$this->config['db_table_prefix']}users`.`user_id` 
                    INNER JOIN `{$this->config['db_table_prefix']}email` ON `{$this->config['db_table_prefix']}email`.`user_id` = `{$this->config['db_table_prefix']}users`.`user_id`
                    INNER JOIN `{$this->config['db_table_prefix']}addresses` ON
                    `{$this->config['db_table_prefix']}addresses`.`user_id` = `{$this->config['db_table_prefix']}users`.`user_id`
                    INNER JOIN `{$this->config['db_table_prefix']}countries` ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`country_id` = `{$this->config['db_table_prefix']}countries`.`country_id`
                    WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`username` = '$user'  AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` <> "._DELETED_ ." LIMIT 1";         
            
            if($this->dbcheck())
            {  
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return cache::saveCache($this->db->result(), $user."_client", "database");
                }
                return FALSE;
            }
            return false;
        }
        return $cacheObject;
    }
    
    
    /*public function loadAdmin()
    {
        $cacheObject = cache::readCache(__CLASS__);
        
        if(!$cacheObject)
        {
            $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE  `admin_type` = "._CLIENT_;
            if($this->dbcheck())
            {
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return cache::saveCache($this->db->result(), __CLASS__, "database");
                }
                return FALSE;
            }
            return false;
        }
        return $cacheObject;
    }*/
    
    public function adminExists($user = '', $type = '', $byId = false) 
    {
        if($byId == TRUE && !empty($type))
        {
            $sql = "SELECT `admin_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `admin_id` = '$user' AND `admin_type` = $type";
 
        }
        else if(!empty($type))
        {
            $sql = "SELECT `admin_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `username` = '$user' AND `admin_type` = $type";
        }
        else if($byId == TRUE && empty($type))
        {
            $sql = "SELECT `admin_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `admin_id` = '$user' AND `admin_type` = $type";
 
        }
        else if(empty($type))
        {
            $sql = "SELECT `admin_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `username` = '$user'";
            
        }
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                return true;
            }
            return false;
        }
        return false;
    }
    //Check if username exists except me
    public function adminExistsExceptMe($user = '') 
    {
        $sql = "SELECT `admin_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `username` = '$user' AND `admin_id` <> ".session::getAdminId();
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                return true;
            }
            return false;
        }
        return false;
    }
    //Check if username exists except this
    public function adminExistsExceptThis($user = '', $adminId = '') 
    {
        $sql = "SELECT `admin_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `username` = '$user' AND `admin_id` <> $adminId";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                return true;
            }
            return false;
        }
        return false;
    }
    //Id To USER ID
    public function idToUserId($adminId = '') 
    {
        $sql = "SELECT `user_id` FROM `{$this->config['db_table_prefix']}users` "
        . " INNER JOIN `{$this->config['db_table_prefix']}{$this->dbTable}` ON `{$this->config['db_table_prefix']}users`.`uid` =  `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_id` "
        . "WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_id` = '$adminId' AND `{$this->config['db_table_prefix']}users`.`user_type` = "._ADMIN_." LIMIT 1";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $user = $this->db->result()->fetch_assoc();
                return $user['user_id'];
            }
            return false;
        }
        return false;
    }
    //Id To Username
    public function idToUsername($id = '') 
    {
        $sql = "SELECT `username` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `admin_id` = '$id' LIMIT 1";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $user = $this->db->result()->fetch_assoc();
                return $user['username'];
            }
            return false;
        }
        return false;
    }
    //username ti id
    
    public function usernameToId($user) 
    {
        $sql = "SELECT `admin_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `username` = '$user'";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $user = $this->db->result()->fetch_assoc();
                return $user['admin_id'];
            }
            return false;
        }
        return false;
    }
    
    //edit admin get admin id for update and use in query
    
    public function edit($user = '', $firstName = '', $lastName = '', $company_name = '', $address = '', $country = '', $dob = '', $phone = '', $username = '', $email = '', $ecnryptedPassword = '', $memType = 1)
    {
        $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}`
		INNER JOIN `{$this->config['db_table_prefix']}membership_types` ON 
                `{$this->config['db_table_prefix']}{$this->dbTable}`.`membership_type_id` =
                `{$this->config['db_table_prefix']}membership_types`.`type_id`
                INNER JOIN `{$this->config['db_table_prefix']}users` 
                ON `{$this->config['db_table_prefix']}users`.`uid` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_id` 
                INNER JOIN `{$this->config['db_table_prefix']}contact_numbers` 
                ON `{$this->config['db_table_prefix']}contact_numbers`.`user_id` =
                `{$this->config['db_table_prefix']}users`.`user_id` 
                INNER JOIN `{$this->config['db_table_prefix']}email` ON
                `{$this->config['db_table_prefix']}email`.`user_id` = `{$this->config['db_table_prefix']}users`.`user_id`
                INNER JOIN `{$this->config['db_table_prefix']}addresses` ON
                `{$this->config['db_table_prefix']}addresses`.`user_id` = `{$this->config['db_table_prefix']}users`.`user_id`
                SET `{$this->config['db_table_prefix']}{$this->dbTable}`.`first_name` = '$firstName' , 
		`{$this->config['db_table_prefix']}{$this->dbTable}`.`last_name` = '$lastName',
                `{$this->config['db_table_prefix']}{$this->dbTable}`.`company_name` = '$company_name',
                `{$this->config['db_table_prefix']}{$this->dbTable}`.`membership_type_id` = '$memType',
		`{$this->config['db_table_prefix']}addresses`.`address` = '$address',
                `{$this->config['db_table_prefix']}{$this->dbTable}`.`country_id` = '$country', 
		`{$this->config['db_table_prefix']}{$this->dbTable}`.`date_of_birth` = '$dob',
                `{$this->config['db_table_prefix']}contact_numbers`.`number` = '$phone',
		`{$this->config['db_table_prefix']}{$this->dbTable}`.`username` = '$username',
                `{$this->config['db_table_prefix']}{$this->dbTable}`.`password` = '$ecnryptedPassword',
		`{$this->config['db_table_prefix']}email`.`email_address` = '$email' 
                WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`username` = '$user' 
                AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` <> "._DELETED_ ;
         if($this->dbcheck())
            {
                      
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return $this->loadDetails($username, TRUE);
                }
                return FALSE;
            }
            return FALSE;
    }
    public function editMembershipDates($username = '', $startDate = '', $endDate = '')
    {
        $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `membership_start_time` = '$startDate' , `membership_end_time` = '$endDate' WHERE `username` = '$username' ";
        
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return $this->loadDetails($username, TRUE);
            }
            return FALSE;
        }
        return FALSE;
    }
    //logo
    
        public function insertLogo($username = '', $logo_path = '')
    {
        $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `logo_path` = '$logo_path'  WHERE `username` = '$username' ";
        
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return $this->loadDetails($username, TRUE);
            }
            return FALSE;
        }
        return FALSE;
    }
    
    //Get Admin Logo
    
    public function getlogo($admin_id = '') 
    {
        $sql = "SELECT `logo_path` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `admin_id` = '$admin_id'";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $user = $this->db->result()->fetch_assoc();
                return $user['logo_path'];
            }
            return false;
        }
        return false;
    }
    
    //Get Global Messages
    
    public function viewMessage($adminId = '', $which = 'all')
    {
        $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_id` = $adminId LIMIT 1";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $data = $this->db->result()->fetch_assoc();
                if($which == "cin")
                {
                    return $data['a_cin_message'];
                }
                elseif($which == "cout")
                {
                    return $data['a_cout_message'];
                }
                elseif($which == "bin")
                {
                    return $data['a_bin_message'];
                }
                elseif($which == "bout")
                {
                    return $data['bout_message'];
                }
                else 
                {
                    return $data;
                }
            }
            return false;
        }
        return false;
    }
    
    //Update Messages
    public function updateMessages($adminId = '', $cin = '', $cout = '', $bin = '', $bout = '') 
    {
        $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `a_cin_message` = '$cin'"
                . ", `a_cout_message` = '$cout', `a_bin_message` = '$bin', `a_bout_message` = '$bout'  WHERE `admin_id` = $adminId LIMIT 1";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return TRUE;
            }
            return false;
        }
        return false;
    } 
    //Update Password
    public function updatePassword($username = '', $ecnryptedPassword = '') 
    {
        $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `password` = '$ecnryptedPassword' "
                . "WHERE `username` = '$username' LIMIT 1";
        
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return TRUE;
            }
            return false;
        }
        return false;
    } 
    //Get Reports Bit 
    public function getReportsBit($admin_id = '') 
    {
        $sql = "SELECT `email_reports` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `admin_id` = '$admin_id'";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found && $this->db->is_true)
            {
                $user = $this->db->result()->fetch_assoc();
                return $user['email_reports'];
            }
            return false;
        }
        return false;
    }
    //Set Gracetime
    public function setReportsBit($admin_id = '', $value = 0, $type = '+')
    { 
        if($type == '+')
        {
            $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `email_reports` = `email_reports` + $value WHERE `admin_id` = $admin_id ";
        }
        else 
        {
            $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `email_reports` = `email_reports` - $value WHERE `admin_id` = $admin_id ";
        }
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {


                return TRUE;
            }

            return FALSE;
        }

        return FALSE;
    }
    //Get Gracetime 
    public function getGraceTime($admin_id = '') 
    {
        $sql = "SELECT `office_grace_time` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `admin_id` = '$admin_id'";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found && $this->db->is_true)
            {
                $user = $this->db->result()->fetch_assoc();
                return $user['office_grace_time'];
            }
            return false;
        }
        return false;
    }
    //Set Gracetime
    public function setGracetime($admin_id = '', $grace_time = '')
    { 
        $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `office_grace_time` = '$grace_time' WHERE `admin_id` = $admin_id ";
        
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {


                return TRUE;
            }

            return FALSE;
        }

        return FALSE;
    }
    //Get Membership type 
    public function getMembershipType($admin_id = '') 
    {
        $sql = "SELECT `membership_type_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `admin_id` = '$admin_id'";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $user = $this->db->result()->fetch_assoc();
                return $user['membership_type_id'];
            }
            return false;
        }
        return false;
    }
    //Get Logo
    public function getLogoPath($admin_id = '') 
    {
        $sql = "SELECT `logo_path` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `admin_id` = '$admin_id'";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $user = $this->db->result()->fetch_assoc();
                return $user['logo_path'];
            }
            return false;
        }
        return false;
    }
    //Set Logo Path
    public function setLogoPath($admin_id = '', $logo_path = '')
    { 
        $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `logo_path` = '$logo_path' WHERE `admin_id` = $admin_id ";
        
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return TRUE;
            }

            return FALSE;
        }

        return FALSE;
    }
}