<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of office
 *
 * @author Msajid
 */
class office extends model
{
    private $dbTable = "office";
    private $created = false;
    
    public function dbcheck()
    { return TRUE;
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (
                    `office_id` int(11) NOT NULL AUTO_INCREMENT,
                    `admin_id` int(11) NOT NULL,
                    `office_name` varchar(255) NOT NULL,
                    `o_cin_message` LONGTEXT NOT NULL,
                    `o_cout_message` LONGTEXT NOT NULL,
                    `o_bin_message` LONGTEXT NOT NULL,
                    `o_bout_message` LONGTEXT NOT NULL,
                    `a_show_cin` TINYINT NOT NULL,
                    `a_show_cout` TINYINT NOT NULL,
                    `a_show_bin` TINYINT NOT NULL,
                    `a_show_bout` TINYINT NOT NULL,
                    `timezone` varchar(6) NOT NULL,
                    `status` TINYINT NOT NULL,
                    `added_date` datetime NOT NULL,
                    `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`office_id`),
                    FOREIGN KEY (`admin_id`) REFERENCES `{$this->config['db_table_prefix']}adminusers` (`admin_id`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return TRUE;
    }
    public function firstCreate()
    {
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (
                    `office_id` int(11) NOT NULL AUTO_INCREMENT,
                    `admin_id` int(11) NOT NULL,
                    `office_name` varchar(255) NOT NULL,
                    `o_cin_message` LONGTEXT NOT NULL,
                    `o_cout_message` LONGTEXT NOT NULL,
                    `o_bin_message` LONGTEXT NOT NULL,
                    `o_bout_message` LONGTEXT NOT NULL,
                    `a_show_cin` TINYINT NOT NULL,
                    `a_show_cout` TINYINT NOT NULL,
                    `a_show_bin` TINYINT NOT NULL,
                    `a_show_bout` TINYINT NOT NULL,
                    `timezone` varchar(6) NOT NULL,
                    `status` TINYINT NOT NULL,
                    `added_date` datetime NOT NULL,
                    `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`office_id`),
                    FOREIGN KEY (`admin_id`) REFERENCES `{$this->config['db_table_prefix']}adminusers` (`admin_id`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return TRUE;
    }
    public function viewForApi($adminId = '', $uuid = '')    {        $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "        . " INNER JOIN `{$this->config['db_table_prefix']}users` "        . " ON `{$this->config['db_table_prefix']}users`.`uid` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`office_id` "        . " INNER JOIN `{$this->config['db_table_prefix']}addresses` "        . " ON `{$this->config['db_table_prefix']}addresses`.`user_id` = `{$this->config['db_table_prefix']}users`.`user_id` "        . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` "        . " ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_id` = `{$this->config['db_table_prefix']}adminusers`.`admin_id` "        . " WHERE `{$this->config['db_table_prefix']}users`.`user_type` = "._OFFICE_        . " AND NOT EXISTS (SELECT * FROM `{$this->config['db_table_prefix']}api_logs` WHERE `{$this->config['db_table_prefix']}api_logs`.`uuid` <> '$uuid' AND `{$this->config['db_table_prefix']}api_logs`.`office_id` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`office_id`) "        . " AND `{$this->config['db_table_prefix']}adminusers`.`admin_id` = '$adminId' AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` <> "._DELETED_;              $this->db->query($sql);        if($this->db->is_true)        {             $obj = $this->db->result();            $object = [];            while($row = $obj->fetch_assoc())            {                $object[] = $row;            }            return $object;        }        return FALSE;    }
    public function insertOffice($adminId = '' , $name = '', $status = _DEFAULT_STATUS_)
    {
        $sql = "INSERT INTO `{$this->config['db_table_prefix']}{$this->dbTable}`(`admin_id`,`office_name`, `status`, `added_date`) VALUES ($adminId, '$name',  '$status',  now())";
        
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return $this->db->last_id;
            }
            return FALSE;
        }
        return FALSE;
    }
    
    public function view($user = '', $createCache = false, $offset = 0, $limit = 10)
    {
        $cacheObject = cache::readCache($user."_".__CLASS__);
        
        if(!$cacheObject || $createCache)
        {
            $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` 
                    INNER JOIN `{$this->config['db_table_prefix']}users`
                    ON `{$this->config['db_table_prefix']}users`.`uid` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`office_id` 
                    INNER JOIN `{$this->config['db_table_prefix']}addresses` 
                    ON `{$this->config['db_table_prefix']}addresses`.`user_id` = `{$this->config['db_table_prefix']}users`.`user_id` 
                    INNER JOIN `{$this->config['db_table_prefix']}adminusers`
                    ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_id` = `{$this->config['db_table_prefix']}adminusers`.`admin_id` 
                    WHERE `{$this->config['db_table_prefix']}users`.`user_type` = "._OFFICE_." 
                    AND `{$this->config['db_table_prefix']}adminusers`.`username` = '$user' AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` <> "._DELETED_." ORDER BY `office_name` ASC LIMIT $limit OFFSET $offset";
            if($this->dbcheck())
            {
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return cache::saveCache($this->db->result(), $user."_".__CLASS__, "database");
                }
                return FALSE;
            }
            return FALSE;
        }
        return $cacheObject;
    }
    //View By Admin ID
    public function viewById($adminId = '', $createCache = false, $adminType = _CLIENT_)
    {
        $cacheObject = cache::readCache($adminId."_".__CLASS__);
        
        if(!$cacheObject || $createCache)
        {
            if($adminType == _ADMIN_)
            {
                $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` 
                        INNER JOIN `{$this->config['db_table_prefix']}users`
                        ON `{$this->config['db_table_prefix']}users`.`uid` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`office_id` 
                        INNER JOIN `{$this->config['db_table_prefix']}addresses` 
                        ON `{$this->config['db_table_prefix']}addresses`.`user_id` = `{$this->config['db_table_prefix']}users`.`user_id` 
                        INNER JOIN `{$this->config['db_table_prefix']}adminusers`
                        ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_id` = `{$this->config['db_table_prefix']}adminusers`.`admin_id` 
                        WHERE `{$this->config['db_table_prefix']}users`.`user_type` = "._OFFICE_." 
                        AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` <> "._DELETED_;
            }
            else 
            {
                $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` 
                        INNER JOIN `{$this->config['db_table_prefix']}users`
                        ON `{$this->config['db_table_prefix']}users`.`uid` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`office_id` 
                        INNER JOIN `{$this->config['db_table_prefix']}addresses` 
                        ON `{$this->config['db_table_prefix']}addresses`.`user_id` = `{$this->config['db_table_prefix']}users`.`user_id` 
                        INNER JOIN `{$this->config['db_table_prefix']}adminusers`
                        ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_id` = `{$this->config['db_table_prefix']}adminusers`.`admin_id` 
                        WHERE `{$this->config['db_table_prefix']}users`.`user_type` = "._OFFICE_." 
                        AND `{$this->config['db_table_prefix']}adminusers`.`admin_id` = '$adminId' AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` <> "._DELETED_;
            }
            if($this->dbcheck())
            {
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return cache::saveCache($this->db->result(), $adminId."_".__CLASS__, "database");
                }
                return FALSE;
            }
            return FALSE;
        }
        return $cacheObject;
    }
    
    public function actions($officeId = '', $action = '')
    {
        $sql = "";
        if($action == "delete")
        {
            $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `status` = "._DELETED_." WHERE `office_id` = $officeId ";
        }
        
        if($this->dbcheck())
            {
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    //Just to Refresh the Cache
                    //$this->viewAdmins(TRUE);
                    return TRUE;
                }
                return FALSE;
            }
            return FALSE;
        
    }
    
    //office exists 
     public function officeExists($officeId = '') 
    {
        $sql = "SELECT `office_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `office_id` = $officeId ";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                return true;
            }
            return false;
        }
        return false;
    }
    
    public function officeToName($officeId) 
    {
        $sql = "SELECT `office_name` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `office_id` = '$officeId' LIMIT 1";
        
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $name = $this->db->result()->fetch_assoc();
                return $name['office_name'];
            }
            return NULL;
        }
        return NULL;
    }
    
    public function getTimezone($officeId = '') 
    {
        $sql = "SELECT `timezone` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `office_id` = '$officeId' LIMIT 1";
        
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $data = $this->db->result()->fetch_assoc();
                return $data['timezone'];
            }
            return NULL;
        }
        return NULL;
    }
    public function setTimezone($officeId = '', $timezone = '') 
    {
        $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `timezone` = '$timezone' WHERE `office_id` = '$officeId' LIMIT 1";
        
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return TRUE;
            }
            return NULL;
        }
        return NULL;
    }
    //office to admin
    
    public function officeToAdmin($office_id = '')
    {
        if($this->dbcheck())
        {
            $sql = "SELECT `admin_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `office_id` = '$office_id'";
            $this->db->query($sql);
            if($this->db->is_found)
                {
                    $admin = $this->db->result()->fetch_assoc();
                    return $admin['admin_id'];
                }
                return FALSE;
        }
        return FALSE;
    }
    //edit office
    
    public function update( $office_id = '', $office_name = '', $address = '')
    {
        $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}`
                INNER JOIN `{$this->config['db_table_prefix']}users` ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`office_id` = `{$this->config['db_table_prefix']}users`.`uid`
                INNER JOIN `{$this->config['db_table_prefix']}addresses` ON `{$this->config['db_table_prefix']}users`.`user_id` = `{$this->config['db_table_prefix']}addresses`.`user_id`
                SET `office_name` = '$office_name', `address` = '$address'
                WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`office_id` = $office_id AND `{$this->config['db_table_prefix']}users`.`user_type` ="._OFFICE_;
            
            if($this->dbcheck())
            {
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return $this->getOffice($office_id , TRUE);
                }
                return FALSE;
            }
            return FALSE;
    }
    
    //get office
    
    public function getOffice($office = '', $createCache = false)
    {
        $cacheObject = cache::readCache($office."_".__CLASS__);
        
        if(!$cacheObject || $createCache)
        {
            $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}`
                    INNER JOIN `{$this->config['db_table_prefix']}users` ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`office_id` = `{$this->config['db_table_prefix']}users`.`uid`
                    INNER JOIN `{$this->config['db_table_prefix']}addresses` ON `{$this->config['db_table_prefix']}users`.`user_id` = `{$this->config['db_table_prefix']}addresses`.`user_id`
                    WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`office_id` =$office AND `{$this->config['db_table_prefix']}users`.`user_type` ="._OFFICE_;
            
            if($this->dbcheck())
            {
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return cache::saveCache($this->db->result(), $office."_".__CLASS__, "database");
                }
                return FALSE;
            }
            return FALSE;
        }
        return $cacheObject;
    }
    //
    public function adminToOfficeRel($adminId = '', $officeId = '')
    {
        if(session::getUserType() == _ADMIN_)
        {
            return TRUE;
        }
        else 
        {
            $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `office_id` = $officeId AND `admin_id` = $adminId ";
            $this->db->query($sql);
            if($this->db->num_rows > 0)
            {
                return TRUE;
            }
            return FALSE;
        }
    }
    //Count Offices
    public function countOffices($adminId) 
    {
        $sql = "SELECT COUNT(*) AS total FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `admin_id` = $adminId AND `status` = "._ACTIVE_;
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                $data = $this->db->result()->fetch_assoc();
                return $data['total'];
            }
            return 0;
        }
        return 0;
    }
    
    //Total Offices
    public function totalOffices() 
    {
        $sql = "SELECT COUNT(*) AS total FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `status` = "._ACTIVE_;
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                $data = $this->db->result()->fetch_assoc();
                return $data['total'];
            }
            return 0;
        }
        return 0;
    }
    //Get Office Messages
    
    public function viewMessage($officeId = '', $which = 'all')
    {
        $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`office_id` = $officeId LIMIT 1";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $data = $this->db->result()->fetch_assoc();
                if($which == "cin")
                {
                    return $data['o_cin_message'];
                }
                elseif($which == "cout")
                {
                    return $data['a_cout_message'];
                }
                elseif($which == "bin")
                {
                    return $data['o_bin_message'];
                }
                elseif($which == "bout")
                {
                    return $data['o_bout_message'];
                }
                else 
                {
                    return $data;
                }
            }
            return false;
        }
        return false;
    }
    
    //Update Office Messages
    public function updateMessages($officeId = '', $cin = '', $cout = '', $bin = '', $bout = '', $s_a_cin = 0, $s_a_cout = 0, $s_a_bin = 0, $s_a_bout = 0) 
    {
        $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `o_cin_message` = '$cin'"
                . ", `o_cout_message` = '$cout', `o_bin_message` = '$bin', `o_bout_message` = '$bout'"
                . ", `a_show_cin` = $s_a_cin, `a_show_cout` = $s_a_cout, `a_show_bin` = $s_a_bin, `a_show_bout` = $s_a_bout WHERE `office_id` = $officeId LIMIT 1";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return TRUE;
            }
            return false;
        }
        return false;
    }
    //Id To Name
    public function idToName($officeId = '') 
    {
        $sql = "SELECT `office_name` AS name FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `office_id` = '$officeId' LIMIT 1";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $office = $this->db->result()->fetch_assoc();
                return $office['name'];
            }
            return false;
        }
        return false;
    }
    public function searchOffice($admin = '', $query = '', $createCache = false)
    {
        $cacheObject = cache::readCache($admin."_".$query."_".__CLASS__);
        
        if(!$cacheObject || $createCache)
        {
              $sql = "SELECT `{$this->config['db_table_prefix']}{$this->dbTable}`.* FROM `{$this->config['db_table_prefix']}{$this->dbTable}`
                    INNER JOIN `{$this->config['db_table_prefix']}adminusers` 
                    ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_id`
                    = `{$this->config['db_table_prefix']}adminusers`.`admin_id`
                    WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_id` = '$admin' AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` <>"._DELETED_." "
                    . "AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`office_name` LIKE '$query%'";
            if($this->dbcheck())
            {   
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return cache::saveCache($this->db->result(), $admin."_".__CLASS__, "database");
                }
                return FALSE;
            }
            return FALSE;
        }
        return $cacheObject;
    }
}
   