<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of api_logs
 *
 * @author Msajid
 */
class api_logs extends model
{
    private $dbTable = "api_logs";
    private $created = false;
    
    //Just To Check Database ,Create If Not Exists
    private function dbcheck()
    { return TRUE;
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (
                    `api_logs_id` int(11) NOT NULL AUTO_INCREMENT,
                    `log_id` int(11) NOT NULL,
                    `office_id` int(11) NOT NULL,
                    `uuid` varchar(255) NOT NULL UNIQUE,
                    `phpssid` varchar(255) NOT NULL,
                    `added_date` datetime NOT NULL,
                    `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`api_logs_id`),
                    FOREIGN KEY (`log_id`) REFERENCES `{$this->config['db_table_prefix']}logs`(`log_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;" ;
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return TRUE;
    }
    //First TIme Create
    public function firstCreate()
    {
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (
                    `api_logs_id` int(11) NOT NULL AUTO_INCREMENT,
                    `log_id` int(11) NOT NULL,
                    `office_id` int(11) NOT NULL,
                    `uuid` varchar(255) NOT NULL UNIQUE,
                    `phpssid` varchar(255) NOT NULL,
                    `added_date` datetime NOT NULL,
                    `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`api_logs_id`),
                    FOREIGN KEY (`log_id`) REFERENCES `{$this->config['db_table_prefix']}logs`(`log_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;" ;
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return TRUE;
    }
    //Insert Data
    
    public function insert($log_id = '', $uuid = '', $phpssid = '', $office_id = 0)
    {
        $sql = "INSERT INTO `{$this->config['db_table_prefix']}{$this->dbTable}`(`log_id` , `uuid` , `office_id`, `phpssid`, `added_date`)
                VALUES ('$log_id' , '$uuid', '$office_id', '$phpssid' , now())";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return $this->db->last_id;
            }
            return FALSE;
        }
        return FALSE;
    }
    //set office
    public function updateOffice($log_id = '', $office_id = '') 
    {
        $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `office_id` = $office_id WHERE `log_id` = '$log_id'";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return TRUE;
            }
            return FALSE;
        }
        return FALSE;
    }
    //Update Log_id From UUID
    public function updateLogId($log_id = '', $uuid = '', $phpssid = '') 
    {
        $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `log_id` = $log_id, `phpssid` = '$phpssid' WHERE `uuid` = '$uuid'";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return TRUE;
            }
            return FALSE;
        }
        return FALSE;
    }
    //Count How Many UUID's Related to an Admin
    public function countUUID($admin_id = '') 
    {
        $sql = "SELECT COUNT(`{$this->config['db_table_prefix']}{$this->dbTable}`.`uuid`) AS total FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
        . "INNER JOIN `{$this->config['db_table_prefix']}logs` ON `{$this->config['db_table_prefix']}logs`.`log_id` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`log_id` "
        . "INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id`  = `{$this->config['db_table_prefix']}logs`.`admin_id` "
        . "WHERE `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $admin_id";
        
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                $data = $this->db->result()->fetch_assoc();
                return $data['total'];
            }
            return FALSE;
        }
        return FALSE;
    }
    //Check if UUID already Exists
    public function uuidExists($adminId = '', $uuid = '') 
    {
        $sql = "SELECT `{$this->config['db_table_prefix']}{$this->dbTable}`.`uuid` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
        . "INNER JOIN `{$this->config['db_table_prefix']}logs` ON `{$this->config['db_table_prefix']}logs`.`log_id` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`log_id` "
        . "INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id`  = `{$this->config['db_table_prefix']}logs`.`admin_id` "
        . "WHERE `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $adminId AND `uuid` = '$uuid'";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                return TRUE;
            }
            return FALSE;
        }
        return FALSE;
    }
    //Check if uuid is unique
    public function isUnique($uuid = '') 
    {
        $sql = "SELECT `uuid` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `uuid` = '$uuid'";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if(!$this->db->is_found)
            {
                return TRUE;
            }
            return FALSE;
        }
        return FALSE;
    }
    //Get api_logs_id from UUID
    public function getUUIDId($logId = '') 
    {
        $sql = "SELECT `uuid` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `api_logs_id` = '$logId'";
        
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $data = $this->db->result()->fetch_assoc();
                return $data['uuid'];
            }
            return FALSE;
        }
        return FALSE;
    }	public function getUUIDIdByApiLog($logId = '')     {        $sql = "SELECT `uuid` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `log_id` = '$logId'";                if($this->dbcheck())        {            $this->db->query($sql);            if($this->db->is_found)            {                $data = $this->db->result()->fetch_assoc();                return $data['uuid'];            }            return FALSE;        }        return FALSE;    }
    //Get All UUID's related to An Account
    public function view($adminId = '', $createCache = false, $offset = 0, $limit = 10)
    {
        $cacheObject = cache::readCache($adminId."_".__CLASS__);
        
        if(!$cacheObject || $createCache)
        {
            $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}office` ON `{$this->config['db_table_prefix']}office`.`office_id` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`office_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}logs` ON `{$this->config['db_table_prefix']}logs`.`log_id` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`log_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id`  = `{$this->config['db_table_prefix']}logs`.`admin_id` "
        . " WHERE `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $adminId LIMIT $limit OFFSET $offset";
            if($this->dbcheck())
            {
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return cache::saveCache($this->db->result(), $adminId."_".__CLASS__, "database");
                }
                return FALSE;
            }
            return FALSE;
        }
        return $cacheObject;
    }
    //Delete By API
    public function delete($logId = '') 
    {
        $sql = "DELETE FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `api_logs_id` = '$logId'";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return TRUE;
            }
            return FALSE;
        }
        return FALSE;
    } 
    //Check If session Id Exists
    public function getDataBySession($phpssid = '') 
    {
        $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
        . " INNER JOIN `{$this->config['db_table_prefix']}logs` ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`log_id` = `{$this->config['db_table_prefix']}logs`.`log_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}logs`.`admin_id` = `{$this->config['db_table_prefix']}adminusers`.`admin_id` "
        . " INNER JOIN `{$this->config['db_table_prefix']}users` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}users`.`uid` "
        . "WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`phpssid` = '$phpssid'";
        
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $data = $this->db->result()->fetch_assoc();
                return $data;
            }
            return FALSE;
        }
        return FALSE;
    }
}
