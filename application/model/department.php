<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of office
 *
 * @author Msajid
 */
class department extends model
{
    private $dbTable = "department";
    private $created = false;
    
    public function dbcheck()
    { return TRUE;
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (
                    `dep_id` int(11) NOT NULL AUTO_INCREMENT,
                    `admin_id` int(11) NOT NULL,
                    `dep_name` varchar(255) NOT NULL,
                    `dep_description` LONGTEXT NOT NULL,
                    `status` TINYINT NOT NULL,
                    `added_date` datetime NOT NULL,
                    `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`dep_id`),
                    FOREIGN KEY (`admin_id`) REFERENCES `{$this->config['db_table_prefix']}adminusers` (`admin_id`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return TRUE;
    }
    public function firstCreate()
    {
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (
                    `dep_id` int(11) NOT NULL AUTO_INCREMENT,
                    `admin_id` int(11) NOT NULL,
                    `dep_name` varchar(255) NOT NULL,
                    `dep_description` LONGTEXT NOT NULL,
                    `status` TINYINT NOT NULL,
                    `added_date` datetime NOT NULL,
                    `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`dep_id`),
                    FOREIGN KEY (`admin_id`) REFERENCES `{$this->config['db_table_prefix']}adminusers` (`admin_id`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return TRUE;
    }
    
    public function insert($adminId = '' , $name = '', $description = '', $status = _DEFAULT_STATUS_)
    {
        $sql = "INSERT INTO `{$this->config['db_table_prefix']}{$this->dbTable}`(`admin_id`, `dep_name`, `dep_description`, `status`, `added_date`) VALUES ($adminId, '$name', '$description', '$status',  now())";
        
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return $this->db->last_id;
            }
            return FALSE;
        }
        return FALSE;
    }
    
    public function view($user = '', $createCache = false, $offset = 0, $limit = 10)
    {
        $cacheObject = cache::readCache($user."_".__CLASS__);
        
        if(!$cacheObject || $createCache)
        {
            $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` 
                    INNER JOIN `{$this->config['db_table_prefix']}users`
                    ON `{$this->config['db_table_prefix']}users`.`uid` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`dep_id` 
                    INNER JOIN `{$this->config['db_table_prefix']}adminusers`
                    ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_id` = `{$this->config['db_table_prefix']}adminusers`.`admin_id` 
                    WHERE `{$this->config['db_table_prefix']}users`.`user_type` = "._DEPARTMENT_." 
                    AND `{$this->config['db_table_prefix']}adminusers`.`username` = '$user' AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` <> "._DELETED_." LIMIT $limit OFFSET $offset";
            if($this->dbcheck())
            {
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return cache::saveCache($this->db->result(), $user."_".__CLASS__, "database");
                }
                return FALSE;
            }
            return FALSE;
        }
        return $cacheObject;
    }
    //View By Admin ID
    public function viewById($adminId = '', $createCache = false, $adminType = _CLIENT_)
    {
        $cacheObject = cache::readCache($adminId."_".__CLASS__);
        
        if(!$cacheObject || $createCache)
        {
            if($adminType == _ADMIN_)
            {
                $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` 
                        INNER JOIN `{$this->config['db_table_prefix']}users`
                        ON `{$this->config['db_table_prefix']}users`.`uid` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`dep_id` 
                        INNER JOIN `{$this->config['db_table_prefix']}adminusers`
                        ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_id` = `{$this->config['db_table_prefix']}adminusers`.`admin_id` 
                        WHERE `{$this->config['db_table_prefix']}users`.`user_type` = "._DEPARTMENT_." 
                        AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` <> "._DELETED_;
            }
            else 
            {
                $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` 
                        INNER JOIN `{$this->config['db_table_prefix']}users`
                        ON `{$this->config['db_table_prefix']}users`.`uid` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`dep_id` 
                        INNER JOIN `{$this->config['db_table_prefix']}adminusers`
                        ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_id` = `{$this->config['db_table_prefix']}adminusers`.`admin_id` 
                        WHERE `{$this->config['db_table_prefix']}users`.`user_type` = "._DEPARTMENT_." 
                        AND `{$this->config['db_table_prefix']}adminusers`.`admin_id` = '$adminId' AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` <> "._DELETED_;
            }
            if($this->dbcheck())
            {
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return cache::saveCache($this->db->result(), $adminId."_".__CLASS__, "database");
                }
                return FALSE;
            }
            return FALSE;
        }
        return $cacheObject;
    }
    
    public function actions($depId = '', $action = '')
    {
        $sql = "";
        if($action == "delete")
        {
            $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `status` = "._DELETED_." WHERE `dep_id` = $depId ";
        }
        
        if($this->dbcheck())
            {
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    //Just to Refresh the Cache
                    //$this->viewAdmins(TRUE);
                    return TRUE;
                }
                return FALSE;
            }
            return FALSE;
        
    }
    
    //office exists 
     public function deptExists($depId = '') 
    {
        $sql = "SELECT `dep_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `dep_id` = $depId ";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                return true;
            }
            return false;
        }
        return false;
    }
    
    public function deptToName($depId = '') 
    {
        $sql = "SELECT `dep_name` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `dep_id` = '$depId' LIMIT 1";
        
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $data = $this->db->result()->fetch_assoc();
                return $data['dep_name'];
            }
            return NULL;
        }
        return NULL;
    }
    //office to admin
    
    public function deptToAdmin($depId = '')
    {
        if($this->dbcheck())
        {
            $sql = "SELECT `admin_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `dep_id` = '$depId'";
            $this->db->query($sql);
            if($this->db->is_found)
                {
                    $admin = $this->db->result()->fetch_assoc();
                    return $admin['admin_id'];
                }
                return FALSE;
        }
        return FALSE;
    }
    //edit office
    
    public function update( $dep_id = '', $dep_name = '', $description = '')
    {
        $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}`
                INNER JOIN `{$this->config['db_table_prefix']}users` ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`dep_id` = `{$this->config['db_table_prefix']}users`.`uid`
                SET `dep_name` = '$dep_name', `dep_description` = '$description'
                WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`dep_id` = $dep_id AND `{$this->config['db_table_prefix']}users`.`user_type` ="._DEPARTMENT_;
            
            if($this->dbcheck())
            {
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return $this->getDept($dep_id , TRUE);
                }
                return FALSE;
            }
            return FALSE;
    }
    
    //get office
    
    public function getDept($dep = '', $createCache = false)
    {
        $cacheObject = cache::readCache($dep."_".__CLASS__);
        
        if(!$cacheObject || $createCache)
        {
            $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}`
                    INNER JOIN `{$this->config['db_table_prefix']}users` ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`dep_id` = `{$this->config['db_table_prefix']}users`.`uid`
                    WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`dep_id` = $dep AND `{$this->config['db_table_prefix']}users`.`user_type` ="._DEPARTMENT_;
            
            if($this->dbcheck())
            {
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return cache::saveCache($this->db->result(), $dep."_".__CLASS__, "database");
                }
                return FALSE;
            }
            return FALSE;
        }
        return $cacheObject;
    }
    //
    public function adminToDeptRel($adminId = '', $depId = '')
    {
        if(session::getUserType() == _ADMIN_)
        {
            return TRUE;
        }
        else 
        {
            $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `dep_id` = $depId AND `admin_id` = $adminId ";
            $this->db->query($sql);
            if($this->db->num_rows > 0)
            {
                return TRUE;
            }
            return FALSE;
        }
    }
    //Count Offices
    public function countDept($adminId = '') 
    {
        $sql = "SELECT COUNT(*) AS total FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `admin_id` = $adminId AND `status` = "._ACTIVE_;
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                $data = $this->db->result()->fetch_assoc();
                return $data['total'];
            }
            return 0;
        }
        return 0;
    }
    
    //Total Offices
    public function totalDept() 
    {
        $sql = "SELECT COUNT(*) AS total FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `status` = "._ACTIVE_;
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                $data = $this->db->result()->fetch_assoc();
                return $data['total'];
            }
            return 0;
        }
        return 0;
    }
    
    //Id To Name
    public function idToName($depId = '') 
    {
        $sql = "SELECT `dep_name` AS name FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `dep_id` = '$depId' LIMIT 1";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $dept = $this->db->result()->fetch_assoc();
                return $dept['name'];
            }
            return false;
        }
        return false;
    }
    //Name To Id
    public function nameToId($depName = '') 
    {
        $sql = "SELECT `dep_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `dep_name` = '$depName' LIMIT 1";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $dept = $this->db->result()->fetch_assoc();
                return $dept['dep_id'];
            }
            return false;
        }
        return false;
    }
    public function searchDept($admin = '', $query = '', $createCache = false)
    {
        $cacheObject = cache::readCache($admin."_".$query."_".__CLASS__);
        
        if(!$cacheObject || $createCache)
        {
              $sql = "SELECT `{$this->config['db_table_prefix']}{$this->dbTable}`.* FROM `{$this->config['db_table_prefix']}{$this->dbTable}`
                    INNER JOIN `{$this->config['db_table_prefix']}adminusers` 
                    ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_id`
                    = `{$this->config['db_table_prefix']}adminusers`.`admin_id`
                    WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_id` = '$admin' AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` <>"._DELETED_." "
                    . "AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`dep_name` LIKE '$query%'";
            if($this->dbcheck())
            {   
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return cache::saveCache($this->db->result(), $admin."_".__CLASS__, "database");
                }
                return FALSE;
            }
            return FALSE;
        }
        return $cacheObject;
    }
}
   