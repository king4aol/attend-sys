<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of settings
 *
 * @author PSSLT2540p
 */
class settings extends model{
    private $dbTable = "adminusers";
    private $created = false;
    
    //Just To Check Database ,Create If Not Exists
    private function dbcheck()
    { return TRUE;
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}`(`admin_id` int(11) NOT NULL AUTO_INCREMENT,
                    `username` varchar(255) NOT NULL UNIQUE,
                    `password` varchar(255) NOT NULL,
                    `first_name` varchar(50) NOT NULL,
                    `last_name` varchar(50) NOT NULL,
                    `office_grace_time` int(11) NOT NULL,
                    `company_name` varchar(255) NOT NULL,
                    `date_of_birth` date NOT NULL,
                    `country_id` int(11) NOT NULL,
                    `admin_type` int(11) NOT NULL,
                    `membership_type_id` int(11) NOT NULL,
                    `membership_start_time` date NOT NULL,
                    `membership_end_time` date NOT NULL,
                    `permissions_mask` int(11) NOT NULL,
                    `status` TINYINT NOT NULL,
                    `logo_path` varchar(255) NOT NULL,
                    `added_date` datetime NOT NULL,
                    `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`admin_id`),
                    FOREIGN KEY (`country_id`) REFERENCES `{$this->config['db_table_prefix']}countries`(`country_id`),
                    FOREIGN KEY (`membership_type_id`) REFERENCES `{$this->config['db_table_prefix']}membership_types` (`type_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return TRUE;
    }
    public function firstCreate()
    {
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}`(`admin_id` int(11) NOT NULL AUTO_INCREMENT,
                    `username` varchar(255) NOT NULL UNIQUE,
                    `password` varchar(255) NOT NULL,
                    `first_name` varchar(50) NOT NULL,
                    `last_name` varchar(50) NOT NULL,
                    `office_grace_time` int(11) NOT NULL,
                    `company_name` varchar(255) NOT NULL,
                    `date_of_birth` date NOT NULL,
                    `country_id` int(11) NOT NULL,
                    `admin_type` int(11) NOT NULL,
                    `membership_type_id` int(11) NOT NULL,
                    `membership_start_time` date NOT NULL,
                    `membership_end_time` date NOT NULL,
                    `permissions_mask` int(11) NOT NULL,
                    `status` TINYINT NOT NULL,
                    `logo_path` varchar(255) NOT NULL,
                    `added_date` datetime NOT NULL,
                    `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`admin_id`),
                    FOREIGN KEY (`country_id`) REFERENCES `{$this->config['db_table_prefix']}countries`(`country_id`),
                    FOREIGN KEY (`membership_type_id`) REFERENCES `{$this->config['db_table_prefix']}membership_types` (`type_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return TRUE;
    }
    //Get Client's Gracetime
    public function graceTime($adminId = '') 
    {
        $sql = "SELECT `office_grace_time` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_id` = $adminId";
            
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                $data = $this->db->result()->fetch_assoc();
                return $data['office_grace_time'];
            }
            return FALSE;
        }
        return FALSE;
    }
    //Update Gracetime
    public function updateGraceTime($adminId = '', $graceTime = '') 
    {
        $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `office_grace_time` = $graceTime WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_id` = $adminId";
            
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return $graceTime;
            }
            return FALSE;
        }
        return FALSE;
    }
}
