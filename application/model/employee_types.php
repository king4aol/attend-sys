<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of membership_types
 *
 * @author Msajid
 */
class employee_types extends model{
    private $dbTable = "employee_types";
    private $created = false;
    
    private function dbcheck()
    { return TRUE;
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (
                    `type_id` int(11) NOT NULL AUTO_INCREMENT,
                    `type_title` varchar(30) NOT NULL UNIQUE,
                    PRIMARY KEY (`type_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                $sql = "SELECT `type_title` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` LIMIT 1";
                $this->db->query($sql);
                if(!$this->db->is_found)
                {
                    $this->defaultTypes();
                }
                return TRUE;
            }
            else
            {
                return false;
            }
       }
       return TRUE;
    }
    public function firstCreate()
    {
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (
                    `type_id` int(11) NOT NULL AUTO_INCREMENT,
                    `type_title` varchar(30) NOT NULL UNIQUE,
                    PRIMARY KEY (`type_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                $sql = "SELECT `type_title` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` LIMIT 1";
                $this->db->query($sql);
                if(!$this->db->is_found)
                {
                    $this->defaultTypes();
                }
                return TRUE;
            }
            else
            {
                return false;
            }
       }
       return TRUE;
    }
    //Get Membership Types
    public function loadTypes() 
    {
        $cacheObject = cache::readCache(__CLASS__);
        
        if(!$cacheObject)
        {
            $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}`";
            if($this->dbcheck())
            {
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return cache::saveCache($this->db->result(), __CLASS__, "database");
                }
                return FALSE;
            }
            return false;
        }
        return $cacheObject;
    }
    //Default Types
    private function defaultTypes() 
    {
        $sql = "INSERT INTO `{$this->config['db_table_prefix']}{$this->dbTable}`(`type_title`) VALUES ('Full Time'), ('Casual'), ('Part Time')";
        $this->db->query($sql);
        
    }
    public function typeExists($type_id)
    {
        $sql = "SELECT `type_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `type_id` = $type_id";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                return TRUE;
            }
            return FALSE;
        }
        return FALSE;
    }
    
    public function getTypeId($title = '')
    {
        $sql = "SELECT `type_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `type_title` = '$title'";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->num_rows > 0)
            { 
                $id = $this->db->result()->fetch_assoc();
                return $id['type_id'];
            }
            return FALSE;
        }
        return FALSE;
    }
    
}
