<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of employee
 *
 * @author Msajid
 */
class employee extends model
{
    private $pinLength = 4;
    private $dbTable = "employee";
    private $created = false;
    
    public function dbcheck()
    { return TRUE;
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (
                    `emp_id` int(11) NOT NULL AUTO_INCREMENT,
                    `admin_id` int(11) NOT NULL,
                    `dep_id` int(11) NOT NULL,
                    `type_id` int(11) NOT NULL,
                    `emp_first_name` varchar(255) NOT NULL,
                    `emp_last_name` varchar(255) NOT NULL,
                    `timing_from` INT(11) NOT NULL,
                    `timing_to` INT(11) NOT NULL,
                    `pay_rate` double NOT NULL,
                    `overtime_pay_rate` double NOT NULL,
                    `pin` varchar(4) NOT NULL,
                    `e_cin_message` LONGTEXT NOT NULL,
                    `e_cout_message` LONGTEXT NOT NULL,
                    `e_bin_message` LONGTEXT NOT NULL,
                    `e_bout_message` LONGTEXT NOT NULL,
                    `a_show_cin` TINYINT NOT NULL,
                    `a_show_cout` TINYINT NOT NULL,
                    `a_show_bin` TINYINT NOT NULL,
                    `a_show_bout` TINYINT NOT NULL,
                    `o_show_cin` TINYINT NOT NULL,
                    `o_show_cout` TINYINT NOT NULL,
                    `o_show_bin` TINYINT NOT NULL,
                    `o_show_bout` TINYINT NOT NULL,
                    `emp_grace_time` int(11) NOT NULL,
                    `emp_break_time` int(11) NOT NULL,
                    `emp_break_grace_time_per` float NOT NULL,
                    `status` tinyint(4) NOT NULL,
                    `added_date` datetime NOT NULL,
                    `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`emp_id`),
                    UNIQUE(`admin_id`,`pin`),
                    FOREIGN KEY (`admin_id`) REFERENCES `{$this->config['db_table_prefix']}adminusers` (`admin_id`),
                    FOREIGN KEY (`dep_id`) REFERENCES `{$this->config['db_table_prefix']}department` (`dep_id`),
                    FOREIGN KEY (`type_id`) REFERENCES `{$this->config['db_table_prefix']}employee_types` (`type_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return TRUE;
    }
    public function firstCreate()
    {
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (
                    `emp_id` int(11) NOT NULL AUTO_INCREMENT,
                    `admin_id` int(11) NOT NULL,
                    `dep_id` int(11) NOT NULL,
                    `type_id` int(11) NOT NULL,
                    `emp_first_name` varchar(255) NOT NULL,
                    `emp_last_name` varchar(255) NOT NULL,
                    `timing_from` INT(11) NOT NULL,
                    `timing_to` INT(11) NOT NULL,
                    `pay_rate` double NOT NULL,
                    `overtime_pay_rate` double NOT NULL,
                    `pin` varchar(4) NOT NULL,
                    `e_cin_message` LONGTEXT NOT NULL,
                    `e_cout_message` LONGTEXT NOT NULL,
                    `e_bin_message` LONGTEXT NOT NULL,
                    `e_bout_message` LONGTEXT NOT NULL,
                    `a_show_cin` TINYINT NOT NULL,
                    `a_show_cout` TINYINT NOT NULL,
                    `a_show_bin` TINYINT NOT NULL,
                    `a_show_bout` TINYINT NOT NULL,
                    `o_show_cin` TINYINT NOT NULL,
                    `o_show_cout` TINYINT NOT NULL,
                    `o_show_bin` TINYINT NOT NULL,
                    `o_show_bout` TINYINT NOT NULL,
                    `status` tinyint(4) NOT NULL,
                    `emp_grace_time` int(11) NOT NULL,
                    `emp_break_time` int(11) NOT NULL,
                    `emp_break_grace_time_per` float NOT NULL,
                    `added_date` datetime NOT NULL,
                    `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`emp_id`),
                    UNIQUE(`admin_id`,`pin`),
                    FOREIGN KEY (`admin_id`) REFERENCES `{$this->config['db_table_prefix']}adminusers` (`admin_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return TRUE;
    }
    //employee to admin
    
    public function employeeToAdmin($emp_id = '')
    {
        if($this->dbcheck())
        {
            $sql = "SELECT `admin_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `emp_id` = '$emp_id'";
            $this->db->query($sql);
            if($this->db->is_found)
                {
                    $admin = $this->db->result()->fetch_assoc();
                    return $admin['admin_id'];
                }
                return FALSE;
        }
        return FALSE;
    }
    
    //exists pin
    
    public function pinExistsExceptThis($adminId = '', $emp_id = '', $pin = '')
    {
        if($this->dbcheck())
        {
            $sql = "SELECT `pin` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `admin_id` = '$adminId' AND `emp_id` <> '$emp_id' AND `pin` = '$pin' ";
            $this->db->query($sql);
            if($this->db->is_found)
            {
                return TRUE;
            }
            return FALSE;
        }
        return FALSE;
    }
    
    //generate pin 
    public function generatePin($adminId = '')
    {
        /*
        $exists = TRUE;
        $pin = 0;
        for($i = 0; $exists; $i++)
        {
            $pin = rand(pow(10, $this->pinLength-1), pow(10, $this->pinLength)-1);
            $sql = "SELECT `emp_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `pin` = $pin AND `admin_id` = '$adminId' ";
            $this->db->query($sql);
            if($this->db->is_found || $i > 1000)
            {
                $exists = FALSE;
            }
        }
        return $pin;
         * 
         */
        $sql = "SELECT `temp_pin` FROM `otl_temp_pins` t1 "
        . " WHERE NOT EXISTS( SELECT * FROM `otl_employee` t2 WHERE t1.`temp_pin` = t2.`pin` AND t2.`admin_id` = $adminId ) ORDER BY RAND() LIMIT 1";
        
        if($this->dbcheck())
        { 
            $this->db->query($sql);
            if($this->db->is_true)
            { 
                $data = $this->db->result()->fetch_assoc();
                return $data['temp_pin'];
            }
            return rand(1111,9999);
        }
    }
    
    public function insertEmployee($adminId = '' , $firstName = '' , $lastName = '', $timing_from = '', $timing_to = '', $pay_rate = '', $overtime_pay_rate = '',  $emp_grace_time = '', $break_time = '', $depId = '1', $typeId = '1', $status = _DEFAULT_STATUS_)
    {
			$sql = "SELECT * FROM `{$this->config['db_table_prefix']}department`";
			$this->db->query($sql);
			if($this->db->is_true && $this->db->is_found)
			{
				$sql = "INSERT INTO `{$this->config['db_table_prefix']}department`(`admin_id`, `dep_name`, `dep_description`, `status`, `added_date`) VALUES (1, 'No Department', 'Default', '1',  now())";
				$this->db->query($sql);
			}
			
            $sql = "INSERT INTO `{$this->config['db_table_prefix']}{$this->dbTable}`(`admin_id`, `dep_id`, `type_id`, `emp_first_name`, `emp_last_name`, `timing_from`, `timing_to`, `pay_rate`, `overtime_pay_rate`, `pin`, `status`, `emp_grace_time`, `emp_break_time`, `added_date`) VALUES ($adminId, $depId, $typeId, '$firstName', '$lastName', '$timing_from', '$timing_to', '$pay_rate', '$overtime_pay_rate', '".$this->generatePin($adminId)."', '$status', '$emp_grace_time', '$break_time',  now())";
            if($this->dbcheck())
            {
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return $this->db->last_id;
                }
                return FALSE;
            }
            return FALSE;
    }
    
    public function viewEmployee($admin = '', $createCache = false, $offset = 0, $limit = 999999999999)
    {
        $cacheObject = cache::readCache($admin."_".__CLASS__);
        
        if(!$cacheObject || $createCache)
        {
            /*$sql = "SELECT `{$this->config['db_table_prefix']}{$this->dbTable}`.* FROM `{$this->config['db_table_prefix']}{$this->dbTable}`
                    INNER JOIN `{$this->config['db_table_prefix']}emp_to_office` 
                    ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id`
                    = `{$this->config['db_table_prefix']}emp_to_office`.`emp_id`
                    INNER JOIN  `{$this->config['db_table_prefix']}office`
                     ON `{$this->config['db_table_prefix']}emp_to_office`.`office_id`
                    = `{$this->config['db_table_prefix']}office`.`office_id`
                    INNER JOIN `{$this->config['db_table_prefix']}adminusers`
                     ON `{$this->config['db_table_prefix']}office`.`admin_id`
                    = `{$this->config['db_table_prefix']}adminusers`.`admin_id`
                    WHERE `{$this->config['db_table_prefix']}office`.`office_id`
                    = $officeId AND `{$this->config['db_table_prefix']}adminusers`.`username` = '$admin'";*/
              $sql = "SELECT `{$this->config['db_table_prefix']}{$this->dbTable}`.* FROM `{$this->config['db_table_prefix']}{$this->dbTable}`
                    INNER JOIN `{$this->config['db_table_prefix']}adminusers` 
                    ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_id`
                    = `{$this->config['db_table_prefix']}adminusers`.`admin_id`
                    WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_id` = '$admin' AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` <>"._DELETED_." ORDER BY `emp_first_name` LIMIT $limit OFFSET $offset";
            if($this->dbcheck())
            {
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return cache::saveCache($this->db->result(), $admin."_".__CLASS__, "database");
                }
                return FALSE;
            }
            return FALSE;
        }
        return $cacheObject;
    }
    //Search Employee
    public function searchEmployee($admin = '', $query = '', $createCache = false)
    {
        $cacheObject = cache::readCache($admin."_".$query."_".__CLASS__);
        
        if(!$cacheObject || $createCache)
        {
              $sql = "SELECT `{$this->config['db_table_prefix']}{$this->dbTable}`.* FROM `{$this->config['db_table_prefix']}{$this->dbTable}`
                    INNER JOIN `{$this->config['db_table_prefix']}adminusers` 
                    ON `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_id`
                    = `{$this->config['db_table_prefix']}adminusers`.`admin_id`
                    WHERE `{$this->config['db_table_prefix']}{$this->dbTable}`.`admin_id` = '$admin' AND `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` <>"._DELETED_." "
                    . "AND (`{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_first_name` LIKE '$query%' OR `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_last_name` LIKE '$query%') ";
            if($this->dbcheck())
            {   
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return cache::saveCache($this->db->result(), $admin."_".__CLASS__, "database");
                }
                return FALSE;
            }
            return FALSE;
        }
        return $cacheObject;
    }
    //edit employee
    
    public function update($emp_id = '',  $firstName = '', $lastName = '', $timing_from = '', $timing_to = '', $pay_rate = '', $overtime_pay_rate = '', $grace_time = '', $pin = '', $break_time = '', $email = '', $depId = '', $typeId = '1')
    {
        $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` "
        . " INNER JOIN `{$this->config['db_table_prefix']}users` ON `{$this->config['db_table_prefix']}users`.`uid` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id` "
        . " LEFT JOIN `{$this->config['db_table_prefix']}email` ON `{$this->config['db_table_prefix']}email`.`user_id` = `{$this->config['db_table_prefix']}users`.`user_id`"
        . "SET `dep_id` = $depId, `type_id` = $typeId, `emp_first_name` = '$firstName', `emp_last_name` = '$lastName' , `timing_from` = '$timing_from' , `timing_to` = '$timing_to' , `pay_rate` = '$pay_rate' , `overtime_pay_rate` = '$overtime_pay_rate' , `emp_grace_time` = '$grace_time' , `emp_break_time` = '$break_time', `pin` = '$pin', `email_address` = '$email' "
        . "WHERE `emp_id` = $emp_id ";
            
            if($this->dbcheck())
            {
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return $this->getEmployee($emp_id, TRUE);
                }
                
                return FALSE;
            }
            return FALSE;
    }
    
    //getemployee
    
    public function getEmployee($employee = '', $createCache = false)
    {
        $cacheObject = cache::readCache($employee."_".__CLASS__);
        
        if(!$cacheObject || $createCache)
        {
            $sql = "SELECT *, `{$this->config['db_table_prefix']}{$this->dbTable}`.`status` FROM `{$this->config['db_table_prefix']}{$this->dbTable}`"
            . " LEFT JOIN `{$this->config['db_table_prefix']}users` ON `{$this->config['db_table_prefix']}users`.`uid` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}email` ON `{$this->config['db_table_prefix']}email`.`user_id` = `{$this->config['db_table_prefix']}users`.`user_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}department` ON `{$this->config['db_table_prefix']}department`.`dep_id` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`dep_id` "
            . " LEFT JOIN `{$this->config['db_table_prefix']}employee_types` ON `{$this->config['db_table_prefix']}employee_types`.`type_id` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`type_id` "
            . " WHERE `emp_id` = $employee";
            
            if($this->dbcheck())
            {
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    return cache::saveCache($this->db->result(), $employee."_".__CLASS__, "database");
                }
                return FALSE;
            }
            return FALSE;
        }
        return $cacheObject;
    }
    //actions like delete
    public function actions($empId = '', $action = '')
    {
        $sql = "";
        if($action == "delete")
        {
            $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `status` = "._DELETED_." WHERE `emp_id` = $empId ";
        }
        
        if($this->dbcheck())
            {
                $this->db->query($sql);
                if($this->db->is_true)
                {
                    //Just to Refresh the Cache
                    $this->viewEmployee($empId, TRUE);
                    return TRUE;
                }
                return FALSE;
            }
            return FALSE;
        
    }
    //admin to employee relation
    
    public function adminToEmployeeRel($adminId = '', $empId = '')
    {
        if(session::getUserType() == _ADMIN_)
        {
            return TRUE;
        }
        else 
        {
            $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `emp_id` = $empId AND `admin_id` = $adminId ";
            $this->db->query($sql);
            if($this->db->is_found)
            {
                return TRUE;
            }
            return FALSE;
        }
    }
  
    //Pin To Employee
    
    public function pinToEmployee($pin = '', $admin_id = '')
    {
        if($this->dbcheck())
        {
            $sql = "SELECT `emp_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `pin` = '$pin' AND `admin_id` = '$admin_id' ";
            $this->db->query($sql);
            if($this->db->is_found)
                {
                    $emp_id = $this->db->result()->fetch_assoc();
                    return $emp_id['emp_id'];
                }
                return FALSE;
        }
        return FALSE;
    }
    //Deactivate Employee
    public function deactivateEmp($empId = '')
    {
        $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `status` = "._DEACTIVE_." WHERE `emp_id` = $empId";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                $details = $this->getEmployee($empId);
                $detail = $details[0];
                /*
                 * Sending Deactivation Email
                 */
                sendmail::send()->deactivateEmp($detail['email_address'], $detail['emp_first_name']);
//                $mail = new PHPMailer\PHPMailer;    
//                $mail->fromAccounts(); //Setting From Email
//                $mail->addAddress($detail['email_address']);
//                $mail->isHTML(true);   
//
//                $mail->Subject = 'Notification Alert';
//                $mail->Body    = "<b>Dear {$detail['emp_first_name']},</b><br>"
//                . "<p>You Had Been Deactivate. Please Contact Your HR. Thanks<br></p>";
//                $mail->send();
                //End
                return TRUE;
            }
            return FALSE;
        }
        return FALSE;
    }
    //Activate Employee
    public function activateEmp($empId = '')
    {
        $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `status` = "._ACTIVE_." WHERE `emp_id` = $empId";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                $details = $this->getEmployee($empId);
                $detail = $details[0];
                /*
                 * Sending Deactivation Email
                 */
                sendmail::send()->activateEmp($detail['email_address'], $detail['emp_first_name']);
//                $mail = new PHPMailer\PHPMailer;    
//                $mail->fromAccounts(); //Setting From Email
//                $mail->addAddress($detail['email_address']);
//                $mail->isHTML(true);   
//
//                $mail->Subject = 'Notification Alert';
//                $mail->Body    = "<b>Dear {$detail['emp_first_name']},</b><br>"
//                . "<p>You Had Been Activate.</p>";
//                $mail->send();
                //End
                return TRUE;
            }
            return FALSE;
        }
        return FALSE;
    }
    //Check if Employee is Activated
    public function isActive($empId = '') 
    {
        $emp = $this->getEmployee($empId);
        if(is_array($emp))
        {
            if($emp[0]['status'] == _ACTIVE_)
            {
                return TRUE;
            }
            return FALSE;
        }
        return FALSE;
    }
    //Count Employees
    public function countEmployees($adminId = '', $showDeactivated = FALSE) 
    {
        if($showDeactivated)
        {
            $sql = "SELECT COUNT(`{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id`) AS total FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `admin_id` = $adminId AND `status` <> "._DELETED_;   
        }
        else 
        {
            $sql = "SELECT COUNT(`{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id`) AS total FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `admin_id` = $adminId AND `status` = "._ACTIVE_;
        }
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                $data = $this->db->result()->fetch_assoc();
                return $data['total'];
            }
            return 0;
        }
        return 0;
    }
    //Id To Name
    public function idToName($empId = '') 
    {
        $sql = "SELECT CONCAT(`emp_first_name`, ' ' ,`emp_last_name`) AS name FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `emp_id` = '$empId' LIMIT 1";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $emp = $this->db->result()->fetch_assoc();
                return $emp['name'];
            }
            return false;
        }
        return false;
    }
    //Employee Id to UserID for email etc
    public function idToUserId($empId = '') 
    {
        $sql = "SELECT `{$this->config['db_table_prefix']}users`.`user_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
        . " INNER JOIN `{$this->config['db_table_prefix']}users` "
        . " ON `{$this->config['db_table_prefix']}users`.`uid` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id` "
        . " WHERE `emp_id` = '$empId' AND `{$this->config['db_table_prefix']}users`.`user_type` = "._EMPLOYEE_." LIMIT 1";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $data = $this->db->result()->fetch_assoc();
                return $data['user_id'];
            }
            return false;
        }
        return false;
    }
    //Total Employees
    //Count Employees
    public function totalEmployees() 
    {
        $sql = "SELECT COUNT(`{$this->config['db_table_prefix']}{$this->dbTable}`.`emp_id`) AS total FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `status` <> "._DELETED_;   
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                $data = $this->db->result()->fetch_assoc();
                return $data['total'];
            }
            return 0;
        }
        return 0;
    }
    //Get Gracetime 
    public function getGraceTime($empId = '') 
    {
        $sql = "SELECT `emp_grace_time` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `emp_id` = '$empId'";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $user = $this->db->result()->fetch_assoc();
                return $user['emp_grace_time'];
            }
            return false;
        }
        return false;
    }
    //Get Job Starting TIme
    public function getFromTime($empId = '') 
    {
        $sql = "SELECT `timing_from` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `emp_id` = '$empId'";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $user = $this->db->result()->fetch_assoc();
                return $user['timing_from'];
            }
            return false;
        }
        return false;
    }
    //Get Job Ending TIme
    public function getToTime($empId = '') 
    {
        $sql = "SELECT `timing_to` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `emp_id` = '$empId'";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $user = $this->db->result()->fetch_assoc();
                return $user['timing_to'];
            }
            return false;
        }
        return false;
    }
    //Get Break Duration TIme
    public function getBreakDuration($empId = '') 
    {
        $sql = "SELECT `emp_break_time` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `emp_id` = '$empId'";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $user = $this->db->result()->fetch_assoc();
                return $user['emp_break_time'];
            }
            return false;
        }
        return false;
    }
    public function alter()
    {
        $sql = "ALTER TABLE `otl_employee` ADD (`dep_id` int(11) NOT NULL, `type_id` int(11) NOT NULL)";
        $this->db->query($sql);
				echo $this->db->error;
    }
    //pin Exists
    
   /* public function pinExists($pin = '')
    {
        if($this->dbcheck())
        {
            $sql = "SELECT `pin` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `pin` = '$pin' ";
            $this->db->query($sql);
            if($this->db->is_found)
            {
                return TRUE;
            }
            return FALSE;
        }
        return FALSE;
    }*/
}
