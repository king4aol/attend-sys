<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of break_in
 *
 * @author PSSLT2540p
 */
class break_in extends model{
    private $dbTable = "break_in";
    private $created = false;
    
    public function dbcheck()
    { return TRUE;
        if(!$this->created)
        {
            
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (`break_in_id` int(11) NOT NULL AUTO_INCREMENT,
                 `check_in_id` int(11) NOT NULL UNIQUE,
                 `break_in_timestamp` int(11) NOT NULL,
                 `image_path` varchar(255) NOT NULL,
                 `status` tinyint(4) NOT NULL,
                 PRIMARY KEY (`break_in_id`),
                 FOREIGN KEY (`check_in_id`) REFERENCES `{$this->config['db_table_prefix']}check_in`(`check_in_id`)
               ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return TRUE;
            }
            else
            {
                return false;
            }
       }
       return TRUE;
    }
    //First TIme
    public function firstCreate()
    {
        if(!$this->created)
        {
            
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (`break_in_id` int(11) NOT NULL AUTO_INCREMENT,
                 `check_in_id` int(11) NOT NULL UNIQUE,
                 `break_in_timestamp` int(11) NOT NULL,
                 `image_path` varchar(255) NOT NULL,
								  `status` tinyint(4) NOT NULL,
                 PRIMARY KEY (`break_in_id`),
                 FOREIGN KEY (`check_in_id`) REFERENCES `{$this->config['db_table_prefix']}check_in`(`check_in_id`)
               ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return TRUE;
            }
            else
            {
                return false;
            }
       }
       return TRUE;
    }
    //Insert Data
    public function insertBreakIn($check_in_id = '', $image_path = '', $break_in_timestamp = '', $status = _DEFAULT_STATUS_)
    {
        $sql = "INSERT INTO `{$this->config['db_table_prefix']}{$this->dbTable}`(`check_in_id`, `image_path`, `break_in_timestamp`, `status`) VALUES ($check_in_id, '$image_path', $break_in_timestamp, $status)";
       
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return $this->db->last_id;
            }
            return FALSE;
        }
        return FALSE;
    }
    //Checkin timestamp
    public function breakinTimestamp($checkinId = '')
    {
        $sql = "SELECT `break_in_timestamp` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `check_in_id` = $checkinId";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                $data = $this->db->result()->fetch_assoc();
                return $data['break_in_timestamp'];
            }
            return false;
        }
        return false;
    } 
    //Breakin Exists
    public function breakinExists($checkinId = '') 
    {
        $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `check_in_id` = $checkinId";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                return TRUE;
            }
            return false;
        }
        return false;
    } 
    //Latest Checkin Details
    public function lastestBreakin($adminId = '', $type = '')
    {
        if($type == _ADMIN_)
        {
            $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
            . " INNER JOIN `{$this->config['db_table_prefix']}check_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_id` "
            . " INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
            . "ORDER BY `break_in_id` DESC LIMIT 1";
        }
        else 
        {
         $sql = "SELECT * FROM `{$this->config['db_table_prefix']}{$this->dbTable}` "
         . "  INNER JOIN `{$this->config['db_table_prefix']}check_in` ON `{$this->config['db_table_prefix']}check_in`.`check_in_id` = `{$this->config['db_table_prefix']}{$this->dbTable}`.`check_in_id` "
         ." INNER JOIN `{$this->config['db_table_prefix']}employee` ON `{$this->config['db_table_prefix']}employee`.`emp_id` = `{$this->config['db_table_prefix']}check_in`.`emp_id` "
         ." INNER JOIN `{$this->config['db_table_prefix']}adminusers` ON `{$this->config['db_table_prefix']}adminusers`.`admin_id` = `{$this->config['db_table_prefix']}employee`.`admin_id` "
         ." WHERE `{$this->config['db_table_prefix']}adminusers`.`admin_id` = $adminId "
         ." ORDER BY `break_in_id` DESC LIMIT 1";
        } 
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
                return $this->db->result()->fetch_assoc();
            }
            return false;
        }
        return false;
    }
}
