<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of temp_pins
 *
 * @author PSSLT2540p
 */
class temp_pins extends model {
    private $dbTable = "temp_pins";
    private $created = false;
    
    private function dbcheck()
    { return TRUE;
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (`temp_pins_id` int(11) NOT NULL AUTO_INCREMENT,
                 `temp_pin` varchar(4) NOT NULL,
                 PRIMARY KEY (`temp_pins_id`)
               ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $sql = "SELECT `temp_pin` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` LIMIT 1";
                $this->db->query($sql);

                if(!$this->db->is_found)
                {
                    $this->created = TRUE;
                    return $this->insertPins();
                }
                $this->created = true;
                return TRUE;
            }
            else
            {
                return false;
            }
       }
       return TRUE;
    }
    //Add Rows From 0000 to 9999
    private function insertPins()
    {
        $pin = 0000;
        $sql = "INSERT INTO `{$this->config['db_table_prefix']}{$this->dbTable}`(`temp_pin`) VALUES ";
        for($i = 1; $i < 10000; $i++)
        {
            $pin = str_pad($pin + 1, 4, 0, STR_PAD_LEFT);
            $sql .= "('$pin'),";
            if($i % 500 == 0)
            {
                $pin = str_pad($pin + 1, 4, 0, STR_PAD_LEFT);
                $sql .= "('$pin');";
                $this->db->query($sql);
                $sql = "INSERT INTO `{$this->config['db_table_prefix']}{$this->dbTable}`(`temp_pin`) VALUES ";
            }
        }
    }
    public function firstCreate() 
    {
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (`temp_pins_id` int(11) NOT NULL AUTO_INCREMENT,
                 `temp_pin` varchar(4) NOT NULL,
                 PRIMARY KEY (`temp_pins_id`)
               ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $sql = "SELECT `temp_pin` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` LIMIT 1";
                $this->db->query($sql);

                if(!$this->db->is_found)
                {
                    $this->created = TRUE;
                    return $this->insertPins();
                }
                $this->created = true;
                return TRUE;
            }
            else
            {
                return false;
            }
    }
    //Create Table
    public function createTable() 
    {
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (`temp_pins_id` int(11) NOT NULL AUTO_INCREMENT,
                 `temp_pin` varchar(4) NOT NULL,
                 PRIMARY KEY (`temp_pins_id`)
               ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $sql = "SELECT `temp_pin` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` LIMIT 1";
                $this->db->query($sql);

                if(!$this->db->is_found)
                {
                    $this->created = TRUE;
                    return $this->insertPins();
                }
                $this->created = true;
                return TRUE;
            }
            else
            {
                return false;
            }
    }
}
