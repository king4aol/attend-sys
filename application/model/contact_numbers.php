<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of contact_numbers
 *
 * @author Msajid
 */
class contact_numbers extends model {
    private $dbTable = __CLASS__;
    private $created = false;
    
    private function dbcheck()
    { return TRUE;
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (
                    `number_id` int(11) NOT NULL AUTO_INCREMENT,
                    `user_id` int(11) NOT NULL,
                    `type` varchar(20) NOT NULL,
                    `number` varchar(20) NOT NULL,
                    `added_date` datetime NOT NULL,
                    `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`number_id`),
                    FOREIGN KEY (`user_id`) REFERENCES `{$this->config['db_table_prefix']}users`(`user_id`),
                    UNIQUE(`type`,`number`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return TRUE;
    }
    public function firstCreate()
    {
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (
                    `number_id` int(11) NOT NULL AUTO_INCREMENT,
                    `user_id` int(11) NOT NULL,
                    `type` varchar(20) NOT NULL,
                    `number` varchar(20) NOT NULL,
                    `added_date` datetime NOT NULL,
                    `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`number_id`),
                    FOREIGN KEY (`user_id`) REFERENCES `{$this->config['db_table_prefix']}users`(`user_id`),
                    UNIQUE(`type`,`number`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return true;
            }
            return false;
        }
        return TRUE;
    }
    public function insertNumber($userId, $number, $type = 'personal') 
    {
        $sql = "INSERT INTO `{$this->config['db_table_prefix']}{$this->dbTable}`(`user_id`, `number`, `type`, `added_date`) VALUES ($userId, '$number', '$type', now())";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return $this->db->last_id;
            }
            return false;
        }
        return false;
    }
    
    public function validate($phone ,$country_code)
    {
        spl_autoload_register(function ($class)
            {
                // project-specific namespace prefix
                $prefix = 'libphonenumber\\';

                // base directory for the namespace prefix
                $base_dir = '../application/libs/libphonenumber/';

                // does the class use the namespace prefix?
                $len = strlen($prefix);
                if (strncmp($prefix, $class, $len) !== 0)
                {
                    // no, move to the next registered autoloader
                    return;
                }

                // get the relative class name
                $relative_class = substr($class, $len);

                // replace the namespace prefix with the base directory, replace namespace
                // separators with directory separators in the relative class name, append
                // with .php
                $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

                // if the file exists, require it
                if (file_exists($file)) 
                {
                    require $file;
                }
            });

        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

        try 
        {
            $phoneProto = $phoneUtil->parse($phone, $country_code);
            if(!$phoneUtil->isValidNumber($phoneProto))
            {
                return FALSE;
            }
            return TRUE;
        }
        catch (\libphonenumber\NumberParseException $e) 
        {
            return FALSE;
        }
    }
    //Check If Number Exists
    public function numberExists($number , $type = 'personal')
    {
        $sql = "SELECT  `user_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `number` = '$number' AND `type` = '$type'";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
               return TRUE;
            }
            return FALSE;
        }
        return FALSE;
    }
    //Check of number exists except me
    public function numberExistsExceptThis($number , $userId = '', $type = 'personal')
    {
        $sql = "SELECT  `user_id` FROM `{$this->config['db_table_prefix']}{$this->dbTable}` WHERE `number` = '$number' AND `type` = '$type' AND `user_id` <> $userId";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_found)
            {
               return TRUE;
            }
            return FALSE;
        }
        return FALSE;
    }
}
