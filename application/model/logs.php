<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of logs
 *
 * @author Msajid
 */
class logs extends model
{
    private $dbTable = "logs";
    private $created = false;
    
    private function dbcheck()
    { return TRUE;
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (`log_id` int(11) NOT NULL AUTO_INCREMENT,
                    `admin_id` int(11) NOT NULL,
                    `ip_address` varchar(15) NOT NULL,
                    `user_agent` varchar(255) NOT NULL,
                    `login_time` datetime NOT NULL,
                    `logout_time` datetime NOT NULL,
                    PRIMARY KEY (`log_id`),
                    FOREIGN KEY (`admin_id`) REFERENCES `{$this->config['db_table_prefix']}adminusers`(`admin_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return TRUE;
            }
            else
            {
                return false;
            }
       }
       return TRUE;
    }
    public function firstCreate()
    {
        if(!$this->created)
        {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->config['db_table_prefix']}{$this->dbTable}` (`log_id` int(11) NOT NULL AUTO_INCREMENT,
                    `admin_id` int(11) NOT NULL,
                    `ip_address` varchar(15) NOT NULL,
                    `user_agent` varchar(255) NOT NULL,
                    `login_time` datetime NOT NULL,
                    `logout_time` datetime NOT NULL,
                    PRIMARY KEY (`log_id`),
                    FOREIGN KEY (`admin_id`) REFERENCES `{$this->config['db_table_prefix']}adminusers`(`admin_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
            $this->dtable->query($sql);
            if($this->dtable->is_true)
            {
                $this->created = true;
                return TRUE;
            }
            else
            {
                return false;
            }
       }
       return TRUE;
    }
    
    public function insertLog($userid)
    {
        if(isset($_SERVER['REMOTE_ADDR']))
        {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        else
        {
            $ip = "";
        }
         if(isset($_SERVER['HTTP_USER_AGENT']))
         {
            $browser = securestr::clean($_SERVER['HTTP_USER_AGENT']);
         }
         else
         {
             $browser = "";
         }
         
        $sql = "INSERT INTO `{$this->config['db_table_prefix']}{$this->dbTable}`( `admin_id`, `ip_address`, `user_agent`, `login_time`) VALUES ('$userid', '$ip', '$browser', now())";
        
        if($this->dbcheck())
        {
            $this->db->query($sql);
                if($this->db->is_true)
                {
                    return $this->db->last_id;
                }
                return FALSE;
        }
        return FALSE;
    }
    
    public function expireLog($logId = "") 
    {
        $sql = "UPDATE `{$this->config['db_table_prefix']}{$this->dbTable}` SET `logout_time` = now() WHERE `log_id` = $logId";
        if($this->dbcheck())
        {
            $this->db->query($sql);
            if($this->db->is_true)
            {
                return TRUE;
            }
            return FALSE;
        }
        return FALSE;
    }

 }
