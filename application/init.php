<?php
//Enable Buffering
ini_set('output_buffering', 'on');
header("Expires: Sat, 01 Jan 2000 00:00:00 GMT"); 
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
header("Cache-Control: post-check=0, pre-check=0",false); 
session_cache_limiter("must-revalidate");
ob_start();
//Configurations
require_once 'includes/config.php';
//Error Display & Hide
if($configuration['debug'])
{
    error_reporting(E_ALL);
}
else
{
    error_reporting(0);
}
//Set Page Render Time
set_time_limit($configuration['render_time']);
//Set Timezone
date_default_timezone_set($configuration['timezone']);
//Auto Helper Class Loader;
spl_autoload_register(function($class){
    $class = str_replace("\\", "/", $class);
    if(file_exists(_INCLUDES_PATH_."/{$class}.php"))
    {
        require_once _INCLUDES_PATH_."/{$class}.php";
    }
    elseif(file_exists(_LIBRARIES_PATH_."/{$class}.php"))
    {
        require_once _LIBRARIES_PATH_."/{$class}.php";
    }
});
function get_timezone_abbr($offset = 0)
{
//    $sess_key = 'timezone_abbr_name';
//    session::sessionStart();
//    if(!isset($_SESSION[$sess_key]))
//    {
        $abbrivations = timezone_abbreviations_list();
        
        foreach($abbrivations as $k => $arr)
	{
	   foreach($arr as $key => $array)
	   {
                if ($array['offset'] == $offset)
                {
                    $_SESSION[$sess_key] = $k;
                    return $k;
                }
	   }
	}
	return false;
//        
//    }
//    return $_SESSION[$sess_key];
}

//Database Class
require_once _INCLUDES_PATH_."/database.php";
//Object for Transactions and Queries
$database = new db();
//Object for Creating Table and unTransactional Queries
$dtable = new db();
//Core Files Required
require_once 'core/app.php';
require_once 'core/model.php';
require_once 'core/controller.php';
//Outputing Buffer
ob_end_flush();
