<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of model
 *
 * @author Msajid
 */
class model {
    //Configuration
    protected $config = [];
    //Database Object
    public $db;
    //Database Object
    protected $dtable;


    public function __construct() 
    {
        global $configuration;
        $this->config = $configuration;
        
        //Intialazing Database Object
        global $database;
        $this->db = $database;
        
        global $dtable;
        $this->dtable = $dtable;
    }
}
