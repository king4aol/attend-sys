<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of app
 *
 * @author Msajid
 */
class app {
    //Default Controller
    private $controller = "home";
    //Default Method
    private $method = "index";
    //Parameters
    private $params = [];
    
    public function __construct() 
    {
        //Taking Route into Url
        $url = $this->splitUrl();
        if(isset($url[0]) && $url[0] != "403.shtml")
        {
            if(file_exists(_CONTROLLER_PATH_."/{$url[0]}.php"))
            {
                $this->controller = $url[0];
                unset($url[0]);
            }
            else
            {
                $this->controller = "error";
                $this->method = "_404";
                unset($url[0]);
                unset($url[1]);
            }
            require_once _CONTROLLER_PATH_."/{$this->controller}.php";
            
            $this->controller = $this->controller."Controller";
            $this->controller = new $this->controller;
        
            if(@method_exists($this->controller, $url[1]))
            {
                $this->method = $url[1];
                unset($url[1]);
            }
        }
        else
        {
            require_once _CONTROLLER_PATH_."/{$this->controller}.php";
            
            $this->controller = $this->controller."Controller";
            $this->controller = new $this->controller;
        }
        $this->params = $url ? array_values($url) : [];
        if((get_class($this->controller) == "accessController" && $this->method == "login") || get_class($this->controller) == "api2Controller")
        {
            call_user_func_array([$this->controller,  $this->method], $this->params);
        }
        else 
        {
            route::getInstance()->setRoute($this->controller, $this->method, $this->params);
        }
    }
    
    private function splitUrl() 
    {
        if(isset($_GET['route']))
        {
            $url = explode("/", rtrim($_GET['route'],"/"));
            return $url;
        }
        return array();
    }
            
}
