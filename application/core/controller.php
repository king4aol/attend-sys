<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of controller
 *
 * @author Msajid
 */
class controller {
    //Configuratoion
    protected $config = [];
    //Stylesheets Path
    protected $stylesheets = [];
    //Javascript Path
    protected $javascripts = [];

    public function __construct() 
    {
        global $configuration;
        $this->config = $configuration;
        //Setting Stylesheets
        //$this->stylesheets[] = "{$this->config['domain']}"._PUBLIC_PATH_."css/style.css";
        //Setting Javascripts
       // $this->javascripts[] = "{$this->config['domain']}"._PUBLIC_PATH_."js/jquery-1.11.3.min.js";
        //Creating Required Database Tables
        //If Script Is Running First Time
        if(is_file(dirname(__FILE__)."/../core/firstTime.php"))
        {
            set_time_limit(0);
            $table = $this->loadModel("users");
            $table->firstCreate();
            $table = $this->loadModel("membership_types");
            $table->firstCreate();
            $table = $this->loadModel("employee_types");
            $table->firstCreate();
            $table = $this->loadModel("countries");
            $table->firstCreate();
            $table = $this->loadModel("email");
            $table->firstCreate();
            $table = $this->loadModel("contact_numbers");
            $table->firstCreate();
            $table = $this->loadModel("addresses");
            $table->firstCreate();
            $table = $this->loadModel("access");
            $table->firstCreate();
            $table = $this->loadModel("api");
            $table->firstCreate();
            $table = $this->loadModel("logs");
            $table->firstCreate();
            $table = $this->loadModel("api_logs");
            $table->firstCreate();
            $table = $this->loadModel("office");
            $table->firstCreate();
            $table = $this->loadModel("department");
            $table->firstCreate();
            $table = $this->loadModel("employee");
            $table->firstCreate();
            $table = $this->loadModel("check_in");
            $table->firstCreate();
            $table = $this->loadModel("check_out");
            $table->firstCreate();
            $table = $this->loadModel("break_in");
            $table->firstCreate();
            $table = $this->loadModel("break_out");
            $table->firstCreate();
            $table = $this->loadModel("temp_pins");
            $table->firstCreate();
            
            $table = $this->loadModel("access");
            $table->makeAdmin();
            
            unlink(dirname(__FILE__)."/../core/firstTime.php");
        }
    }
    //Load View
    protected function loadView($folder = '', $view = '', $data = []) 
    {
        $stylesheets = $this->stylesheets;
        $javascripts = $this->javascripts;
        
        //To Convert Array Indexes Into Variables
        if(is_array($data))
        {
            extract($data);
        }
        if(isset($error))
        {
            if(is_array($error))
            {
                $error = array_map("ucwords", array_map("strtolower", $error));
            }
            else
            {
                $error = ucwords($error);
            }
        }
        if(session::isLogin())
        {
            require_once _TEMPLATE_PATH_."/headincludes.php";
            require_once _TEMPLATE_PATH_."/header.php";
            require_once _TEMPLATE_PATH_."/leftmenu.php";
            require_once _VIEW_PATH_."/{$folder}/{$view}.php";
            require_once _TEMPLATE_PATH_."/footer.php";
        } 
        else 
        {
            require_once _VIEW_PATH_."/{$folder}/{$view}.php";
        }
    }
    //Load Model
    public function loadModel($model) 
    {
        if(file_exists(_MODEL_PATH_."/{$model}.php"))
        { 
            require_once _MODEL_PATH_."/{$model}.php";
            return new $model;
        }
        return false;
    }
    //Load Controller
    protected function loadController($controller) 
    {
        if(file_exists(_CONTROLLER_PATH_."/{$controller}.php"))
        {
            require_once _CONTROLLER_PATH_."/{$controller}.php";
            $controller = $controller."Controller";
            return new $controller;
        }
        return false;
    }
    
}
