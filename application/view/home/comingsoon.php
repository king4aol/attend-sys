﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="OTL" content="Dashboard">
 
    <title><?=$this->config['title']?></title>
    
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/favicon.png">
 
    <!-- Bootstrap core CSS -->
    <link href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
     
    <!-- Custom styles for this template -->
    <link href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/css/style.css" rel="stylesheet">
    <link href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/css/style-responsive.css" rel="stylesheet">
 
  </head>
 
  <body>
 
  <section id="container" >
   <!--header start-->
      <header class="header black-bg">
               
            <div class="nav notify-row" id="top_menu">
                <!--  notification start -->
                 
                <!--  notification end -->
            </div>
            <div class="top-menu">
                <ul class="nav pull-right top-menu">
                    <li><a style="margin-top: 12px" class="bakground_commingsoon_pagebutton" href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>access/login"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/login_button_header.png"></a></li>
                </ul>
            </div>
        </header>
      <!--header end-->
      
       
       
     <!--main content start-->
       
      <section class="wrapper">
     
            <div class="row">
               <dvi class="col-lg-12">
                 <div class="col-lg-2">  </div>
                 <div class="col-lg-8 centered select_admin_page_hading" style="background:#fff">
                    <h1> Comming Soon !</h1><br><br> <br>
                     <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/otl_commingsoon_page.png">
                    
                   </div>
                 <div class="col-lg-2"> </div>
               </div>
             
                      
             
      </section>
 
      <!--main content end-->
    </section>
      <!--footer start-->
      <footer class="site-footer">
            <div class="container">
               <div class="col-lg-8" style="text-align:center;padding:12px;">Copyright © <?=date("Y")?>asdf Odyssey Time Logic. All Rights Reserved</div>
               <div class="col-lg-4" style="text-align:left;"></div>
            </div>   
    </footer>
      <!--footer end-->
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/js/jquery.js"></script>
    <script src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/js/jquery-1.8.3.min.js"></script>
    <script src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <!--common script for all pages-->
    <script src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/js/common-scripts.js"></script>
     
    <!-- sidebar move down scrool -->
    <script src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/js/jquery.scrollTo.min.js"></script>
 
  </body>
</html>