<section class="wrapper">
     <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 11px;top:50px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 11px;font-size: 16px;"> Dashboard  </span>
         </div>
     <!-- top menue bar end --> 
              <div class="row">
                  <div class="col-lg-9 main-chart" style="font-size:17px">
                  
                  	<div class="row">
                            <?php if(session::getUserType() == _ADMIN_){?>
                  		<div class="col-md-4">
						<div style="height:91px;background:#e9e7e9">
                                    <div class="box1">
                                        <span class="li_heart" style="float:left"> <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/clients1.png" style="width:63px;padding-right:3px;"></span>
                                    </div>
                                    <div style="font-size:14px">Companies</div>
                                    <div style="font-weight:bold;color:#333"><?=$detail['clients']?></div>
                                    <div>
                                        <div style="width:60%;height:9px;background:#3DB8A2;margin-left:76px;border-radius:5px"></div>
                                    </div>
                  		</div> </div>
                            <?php }else{ ?>
                                <div class="col-md-4">
						<div style="height:91px;background:#e9e7e9">
                                    <div class="box1">
                                        <span class="li_heart" style="float:left"> <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/employe_21.png" style="width:63px;padding-right:3px;"></span>
                                    </div>
                                    <div style="font-size:14px">Total Employees</div>
                                    <div style="font-weight:bold;color:#333"><?=$detail['employees']?></div>
                                    <div>
                                        <div style="width:60%;height:9px;background:#3DB8A2;margin-left:76px;border-radius:5px"></div>
                                    </div>
                                </div>
								</div>
                            <?php } ?>
                  		<div class="col-md-4">
						<div style="height:91px;background:#e9e7e9">
                                    <div class="box1">
                                        <span class="li_heart" style="float:left"> <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/check-In1.png" style="width:63px;padding-right:3px;"></span>
                                    </div>
                                    <div style="font-size:14px ">Checked-in</div>
                                <div style="font-weight:bold;color:#333"><?=$detail['check_in']?></div>
                                <div>
                                   
                                        <div style="width:60%;height:9px;background:#FFC101;margin-left:76px;border-radius:5px"></div>
 </div>
                  		</div>
						</div>
                        
                  		<div class="col-md-4">
						<div style="height:91px;background:#e9e7e9">
                  				<div class="box1">
					  			<span class="li_heart" style="float:left"> <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/check-out1.png" style="width:63px;padding-right:3px;"></span>
                  			</div>
					  			<div style="font-size:14px" >Checked-out</div>
                                <div style="font-weight:bold;color:#333"><?=$detail['check_out']?></div>
                                
                                        <div style="width:60%;height:9px;background:#A7D86A;margin-left:76px;border-radius:5px"></div>
  								
                             
                           </div>
                               </div>
                        </div>
                       
                            <div class="col-lg-12"> 
 
							   <form class="form-inline" action="#" method="#" role="form">
								<div class="dropdown" style="margin-left:-16px; margin-top:20px; ">
								<div class="form-group">
							  <span style="font-size:15px;font-weight:bold"> Site Based Stats | </span>
                                                          <select class="form-control graph_office" name="type" style="border:none">
									 <?php foreach($offices as $office){?>
                                        <option value="<?=$office['office_id']?>" <?php if($selectedOffice == $office['office_id'])echo "selected"?>><?=$office['office_name']?></option>
                                        <?php } ?>
								</select>
								</div> |
								<div class="form-group">
                                                                    <select class="form-control graph_filter" name="type" style="border:none">
									<option value="today" <?php if($selectedFilter == "today")echo "selected"?>>Today</option>
									<option value="yesterday" <?php if($selectedFilter == "yesterday")echo "selected"?>>Yesterday</option>
								</select>
								</div>
 <div class="col-lg-12">
                      <div id="chart"></div>
                  </div>
							 
								</div>
							</form>




<br>




</div>
                        <script type="text/javascript">
                            $(".graph_filter").change(function(){
                                var filter = $(this).val();
                                
                                var form = $(document.createElement('form'));
                                $(form).attr("action", "<?=$this->config['domain'].""._PUBLIC_PATH_."home/index"?>");
                                $(form).attr("method", "POST");

                                var input = $("<input>")
                                    .attr("type", "hidden")
                                    .attr("name", "filter")
                                    .val(filter);


                                $(form).append($(input));

                                form.appendTo( document.body )

                                $(form).submit();
                            });
                            
                            $(".graph_office").change(function(){
                                var office = $(this).val();
                                
                                var form = $(document.createElement('form'));
                                $(form).attr("action", "<?=$this->config['domain'].""._PUBLIC_PATH_."home/index"?>");
                                $(form).attr("method", "POST");

                                var input = $("<input>")
                                    .attr("type", "hidden")
                                    .attr("name", "office")
                                    .val(office);


                                $(form).append($(input));

                                form.appendTo( document.body )

                                $(form).submit();
                            });
                        </script>
                  	
                  	<!-- /row mt -->	
                  
                      </div>
                      <!-- /col-lg-9 END SECTION MIDDLE -->
                  
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                   <div class="col-lg-3 ds">
                    <!--COMPLETED ACTIONS DONUTS CHART-->
                    <h3>Activity Feeds</h3>
                    <div id="notifications">  
                       
                    </div>
                  </div>
                    <!-- --/row ---->
                  
                 
    <div class="col-sm-9" id="stats" style="margin-top:15px;">
        <div> <span style="font-size: 17px;">Live Stats | Today </span> <a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>attendance/live"><div class="pull-right btn btn-primary btn-md" style="background: #071D24;border:none"> View Details</div></a></div><br><br>
    <table class="table" style="">
    <thead class="otl_list_employes_color">
      <tr>
        <th>SN#</th>
        <th>Employee Name</th>
        <th>Status</th>
        <th>Site/office</th>
        
     </tr>
    </thead>
    <tbody style="background:#ccc;">
        <?php 
        if(is_array($checkIns))
        {
            $i = 1; 
            $timezone = '';
            $set_offset = '';
            $cur_offset = '';
            foreach($checkIns as $in){
                
                $cur_offset = $in['timezone'];
                if($cur_offset != $set_offset)
                {
                    $set_offset = $cur_offset;
                    list($hours, $minutes) = explode(':', $set_offset);
                    $seconds = $hours * 60 * 60 + $minutes * 60;
                    $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 1);
                    // Workaround for bug #44780
                    if($timezone === false) 
                    {
                        $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 0);
                    }
                    date_default_timezone_set($timezone);
                }
                ?>
            <tr>
                <td class="border_color"><?=$offset + $i++?></td>
                <td class="border_color"><?=$in['emp_first_name']." ".$in['emp_last_name']?></td>
                <td class="border_color">Checked-in | <?=date("h:i A", $in['check_in_timestamp'])?></td>
                <td class="border_color"><?=$in['office_name']?></td>
            </tr>
        <?php 
        }
        date_default_timezone_set($this->config['timezone']);
        }else{?>
            <tr><td colspan="4">No record found</td></tr>
        <?php } ?>
        </tbody>
    </table>
        <!--pagination start -->    
    <div class="row">
        <ul class="pagination pull-right" style="margin-top:-9px">
            <li><a href="" id="first_page" style="background:#071d24;border:#071d24;color:#fff;"><<</a></li>
            <li><a href="" id="previous_page" style="background:#071d24;border:#071d24;color:#fff"><</a></li>
            <li><a style="padding:1px !important;background:#071d24;"><input id="current_page" type="text" data-current="<?=$current?>" data-total="<?php echo(ceil($total/$limit))?ceil($total/$limit):"1"?>" data-limit="<?=$limit?>" value="<?=$current?>" style="color:#000;width:30px;height:27px;border:none;text-align:center"></a></li>
            <li><a style="padding:1px !important;height: 32px;width: 46px;text-align: center;line-height: 30px;text-decoration: none;background: none;border: none;text-align:left;color:#000"><span style="text-align:left"> &nbsp;of</span> &nbsp; <?php echo(ceil($total/$limit))?ceil($total/$limit):"1"?></a></li>
            <li><a href="" id="next_page" style="background:#071d24;border:#071d24;color:#fff">></a></li>
            <li><a href="" id="last_page" style="background:#071d24;border:#071d24;color:#fff;margin-right:-4px;">>></a></li>
        </ul> 
        <script type="text/javascript">
            $("#current_page").on("keyup", function(e){
                if($(this).val() != "")
                {
                    var input = parseInt($(this).val());
                    var current = $("#current_page").attr("data-current");
                    var total = $("#current_page").attr("data-total");
                    var limit = $("#current_page").attr("data-limit");
                    if(!isNaN(input))
                    {
                        if(input > total)
                        {
                            input = current;
                        }
                        else if(input <= 0)
                        {
                            input = current;
                        }
                    }
                    else 
                    {
                        input = current;
                    }
                    $(this).val(input);
                    
                    if(e.which == 13)
                    {
                        ajax("<?=$this->config['domain'].""._PUBLIC_PATH_."home/index/"?>"+limit+"/"+(input*limit-limit));
                        //window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."home/index/"?>"+limit+"/"+(input*limit-limit);
                    }
                }
            });
            $("#first_page, #previous_page, #next_page, #last_page").click(function(e){
                e.preventDefault();
                if($(this).attr("id") == "first_page")
                {
                    ajax("<?=$this->config['domain'].""._PUBLIC_PATH_."home/index/{$limit}/0"?>");
                    //window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."home/index/{$limit}/0"?>";
                }
                else if($(this).attr("id") == "last_page")
                {
                    ajax("<?=$this->config['domain'].""._PUBLIC_PATH_."home/index/{$limit}/".($total-$limit)?>");
                    //window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."home/index/{$limit}/".($limit * ceil($total/$limit) - $limit)?>";
                }
                else if($(this).attr("id") == "previous_page")
                {
                    <?php 
                        if($offset - $limit <= 0)
                        {
                            echo "var previous = 0;";
                        }
                        else 
                        {
                            echo "var previous = ".($offset-$limit).";";
                        }
                    ?>
                    ajax("<?=$this->config['domain'].""._PUBLIC_PATH_."home/index/{$limit}/";?>" + previous);
                    //window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."home/index/{$limit}/";?>" + previous;
                }
                else if($(this).attr("id") == "next_page")
                {
                    if($("#current_page").val() == $("#current_page").attr("data-current"))
                    {
                        <?php
                            if(ceil($total/$limit) == ceil(($offset+$limit)/$limit))
                            {
                                echo "var next = $offset;";
                            }
                            elseif(ceil($total/$limit) == 0 || ceil($total/$limit) == 1)
                            {
                                echo "var next = 0;";
                            }
                            else 
                            {
                                echo "var next = ".($offset+$limit).";";
                            }
                        ?>
                    ajax("<?=$this->config['domain'].""._PUBLIC_PATH_."home/index/{$limit}/";?>"+next);
                    //window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."home/index/{$limit}/";?>"+next;
                    }
                    else 
                    {
                        var input = $("#current_page").val();
                        var total = $("#current_page").attr("data-total");
                        var limit = $("#current_page").attr("data-limit");
                        ajax("<?=$this->config['domain'].""._PUBLIC_PATH_."home/index/"?>"+limit+"/"+(input*limit-limit));
                        //window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."home/index/"?>"+limit+"/"+(input*limit-limit);
                    }
                }
            });
        </script>       
    </div>
       <!--pagination end -->   
       
    </div>
                   
            </div>
</section>
<script type="text/javascript">
    function ajax(urlPath)
    { 
        $.ajax({
            type: "GET",
            url: urlPath,
            cache: "FALSE",
            datatype: "HTML",
            success: function(response)
            {
                var data = $(response).find("#stats").html();
                $("#stats").html(data);
            }
        });
    }
    $(document).ready(function(){
        $("body").attr("style","background:#fff");
       setInterval(
            function(){
                $.ajax({
                     type: "POST",
                     url: "<?php echo $this->config['domain'].'/'._PUBLIC_PATH_.'/home/ajax/activity';?>",
                     cache: "FALSE",
                     success: function(response){
                         $("#notifications").html(response);
                     }
                 });
            }
            ,1000);
               
    google.setOnLoadCallback(barsChart);

      function barsChart() {
        var data = new google.visualization.arrayToDataTable([
          <?php if(session::getUserType() == _ADMIN_ && 1 > 2){?>
                    ['Clients', 'Checked-in', 'Checked-out', 'On Break', 'Back from Break'],
                    <?php 
                    if(sizeof($charts['bars']) > 0)
                    {
                        foreach ($charts['bars'] as $row): ?>//
                            ['<?=$row['client_name']?>',     <?=$row['cin_total'].",".$row['cout_total'].",".$row['bin_total'].",".$row['bout_total']?>],
                    <?php endforeach;
                    }
                    else 
                    {
                    ?>
                        ['No Information', 0, 0, 0, 0]
                    <?php
                    }
                    ?>
          <?php }else{?>
                    <?php 
                    $max = 4;
                    if(sizeof($charts['bars']) > 0 && $charts['bars']['cin_total'] != 0)
                    {
											$row = $charts['bars'];
                        //foreach ($charts['bars'] as $row): ?>
                        ['Site', '<?=$selectedOfficeName?>', {role: 'style'}],
                        ['Checked-in', <?=$row['cin_total']?>, 'color: #f4b400'], 
                        ['Checked-out', <?=$row['cout_total']?>, 'color: #0f9d58'], 
                        ['On Break', <?=$row['bin_total']?>, 'color: #4285f4'], 
                        ['Back from Break', <?=$row['bout_total']?>, 'color: #db4437']
                    <?php 
                    $max = $row['cin_total'];
                    //endforeach;
                    }
                    else 
                    {?>
                        ['Site', 'No Information'],     
                        ['Checked-in', 0], 
                        ['Checked-out', 0], 
                        ['On Break', 0,], 
                        ['Back from Break', 0]
                    <?php
                    }
                    ?>
          <?php } ?>
        ]);

        var options = {
          width: '100%',
          height: 400,
          legend: 'none',
          bar: {groupWidth: '75%'},
            vAxis: {minValue: '4', format: '0', viewWindow: {min: 0, max: <?=$max?>}, gridlines: { count: 5 }}
        };

        var barchart = new google.visualization.ColumnChart(document.getElementById('chart'));
        barchart.draw(data, options);
    };
	
    });
	
//	$(window).bind("load", function() {
//		var func = function(){$("#chart").find("svg").attr("width", "500")};
//		setTimeout(func, 500);
//	});
    </script>