<section class="wrapper">
     <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;"> Dashboard  </span>
         </div>
     <!-- top menue bar end --> 
              <div class="row">
                  <div class="col-lg-9 main-chart">
                  
                  	<div class="row">
                            <?php if(session::getUserType() == _ADMIN_){?>
                  		<div class="col-md-4">
                  			<div class="box1">
                                            <span class="li_heart" style="float:left"> <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/clients1.png" style="width:63px;padding-right:3px;"></span>
                  			</div>
                                            <div style="padding-top:10px">Clients</div>
                                <div><?=$detail['clients']?></div>
                                <div>
                                    <div class="progress">
                                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:<?=$bar['clients']?>%">
                                <?=$bar['clients']?>%
                                </div>
                                </div>
                                </div>
                  		</div>
                            <?php } ?>
                        <!--2nd box-->
                  		<div class="col-md-4">
                                    <div class="box1">
                                        <span class="li_heart" style="float:left"> <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/site1.png" style="width:63px;padding-right:3px;"></span>
                                    </div>
                                    <div style="padding-top:10px">Sites</div>
                                <div><?=$detail['offices']?></div>
                                <div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:<?=$bar['offices']?>%">
                                <?=$bar['offices']?>%
                                        </div>
                                    </div>
                                </div>
                  		</div>
                        
                        <!--2nd box end -->
                        <!--3nd box-->
                  		<div class="col-md-4">
                  				<div class="box1">
					  			<span class="li_heart" style="float:left"> <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/employes1.png" style="width:63px;padding-right:3px;"></span>
                  			</div>
					  			<div style="padding-top:10px">Employees</div>
                                <div><?=$detail['employees']?></div>
                                <div><div class="progress">
  								<div class="progress-bar progress-bar-striped active" role="progressbar"
  								aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:<?=$bar['employees']?>%">
                                <?=$bar['employees']?>%
                              </div>
                           </div></div>
                  		</div>
                        
                        <!--3nd box end -->
                  		                        <!--2nd box-->
                  		<div class="col-md-4">
                  				<div class="box1">
					  			<span class="li_heart" style="float:left"> <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/check-In1.png" style="width:63px;padding-right:3px;"></span>
                  			</div>
					  			<div style="padding-top:10px">Check In</div>
                                <div><?=$detail['check_in']?></div>
                                <div><div class="progress">
  								<div class="progress-bar progress-bar-striped active" role="progressbar"
  								aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:<?=$bar['check_in']?>%">
                                <?=$bar['check_in']?>%
                              </div>
                           </div></div>
                  		</div>
                        
                        <!--2nd box end -->
                  		                        <!--2nd box-->
                  		<div class="col-md-4">
                  				<div class="box1">
					  			<span class="li_heart" style="float:left"> <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/check-out1.png" style="width:63px;padding-right:3px;"></span>
                  			</div>
					  			<div style="padding-top:10px">Check Out</div>
                                <div><?=$detail['check_out']?></div>
                                <div><div class="progress">
  								<div class="progress-bar progress-bar-striped active" role="progressbar"
  								aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:<?=$bar['check_out']?>%">
                                <?=$bar['check_out']?>%
                              </div>
                           </div></div>
                  		</div>
                        
                        <!--2nd box end -->
                  	
                  	</div><!-- /row mt -->	
                  
                      </div>
                      <!-- /col-lg-9 END SECTION MIDDLE -->
                  
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  
                  <div class="col-lg-3 ds">
                    <!--COMPLETED ACTIONS DONUTS CHART-->
                    <h3>NOTIFICATIONS</h3>
                    <div id="notifications">  
                       
                    </div>
                  </div>  <!-- --/row ---->
                  <div class="col-lg-6">
                      <div id="piechart" ></div>
                  </div>
                  <div class="col-lg-6">
                      <div id="dual_y_div"></div>
                  </div>
          </section>
<script type="text/javascript">
    $(document).ready(function(){
        $("body").attr("style","background:#fff");
       setInterval(
            function(){
                $.ajax({
                     type: "POST",
                     url: "<?php echo $this->config['domain'].'/'._PUBLIC_PATH_.'/home/ajax/activity';?>",
                     cache: "FALSE",
                     success: function(response){
                         $("#notifications").html(response);
                     }
                 });
            }
               ,1000);
               
        google.setOnLoadCallback(pieChart);
        function pieChart() {

          var data = google.visualization.arrayToDataTable([
              <?php if(session::getUserType() == _ADMIN_){?>
                        ['Clients', 'Check In'],  
                        <?php foreach ($charts['pie'] as $row): ?>
                                ['<?=$row['client_name']?>',     <?=$row['total']?>],
                        <?php endforeach;?>
              <?php }else{?>
                        ['Sites', 'Check In'],
                        <?php foreach ($charts['pie'] as $row): ?>
                        ['<?=$row['office_name']?>',     <?=$row['total']?>],
                        <?php endforeach;?>
              <?php } ?>
          ]);

          var options = {
              <?php if(session::getUserType() == _ADMIN_){?>
                        title: 'Clients Based Check In Last 30 Days'  
              <?php }else{?>
                        title: 'Site Based Check In Last 30 Days'
              <?php } ?>
          };

          var piechart = new google.visualization.PieChart(document.getElementById('piechart'));

          piechart.draw(data, options);
        }
       
       
    google.setOnLoadCallback(barsChart);

      function barsChart() {
        var data = new google.visualization.arrayToDataTable([
          <?php if(session::getUserType() == _ADMIN_){?>
                    ['Clients', 'Check In', 'Break In', 'Break Out', 'Check Out'],
                    <?php 
                    if(sizeof($charts['bars']) > 0)
                    {
                        foreach ($charts['bars'] as $row): ?>
                            ['<?=$row['client_name']?>',     <?=$row['cin_total'].",".$row['bin_total'].",".$row['bout_total'].",".$row['cout_total']?>],
                    <?php endforeach;
                    }
                    else 
                    {
                    ?>
                        ['No Info', 0, 0, 0, 0]
                    <?php
                    }
                    ?>
          <?php }else{?>
                    ['Sites', 'Check In', 'Break In', 'Break Out', 'Check Out'],
                    <?php 
                    if(sizeof($charts['bars']) > 0)
                    {
                        foreach ($charts['bars'] as $row): ?>
                    ['<?=$row['office_name']?>',     <?=$row['cin_total'].",".$row['bin_total'].",".$row['bout_total'].",".$row['cout_total']?>],
                    <?php endforeach;
                    }
                    else 
                    {?>
                        ['No Info', 0, 0, 0, 0]
                    <?php
                    }
                    ?>
          <?php } ?>
        ]);

        var options = {
          //width: 900,
          chart: {
            <?php if(session::getUserType() == _ADMIN_){?>
                      title: 'Clients Based Last 7 Days Report'  
            <?php }else{?>
                      title: 'Site Based Last 7 Days Report'
            <?php } ?>
          },
          series: {
            0: { axis: 'Attendance' }, // Bind series 0 to an axis named 'distance'.
            1: { axis: 'Attendance' } // Bind series 1 to an axis named 'brightness'.
          },
          axes: {
            y: {
              distance: {label: 'Attendance'}, // Left y-axis.
              brightness: {side: 'right', label: 'Attendance'} // Right y-axis.
            }
          }
        };

      var barchart = new google.charts.Bar(document.getElementById('dual_y_div'));
      barchart.draw(data, options);
    };
    });
    </script>