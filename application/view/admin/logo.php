<?php if(!isset($_POST['submit'])){ ?>
<section class="wrapper">
     <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;">Company Logo </span>
         </div>
     <!-- top menue bar end -->  
     
            <div class="row">
               <dvi class="col-lg-12">
                 <div class="col-lg-2">  </div>
                 <div class="col-lg-8 centered select_admin_page_hading"> <h1>Select a Company to Set Company Logo </h1></div>
                 <div class="col-lg-2"> </div>
            </div>
            <div class="row">
               <div class="col-lg-12" >
                 <div class="col-lg-2">  </div>
                   <div class="col-lg-8 centered select_admin_page_hading_dropdown">
                     
                        <div class="col-lg-2"> </div>
                        <form class="form-horizontal style-form" autocomplete="off" action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>admin/logo" method="POST">
                            <div class="form-group">
                                <div class="col-sm-8 <?php echo(isset($error['admin_error']))?"has-error":""?>">
                                    <div class="<?php echo(isset($error['admin_error']))?"alert alert-danger":""?>">
                                        <?=@$error['admin_error']?>
                                    </div>
                                    <input type="text" name="admin_name" class="form-control" id="admin_name"/>
                                    <input type="hidden" name="admin" class="form-control" id="admin"/>
                                    <div id="predict" class="preres" style="display: none;">
                                        <ul></ul>
                                    </div>
                                    <script type="text/javascript">
                                    $(document).ready(function(){
                                        $("#admin_name").on("keyup click", function(){
                                            var request = "action=select&type=companies&source=admin&input=" + $(this).val();
                                            console.log(request);
                                            $.ajax({
                                            type: "POST",
                                            url: "<?php echo $this->config['domain'].'/'._PUBLIC_PATH_.'/admin/ajax';?>",
                                            data: request,
                                            cache: "FALSE",
                                            success: function(response){
                                                console.log(response);
                                                var str = '';
                                                try
                                                {
                                                    var data = JSON.parse(response);
                                                    //console.log(data.response+" => ");
                                                    $.each(data.response, function(k, v){
                                                        console.log(k + " => "+v);
                                                        str += "<li data-val='"+v['admin_id']+"'>"+v['first_name']+" "+v['last_name']+"("+v['username']+")</li>";
                                                    });
                                                }
                                                catch(err)
                                                {
                                                    $("#predict").css("display", "none");
                                                    console.log(err.message);
                                                }
                                                $("#predict ul").html(str);
                                                $("#predict").css("display", "block");
                                            },
                                            error: function(){
                                                alert("Something Went Wrong. Please Try Again");
                                            }
                                            });
                                        });
                                        $("#predict ul").on("click", "li", function(){
                                            $("#admin").val($(this).attr("data-val"));
                                            $("#admin_name").val($(this).text());
                                            $("#predict").css("display", "none");
                                        });
                                    });
                                </script>
                                </div>
                            </div>

                            <div class="form-panel"  style="box-shadow:none;">
                                <div class="col-lg-2"> </div>

                               <div class="form-group">
                                 <div class="col-sm-8 centered form_selectadmin_submit_button" >
                                    <input  type="submit" name="submit" value="Next"> 
                                  </div>     
                                 </div>
                            </div>
                        </form> 
                        </div>
                       </div>
                 <div class="col-lg-2"> </div>
    </section>

<?php }elseif(!isset($_POST['update_logo']) && isset($_POST['submit'])){
   
    ?>
<section class="wrapper">
       <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;">Company Logo </span>
         </div>
     <!-- top menue bar end -->   
      <div class="row">
            <div class="col-lg-9 main-chart">
            
             <div class="row">
                <div class="form-panel"  style="box-shadow:none;color:#333">
                <div class="<?php echo (!isset($error['error'])) ? '':'alert alert-danger' ?>">
                     <?=@$error['error']?>
                </div>
                <div class="<?php echo (!isset($res['response'])) ? '':'alert alert-success' ?>">
                    <?=@$res['response']?>
                </div>
                    <form action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>admin/logo" method="POST" enctype="multipart/form-data">
			<div class="col-md-12 col-md-offset-2">
				<div class="panel panel-login">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <h3 class="centered">Company Logo</h3>
                                        </div>
                                        <hr>
                                    </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-4 col-sm-4 control-label">Current Logo</label>
                                <div class="col-sm-8">
                                    <img class="img-responsive" src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>shared/<?=$values['logo_path']?>" width="426px" height="109px"/>
                                    <br>
                                </div>
                          </div>
                            <div class="form-group">
                                <label class="col-sm-4 col-sm-4 control-label">Select New Logo</label>
                                <div class="col-sm-8">
                                    <input type="file" class="form-control" name="logo"/>
                                    <br>
                                    <pre><strong>Dimensions: </strong> 420w x 110h<br><strong>Allowed Extensions: </strong> JPEG, PNG, GIF, JPG<br><strong>Max Image Size: </strong> 1 MB</pre>
                                </div>
                          </div>
                          
                          <div class="form-group">
                          <label class="col-sm-4 col-sm-4 control-label"></label>
                              <div class="col-sm-8">
                                  <input type="submit" name="update_logo" value="Update" class="btn btn-success btn-sm pull-right" >
                               </div>
                          </div>
                        </div>
			</div>
		</div>
             </form>
        
        </div>
                  
       </div></div>
      </section>    

<?php }?>
