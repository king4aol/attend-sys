<?php if($isForm){ ?>
<form action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>admin/settings/gracetime" method="POST" style="height:300px; ">
            <div align="center" >
                <h2 align="center">Add Grace Time</h2>
                <div class="error-text"><?=@$error['csrf_error']?></div>
                <table >
               <tr>
                   <td><label><span>Add Grace Time</span></label></td>
                    <td>
                    <input type="text" placeholder="please enter grace time in minutes" name="grace_time" value="<?=@$values['grace_time']?>"/>
                    <span class="error"><?=@$error['grace_time_error']?></span>
                    </td>
                    <td><input type="submit" name="submit" value="submit"/></td>
               </tr>
                </table>
            </div>
</form>

<?php 

}
else if($action == "logo")
{
?>
<div id="mainform">
    <h2>Your Current Logo is</h2>
    <img  src="/otl/admin/shared/<?php echo $logo; ?>">
    <div id="innerdiv">
        <h2>Upload Logo</h2>
        <!-- Required Div Starts Here -->
        <div id="formdiv">
            <form action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>admin/settings/logo" enctype="multipart/form-data" id="form" method="post" name="form">
                <div id="upload">
                        <input id="file" name="files" type="file">
                <input id="submit" name="submit" type="submit" value="Upload">
                 </div><img  src="/otl/admin/shared/<?php echo $new_image_name; ?>">
            </form>
         </div>
        <div id="clear">
        </div>
        <div id="preview" style="margin-left: 400px; margin-top: -500px; ">
            <img id="previewimg" src="">
        </div>
        <div id="message">
        </div>
    </div>
</div>

<?php  } ?>

<script>
$(document).ready(function(){
	// Function for Preview Image.
	$(function(){
		$(":file").change(function(){
			if (this.files && this.files[0]){
				var reader = new FileReader();
				reader.onload = imageIsLoaded;
				reader.readAsDataURL(this.files[0]);
			}
		});
	});

	function imageIsLoaded(e){
		$('#message').css("display", "none");
		$('#preview').css("display", "block");
		$('#previewimg').attr('src', e.target.result);
	};
	// Function for Deleting Preview Image.
		$("#deleteimg").click(function(){
		$('#preview').css("display", "none");
		$('#file').val("");
	});
	// Function for Displaying Details of Uploaded Image.
		$("#submit").click(function(){
		$('#preview').css("display", "block");
		$('#message').css("display", "block");
	});
});
</script>