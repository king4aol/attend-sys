<?php $detail = @$details[0];?>
<section class="wrapper">
    <div class="row topbar"> 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;">Edit Profile</span>
         </div>
    <div class="row">
        <div class="col-lg-9 main-chart">
<div class="container">
             
             <div class="col-lg-10"> 
             <div class="form-panel">
                 <div class="<?php echo (!isset($error['csrf_error'])) ? '':'alert alert-danger' ?>">
                     <?=@$error['csrf_error']?>
                 </div>
                 <div class="<?php echo (!isset($error['error'])) ? '':'alert alert-danger' ?>">
                     <?=@$error['error']?>
                 </div>
                 <div class="<?php echo (!isset($res['response'])) ? '':'alert alert-success' ?>">
                     <?=@$res['response']?>
                 </div>
                 <form class="form-horizontal style-form" action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>admin/edit/<?=@$company?>" method="POST">
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">First Name</label>
                              <div class="col-sm-10 <?php echo(!isset($error['fname_error'])) ? '':'has-error'?>">
                                  <input type="text" placeholder="First Name" name="fname" value="<?=$detail['first_name']?>" class = "form-control"/>
                                    <small class="text-danger"><?=@$error['fname_error']?></small>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Last Name</label>
                              <div class="col-sm-10 <?php echo(!isset($error['lname_error'])) ? '':'has-error'?>">
                                <input type="text" class="form-control" placeholder="Last Name" name="lname" value="<?=$detail['last_name']?>"/>
                                <small class="text-danger"><?=@$error['lname_error']?></small>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Company Name</label>
                              <div class="col-sm-10 <?php echo(!isset($error['company_name_error'])) ? '':'has-error'?>">
                                  <input type="text" class="form-control"  placeholder="Company Name" name="company_name" value="<?=$detail['company_name']?>" />
								  <small class="text-danger"><?=@$error['company_name_error']?></small>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Address</label>
                              <div class="col-sm-10 <?php echo(!isset($error['address_error'])) ? '':'has-error'?>">
                                  <input class="form-control" type="text" placeholder="Enter Address" name="address" value="<?=$detail['address']?>" />
								  <small class="text-danger"><?=@$error['address_error']?></small>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Country</label>
                              <div class="col-sm-10 <?php echo(!isset($error['country_error'])) ? '':'has-error'?>">
                                  <select name="country" class="form-control">
                                      <option value="">Select Country</option>
                                <?php foreach ($countries as $key => $value) {?>
                                        <option value="<?=$value['iso_2']?>" <?php if($value['iso_2'] == $country) echo 'selected';?>><?=$value['country_name']?></option>
                                <?php }?>
                          </select>
                                <small class="text-danger"><?=@$error['country_error']?></small>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Company Registration Date</label>
                              <div class="col-sm-10 <?php echo(!isset($error['dob_error'])) ? '':'has-error'?>">
                                  <input class="form-control" type="text" placeholder="DD/MM/YYYY" name="dob" value="<?=$detail['date_of_birth']?>"  id="dob" data-date-format="dd/mm/yyyy"/>
								  <small class="text-danger"><?=@$error['dob_error']?></small>
                              </div>
                          </div>
						  <div class="form-group">
                              <label class="col-lg-2 col-sm-2 control-label">Phone</label>
                              <div class="col-lg-10 <?php echo(!isset($error['phone_error'])) ? '':'has-error'?>">
                                 <input type = "text" class="form-control" placeholder="Phone Number (+6192010777)" name="phone" value="<?=$detail['number']?>" />
								 <small class="text-danger"><?=@$error['phone_error']?></small>
                              </div>
                          </div>
						  <?php /*
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">User Name</label>
                              <div class="col-sm-10 <?php echo(!isset($error['user_error'])) ? '':'has-error'?>">
                                  <input class="form-control" type="text" placeholder="User name" name="user" value="<?=$detail['username']?>"/>
								  <small class="text-danger"><?=@$error['user_error']?></small>
                              </div>
                          </div>
						  */ ?>
						  <div class="form-group">
                              <label class="col-lg-2 col-sm-2 control-label">Email</label>
                              <div class="col-lg-10 <?php echo(!isset($error['email_error'])) ? '':'has-error'?>">
                                 <input class="form-control" type="email" placeholder="Email Address" name="email" value="<?=$detail['email_address']?>"/>
                                <small class="text-danger"><?=@$error['email_error']?></small>
                              </div>
                          </div>
                          <?php if(session::getUserType() == _ADMIN_ && session::getAdminId() !== $id ){?>
                            <div class="form-group">
                              <label class="col-lg-2 col-sm-2 control-label">Membership Type</label>
                                <div class="col-lg-10 <?php echo(!isset($error['memtype_error'])) ? '':'has-error'?>">
                                    <select name="memtype" id="list" class="form-control">
                                        <option>Select Membership Type</option>
                                        <?php foreach ($memberships as $membership) { ?>
                                        <option value="<?=$membership['type_id']?>" <?php if($membership['type_id'] == $detail['membership_type_id']) echo 'selected';?>><?=$membership['membership_title']?></option>
                                        <?php } ?>
                                    </select>
                                    <small class="text-danger"><?=@$error['memtype_error']?></small>
                                </div>
                            </div>
                          <?php } ?>
						  <div class="form-group">
                              <label class="col-lg-2 col-sm-2 control-label">Password</label>
                              <div class="col-lg-10 <?php echo(!isset($error['pass_error'])) ? '':'has-error'?>">
                                 <input type="password" class="form-control"  placeholder="Password" name="pass">
								 <small class="text-danger"><?=@$error['pass_error']?></small>
                              </div>
                          </div>
						  <div class="form-group">
                              <label class="col-lg-2 col-sm-2 control-label">Confirm Password</label>
                              <div class="col-lg-10">
                                 <input type="password" class="form-control" placeholder="Confirm Password" name="confirmpass">
                              </div>
                          </div>
                              
                          <div class="form-group">
                          <label class="col-lg-2 col-sm-2 control-label"></label>
                              <div class="col-lg-10">
                               <input type="hidden" value="<?=@$csrf?>" name="csrf">
                               <input class="btn btn-success btn-sm pull-right" type="submit" value="Update" name="update">
                               </div>
                          </div>
                      </form>
                  </div>
                  
                  </div>
          </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function(){
        $("select[name='admintype']").change(function(){
            var val = $(this).val();
            if(val == "admin")
            {
                $("select[name='memtype']").prop("disabled", true);
                $("#membership_type_limit").hide();
            }
            else
            {
                $("select[name='memtype']").prop("disabled", false);
                $("#membership_type_limit").show();
            }
        });
       
       $("select[name='memtype']").change(function(){
          var val = $(this).find("option:selected").text();
          if(val == "Free/Basic")
          {
            $("#membership_type_limit").hide();
          }
          else
          {
              $("#membership_type_limit").show();
          }
       });
       
       $('#dob').datetimepicker({

            weekStart: 1,

            todayBtn:  1,

                    autoclose: 1,

                    todayHighlight: 1,

                    startView: 2,

                    minView: 2,

                    forceParse: 0,

            showMeridian: 1

        });
    });
</script>

