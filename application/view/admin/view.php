<section class="wrapper">
     <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;">Companies  </span>
         </div>
              <div class="col-lg-12">   
                  <div style="margin-bottom: -45px;margin-top: 10px;<?=isset($_POST['success']) ? '':'display:none'?>" class="alert alert-success"><?=@$_POST['success']?></div>
                  <?php if(isset($_POST['success'])){?>
                  <script type="text/javascript">
                      history.pushState("", "", '../view');
                    </script>
                  <?php }?>
                      <div class="wrapper">
                       <!--company listing container start here-->
                        <?php
                            $i = 0;
                              foreach ($requests as $request)
                              {
                        ?>
                      <div class="col-lg-4" style="height:134px;"> 
                      <!--company listing container start here-->
                         <div class="companies_listing_container">
                          <div class="listing_comapnies_left">
                            <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>shared/logos/<?php echo(!empty($request['logo_path']))? "{$request['logo_path']}": "default.png";?>" class="img-responsive"> 
                          </div>
                          <div class="listing_comapnies_right"> 
                           <p> <?=$request['company_name']?> </p>
                           <p class="other_lsiting_companies_color"> <?=$request['username'];?> </p>
                           <p> <?=$request['first_name'];?> <?=$request['last_name'];?> </p>
                           <p> <?=$request['number'];?> </p>
                           <p> <a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>admin/details/<?=$request['username'];?>" name="color" type="button" class="btn btn-inverse black" value="btn btn-inverse">Details</a></p>
                          </div>
                          </div>
                      </div>
                        <?php } ?>
                        <!--company listing container start here-->                        <!--company listing container start here-->
                       
                      </div>
<!--pagination start -->    
    <div class="row">
        <ul class="pagination pull-right" style="margin-top:-15px">
            <li><a href="" id="first_page" style="background:#071d24;border:#071d24;color:#fff"><<</a></li>
            <li><a href="" id="previous_page" style="background:#071d24;border:#071d24;color:#fff"><</a></li>
            <li><a style="padding:1px !important;background:#071d24;"><input id="current_page" type="text" data-current="<?=$current?>" data-total="<?php echo(ceil($total/$limit))?ceil($total/$limit):"1"?>" data-limit="<?=$limit?>" value="<?=$current?>" style="color:#000;width:30px;height:27px;border:none;text-align:center"></a></li>
            <li><a style="padding:1px !important;height: 32px;width: 46px;text-align: center;line-height: 30px;text-decoration: none;background: none;border: none;text-align:left;color:#000"><span style="text-align:left"> &nbsp;of</span> &nbsp; <?php echo(ceil($total/$limit))?ceil($total/$limit):"1"?></a></li>
            <li><a href="" id="next_page" style="background:#071d24;border:#071d24;color:#fff">></a></li>
            <li><a href="" id="last_page" style="background:#071d24;border:#071d24;color:#fff;margin-right:10px;">>></a></li>
        </ul>
        <script type="text/javascript">
            $("#current_page").on("keyup", function(e){
                if($(this).val() != "")
                {
                    var input = parseInt($(this).val());
                    var current = $("#current_page").attr("data-current");
                    var total = $("#current_page").attr("data-total");
                    var limit = $("#current_page").attr("data-limit");
                    if(!isNaN(input))
                    {
                        if(input > total)
                        {
                            input = current;
                        }
                        else if(input <= 0)
                        {
                            input = current;
                        }
                    }
                    else 
                    {
                        input = current;
                    }
                    $(this).val(input);
                    
                    if(e.which == 13)
                    {
                        window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."admin/view/"?>"+limit+"/"+(input*limit-limit);
                    }
                }
            });
            $("#first_page, #previous_page, #next_page, #last_page").click(function(e){
                e.preventDefault();
                console.log($(this).attr("id"));
                if($(this).attr("id") == "first_page")
                { 
                    window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."admin/view/{$limit}/0"?>";
                }
                else if($(this).attr("id") == "last_page")
                {
                    window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."admin/view/{$limit}/".($limit * ceil($total/$limit) - $limit)?>";
                }
                else if($(this).attr("id") == "previous_page")
                {
                    <?php 
                        if($offset - $limit <= 0)
                        {
                            echo "var previous = 0;";
                        }
                        else 
                        {
                            echo "var previous = ".($offset-$limit).";";
                        }
                    ?>
                    window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."admin/view/{$limit}/";?>" + previous;
                }
                else if($(this).attr("id") == "next_page")
                {
                    if($("#current_page").val() == $("#current_page").attr("data-current"))
                    {
                        <?php
                            if(ceil($total/$limit) == ceil(($offset+$limit)/$limit))
                            {
                                echo "var next = $offset;";
                            }
                            elseif(ceil($total/$limit) == 0 || ceil($total/$limit) == 1)
                            {
                                echo "var next = 0;";
                            }
                            else 
                            {
                                echo "var next = ".($offset+$limit).";";
                            }
                        ?>
                    window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."admin/view/{$limit}/";?>"+next;
                    }
                    else 
                    {
                        var input = $("#current_page").val();
                        var total = $("#current_page").attr("data-total");
                        var limit = $("#current_page").attr("data-limit");
                        window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."admin/view/"?>"+limit+"/"+(input*limit-limit);
                    }
                }
            });
        </script>       
    </div>
       <!--pagination end --> 
           
                       
         </div>
       
    </section>