<?php 
if(!isset($_POST['submit']))
{
    ?>
<section class="wrapper">
     <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;">Device Login</span>
         </div>
     <!-- top menue bar end -->  
     
            <div class="row">
               <dvi class="col-lg-12">
                 <div class="col-lg-2">  </div>
                 <div class="col-lg-8 centered select_admin_page_hading"> <h1>Select a Company to View/Add Device Login</h1></div>
                 <div class="col-lg-2"> </div>
            </div>
            <div class="row">
               <div class="col-lg-12" >
                 <div class="col-lg-2">  </div>
                   <div class="col-lg-8 centered select_admin_page_hading_dropdown">
                     
                        <div class="col-lg-2"> </div>
                        <form class="form-horizontal style-form" autocomplete="off" action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>admin/api" method="POST">
                            <div class="form-group">
                                <div class="col-sm-8 <?php echo(isset($error['admin_error']))?"has-error":""?>">
                                    <div class="<?php echo(isset($error['admin_error']))?"alert alert-danger":""?>">
                                        <?=@$error['admin_error']?>
                                    </div>
                                    <input type="text" name="admin_name" class="form-control" id="admin_name"/>
                                    <input type="hidden" name="admin" class="form-control" id="admin"/>
                                    <div id="predict" class="preres" style="display: none;">
                                        <ul></ul>
                                    </div>
                                    <script type="text/javascript">
                                    $(document).ready(function(){
                                        $("#admin_name").on("keyup click", function(){
                                            var request = "action=select&type=companies&source=admin&input=" + $(this).val();
                                            console.log(request);
                                            $.ajax({
                                            type: "POST",
                                            url: "<?php echo $this->config['domain'].'/'._PUBLIC_PATH_.'/admin/ajax';?>",
                                            data: request,
                                            cache: "FALSE",
                                            success: function(response){
                                                console.log(response);
                                                var str = '';
                                                try
                                                {
                                                    var data = JSON.parse(response);
                                                    //console.log(data.response+" => ");
                                                    $.each(data.response, function(k, v){
                                                        console.log(k + " => "+v);
                                                        str += "<li data-val='"+v['admin_id']+"'>"+v['first_name']+" "+v['last_name']+" ("+v['username']+")</li>";
                                                    });
                                                }
                                                catch(err)
                                                {
                                                    $("#predict").css("display", "none");
                                                    console.log(err.message);
                                                }
                                                $("#predict ul").html(str);
                                                $("#predict").css("display", "block");
                                                $("#predict").niceScroll({styler:"fb",cursorcolor:"#4ECDC4", cursorwidth: '10', cursorborderradius: '10px', background: '#404040', spacebarenabled:false, cursorborder: ''});
                                            },
                                            error: function(){
                                                alert("Something Went Wrong. Please Try Again");
                                            }
                                            });
                                        });
                                        $("#predict ul").on("click", "li", function(){
                                            $("#admin").val($(this).attr("data-val"));
                                            $("#admin_name").val($(this).text());
                                            $("#predict").css("display", "none");
                                        });
                                    });
                                </script>
                                </div>
                            </div>

                            <div class="form-panel"  style="box-shadow:none;">
                                <div class="col-lg-2"> </div>

                               <div class="form-group">
                                 <div class="col-sm-8 centered form_selectadmin_submit_button" >
                                    <input  type="submit" name="submit" value="Next"> 
                                  </div>     
                                 </div>
                            </div>
                        </form> 
                        </div>
                       </div>
                 <div class="col-lg-2"> </div>
    </section>
<?php
}
elseif($isForm && isset($_POST['submit']))
{
?>
 <section class="wrapper">
     <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;"> Device Login  </span>
         </div>
     <!-- top menue bar end -->  
     
            <div class="row">
               <dvi class="col-lg-12">
                 <div class="col-lg-2">  </div>
                 <div class="col-lg-8 centered select_admin_page_hading"> <h1>Device Login</h1></div>
                 <div class="col-lg-2"> </div>
            </div>
     
            <div class="row">
               <div class="col-lg-12">
                 <div class="col-lg-2">  </div>
                 <div class="col-lg-8 centered select_admin_page_hading1"> 
                 <!-- user data api form start here-->
                 <div class="clearfix margin-top-15p">
                 <form class="col-md-12" action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>admin/api/" method="POST">
                    <div class="<?php echo (!isset($error['csrf_error'])) ? '':'alert alert-danger' ?>">
                        <span class="">
                        <?=@$error['csrf_error']?>
                        </span>
                    </div>
                    <div class="<?php echo (!isset($error['error'])) ? '':'alert alert-danger' ?>">
                        <span class="">
                        <?=@$error['error']?>
                        </span>
                    </div>
                     
                     <div class="form-group">
                        <input type="text" class="form-control input-lg" placeholder="Username" name="username" value="<?=@$values['username']?>">
                        <small class="text-danger pull-left" ><?=@$error['username_error']?></small>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" placeholder="Password" name="pass">
                        <small class="text-danger pull-left" ><?=@$error['pass_error']?></small>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" placeholder="Confirm Password" name="confirmpass">
                        <small class="text-danger pull-left" ><?=@$error['pass_error']?></small>
                    </div>
                    <div class="form-group">
                        <input name="api_submit" type="submit" value="Save" class="btn btn-primary btn-lg btn-block" style="background:#071d24;border:none;">
                        
                    </div>
                </form>
                    </div>
                    
                    <!--api user data form end here-->
            </div>            
        </div>
        </div>
      </section>

<?php }
 else
     {
?>

 <section class="wrapper">
     <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;"> Device Login  </span>
         </div>
     <!-- top menue bar end -->  
     
            <div class="row">
               <div class="col-lg-12">
                 <div class="col-lg-2">  </div>
                 <div class="col-lg-8 centered select_admin_page_hading"> <h1>Device Login</h1></div>
                 <div class="col-lg-2"> </div>
               </div>
            </div>
            <div class="row">
               <dvi class="col-lg-12">
                 <div class="col-lg-2">  </div>
                 <div class="col-lg-8 centered select_admin_page_hading1"> 
                     <div class="<?php echo (!isset($error['error'])) ? '':'alert alert-danger' ?>" style="margin-top:10px">
                        <span class="">
                        <?=@$error['error']?>
                        </span>
                    </div>
                    <div class="<?php echo (!isset($res['response'])) ? '':'alert alert-success' ?>"  style="margin-top:10px">
                        <span class="">
                        <?=@$res['response']?>
                        </span>
                    </div>
                  <table class="table table-bordered" style="margin-top:25px;">
<tbody><tr >
<th style="text-align:center">Username</th>
<th style="text-align:center">Options</th>
</tr>

<td><?=@$username;?></td>
<td><a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>admin/api/edit" >Edit</a></td>
</tr>

</tbody></table>
            </div>            
            
      </section>

     <?php } ?>