<section class="wrapper">
     <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;"> Company Details  </span>
         </div>
     <!-- top menue bar end -->  
     
            <div class="row">
               <div class="col-lg-12">
                 <div class="col-lg-2">  </div>
                 <div class="col-lg-8 centered select_admin_page_hading"> <h1> Company Details</h1></div>
                 <div class="col-lg-2"> </div>
               </div>
            </div>
            <div class="row">
               <dvi class="col-lg-12">
                 <div class="col-lg-2">  </div>
                 <div class="col-lg-8 centered select_admin_page_hading1"> 
                     <div class="<?php echo (!isset($error['error'])) ? '':'alert alert-danger' ?>" style="margin-top:10px">
                        <span class="">
                        <?=@$error['error']?>
                        </span>
                    </div>
                <?php if(!$error){ ?> 
                  <table class="table table-bordered" style="margin-top:25px;">
                    <tbody>
                         <?php $detail = $details[0];?>
                    <tr>
                    <th style="text-align:center">Logo</th>
                    <th style="text-align:center"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>shared/logos/<?php echo(!empty($detail['logo_path']))? "{$detail['logo_path']}": "default.png";?>" class="img-responsive" style="width:100%;text-align:center"></th>
                    </tr>
                    <tr>
                    <td>Client Id</td>
                    <td><?=$detail['admin_id']; ?></td>
                    </tr>
                    <tr>
                    <td>First Name</td>
                    <td><?=$detail['first_name']; ?></td>
                    </tr>
                    <tr>
                    <td>Last Name</td>
                    <td><?=$detail['last_name'];?></td>
                    </tr>
                    <?php /* <tr>
                    <td>User Name</td>
                    <td><?=$detail['username'];?></td> */ ?>
                    </tr>
                    <tr>
                    <td>Membership Type</td>
                    <td><?=$detail['membership_title'];?></td>
                    </tr>
                    <tr>
                    <td>Company Registration</td>
                    <td><?=date("d/m/Y", strtotime($detail['date_of_birth']));?></td>
                    </tr>
                    <tr>
                    <td>Country</td>
                    <td><?=$detail['country_name'];?></td>
                    </tr>
                    <tr>
                    <td>Contact Number</td>
                    <td><?=$detail['number'];?></td>
                    </tr>
                    <tr>
                    <td>Email</td>
                    <td><?=$detail['email_address'];?></td>
                    </tr>    
                    <tr>                
                    <td>Status</td>
                    <td>
                        <?php
                    if($detail['status'] == _ACTIVE_)
                    {
                        echo "Active"; 
                    }
                    elseif($detail['status'] == _DEACTIVE_)
                    {
                        echo "Deactive"; 
                    }
                    elseif($detail['status'] == _BANNED_)
                    {
                        echo "Bandded"; 
                    }
                    elseif($detail['status'] == _REQUEST_)
                    {
                        echo "Request"; 
                    }
                       ?>
                    </td>
                    </tr>
                    <tr>
                    <td>Options</td>
                    <td><?php 
                    if($detail['status'] == _ACTIVE_)
                        {?>
                        <div class="bs-example">
                       <a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>admin/action/<?=$detail['username'];?>/deactivate"/> <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/not.png" class="img-rounded" alt="Deactivate"></a>
                       <a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>admin/action/<?=$detail['username'];?>/delete"/><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/delete_at.png" class="img-rounded" alt="Delete"></a>
                      <a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>admin/edit/<?=$detail['username'];?>"/><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/edit_at.png" class="img-rounded" alt="Edit"></a>
                        </div>
                       <?php
                        }
                        elseif($detail['status'] == _DEACTIVE_)
                        {
                        ?>
                            <a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>admin/action/<?=$detail['username'];?>/activate"/>Active</a>
                        <?php 
                        }
                        elseif($detail['status'] == _BANNED_)
                        {?>
                            <a href="#">Activate</a>

                        <?php
                        }
                        elseif($detail['status'] == _REQUEST_)
                        {
                        ?>
                            <a href="#">Approve</a>
                        <?php } ?>  
                        
                     </td>
                    </tr>
                    
                    
                    </tbody></table>
                    <br><br>
                    <?php } ?>
            </div>            
            
      </section>


