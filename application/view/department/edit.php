
<section class="wrapper">
       <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;">Edit Department </span>
         </div>
     <!-- top menue bar end -->   
      <div class="row">
          <div class="col-lg-9 main-chart">
            
             <div class="row">
                 <div class="form-panel"  style="box-shadow:none;color:#333">
                    <div class="<?php echo (!isset($error['error'])) ? '':'alert alert-danger' ?>">
                    <span class="">
                    <?=@$error['error']?>
                    </span>
                    </div>
                     <div class="<?php echo (!isset($res['response'])) ? '':'alert alert-success' ?>">
                    <span class="">
                    <?=@$res['response']?>
                    </span>
                    </div>
                 <form action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>department/edit/<?=$departments['dep_id'];?>" method="POST">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<h3 class="centered">Edit Department </h3>
							
						</div>
						<hr>
					</div> 
                                    <div class="panel-body">
						<div class="form-group">
                              <label class="col-sm-4 col-sm-4 control-label">Department Name</label>
                              <div class="col-sm-8">
                                  <input type="text" class="form-control" name="name" value="<?=$departments['dep_name'];?>" placeholder="Enter Department Name">
                                   <br>
                              </div>
                          </div>
                         
                          <div class="form-group">
                              <label class="col-sm-4 col-sm-4 control-label">Description</label>
                              <div class="col-sm-8">
                                  <textarea class="form-control" placeholder="Description" name="description"><?=@$departments['dep_description']?></textarea>
<!--                                  <input type="text" class="form-control" placeholder="Description" name="address" value="<?=$departments['dep_description']?>">-->
                                  <br>
                              </div>
                           </div>
                          
                          <div class="form-group">
                          <label class="col-sm-4 col-sm-4 control-label"></label>
                              <div class="col-sm-8">
                                  <input type="submit" name="submit" value="Update" class="btn btn-success btn-sm pull-left" >
                               </div>
                          </div>
				</div>
			</div>
		</div>
             </form>
        
        </div>
                  
       </div></div>
      </section>