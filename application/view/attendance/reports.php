<?php 
    if(!isset($_POST['submit']))
    {
?>
<section class="wrapper">
     <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;">Download Attendance Reports</span>
         </div>
     <!-- top menue bar end -->  
     
            <div class="row">
               <dvi class="col-lg-12">
                 <div class="col-lg-2">  </div>
                 <div class="col-lg-8 centered select_admin_page_hading"> <h3>Select a Company to Download Attendance Reports</h3></div>
                 <div class="col-lg-2"> </div>
            </div>
            <div class="row">
               <div class="col-lg-12" >
                 <div class="col-lg-2">  </div>
                   <div class="col-lg-8 centered select_admin_page_hading_dropdown">
                     
                        <div class="col-lg-2"> </div>
                        <form class="form-horizontal style-form" autocomplete="off" action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>attendance/reports" method="POST">
                            <div class="form-group">
                                <div class="col-sm-8 <?php echo(isset($error['admin_error']))?"has-error":""?>">
                                    <div class="<?php echo(isset($error['admin_error']))?"alert alert-danger":""?>">
                                        <?=@$error['admin_error']?>
                                    </div>
                                    <input type="text" name="admin_name" class="form-control" id="admin_name"/>
                                    <input type="hidden" name="admin" class="form-control" id="admin"/>
                                    <div id="predict" class="preres" style="display: none;">
                                        <ul></ul>
                                    </div>
                                    <script type="text/javascript">
                                    $(document).ready(function(){
                                        $("#admin_name").on("keyup click", function(){
                                            var request = "action=select&type=companies&source=admin&input=" + $(this).val();
                                            console.log(request);
                                            $.ajax({
                                            type: "POST",
                                            url: "<?php echo $this->config['domain'].'/'._PUBLIC_PATH_.'/admin/ajax';?>",
                                            data: request,
                                            cache: "FALSE",
                                            success: function(response){
                                                console.log(response);
                                                var str = '';
                                                try
                                                {
                                                    var data = JSON.parse(response);
                                                    //console.log(data.response+" => ");
                                                    $.each(data.response, function(k, v){
                                                        console.log(k + " => "+v);
                                                        str += "<li data-val='"+v['admin_id']+"'>"+v['first_name']+" "+v['last_name']+" ("+v['username']+")</li>";
                                                    });
                                                }
                                                catch(err)
                                                {
                                                    $("#predict").css("display", "none");
                                                    console.log(err.message);
                                                }
                                                $("#predict ul").html(str);
                                                $("#predict").css("display", "block");
                                            },
                                            error: function(){
                                                alert("Something Went Wrong. Please Try Again");
                                            }
                                            });
                                        });
                                        $("#predict ul").on("click", "li", function(){
                                            $("#admin").val($(this).attr("data-val"));
                                            $("#admin_name").val($(this).text());
                                            $("#predict").css("display", "none");
                                        });
                                    });
                                </script>
                                </div>
                            </div>

                            <div class="form-panel"  style="box-shadow:none;">
                                <div class="col-lg-2"> </div>

                               <div class="form-group">
                                 <div class="col-sm-8 centered form_selectadmin_submit_button" >
                                    <input  type="submit" name="submit" value="Next"> 
                                  </div>     
                                 </div>
                            </div>
                        </form> 
                        </div>
                       </div>
                 <div class="col-lg-2"> </div>
    </section>

 <?php 
}else
{
?>
 <section class="wrapper">
     <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;">Download Attendance Reports </span>
         </div>
     <!-- top menue bar end -->  
     
            <div class="row">
               <dvi class="col-lg-12">
                 <div class="col-lg-2">  </div>
                 <div class="col-lg-8 centered select_admin_page_hading"> <h1> Download Reports </h1></div>
                 <div class="col-lg-2"> </div>
               </div>
            </div>
            <div class="row">
               <dvi class="col-lg-12">
                 <div class="col-lg-2">  </div>
                 <div class="col-lg-8 centered select_admin_page_hading1"> 
                    <div style="display:none;margin-top:15px" id="response">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <span></span>
                    </div>
                     <table class="table table-bordered" style="margin-top:25px;">
                    <tbody><tr>
                    <th style="text-align:center">Reports</th>
                    <th style="text-align:center">Email</th>
                    <th style="text-align:center">Download</th>
                    </tr>
                    
                    <tr><td>Daily Flag Report</td>
                        <td><input type="checkbox" name="daily_flag" value="<?=_DAILY_FLAG_?>" <?=@($reports_bit & _DAILY_FLAG_ && $reports_bit != 0) ? 'CHECKED' : ''?> /> </td>
                     <td>   
                       <div class="bs-example">
                           <a href="<?=$this->config['domain'].'/'._PUBLIC_PATH_.'/attendance/exportit/csv/flag/daily/company';?>"> <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/csv.png" class="img-rounded" alt="CSV" title="CSV"></a>
                           <a href="<?=$this->config['domain'].'/'._PUBLIC_PATH_.'/attendance/exportit/pdf/flag/daily/company';?>"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/pdf.png" class="img-rounded" alt="PDF" title="PDF"></a>
                       </div>
                    </td>
                    </tr>
                    <tr><td>Daily Check-in Report</td>
                        <td><input type="checkbox" name="daily_checkin" value="<?=_DAILY_CHECKIN_?>" <?=@($reports_bit & _DAILY_CHECKIN_ && $reports_bit != 0) ? 'CHECKED' : ''?> /> </td>
                     <td>   
                       <div class="bs-example">
                           <a href="<?=$this->config['domain'].'/'._PUBLIC_PATH_.'/attendance/exportit/csv/checkin/daily/company';?>"> <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/csv.png" class="img-rounded" alt="CSV" title="CSV"></a>
                           <a href="<?=$this->config['domain'].'/'._PUBLIC_PATH_.'/attendance/exportit/pdf/checkin/daily/company';?>"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/pdf.png" class="img-rounded" alt="PDF" title="PDF"></a>
                       </div>
                    </td>
                    </tr>
                    <tr><td>Daily Attendance Report</td>
                        <td><input type="checkbox" name="daily_attnd" value="<?=_DAILY_ATTND_?>" <?=@($reports_bit & _DAILY_ATTND_ && $reports_bit != 0) ? 'CHECKED' : ''?> /> </td>
                     <td>   
                       <div class="bs-example">
                           <a href="<?=$this->config['domain'].'/'._PUBLIC_PATH_.'/attendance/exportit/csv/attendance/daily/company';?>"> <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/csv.png" class="img-rounded" alt="CSV" title="CSV"></a>
                           <a href="<?=$this->config['domain'].'/'._PUBLIC_PATH_.'/attendance/exportit/pdf/attendance/daily/company';?>"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/pdf.png" class="img-rounded" alt="PDF" title="PDF"></a>
                       </div>
                    </td>
                    </tr>
                    <tr>
                    <td>Summary Report</td>
                    <td>Not Available</td>
                    <td>   
                       <div class="bs-example">
                          <a href="<?=$this->config['domain'].'/'._PUBLIC_PATH_.'/attendance/exportit/csv/summary/normal/company';?>"> <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/csv.png" class="img-rounded" alt="CSV" title="CSV"></a>
                           <a href="<?=$this->config['domain'].'/'._PUBLIC_PATH_.'/attendance/exportit/pdf/summary/normal/company';?>"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/pdf.png" class="img-rounded" alt="PDF" title="PDF"></a>
                       </div>
                    </td>
                    </tr>
                     <tr>
                    <td>Detail Report (Employee photos  included)</td>
                    <td>Not Available</td>
                    
                    <td>   
                       <div class="bs-example">
                          <a href="<?=$this->config['domain'].'/'._PUBLIC_PATH_.'/attendance/exportit/csv/detailed/normal/company';?>"> <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/csv.png" class="img-rounded" alt="CSV" title="CSV"></a>
                           <a href="<?=$this->config['domain'].'/'._PUBLIC_PATH_.'/attendance/exportit/pdf/detailed/normal/company';?>"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/pdf.png" class="img-rounded" alt="PDF" title="PDF"></a>
                       </div>
                    </td>
                    </tr>
                   <tr>
                    <td>Select Date Range</td>
                    <td>Not Available</td>
                    
                    <td>   
                       <div class="bs-example">
                          <a href="<?=$this->config['domain'].'/'._PUBLIC_PATH_.'/attendance/export/custom';?>">Download</a>
                       </div>
                    </td>
                    </tr>
                  
                    </tbody></table>
            </div>            
            
      </section>
<script type="text/javascript">
    var process = true;
    $("input[name='daily_flag'], input[name='daily_checkin'], input[name='daily_attnd']").change(function(){
        process = false;
        var value = $(this).val();
        var type = $(this).attr("name");
        var isChecked = "true";
        if($(this).is(":checked"))
        {
            isChecked = "true";
        }
        else 
        {
            isChecked = "false";
        }
        var request = "value="+value+"&type="+type+"&box="+isChecked;
        console.log(request);
        if(!process)
        {
            process = true;
            $.ajax({
                type: "POST",
                url: "<?=$this->config['domain'].'/'._PUBLIC_PATH_.'/'?>attendance/ajax/subscribe",
                data: request,
                cache: false,
                success: function(data)
                {
                    process = false;
                    try
                    {
                        var response = JSON.parse(data);
                        if(response.isError)
                        {
                            $('#response').removeAttr("class");
                            $('#response').addClass("alert alert-danger");
                            $('#response').find('span').text(response.msg);
                            $('#response').show();
                        }
                        else 
                        {
                            $('#response').removeAttr("class");
                            $('#response').addClass("alert alert-success");
                            $('#response').find('span').text(response.msg);
                            $('#response').show();
                        }
                    }
                    catch(e)
                    {
                        $('#response').removeAttr("class");
                        $('#response').addClass("alert alert-danger");
                        $('#response').find('span').text("Something Went Wrong. Please Try Again.");
                        $('#response').show();
                    }
                },
                error: function(data)
                {
                    $('#response').removeAttr("class");
                    $('#response').addClass("alert alert-danger");
                    $('#response').find('span').text("Something Went Wrong. Please Try Again.");
                    $('#response').show();
                }
            });
        }
        else 
        {
            window.reload();
        }
    });
    </script>

<?php } ?>
