 <section class="wrapper">
     <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;"> Download Attendance Reports </span>
         </div>
     <!-- top menue bar end -->  
     
            <div class="row">
               <div class="col-lg-12">
                 <div class="col-lg-2">  </div>
                 <div class="col-lg-8 centered select_admin_page_hading"> <h1>Download Date Range Reports</h1></div>
                 <div class="col-lg-2"> </div>
            </div>
     
            <div class="row">
               <div class="col-lg-12">
                 <div class="col-lg-2">  </div>
                 <div class="col-lg-8 centered select_admin_page_hading1"> 
                 <!-- user data api form start here-->
                 <div class="clearfix margin-top-15p">
                 <form class="col-md-12" action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>attendance/export/custom" method="POST">
                    <div class="<?php echo (!isset($error['error'])) ? '':'alert alert-danger' ?>">
                        <span class="">
                        <?=@$error['error']?>
                        </span>
                    </div>
                     
                    <div class="form-group">
                        <div class="col-sm-12 pull-right <?php echo (!isset($error['date_from_error'])) ? '':'has-error' ?>">
                            <div class="input-group bootstrap-timepicker ">
                                <input id="timepickerone" data-date="" data-date-format="dd/mm/yyyy" data-link-field="dtp_input3" data-link-format="dd/mm/yyyy" id="test" type="text" name="date_from" placeholder="Date From" value="<?php if(isset($values['date_from'])){ echo $values['date_from'];}?>" class="form-control input-small " readonly>
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            </div>
                            <small class="text-danger"><?=@$error['date_from_error']?></small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 col-sm-3 control-label pull-left"></label>
                        <div class="col-sm-12 pull-right <?php echo (!isset($error['date_to_error'])) ? '':'has-error' ?>">
                            <div class="input-group bootstrap-timepicker ">
                                <input id="timepickertwo" data-date="" data-date-format="dd/mm/yyyy" data-link-field="dtp_input3" data-link-format="dd/mm/yyyy" id="test" type="text" name="date_to" placeholder="Date To" value="<?php if(isset($values['date_to'])){ echo $values['date_to'];}?>" class="form-control input-small " readonly>
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            </div>
                            <small class="text-danger"><?=@$error['date_to_error']?></small>
                        </div>
                    </div>
                     <div class="form-group">
                         <label class="col-sm-3 col-sm-3 control-label pull-left"></label>
                              <label class="col-sm-2 col-sm-2 control-label"></label>
                              <div class="col-sm-12 <?php echo(!isset($error['file_error'])) ? '':'has-error'?>">
                                    <select name="file" class="form-control">
                                      <option value="">Select File Format</option>
                                      <option value="pdf">PDF</option>
                                      <option value="csv">CSV</option>
                                    </select>
                                <small class="text-danger"><?=@$error['file_error']?></small>
                              </div>
                          </div>
                     <div class="form-group">
                         <label class="col-sm-3 col-sm-3 control-label pull-left"></label>
                              <label class="col-sm-2 col-sm-2 control-label"></label>
                              <div class="col-sm-12 <?php echo(!isset($error['report_type_error'])) ? '':'has-error'?>">
                                    <select name="report_type" class="form-control">
                                      <option value="">Report Type</option>
                                      <option value="flag">Flag</option>
                                      <option value="summary">Summary</option>
                                      <option value="detailed">Detailed</option>
                                    </select>
                                <small class="text-danger"><?=@$error['report_type_error']?></small>
                              </div>
                          </div>
                     <div class="form-group" style="margin-left: 12px; margin-right: 12px;">
                        <label class="col-sm-3 col-sm-3 control-label pull-left"></label>
                    <input name="export_submit" type="submit" value="Download" class="btn btn-primary btn-lg btn-block" style="background:#071d24;border:none;">
                    </div>
                </form>
                    </div>
                    
                    <!--api user data form end here-->
            </div>            
        </div>
        </div>
      </section>

<script type="text/javascript">
    
    $(document).ready(function(){
        $('#timepickerone').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0,
        showMeridian: 1
    });
	$('#timepickertwo').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0,
        showMeridian: 1
    });
        
    });
</script>