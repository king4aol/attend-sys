<?php  
    if(!isset($_POST['submit']))
    {
?>
    <section class="wrapper">
     <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;">View Attendance</span>
         </div>
     <!-- top menue bar end -->  
     
            <div class="row">
               <dvi class="col-lg-12">
                 <div class="col-lg-2">  </div>
                 <div class="col-lg-8 centered select_admin_page_hading"> <h3>Select a Company to View Attendance</h3></div>
                 <div class="col-lg-2"> </div>
            </div>
            <div class="row">
               <div class="col-lg-12" >
                 <div class="col-lg-2">  </div>
                   <div class="col-lg-8 centered select_admin_page_hading_dropdown">
                     
                        <div class="col-lg-2"> </div>
                        <form class="form-horizontal style-form" autocomplete="off" action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>attendance/view" method="POST">
                            <div class="form-group">
                                <div class="col-sm-8 <?php echo(isset($error['admin_error']))?"has-error":""?>">
                                    <div class="<?php echo(isset($error['admin_error']))?"alert alert-danger":""?>">
                                        <?=@$error['admin_error']?>
                                    </div>
                                    <input type="text" name="admin_name" class="form-control" id="admin_name"/>
                                    <input type="hidden" name="admin" class="form-control" id="admin"/>
                                    <div id="predict" class="preres" style="display: none;">
                                        <ul></ul>
                                    </div>
                                    <script type="text/javascript">
                                    $(document).ready(function(){
                                        $("#admin_name").on("keyup click", function(){
                                            var request = "action=select&type=companies&source=admin&input=" + $(this).val();
                                            console.log(request);
                                            $.ajax({
                                            type: "POST",
                                            url: "<?php echo $this->config['domain'].'/'._PUBLIC_PATH_.'/admin/ajax';?>",
                                            data: request,
                                            cache: "FALSE",
                                            success: function(response){
                                                console.log(response);
                                                var str = '';
                                                try
                                                {
                                                    var data = JSON.parse(response);
                                                    //console.log(data.response+" => ");
                                                    $.each(data.response, function(k, v){
                                                        console.log(k + " => "+v);
                                                        str += "<li data-val='"+v['admin_id']+"'>"+v['first_name']+" "+v['last_name']+" ("+v['username']+")</li>";
                                                    });
                                                }
                                                catch(err)
                                                {
                                                    $("#predict").css("display", "none");
                                                    console.log(err.message);
                                                }
                                                $("#predict ul").html(str);
                                                $("#predict").css("display", "block");
                                            },
                                            error: function(){
                                                alert("Something Went Wrong. Please Try Again");
                                            }
                                            });
                                        });
                                        $("#predict ul").on("click", "li", function(){
                                            $("#admin").val($(this).attr("data-val"));
                                            $("#admin_name").val($(this).text());
                                            $("#predict").css("display", "none");
                                        });
                                    });
                                </script>
                                </div>
                            </div>

                            <div class="form-panel"  style="box-shadow:none;">
                                <div class="col-lg-2"> </div>

                               <div class="form-group">
                                 <div class="col-sm-8 centered form_selectadmin_submit_button" >
                                     <input  type="submit" name="submit" value="Next"> 
                                  </div>     
                                 </div>
                            </div>
                        </form> 
                        </div>
                       </div>
                 <div class="col-lg-2"> </div>
      </section>
 <?php 
}elseif(isset($_POST['submit']) && !isset ($_POST['emp_submit']))
{
    ?>
    <section class="wrapper">
     <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;">View Attendance</span>
         </div>
     <!-- top menue bar end -->  
     
            <div class="row">
               <dvi class="col-lg-12">
                 <div class="col-lg-2">  </div>
                 <div class="col-lg-8 centered select_admin_page_hading"> <h3>Select an Employee to View Attendance</h3></div>
                 <div class="col-lg-2"> </div>
            </div>
            <div class="row">
               <div class="col-lg-12" >
                 <div class="col-lg-2">  </div>
                   <div class="col-lg-8 centered select_admin_page_hading_dropdown">
                     
                        <div class="col-lg-2"> </div>
                        <form class="form-horizontal style-form" autocomplete="off" id="emp_form" action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>attendance/view" method="POST">
                            <div class="form-group">
                                <div class="col-sm-8 <?php echo(isset($error['employee_error']))?"has-error":""?>">
                                    <div class="<?php echo(isset($error['employee_error']))?"alert alert-danger":""?>">
                                        <?=@$error['employee_error']?>
                                    </div>
                                    <div class="<?php echo(isset($error['csrf_error']))?"alert alert-danger":""?>">
                                        <?=@$error['csrf_error']?>
                                    </div>
                                    <input type="text" name="employee_name" class="form-control" id="employee_name"/>
                                    <input type="hidden" name="employee" class="form-control" id="employee"/>
                                    <div id="predict" class="preres" style="display: none;">
                                        <ul></ul>
                                    </div>
                                </div>
                            </div>

                            <div class="form-panel"  style="box-shadow:none;">
                                <div class="col-lg-2"> </div>

                               <div class="form-group">
                                 <div class="col-sm-8 centered form_selectadmin_submit_button" >
                                    <input type="hidden" name="csrf" value="<?=$csrf?>" />
                                    <input  type="submit" name="emp_submit" value="View Attendance"> 
                                  </div>     
                                 </div>
                            </div>
                        </form> 
                        </div>
                       </div>
                 <div class="col-lg-2"> </div>
      </section>
<script type="text/javascript">
    $(document).ready(function(){
        $("#employee_name").on("keyup click", function(){
            var request = "action=select&type=employee&source=admin&input=" + $(this).val();
            console.log(request);
            $.ajax({
            type: "POST",
            url: "<?php echo $this->config['domain'].'/'._PUBLIC_PATH_.'/employee/ajax';?>",
            data: request,
            cache: "FALSE",
            success: function(response){
                console.log(response);
                var str = '';
                try
                {
                    var data = JSON.parse(response);
                    if(!response.error)
                    {
                        //console.log(data.response+" => ");
                        $.each(data.response, function(k, v){
                            console.log(k + " => "+v);
                            str += "<li data-val='"+v['emp_id']+"'>"+v['emp_first_name']+" "+v['emp_last_name']+"</li>"
                        });
                    }
                }
                catch(err)
                {
                    $("#predict").css("display", "none");
                    console.log(err.message);
                }
                $("#predict ul").html(str);
                $("#predict").css("display", "block");
            },
            error: function(){
                alert("Something Went Wrong. Please Try Again");
            }
            });
        });
        $("#predict ul").on("click", "li", function(){
            $("#employee").val($(this).attr("data-val"));
            $("#employee_name").val($(this).text());
            $("#predict").css("display", "none");
        });
    });
</script>
<?php
}
else
{
   ?>
      <section class="wrapper">
        <div id="divLargerImage"></div>

        <div id="divOverlay"></div>
      <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;"><?=$empName?>'s Attendance Record </span>
         </div>
     <!-- top menue bar end -->          
       
<div class="col-lg-12">   
    <div class="wrapper">
            <div class="col-lg-12">

			
 
<form class="form-inline" style="width:80%" action="<?=$this->config['domain'].""._PUBLIC_PATH_."attendance/view/{$emp}"?>" method="post" role="form">
    <div class="dropdown" style="margin-left:-16px; ">
    <div class="form-group">
    </div>
    <div class="form-group">
    <select class="form-control" name="filter">
        <option value="today" <?php if($filter == "today") echo "selected";?>>Today</option>
        <option value="all" <?php if($filter == "all") echo "selected";?>>All</option>
        <option value="custom" <?php if($filter == "custom") echo "selected";?>>Custom</option>
    </select>
    </div>
        <div class="form-group">
            <input type="text" class="form-control filter_date" name="from_date" value="<?=@$values['from_date']?>" data-enable="custom" <?php if($filter != "custom") echo 'disabled="disabled"';?> placeholder="From Date" data-date-format="dd/mm/yyyy">
            <small class="text-danger"><?=@$error['from_date']?></small>
        </div>

      <div class="form-group">
          <input type="text" class="form-control filter_date" name="to_date" data-enable="custom" value="<?=@$values['to_date']?>" <?php if($filter != "custom") echo 'disabled="disabled"';?> placeholder="To Date" data-date-format="dd/mm/yyyy">
          <small class="text-danger"><?=@$error['to_date']?></small>
      </div>
 
<input type="submit" class="btn btn-default" style="background:#071d24;border:none;color:#fff" value="Filter"/>
    </div>
</form>
                <script type="text/javascript">
                    $(document).ready(function()
                    {
                        $("select[name='filter']").change(function(){
                            if($(this).val() == "custom")
                            {
                                $("input[data-enable='custom']").removeAttr("disabled");
                            }
                            else
                            {
                                $("input[data-enable='custom']").attr("disabled", "disabled");
                            }
                        });
                        $('input[name="from_date"]').datetimepicker({
                            weekStart: 1,
                            todayBtn:  1,
                                    autoclose: 1,
                                    todayHighlight: 1,
                                    startView: 2,
                                    minView: 2,
                                    forceParse: 0,
                            showMeridian: 1
                        });
                        $('input[name="to_date"]').datetimepicker({
                            weekStart: 1,
                            todayBtn:  1,
                                    autoclose: 1,
                                    todayHighlight: 1,
                                    startView: 2,
                                    minView: 2,
                                    forceParse: 0,
                            showMeridian: 1
                        });
                    });
                </script>
<button type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#myModal" style="background:#071d24;border:none;color:#fff;margin-right: -14px;margin-top:-36px;">Download Report</button>

<!-- Trigger the modal with a button -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background:#fa8564">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Download Reports</h4>
      </div>
      <div class="modal-body">
        <div style="text-align:center;"> 
                 
                  <table class="table table-bordered" style="margin-top:25px;">
                    <tbody><tr>
                    <th style="text-align:center">Reports</th>
                    <th style="text-align:center">Download</th>
                    </tr>
                    
                    <tr><td>Flag Report</td>
                     <td>   
                       <div class="bs-example">
                           <a href="<?=$this->config['domain'].'/'._PUBLIC_PATH_.'/attendance/exportit/csv/flag/filtered/employee';?>"> <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/csv.png" class="img-rounded" alt="CSV" title="CSV"></a>
                           <a href="<?=$this->config['domain'].'/'._PUBLIC_PATH_.'/attendance/exportit/pdf/flag/filtered/employee';?>"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/pdf.png" class="img-rounded" alt="PDF" title="PDF"></a>
                       </div>
                    </td>
                    </tr>
                    <tr>
                    <td>Summary Report</td>
                    <td>   
                       <div class="bs-example">
                          <a href="<?=$this->config['domain'].'/'._PUBLIC_PATH_.'/attendance/exportit/csv/summary/filtered/employee';?>"> <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/csv.png" class="img-rounded" alt="CSV" title="CSV"></a>
                           <a href="<?=$this->config['domain'].'/'._PUBLIC_PATH_.'/attendance/exportit/pdf/summary/filtered/employee';?>"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/pdf.png" class="img-rounded" alt="PDF" title="PDF"></a>
                       </div>
                    </td>
                    </tr>
                     <tr>
                    <td>Detail Report (Employee photos  included)</td>
                    
                    <td>   
                       <div class="bs-example">
                          <a href="<?=$this->config['domain'].'/'._PUBLIC_PATH_.'/attendance/exportit/csv/detailed/filtered/employee';?>"> <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/csv.png" class="img-rounded" alt="CSV" title="CSV"></a>
                           <a href="<?=$this->config['domain'].'/'._PUBLIC_PATH_.'/attendance/exportit/pdf/detailed/filtered/employee';?>"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/pdf.png" class="img-rounded" alt="PDF" title="PDF"></a>
                       </div>
                    </td>
                    </tr>
                   
                  
                    </tbody></table>
            </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>

  </div>
</div>

<br /><br />

</div>
    <table class="table">
    <thead class="otl_list_employes_color">
        <tr>
            <th>SN#</th>
            <th>Date </th>
            <th>Office Name</th>
            <th>Check-in Time</th>
            <th>Start Break</th>
            <th>End Break</th>
            <th>Check-out Time</th>
            <th>Time Zone</th>
            <th>Action</th>        
        </tr>
    </thead>
    <tbody style="background:#ccc">
        <?php 
        $i = 1;
        $timezone = '';
        $set_offset = '';
        $cur_offset = '';
        foreach ($attendance as $attnd)
        {
            $cur_offset = $attnd['timezone'];
            if($cur_offset != $set_offset)
            {
                $set_offset = $cur_offset;
                list($hours, $minutes) = explode(':', $set_offset);
                $seconds = $hours * 60 * 60 + $minutes * 60;
                $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 1);
                // Workaround for bug #44780
                if($timezone === false) 
                {
                    $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 0);
                }
                date_default_timezone_set($timezone);
            }
        ?> 
        <tr>
            <td><?=$offset + $i++?></td>
            <td><?=date("d-m-Y", $attnd['check_in_timestamp'])?></td>
            <td><?=$attnd['office_name'];?></td>
            <td>
                <?=date("H:i:s", $attnd['check_in_timestamp'])?>
                <br> 
                <span>
                    <img class="emp_img" src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>shared/<?php echo(is_file(_SHARED_UPLOADS_."/photos/".$attnd['cin_image']))? "photos/{$attnd['cin_image']}":"nopreview.png" ?>" width="100px" height="100px"> 
                </span>
            </td>
            <td>
                <?php if(!empty($attnd['break_in_timestamp']) && $attnd['break_in_timestamp'] != $attnd['check_out_timestamp']){ echo date("H:i:s", $attnd['break_in_timestamp']);}else{ echo "Not Available";}?>
                <br> 
                <span>
                    <img class="emp_img" src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>shared/<?php echo(is_file(_SHARED_UPLOADS_."/photos/".$attnd['bin_image']) && $attnd['break_in_timestamp'] != $attnd['check_out_timestamp'])? "photos/{$attnd['bin_image']}":"nopreview.png" ?>" width="100px" height="100px"> 
                </span>
            </td>
            <td>
                <?php if(!empty($attnd['break_out_timestamp']) && $attnd['break_out_timestamp'] != $attnd['check_out_timestamp']){ echo date("H:i:s", $attnd['break_out_timestamp']);}else{ echo "Not Available";}?>
                <br> 
                <span>
                    <img class="emp_img" src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>shared/<?php echo(is_file(_SHARED_UPLOADS_."/photos/".$attnd['bout_image']) && $attnd['break_out_timestamp'] != $attnd['check_out_timestamp'])? "photos/{$attnd['bout_image']}":"nopreview.png" ?>" width="100px" height="100px"> 
                </span>
            </td>
            <td>
                <?php if(!empty($attnd['check_out_timestamp'])){ echo date("H:i:s", $attnd['check_out_timestamp']);}else{ echo "Not Available";}?>
                <br> 
                <span>
                    <img class="emp_img" src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>shared/<?php echo(is_file(_SHARED_UPLOADS_."/photos/".$attnd['cout_image']))? "photos/{$attnd['cout_image']}":"nopreview.png" ?>" width="100px" height="100px"> 
                </span>
            </td>
            <td><?=$attnd['timezone'];?> UTC</td>
            <td>
                <a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>attendance/edit/<?=$attnd['id']?>"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/edit_at.png" class="img-rounded" alt="Edit" title="Edit"></a></td>
        </tr>
        <?php } 
            date_default_timezone_set($this->config['timezone']);
        ?>
    </tbody>
  </table>
                  
    </div>
     <!--pagination start -->    
    <div class="row">
        <ul class="pagination pull-right" style="margin-top:-15px">
            <li><a href="" id="first_page" style="background:#071d24;border:#071d24;color:#fff"><<</a></li>
            <li><a href="" id="previous_page" style="background:#071d24;border:#071d24;color:#fff"><</a></li>
            <li><a style="padding:1px !important;background:#071d24;"><input id="current_page" type="text" data-current="<?=$current?>" data-total="<?php echo(ceil($total/$limit))?ceil($total/$limit):"1"?>" data-limit="<?=$limit?>" value="<?=$current?>" style="color:#000;width:30px;height:27px;border:none;text-align:center"></a></li>
            <li><a style="padding:1px !important;height: 32px;width: 46px;text-align: center;line-height: 30px;text-decoration: none;background: none;border: none;text-align:left;color:#000"><span style="text-align:left"> &nbsp;of</span> &nbsp; <?php echo(ceil($total/$limit))?ceil($total/$limit):"1"?></a></li>
            <li><a href="" id="next_page" style="background:#071d24;border:#071d24;color:#fff">></a></li>
            <li><a href="" id="last_page" style="background:#071d24;border:#071d24;color:#fff;margin-right:10px;">>></a></li>
        </ul>
        <script type="text/javascript">
            $("#current_page").on("keyup", function(e){
                if($(this).val() != "")
                {
                    var input = parseInt($(this).val());
                    var current = $("#current_page").attr("data-current");
                    var total = $("#current_page").attr("data-total");
                    var limit = $("#current_page").attr("data-limit");
                    if(!isNaN(input))
                    {
                        if(input > total)
                        {
                            input = current;
                        }
                        else if(input <= 0)
                        {
                            input = current;
                        }
                    }
                    else 
                    {
                        input = current;
                    }
                    $(this).val(input);
                    
                    if(e.which == 13)
                    {
                        window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."attendance/view/$emp/"?>"+limit+"/"+(input*limit-limit);
                    }
                }
            });
            $("#first_page, #previous_page, #next_page, #last_page").click(function(e){
                e.preventDefault();
                console.log($(this).attr("id"));
                if($(this).attr("id") == "first_page")
                {
                    window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."attendance/view/$emp/{$limit}/0"?>";
                }
                else if($(this).attr("id") == "last_page")
                {
                    window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."attendance/view/$emp/{$limit}/".($limit * ceil($total/$limit) - $limit)?>";
                }
                else if($(this).attr("id") == "previous_page")
                {
                    <?php 
                        if($offset - $limit <= 0)
                        {
                            echo "var previous = 0;";
                        }
                        else 
                        {
                            echo "var previous = ".($offset-$limit).";";
                        }
                    ?>
                    window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."attendance/view/$emp/{$limit}/";?>" + previous;
                }
                else if($(this).attr("id") == "next_page")
                {
                    if($("#current_page").val() == $("#current_page").attr("data-current"))
                    {
                        <?php
                            if(ceil($total/$limit) == ceil(($offset+$limit)/$limit))
                            {
                                echo "var next = $offset;";
                            }
                            elseif(ceil($total/$limit) == 0 || ceil($total/$limit) == 1)
                            {
                                echo "var next = 0;";
                            }
                            else 
                            {
                                echo "var next = ".($offset+$limit).";";
                            }
                        ?>
                    window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."attendance/view/$emp/{$limit}/";?>"+next;
                    }
                    else 
                    {
                        var input = $("#current_page").val();
                        var total = $("#current_page").attr("data-total");
                        var limit = $("#current_page").attr("data-limit");
                        window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."attendance/view/$emp/"?>"+limit+"/"+(input*limit-limit);
                    }
                }
            });
        </script>       
    </div>
       <!--pagination end -->   
                       
         </div>
       
      </section>

     <!--main content end-->
     <script type="text/javascript">
         $('.emp_img').click(function () {
                var $img = $(this);
                $('#divLargerImage').html($img.clone().height(250).width(250)).add($('#divOverlay')).fadeIn();
            });

            $('#divLargerImage').add($('#divOverlay')).click(function () {
                $('#divLargerImage').add($('#divOverlay')).fadeOut(function () {
                    $('#divLargerImage').empty();
                });
            });
         </script>
  
     <?php if(isset($exportId)){?>
     <div> <a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>attendance/export/<?=$exportId?>">Export Current Data To CSV</a></div>
                    
     <?php }} ?>