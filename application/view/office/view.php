<?php if(!isset($_POST['submit']))
{
?>
<section class="wrapper">
     <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;">View Sites</span>
         </div>
     <!-- top menue bar end -->  
     
            <div class="row">
               <dvi class="col-lg-12">
                 <div class="col-lg-2">  </div>
                 <div class="col-lg-8 centered select_admin_page_hading"> <h1>Select a Company to View Sites</h1></div>
                 <div class="col-lg-2"> </div>
            </div>
            <div class="row">
               <div class="col-lg-12" >
                 <div class="col-lg-2">  </div>
                   <div class="col-lg-8 centered select_admin_page_hading_dropdown">
                     
                        <div class="col-lg-2"> </div>
                        <form class="form-horizontal style-form" autocomplete="off" action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>office/view" method="POST">
                            <div class="form-group">
                                <div class="col-sm-8 <?php echo(isset($error['admin_error']))?"has-error":""?>">
                                    <div class="<?php echo(isset($error['admin_error']))?"alert alert-danger":""?>">
                                        <?=@$error['admin_error']?>
                                    </div>
                                    <input type="text" name="admin_name" class="form-control" id="admin_name"/>
                                    <input type="hidden" name="client" class="form-control" id="admin"/>
                                    <div id="predict" class="preres" style="display: none;">
                                        <ul></ul>
                                    </div>
                                    <script type="text/javascript">
                                    $(document).ready(function(){
                                        $("#admin_name").on("keyup click", function(){
                                            var request = "action=select&type=companies&source=admin&input=" + $(this).val();
                                            console.log(request);
                                            $.ajax({
                                            type: "POST",
                                            url: "<?php echo $this->config['domain'].'/'._PUBLIC_PATH_.'/admin/ajax';?>",
                                            data: request,
                                            cache: "FALSE",
                                            success: function(response){
                                                console.log(response);
                                                var str = '';
                                                try
                                                {
                                                    var data = JSON.parse(response);
                                                    //console.log(data.response+" => ");
                                                    $.each(data.response, function(k, v){
                                                        console.log(k + " => "+v);
                                                        str += "<li data-val='"+v['username']+"'>"+v['first_name']+" "+v['last_name']+" ("+v['username']+")</li>";
                                                    });
                                                }
                                                catch(err)
                                                {
                                                    $("#predict").css("display", "none");
                                                    console.log(err.message);
                                                }
                                                $("#predict ul").html(str);
                                                $("#predict").css("display", "block");
                                            },
                                            error: function(){
                                                alert("Something Went Wrong. Please Try Again");
                                            }
                                            });
                                        });
                                        $("#predict ul").on("click", "li", function(){
                                            $("#admin").val($(this).attr("data-val"));
                                            $("#admin_name").val($(this).text());
                                            $("#predict").css("display", "none");
                                        });
                                    });
                                </script>
                                </div>
                            </div>

                            <div class="form-panel"  style="box-shadow:none;">
                                <div class="col-lg-2"> </div>

                               <div class="form-group">
                                 <div class="col-sm-8 centered form_selectadmin_submit_button" >
                                    <input  type="submit" name="submit" value="Next"> 
                                  </div>     
                                 </div>
                            </div>
                        </form> 
                        </div>
                       </div>
                 <div class="col-lg-2"> </div>
    </section>

<?php 
}
else
{
   ?>
<?php if(isset($_POST['submit']))
   {
      $admin = $_POST['client'];
      ?><?php }?>
<section class="wrapper">
     <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;">View Sites</span>
         </div>
     <!-- top menue bar end --> 
          <div class="wrapper">
            <div class="col-lg-12">
                <?php 
                if(isset($_POST['success']))
                {
                ?>
                <div class="col-lg-8 pull-left alert alert-success"><?=$_POST['success']?></div>
                  <script>
                      window.history.pushState("", "", "../view");
                  </script>
                <?php
                }
                ?>
                <div class="col-lg-8 pull-left alert alert-danger" id="error" style="display:none">
                    
                </div>
          </div>
    <table class="table">
    <thead class="otl_list_employes_color">
      <tr>
        <th>SN#</th>
        <th>Site Name  </th>
        <th>Address</th>
        <th>Options</th>
        
     </tr>
    </thead>
    <tbody style="background:#ccc">
         <?php
                   $i = 1;
                    foreach ($offices as $office)
                    {
                ?>  
        <tr>
            <td class="border_color"><?=$offset + $i++?></td>
            <td class="border_color"><?=$office['office_name'];?></td>
            <td class="border_color"><?=$office['address'];?></td>
            <td class="border_color">
                <div class="bs-example">
                    
                    <a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>office/edit/<?=$office['office_id'];?>"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/edit_at.png" class="img-rounded" title="Edit" alt="Edit"></a>
                
                   <a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>office/action/<?=$office['office_id'];?>/delete"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/delete_at.png" class="img-rounded" alt="Delete" title="Delete"></a>
                </div>
            </td>
        </tr> 
             <?php } ?>
    </tbody>
    </table>   
<!--pagination start -->    
    <div class="row">
        <ul class="pagination pull-right" style="margin-top:-15px">
            <li><a href="" id="first_page" style="background:#071d24;border:#071d24;color:#fff"><<</a></li>
            <li><a href="" id="previous_page" style="background:#071d24;border:#071d24;color:#fff"><</a></li>
            <li><a style="padding:1px !important;background:#071d24;"><input id="current_page" type="text" data-current="<?=$current?>" data-total="<?php echo(ceil($total/$limit))?ceil($total/$limit):"1"?>" data-limit="<?=$limit?>" value="<?=$current?>" style="color:#000;width:30px;height:27px;border:none;text-align:center"></a></li>
            <li><a style="padding:1px !important;height: 32px;width: 46px;text-align: center;line-height: 30px;text-decoration: none;background: none;border: none;text-align:left;color:#000"><span style="text-align:left"> &nbsp;of</span> &nbsp; <?php echo(ceil($total/$limit))?ceil($total/$limit):"1"?></a></li>
            <li><a href="" id="next_page" style="background:#071d24;border:#071d24;color:#fff">></a></li>
            <li><a href="" id="last_page" style="background:#071d24;border:#071d24;color:#fff;margin-right:10px;">>></a></li>
        </ul>
        <script type="text/javascript">
            $("#current_page").on("keyup", function(e){
                if($(this).val() != "")
                {
                    var input = parseInt($(this).val());
                    var current = $("#current_page").attr("data-current");
                    var total = $("#current_page").attr("data-total");
                    var limit = $("#current_page").attr("data-limit");
                    if(!isNaN(input))
                    {
                        if(input > total)
                        {
                            input = current;
                        }
                        else if(input <= 0)
                        {
                            input = current;
                        }
                    }
                    else 
                    {
                        input = current;
                    }
                    $(this).val(input);
                    
                    if(e.which == 13)
                    {
                        window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."/office/view/load/"?>"+limit+"/"+(input*limit-limit);
                    }
                }
            });
            $("#first_page, #previous_page, #next_page, #last_page").click(function(e){
                e.preventDefault();
                console.log($(this).attr("id"));
                if($(this).attr("id") == "first_page")
                {
                    window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."/office/view/load/{$limit}/0"?>";
                }
                else if($(this).attr("id") == "last_page")
                {
                    window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."/office/view/load/{$limit}/".($limit * ceil($total/$limit) - $limit)?>";
                }
                else if($(this).attr("id") == "previous_page")
                {
                    <?php 
                        if($offset - $limit <= 0)
                        {
                            echo "var previous = 0;";
                        }
                        else 
                        {
                            echo "var previous = ".($offset-$limit).";";
                        }
                    ?>
                    window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."/office/view/load/{$limit}/";?>" + previous;
                }
                else if($(this).attr("id") == "next_page")
                {
                    if($("#current_page").val() == $("#current_page").attr("data-current"))
                    {
                        <?php
                            if(ceil($total/$limit) == ceil(($offset+$limit)/$limit))
                            {
                                echo "var next = $offset;";
                            }
                            elseif(ceil($total/$limit) == 0 || ceil($total/$limit) == 1)
                            {
                                echo "var next = 0;";
                            }
                            else 
                            {
                                echo "var next = ".($offset+$limit).";";
                            }
                        ?>
                    window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."/office/view/load/{$limit}/";?>"+next;
                    }
                    else 
                    {
                        var input = $("#current_page").val();
                        var total = $("#current_page").attr("data-total");
                        var limit = $("#current_page").attr("data-limit");
                        window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."/office/view/load/"?>"+limit+"/"+(input*limit-limit);
                    }
                }
            });
        </script>       
    </div>
       <!--pagination end -->               
</div>
</section>
          
                    
<?php } ?>