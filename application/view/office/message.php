<?php 
    if(!isset($_POST['submit']) && !isset($_POST['msg_submit']))
    {
?>
<section class="wrapper">
<!-- top menue bar start -->
         <div class="row" style="background:#fff;height:40px;border-bottom: 3px solid #333;"> 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;">Site Messages  </span>
         </div>
     <!-- top menue bar end --> 
<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-2">  </div>
        <div class="col-lg-8 centered select_admin_page_hading"> <h3>Select a Company to Broadcast Site Based Messages </h3></div>
        <div class="col-lg-2"> </div>
    </div>
</div>

<div class="row">
<div class="col-lg-12" >
  <div class="col-lg-2">  </div>
    <div class="col-lg-8 centered select_admin_page_hading_dropdown">

         <div class="col-lg-2"> </div>
          <form class="form-horizontal style-form" autocomplete="off" action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>office/message" method="POST">
                <div class="form-group">
                    <div class="col-sm-8 <?php echo(isset($error['admin_error']))?"has-error":""?>">
                        <div class="<?php echo(isset($error['admin_error']))?"alert alert-danger":""?>">
                            <?=@$error['admin_error']?>
                        </div>
                        <input type="text" name="admin_name" class="form-control" id="admin_name"/>
                        <input type="hidden" name="admin" class="form-control" id="admin"/>
                        <div id="predict" class="preres" style="display: none;">
                            <ul></ul>
                        </div>
                        <script type="text/javascript">
                        $(document).ready(function(){
                            $("#admin_name").on("keyup click", function(){
                                var request = "action=select&type=companies&source=admin&input=" + $(this).val();
                                console.log(request);
                                $.ajax({
                                type: "POST",
                                url: "<?php echo $this->config['domain'].'/'._PUBLIC_PATH_.'/admin/ajax';?>",
                                data: request,
                                cache: "FALSE",
                                success: function(response){
                                    console.log(response);
                                    var str = '';
                                    try
                                    {
                                        var data = JSON.parse(response);
                                        //console.log(data.response+" => ");
                                        $.each(data.response, function(k, v){
                                            console.log(k + " => "+v);
                                            str += "<li data-val='"+v['admin_id']+"'>"+v['first_name']+" "+v['last_name']+"("+v['username']+")</li>"
                                        });
                                    }
                                    catch(err)
                                    {
                                        $("#predict").css("display", "none");
                                        console.log(err.message);
                                    }
                                    $("#predict ul").html(str);
                                    $("#predict").css("display", "block");
                                },
                                error: function(){
                                    alert("Something Went Wrong. Please Try Again");
                                }
                                });
                            });
                            $("#predict ul").on("click", "li", function(){
                                $("#admin").val($(this).attr("data-val"));
                                $("#admin_name").val($(this).text());
                                $("#predict").css("display", "none");
                            });
                        });
                    </script>
                    </div>
                </div>

                <div class="form-panel"  style="box-shadow:none;">
                    <div class="col-lg-2"> </div>

                   <div class="form-group">
                     <div class="col-sm-8 centered form_selectadmin_submit_button" >
                         <input  type="submit" name="submit" value="Next"> 
                      </div>     
                     </div>
                </div>
            </form>
           </div>
        </div>

        <!--button end-->
        </div>
     </div>
</section>
<?php }elseif(isset($_POST['submit']) && !isset($_POST['msg_submit']) && !isset($_POST['ofc_submit'])){
    ?>
<section class="wrapper">
<!-- top menue bar start -->
         <div class="row" style="background:#fff;height:40px;border-bottom: 3px solid #333;"> 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;">Site Messages  </span>
         </div>
     <!-- top menue bar end --> 
<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-2">  </div>
        <div class="col-lg-8 centered select_admin_page_hading"> <h3>Set a Site for Broadcast Site Based Messages</h3></div>
        <div class="col-lg-2"> </div>
    </div>
</div>

<div class="row">
<div class="col-lg-12" >
  <div class="col-lg-2">  </div>
    <div class="col-lg-8 centered select_admin_page_hading_dropdown">

         <div class="col-lg-2"> </div>
            <form class="form-horizontal style-form" autocomplete="off" id="emp_form" action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>office/message" method="POST">
                            <div class="form-group">
                                <div class="col-sm-8 <?php echo(isset($error['office_error']))?"has-error":""?>">
                                    <div class="<?php echo(isset($error['office_error']))?"alert alert-danger":""?>">
                                        <?=@$error['office_error']?>
                                    </div>
                                    <div class="<?php echo(isset($error['csrf_error']))?"alert alert-danger":""?>">
                                        <?=@$error['csrf_error']?>
                                    </div>
                                    <input type="text" name="office_name" class="form-control" id="office_name"/>
                                    <input type="hidden" name="office" class="form-control" id="office"/>
                                    <div id="predict" class="preres" style="display: none;">
                                        <ul></ul>
                                    </div>
                                </div>
                            </div>

                            <div class="form-panel"  style="box-shadow:none;">
                                <div class="col-lg-2"> </div>

                               <div class="form-group">
                                 <div class="col-sm-8 centered form_selectadmin_submit_button" >
                                    <input type="hidden" name="csrf" value="<?=@$csrf?>" />
                                    <input  type="submit" name="ofc_submit" value="Next"> 
                                  </div>     
                                 </div>
                            </div>
                        </form> 
           </div>
        </div>

        <!--button end-->
        </div>
     </div>
<script type="text/javascript">
    $(document).ready(function(){		$("#office_name").on("keyup click", function(){
            var request = "action=select&type=office&source=admin&input=" + $(this).val();
            console.log(request);
            $.ajax({
            type: "POST",
            url: "<?php echo $this->config['domain'].'/'._PUBLIC_PATH_.'/office/ajax';?>",
            data: request,
            cache: "FALSE",
            success: function(response){
                console.log(response);
                var str = '';
                try
                {
                    var data = JSON.parse(response);
                    //console.log(data.response+" => ");
                    $.each(data.response, function(k, v){
                        console.log(k + " => "+v);
                        str += "<li data-val='"+v['office_id']+"'>"+v['office_name']+"</li>"
                    });
                }
                catch(err)
                {
                    $("#predict").css("display", "none");
                    console.log(err.message);
                }
                $("#predict ul").html(str);
                $("#predict").css("display", "block");
            },
            error: function(){
                alert("Something Went Wrong. Please Try Again");
            }
            });
        });
        $("#predict ul").on("click", "li", function(){
            $("#office").val($(this).attr("data-val"));
            $("#office_name").val($(this).text());
            $("#predict").css("display", "none");
        });
    });
</script>
<?php
}else{ ?>

<section class="wrapper">
     <!-- top menue bar start -->
         <div class="row" style="background:#fff;height:40px;border-bottom: 3px solid #333;"> 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;">Site Message  </span>
         </div>
     <!-- top menue bar end --> 
      <div class="col-lg-9 main-chart">
            <div class="row centered">
            <div class="col-lg-12" > 
			
             
<div class="form-panel"  style="box-shadow:none;color:#333">
        <div class="<?php echo (!isset($error['error'])) ? '':'alert alert-danger' ?>">
    <span class="">
    <?=@$error['error']?>
    </span>
</div>
<div class="<?php echo (!isset($res['response'])) ? '':'alert alert-success' ?>">
    <span class="">
    <?=@$res['response']?>
    </span>
</div>          
  <form class="form-horizontal" role="form"  method="POST" action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>office/message" style="color:#333">
    
    <div class="form-group">
        <label for="message" class="col-sm-2 control-label">Check-in Message</label>
        <div class="col-sm-8">
            <textarea class="form-control global_message_textarea_box" rows="7" name="cin_message" placeholder="Check-in Message Here..."><?=@$messages['o_cin_message']?></textarea>
        </div>
     <div class="form-group" style="display:none">
           <textarea class="form-control" rows="4" name="message"></textarea>
        </div>
    </div>
    
     <div class="form-group">
        <label for="message" class="col-sm-2 control-label">Check-out Message</label>
        <div class="col-sm-8">
            <textarea class="form-control global_message_textarea_box" rows="7" name="cout_message" placeholder="Check-out Message Here..."><?=@$messages['o_cout_message']?></textarea>
        </div>
     <div class="form-group" style="display:none">
           <textarea class="form-control" rows="4" name="message"></textarea>
        </div>
    </div>
    
     <div class="form-group">
        <label for="message" class="col-sm-2 control-label">Break Start Message</label>
        <div class="col-sm-8">
            <textarea class="form-control global_message_textarea_box" rows="7" name="bin_message" placeholder="Break Start Message Here..."><?=@$messages['o_bin_message']?></textarea>
        </div>
     <div class="form-group" style="display:none">
           <textarea class="form-control" rows="4" name="message"></textarea>
        </div>
    </div>
    
     <div class="form-group" >
        <label for="message" class="col-sm-2 control-label">Break End Message</label>
        <div class="col-sm-8">
            <textarea class="form-control global_message_textarea_box"  rows="7" name="bout_message" placeholder="Break End Message Here..."><?=@$messages['o_bout_message']?></textarea>
        </div>
        
        <!-- global visibility message box-->
        <div class="col-sm-2">
        <p> Global Visibility </p>
          <div class="form-group " style="background:#d8d9db;padding:5px; font-size: smaller;">
             
             <label class="label_main_globalcheckbox">
               <span class="checkbox_button_global_message"><input type="checkbox" name="a_cin" <?php if(@$messages['a_show_cin'] == 1) echo "checked";?>></span>
               <span class="textcheckbox_button_global_message">Global Check-in </span>
               </label>
                <label class="label_main_globalcheckbox">
               <span class="checkbox_button_global_message"><input type="checkbox" name="a_cout" <?php if(@$messages['a_show_cout'] == 1) echo "checked";?>></span>
               <span class="textcheckbox_button_global_message">Global Check-out </span>
               </label>
                <label class="label_main_globalcheckbox">
               <span class="checkbox_button_global_message"><input type="checkbox" name="a_bin" <?php if(@$messages['a_show_bin'] == 1) echo "checked";?>></span>
               <span class="textcheckbox_button_global_message">Global Break Start </span>
               </label>
                <label class="label_main_globalcheckbox">
               <span class="checkbox_button_global_message"><input type="checkbox" name="a_bout" <?php if(@$messages['a_show_bout'] == 1) echo "checked";?>></span>
               <span class="textcheckbox_button_global_message">Global Break End </span>
               </label>
            
            </div>
        </div>
    
     <!-- global visibility message box-->
    </div>
     <div class="form-group" >
        <label for="message" class="col-sm-2 control-label"></label>
        <div class="col-sm-8">
           <input type="submit" name="msg_submit" value="save" class="btn btn-primary pull-right" style="background:#333;border:none;">
        </div>
     </div>
   
  
</form>
</div>
</div>
</div>
</div>         
</section>
<?php } ?>