
<section class="wrapper">
       <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;">Edit Site </span>
         </div>
     <!-- top menue bar end -->   
      <div class="row">
          <div class="col-lg-9 main-chart">
            
             <div class="row">
                 <div class="form-panel"  style="box-shadow:none;color:#333">
                    <div class="<?php echo (!isset($error['error'])) ? '':'alert alert-danger' ?>">
                    <span class="">
                    <?=@$error['error']?>
                    </span>
                    </div>
                     <div class="<?php echo (!isset($res['response'])) ? '':'alert alert-success' ?>">
                    <span class="">
                    <?=@$res['response']?>
                    </span>
                    </div>
                 <form action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>office/edit/<?=$offices['office_id'];?>" method="POST">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<h3 class="centered">Edit Site </h3>
							
						</div>
						<hr>
					</div> 
                                    <div class="panel-body">
						<div class="form-group">
                              <label class="col-sm-4 col-sm-4 control-label">Site Name</label>
                              <div class="col-sm-8">
                                  <input type="text" class="form-control" name="name" value="<?=$offices['office_name'];?>" placeholder="Enter Office Name">
                                   <br>
                              </div>
                          </div>
                         
                          <div class="form-group">
                              <label class="col-sm-4 col-sm-4 control-label">Address</label>
                              <div class="col-sm-8">
                                  <input type="text" class="form-control" placeholder="Address" name="address" value="<?=$offices['address']?>">
                                  <br>
                              </div>
                           </div>
                          
                          <div class="form-group">
                          <label class="col-sm-4 col-sm-4 control-label"></label>
                              <div class="col-sm-8">
                                  <input type="submit" name="submit" value="Update" class="btn btn-success btn-sm pull-left" >
                               </div>
                          </div>
				</div>
			</div>
		</div>
             </form>
        
        </div>
                  
       </div></div>
      </section>









<!--<form action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>office/edit/<?=$offices['office_id'];?>" method="POST" style="height:300px; ">
        <div align="center" >
                <h2 align="center">Edit <?=$offices['office_name'];?></h2>
                <div class="error-text"><?=@$error['csrf_error']?></div>
            <table >
               <tr> <td><label><span>Office Name</span></label></td>
                    <td>
                    <input type="text" placeholder="Office name" name="name" value="<?=$offices['office_name'];?>"/>
                    <span class="error"><?=@$error['name_error']?></span>
                    </td>
               </tr>
                <tr>
                    <td><label><span>Address</span></label></td>
                    <td>
                    <input type="text" placeholder="Address" name="address" value="<?=$offices['address']?>"/>
                    <span class="error"><?=@$error['address_error']?></span>
                    </td>
               </tr>
               <tr>
               <input type="hidden" name="csrf" value="<?=$csrf?>"/>
                   <td><input type="submit" name ="submit" value="update" /></td>
               </tr>
            </table>
        </div>
    </form>-->
