<html>
<head>

 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/favicon.png">

    <title><?=$this->config['title']?></title>
    <link type="text/css" href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/css/bootstrap.css" rel="stylesheet">
    <link type="text/css" href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/css/style.css" rel="stylesheet">
    <link type="text/css" href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/css/style-responsive.css" rel="stylesheet">
    <script src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/js/jquery-1.8.3.min.js" type="text/javascript" ></script>
        <!-- js Bootstrap -->
     <script src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/js/bootstrap.min.js" type="text/javascript" ></script>
    </head>
    
    <body style="background:#e7e7e7;">
  		 <!-----------------------------------  header Start----------------------------------->
         <header class="bg">
             
        <div class="row background">
        
             <div class="col-sm-12 top-buffer" style="text-align:center;"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/login_logo.png" class="logo"></div>

        </div>
    </header>
    
    <!----------------------------------- header End ----------------------->
  
       <div class="wrapper_login">
           <div class="bs-example">
               <div class="alert alert-danger fade in" style="<?php if(@sizeof($error) < 1) echo "display:none"?>">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <?php if(isset($error['error'])){echo $error['error']."<br>";}?>
                    <?php if(isset($error['csrf_error'])){echo $error['csrf_error']."<br>";}?>
                    <?php if(isset($error['user_error'])){echo $error['user_error']."<br>";}?>
                    <?php if(isset($error['pass_error'])){echo $error['pass_error']."<br>";}?>
                </div>
               <div class="alert alert-success fade in" style="<?php if(@$action != "logout") echo "display:none"?>">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    Successfully Logged Out.
                </div>
           </div>
           
    <form action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>access/login" method="POST">
         <div class="image_wrap"> <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/usericon.png" class="img-responsive">  </div>
         
         <div class="input_fields_wrap">
         <div class="image_icon1"> 
           <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/username_field.png" class="img-responsive user_name_img1">
           <input type="text" name="user" placeholder="Username" value="<?=@$values['user']?>" class="input_username">
         </div>
         <div class="image_icon1"> 
             <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/password_field.png" class="img-responsive user_name_img2" style="width: 177px;">
           <input type="password" name="pass" placeholder="password" class="input_username">
           <input type="hidden" name="csrf" value="<?=$csrf?>"/>
         </div> 
             <div class="image_icon_other"> 
                 <span> <input type="checkbox" name="remember" class="img-responsive user_name_img2" style="width: 15px; height: 15px;float: left;"></span>
                 <span style="width:62px;height:10px;float:left;font-size:8px;margin-top:5px;">Remember me</span>
           
         </div> 
             <div class="image_icon1" style="font-size:10px;margin-top:4px;"> 
                 <i> <a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>access/forgetpassword" >Forgot password ?</a></i>
           
         </div> 
             
         </div>
         <div class="login_button_wrap"><button class="login_button"> 
                 <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/login_button.png" class="img-responsive" width="79px" height="70px" style="margin-top: -1px; height: 71px;
    width: 84px;">
</button></div> 
         </form>
           
<!--         <form action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>access/login" method="POST">
         <div class="image_wrap"> <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/usericon.png">  </div>
         
         <div class="input_fields_wrap">
         <div class="image_icon1"> 
           <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/username_field.png" class="img-responsive user_name_img1">
           <input type="text" name="user" placeholder="username" class="input_username" value="<?=@$values['user']?>">
         </div>
         <div class="image_icon1"> 
           <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/password_field.png" class="img-responsive user_name_img2">
           <input type="password" name="pass" placeholder="password" class="input_username">
           <input type="hidden" name="csrf" value="<?=$csrf?>">
         </div> 
             
        <div class="image_icon_other"> 
            <span> <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/check_box.png" class="img-responsive user_name_img2" style="width: 30px; height:30px;"></span>
            <span>Remember me</span>
         </div> 
             <div class="image_icon1"> 
            Forgot password
           <input type="password" name="text" placeholder="password" class="input_username">
         </div> 
         </div>
         <div class="login_button_wrap">
            <button class="login_button"> 
                <img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/login_button.png" class="img-responsive">
                <input type="hidden" name="submit" value="login" class="login_button">
            </button>
         </div>
         </form>-->
         
         </div>
      <!------------------------->                                     
       </body>
       </html>

