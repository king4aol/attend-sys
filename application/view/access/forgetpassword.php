<html>
<head>

 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OTL | Forgot Password</title>
    <link type="text/css" href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/css/bootstrap.css" rel="stylesheet">
    <link type="text/css" href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/css/style.css" rel="stylesheet">
    <link type="text/css" href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/css/style-responsive.css" rel="stylesheet">
    <script src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/js/jquery-1.11.3.min.js" type="text/javascript" ></script>
        <!-- js Bootstrap -->
     <script src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/js/bootstrap.js" type="text/javascript" ></script>
     <script src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/js/bootstrap.min.js" type="text/javascript" ></script>
     <script src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/js/npm.js" type="text/javascript" ></script>
    </head>
    
    <body style="background:#e7e7e7;">
  		 <!-----------------------------------  header Start----------------------------------->
         <header class="bg">
             
        <div class="row background">
        
             <div class="col-sm-12 top-buffer" style="text-align:center;"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/login_logo.png" class="logo"></div>

        </div>
    </header>
    
    <!----------------------------------- header End ----------------------->
      
       
             <!--main content start-->
      <section class="forgot_pass">
      <section class="wrapper">
    
      <div class="col-lg-4 col-lg-offset-4 main-chart" style="background:#797979;padding-bottom:70px;">
            <div class="row centered">
<?php if($type == "reset"){ ?>
                
    <div class="col-lg-12" > 
             <h2 style="color:white">Set New Password</h2><br><br> 
             <div class="form-panel"  style="box-shadow:none;">
                  
  <div class="col-sm-12">
    <div class="col-sm-12 ">
                <div class="<?php echo(isset($res['response']))? 'alert alert-success':'';?>">
                    <?=@$res['response']?>
                </div> 
                <div class="<?php echo(isset($error['error']))? 'alert alert-danger':'';?>">
                    <?=@$error['error']?>
                </div>
        <form method="post" action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>access/forgetpassword">
            <input type="password" class="input-lg form-control" name="password" placeholder="New Password" autocomplete="off">
            <div class="row">
            
            </div>
            <input type="password" class="input-lg form-control" name="c_password" placeholder="Repeat Password" autocomplete="off">
            <div class="row">
            
            </div>
            <input type="submit" name="p_submit" class="col-xs-12 btn btn-primary btn-load btn-lg color_button_forgot_pass" data-loading-text="Changing Password..." value="Change Password">
            </form>
            </div>
      </div>                 
                  
 
           </div>
           </div>
                
                
                
       
<?php }else{ ?>
      <div class="col-lg-12" > 
          <h2 style="color:white">Forgot Password</h2><br><br> 
            <div class="form-panel"  style="box-shadow:none;">
                <div class="<?php echo(isset($res['response']))? 'alert alert-success':'';?>">
                    <?=@$res['response']?>
                </div> 
                <div class="<?php echo(isset($error['error']))? 'alert alert-danger':'';?>">
                    <?=@$error['error']?>
                </div>
  <div class="col-sm-12">
      <div class="col-sm-12">
     <p class="text-center">Username:</p>
     <div class="row">
         <form method="POST" id="passwordForm" action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>access/forgetpassword">
             <input type="text" class="input-lg form-control" value="<?=@$values['username']?>" name="username" placeholder="Username" autocomplete="off">
            
            <input type="submit" name="submit" class="col-xs-12 btn btn-primary btn-load btn-lg color_button_forgot_pass" value="Submit">
        </form>
         </div>
      </div>   
    </div>
    </div>
    </div>
<?php } ?>
<!------------------------->   
      
    </div>
    </div> 
      </section>
      </section>
 </body>
 </html>