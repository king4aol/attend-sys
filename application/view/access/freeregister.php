<html>
    <head>
<title><?=$this->config['title']?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/css/font-awesome.css" rel="stylesheet" />
<!--    Datetime Picker-->
    <link href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/css/bootstrap-datetimepicker.css" rel="stylesheet" media="screen">
    
    <!-- Custom styles for this template -->
    <link href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/css/style.css" rel="stylesheet">
    <link href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/css/style-responsive.css" rel="stylesheet">
    
    <script src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/js/jquery.js"></script>
    </head>
    <body style="background:#e7e7e7;">
          		 <!-----------------------------------  header Start----------------------------------->
         <header class="bg">
             
        <div class="row background">
        
             <div class="col-sm-12 top-buffer"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/login_logo.png" class="img-responsive logo"></div>

        </div>
    </header>
    <div class="row topbar"> 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;">Register New Client</span>
         </div>
    <div class="row">
        <div class="col-lg-9 main-chart">
<div class="container">
             
             <div class="col-lg-10"> 
             <div class="form-panel">
                 <div class="<?php echo (!isset($error['csrf_error'])) ? '':'alert alert-danger' ?>">
                     <?=@$error['csrf_error']?>
                 </div>
                 <div class="<?php echo (!isset($error['error'])) ? '':'alert alert-danger' ?>">
                     <?=@$error['error']?>
                 </div>
                 <div class="<?php echo (!isset($res['response'])) ? '':'alert alert-success' ?>">
                     <?=@$res['response']?>
                 </div>
                 <form class="form-horizontal style-form" action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>access/freeregistered" method="POST">
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">First Name*</label>
                              <div class="col-sm-10 <?php echo(!isset($error['fname_error'])) ? '':'has-error'?>">
                                  <input type="text" placeholder="First Name" name="fname" value="<?=@$values['fname']?>" class = "form-control"/>
                                    <small class="text-danger"><?=@$error['fname_error']?></small>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Last Name*</label>
                              <div class="col-sm-10 <?php echo(!isset($error['lname_error'])) ? '':'has-error'?>">
                                <input type="text" class="form-control"  placeholder="Last Name" name="lname" value="<?=@$values['lname']?>"/>
                                <small class="text-danger"><?=@$error['lname_error']?></small>
                              </div>
                          </div>
<!--                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Company Name*</label>
                              <div class="col-sm-10 <?php echo(!isset($error['company_name_error'])) ? '':'has-error'?>">
                                  <input type="text" class="form-control"  placeholder="Company Name" name="company_name" value="<?=@$values['company_name']?>" />
								  <small class="text-danger"><?=@$error['company_name_error']?></small>
                              </div>
                          </div>-->
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Address*</label>
                              <div class="col-sm-10 <?php echo(!isset($error['address_error'])) ? '':'has-error'?>">
                                  <input class="form-control" type="text" placeholder="enter address" name="address" value="<?=@$values['address']?>" />
                            <small class="text-danger"><?=@$error['address_error']?></small>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Country*</label>
                              <div class="col-sm-10 <?php echo(!isset($error['country_error'])) ? '':'has-error'?>">
                                  <select name="country" class="form-control">
                                      <option value="">Select Country</option>
                                  <?php foreach ($countries as $key => $value) {?>
                            <option value="<?=$value['iso_2']?>" <?php if($value['iso_2'] == @$values['country']) echo 'selected';?>><?=$value['country_name']?></option>
                            <?php }?>
                          </select>
                                <small class="text-danger"><?=@$error['country_error']?></small>
                              </div>
                          </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Date of Birth*</label>
                              <div class="col-sm-10 <?php echo (!isset($error['dob_error'])) ? '':'has-error' ?>">
                                <div class="input-group bootstrap-timepicker form_date">
                                    <input value="<?=@$values['dob']?>" id="dob" data-date-format="dd/mm/yyyy" type="text" name="dob" placeholder="DD/MM/YYYY"  class="form-control" readonly>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                </div>
                                  <small class="text-danger"><?=@$error['dob_error']?></small>
                              </div>
                          </div>
			<div class="form-group">
                              <label class="col-lg-2 col-sm-2 control-label">Phone*</label>
                              <div class="col-lg-10 <?php echo(!isset($error['phone_error'])) ? '':'has-error'?>">
                                 <input type = "text" class="form-control" placeholder="Phone Number" name="phone" value="<?=@$values['phone']?>" />
				 <small class="text-danger"><?=@$error['phone_error']?></small>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">User Name*</label>
                              <div class="col-sm-10 <?php echo(!isset($error['user_error'])) ? '':'has-error'?>">
                                  <input class="form-control" type="text" placeholder="User name" name="user" value="<?=@$values['user']?>" />
				<small class="text-danger"><?=@$error['user_error']?></small>
                              </div>
                          </div>
			 <div class="form-group">
                              <label class="col-lg-2 col-sm-2 control-label">Email*</label>
                              <div class="col-lg-10 <?php echo(!isset($error['email_error'])) ? '':'has-error'?>">
                                 <input class="form-control" type="email"  placeholder="enter email" name="email" value="<?=@$values['email']?>" />
				<small class="text-danger"><?=@$error['email_error']?></small>
                              </div>
                          </div>
			<div class="form-group">
                              <label class="col-lg-2 col-sm-2 control-label">Password*</label>
                              <div class="col-lg-10 <?php echo(!isset($error['pass_error'])) ? '':'has-error'?>">
                                 <input type="password" class="form-control"   placeholder="enter password" name="pass" >
				 <small class="text-danger"><?=@$error['pass_error']?></small>
                              </div>
                          </div>
			<div class="form-group">
                              <label class="col-lg-2 col-sm-2 control-label">Confirm Password*</label>
                              <div class="col-lg-10">
                                 <input type="password" class="form-control" placeholder="enter confirm password" name="confirmpass">
                              </div>
                          </div>
                          <div class="form-group">
                          <label class="col-lg-2 col-sm-2 control-label"></label>
                              <div class="col-lg-10">
                               <input type="hidden" value="<?=@$csrf?>" name="csrf">
                               <input class="btn btn-success btn-sm pull-left" type="submit" value="Register Free Client" name="submit">
                               </div>
                          </div>
                      </form>
                  </div>
                  
                  </div>
          </div>
        </div>
    </div>
        <footer class="site-footer">
      
    <div class="container">
       <div class="col-lg-6" style="text-align:center;padding:12px;">Copyright © <?=date("Y")?> Odyssey Time Logic. All Rights Reserved</div>
       <div class="col-lg-6" style="text-align:left;"></div>
    </div>     
        </footer>
  <!-- js placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <script src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/js/bootstrap.min.js"></script>
    </body>
</html>

<script type="text/javascript">
    
    $(document).ready(function(){
       
        $('#dob').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0,
        showMeridian: 1
        });
        
        
        
    });
</script>