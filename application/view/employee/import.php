<?php 
    if(!isset($_POST['submit']))
    {
?>
<section class="wrapper">
   <!-- top menu bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;">Import Employees</span>
         </div>
     <!-- top menu bar end -->  
     
            <div class="row">
               <dvi class="col-lg-12">
                 <div class="col-lg-2">  </div>
                 <div class="col-lg-8 centered select_admin_page_hading"> <h3>Select a Company to Import Employees</h3></div>
                 <div class="col-lg-2"> </div>
            </div>
            <div class="row">
               <div class="col-lg-12" >
                 <div class="col-lg-2">  </div>
                   <div class="col-lg-8 centered select_admin_page_hading_dropdown">
                     
                        <div class="col-lg-2"> </div>
                        <form class="form-horizontal style-form" autocomplete="off" action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>employee/import" method="POST">
                            <div class="form-group">
                                <div class="col-sm-8 <?php echo(isset($error['admin_error']))?"has-error":""?>">
                                    <div class="<?php echo(isset($error['admin_error']))?"alert alert-danger":""?>">
                                        <?=@$error['admin_error']?>
                                    </div>
                                    <input type="text" name="admin_name" class="form-control" id="admin_name"/>
                                    <input type="hidden" name="admin" class="form-control" id="admin"/>
                                    <div id="predict" class="preres" style="display: none;">
                                        <ul></ul>
                                    </div>
                                    <script type="text/javascript">
                                    $(document).ready(function(){
                                        $("#admin_name").on("keyup click", function(){
                                            var request = "action=select&type=companies&source=admin&input=" + $(this).val();
                                            console.log(request);
                                            $.ajax({
                                            type: "POST",
                                            url: "<?php echo $this->config['domain'].'/'._PUBLIC_PATH_.'/admin/ajax';?>",
                                            data: request,
                                            cache: "FALSE",
                                            success: function(response){
                                                console.log(response);
                                                var str = '';
                                                try
                                                {
                                                    var data = JSON.parse(response);
                                                    //console.log(data.response+" => ");
                                                    $.each(data.response, function(k, v){
                                                        console.log(k + " => "+v);
                                                        str += "<li data-val='"+v['admin_id']+"'>"+v['first_name']+" "+v['last_name']+" ("+v['username']+")</li>"
                                                    });
                                                }
                                                catch(err)
                                                {
                                                    $("#predict").css("display", "none");
                                                    console.log(err.message);
                                                }
                                                $("#predict ul").html(str);
                                                $("#predict").css("display", "block");
                                            },
                                            error: function(){
                                                alert("Something Went Wrong. Please Try Again");
                                            }
                                            });
                                        });
                                        $("#predict ul").on("click", "li", function(){
                                            $("#admin").val($(this).attr("data-val"));
                                            $("#admin_name").val($(this).text());
                                            $("#predict").css("display", "none");
                                        });
                                    });
                                </script>
                                </div>
                            </div>

                            <div class="form-panel"  style="box-shadow:none;">
                                <div class="col-lg-2"> </div>

                               <div class="form-group">
                                 <div class="col-sm-8 centered form_selectadmin_submit_button" >
                                    <input  type="submit" name="submit" value="Next"> 
                                  </div>     
                                 </div>
                            </div>
                        </form> 
                        </div>
                       </div>
            </div>
                 <div class="col-lg-2"> </div>
    </section>
<?php 
}
else
{
?>
<img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/loadin.GIF" style="display:none;width:100px;height:100px;position:absolute;left:50%;top:50%; z-index: 12345" id="loading" />
<section class="wrapper">
     <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;">Import Employees</span>
         </div>
     <!-- top menue bar end --> 
     
    <br>
    <h5><a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>employee/import/download">Download Sample</a></h5>
    <br>
    <div class="row">
            <div class="col-lg-12">
            <div class="col-lg-2">  </div>
            <div class="col-lg-8 centered select_admin_page_hading"> <h3>Upload CSV</h3></div>
            <div class="col-lg-2"> </div>
            </div>
               <div class="col-lg-12" >
                 <div class="col-lg-2">  </div>
                   <div class="col-lg-8 centered select_admin_page_hading_dropdown">
                     
                        <div class="col-lg-2"> </div>
                        <form class="form-horizontal style-form" action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>employee/import" method="POST">
                            <div class="form-group">
                                <div class="col-sm-8 <?php echo(isset($error['admin_error']))?"has-error":""?>">
                                    <div class="<?php echo(isset($error['admin_error']))?"alert alert-danger":""?>">
                                        <?=@$error['admin_error']?>
                                    </div>
                                    <input id="fileupload" type="file" name="files" class="form-control">
                                </div>
                                <div class="col-sm-12 form-panel" style="padding-left:70px">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-7">
                                        <!-- The global progress bar -->
                                        <div id="progress" class="progress">
                                            <div class="progress-bar progress-bar-success"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form> 
                        </div>
                       </div>
            </div>
    <div class="col-lg-2"> </div>
      
    <table id="report" class="table" style="display: none;">
    <thead class="otl_list_employes_color">
      <tr>
        <th>Row #</th>
        <th>Status</th>
        <th>Response</th>
     </tr>
    </thead>
    <tbody style="background:#ccc">
    </tbody>
   </table>
    
</section>
<script> var aaa = '';
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';
    var url = "<?=$this->config['domain']?><?=_PUBLIC_PATH_?>employee/import/upload";
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        done: function (e, data) {
            if(typeof data.result.files[0] !== 'undefined')
            {
                $.ajax({
                    type: "POST",
                    url: "<?=$this->config['domain']?><?=_PUBLIC_PATH_?>employee/import/import/" + data.result.files[0].name,
                    cache: "false",
                    timeout: "0",
                    beforeSend: function()
                    {
                        $("body").css("pointer-events","none");
                        $(".wrapper").css("opacity", "0.1");
                        $("#loading").show();
                    },
                    success: function(data)
                    {
                        console.log(data);
                        $("#loading").hide();
                        $("body").css("pointer-events", "auto");
                        $(".wrapper").css("opacity", "1");
                        $('#progress .progress-bar').css(
                            'width',
                            '0%'
                        );
                        var obj = JSON.parse(data);
                        aaa = obj;
                        var str = '';
                        if(obj != "")
                        {
                            if(obj.response.is_error)
                            {
                                alert(obj.response.response)
                            }
                            else 
                            {
                                for(var i =2; i <= obj.response.response; i++)
                                {
                                    if(obj[i].is_error)
                                    {
                                        str = "<tr><td><strong>"+i+"</strong></td><td><a class='btn btn-xs btn-danger' href='#'>Failed</a></td><td>";
                                        $.each(obj[i].errors, function(k,v){
                                            str += v+",<br>";
                                        });
                                        
                                        str += "</td></tr>";
                                        $("#report tbody").append(str);
                                    }
                                    else 
                                    {
                                        $("#report tbody").append("<tr><td>"+i+"</td><td><a class='btn btn-xs btn-success' href='#'>Success</a></td><td>"+obj[i].response+"</td></tr>");
                                    }
                                }
                                $("#report").show();
                            }
                        }
                    }
                });
            }
            else 
            {
                alert("Something Went Wrong.");
            }
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
</script>
<?php
}
?>
