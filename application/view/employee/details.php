<section class="wrapper">
     <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;"> Employee Details  </span>
         </div>
     <!-- top menue bar end -->  
     
            <div class="row">
               <dvi class="col-lg-12">
                 <div class="col-lg-2">  </div>
                 <div class="col-lg-8 centered select_admin_page_hading"> <h1> Employee Details</h1></div>
                 <div class="col-lg-2"> </div>
               </div>
            </div>
            <div class="row">
               <dvi class="col-lg-12">
                 <div class="col-lg-2">  </div>
                 <div class="col-lg-8 centered select_admin_page_hading1"> 
                 <div <?php echo (!isset($error['error'])) ? '':'class="alert alert-danger" style="margin-top:10px"' ?>>
                     <?=@$error['error']?>
                 </div>
                    <div <?php echo (!isset($res['response'])) ? '':'class="alert alert-success" style="margin-top:10px"' ?>>
                     <?=@$res['response']?>
                 </div>
                  <table class="table table-bordered" style="margin-top:25px;">
                    <tbody>
                         <?php $detail = $employee[0];?>
                    <tr>
                    <td>First Name</td>
                    <td><?=$detail['emp_first_name']; ?></td>
                    </tr>
                    <tr>
                    <td>Last Name</td>
                    <td><?=$detail['emp_last_name'];?></td>
                    </tr>
<!--                    <tr>
                    <td>Department</td>
                    <td><?=$detail['dep_name'];?></td>
                    </tr>
                    <tr>
                    <td>Employee Type</td>
                    <td><?=$detail['type_title'];?></td>
                    </tr>-->
                    <tr>
                    <td>Email Address</td>
                    <td><?=$detail['email_address'];?></td>
                    </tr>
                    <tr>
                    <td>Shift Time Start</td>
                    <td><?=@date("H:i", $detail['timing_from']);?></td>
                    </tr>
                    <tr>
                    <td>Shift Time End</td>
                    <td><?=@date("H:i", $detail['timing_to']);?></td>
                    </tr>
                    <tr>
                    <td>Pay Rate Per Hour</td>
                    <td><?=$detail['pay_rate'];?></td>
                    </tr>
                    <tr>
                    <td>Pin</td>
                    <td><?=$detail['pin'];?></td>
                    </tr>
                    <tr>
                    <td>Grace Time</td>
                    <td><?=$detail['emp_grace_time'];?></td>
                    </tr>
                    <tr>
                    <td>Lunch Duration</td>
                    <td><?=$detail['emp_break_time'];?></td>
                    </tr>
                    <tr>
                    <td>Status</td>
                    <td>
                        <?php
                    if($detail['status'] == _ACTIVE_)
                    {
                        echo "Active"; 
                    }
                    elseif($detail['status'] == _DEACTIVE_)
                    {
                        echo "Deactive"; 
                    }
                    elseif($detail['status'] == _DELETED_)
                    {
                        echo "Deleted"; 
                    }
                    elseif($detail['status'] == _REQUEST_)
                    {
                        echo "Request"; 
                    }
                       ?>
                     </td>
                    </tr>
                    <tr>
                    <td>Options</td>
                    <td>
                       <a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>attendance/view/<?=$detail['emp_id']?>"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/view_at.png" class="img-rounded" title="View Attendance" alt="Attendance"></a>
                       <a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>employee/edit/<?=$detail['emp_id'];?>"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/edit_at.png" class="img-rounded" title="Edit" alt="Edit"></a>
                       <a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>employee/action/<?=$detail['emp_id']; if($detail['status'] == _ACTIVE_){echo '/deactivate"><img src="'.$this->config['domain'].'/'._PUBLIC_PATH_.'/assets/img/not.png" class="img-rounded" alt="Deactivate" title="Deactivate">';}else{echo '/activate"><img src="'.$this->config['domain'].'/'._PUBLIC_PATH_.'/assets/img/deactive_at.png" class="img-rounded" alt="Activate" title="Activate">';}?></a>
                       <a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>employee/action/<?=$detail['emp_id'];?>/delete"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/delete_at.png" class="img-rounded" alt="Delete" title="Delete"></a>
                    </td>
                    </tr>
                    
                    
                    </tbody></table>
                    <br><br>
            </div>            
            
      </section>