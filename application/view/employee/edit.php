<section class="wrapper">

     <!-- top menue bar start -->

         <div class="row topbar" > 

          <span style="line-height: 44px;padding-left: 17px;"> </span> 

          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;"> Edit Employee </span>

         </div>

     <!-- top menue bar end --> 

        <div class="row"> 

             <div class="container">

             <div class="col-lg-6"> 

             <div class="form-panel">

                 <div class="<?php echo (!isset($error['csrf_error'])) ? '':'alert alert-danger' ?>">

                     <?=@$error['csrf_error']?>

                 </div>

                 <div class="<?php echo (!isset($error['emp_limit'])) ? '':'alert alert-danger' ?>">

                     <?=@$error['emp_limit']?>

                 </div>

                 <div class="<?php echo (!isset($error['error'])) ? '':'alert alert-danger' ?>">

                     <?=@$error['error']?>

                 </div>

                 <div class="<?php echo (!isset($res['response'])) ? '':'alert alert-success' ?>">

                     <?=@$res['response']?>

                 </div>

                 <form class="form-horizontal style-form" role="form" action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>employee/edit/<?=$employee['emp_id'];?>" method="POST">

                          <div class="form-group">

                              <label class="col-sm-4 col-sm-4 control-label">First Name*</label>

                              <div class="col-sm-8 <?php echo (!isset($error['firstname_error'])) ? '':'has-error' ?>">

                                  <input type="text"  class="form-control" name="firstname" value="<?=$employee['emp_first_name']?>" placeholder="First Name">

                                  <small class="text-danger"><?=@$error['firstname_error']?></small>

                              </div>

                          </div>

                          <div class="form-group">

                              <label class="col-sm-4 col-sm-4 control-label">Last Name*</label>

                              <div class="col-sm-8 <?php echo (!isset($error['lastname_error'])) ? '':'has-error' ?>">

                                  <input type="text" class="form-control" placeholder="Last Name" name="lastname" value="<?=$employee['emp_last_name']?>" placeholder="Last Name"/>

                                  <small class="text-danger"><?=@$error['lastname_error']?></small>

                              </div>

                          </div>

                     

                          <div class="form-group">

                            <label class="col-sm-4 col-sm-4 control-label">Shift Start*</label>

                              <div class="col-sm-8 <?php echo (!isset($error['timing_from_error'])) ? '':'has-error' ?>">

                                <div class="input-group bootstrap-timepicker timepicker">

                                    <input id="timepickerone" data-date="" data-date-format="hh:ii" data-link-field="dtp_input3" data-link-format="hh:ii" id="test" type="text" name="timing_from" placeholder="Timing From" value="<?php if(isset($employee['timing_from'])){ echo date("H:i", $employee['timing_from']);}?>" class="form-control input-small " readonly>

                                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>

                                </div>

                                  <small class="text-danger"><?=@$error['timing_from_error']?></small>

                              </div>

                          </div>

                          <div class="form-group">

                               <label class="col-sm-4 col-sm-4 control-label">Shift End*</label>

                              <div class="col-sm-8 <?php echo (!isset($error['timing_to_error'])) ? '':'has-error' ?>">

                                  <div class="input-group bootstrap-timepicker timepicker">

                                    <input data-date="" data-date-format="hh:ii" data-link-field="dtp_input3" data-link-format="hh:ii" id="timepickertwo" class="form-control mytimepicker" name="timingto" type="text" placeholder="Timing To" value="<?php if(isset($employee['timing_to'])){ echo date("H:i", $employee['timing_to']);}?>" readonly>

                                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>

                                    </div>

                                  <small class="text-danger"><?=@$error['timing_to_error']?></small>

                              </div>

                          </div>

                          <div class="form-group">

                               <label class="col-sm-4 col-sm-4 control-label">Per Hour Rate*</label>

                              <div class="col-sm-8 <?php echo (!isset($error['pay_rate_error'])) ? '':'has-error' ?>">

                                  <input class="form-control"  name="pay_rate" value="<?=$employee['pay_rate']?>" placeholder="Pay Rate Per Hour" type="text">

                                  <small class="text-danger"><?=@$error['pay_rate_error']?></small>

                              </div>

                          </div>

<!--                          <div class="form-group">

                               <label class="col-sm-4 col-sm-4 control-label">OverTime Per Hour Rate*</label>

                              <div class="col-sm-8 <?php /*echo (!isset($error['overtime_pay_rate_error'])) ? '':'has-error';*/ ?>">

                                  <input type="text" class="form-control"  placeholder="please enter overtime pay rate per hour" name="overtime_pay_rate" value="<?php//$employee['overtime_pay_rate']?>" >

                                  <small class="text-danger"><?=php//$error['overtime_pay_rate_error']?></small>

                              </div>

                          </div>-->

                          <div class="form-group">

                               <label class="col-sm-4 col-sm-4 control-label">Employee Grace Time</label>

                              <div class="col-sm-8 <?php echo (!isset($error['emp_grace_time_error'])) ? '':'has-error' ?>">

                                  <input type="text" class="form-control" placeholder="please enter employee grace time" name="emp_grace_time" value="<?=$employee['emp_grace_time']?>">

                                  <small class="text-danger"><?=@$error['grace_time_error']?></small>

                              </div>

                          </div>

                           <div class="form-group">

                               <label class="col-sm-4 col-sm-4 control-label">Break Duration (Min)</label>

                              <div class="col-sm-8 <?php echo (!isset($error['break_time_error'])) ? '':'has-error' ?>">



			     <div class="col-sm-3" style="margin-left:-15px;text-align: center;">

                                 <input type="text" class="form-control" placeholder="Break Duration In Mintues" name="break_time" id="break_time" value="<?=$employee['emp_break_time']?>" readonly="readonly">

                             </div>

<div class="col-sm-8" style="margin-top: 10px;width: 242px;float:right;">

								 

                                  <div id="slider" style="background:#071d24"></div>

                              </div>





                              </div>

                          </div>

                            <div class="form-group">

                              <label class="col-sm-4 col-sm-4 control-label">Pin*</label>

                             <div class="col-sm-8 <?php echo (!isset($error['pin_error'])) ? '':'has-error' ?>">

                                 <input type="text" class="form-control"  name="pin" value="<?=$employee['pin']?>" placeholder="">

                                 <small class="text-danger"><?=@$error['pin_error']?></small>

                              </div>

                          </div>

                          <div class="form-group">

                              <label class="col-sm-4 col-sm-4 control-label">Email</label>

                             <div class="col-sm-8 <?php echo (!isset($error['email_error'])) ? '':'has-error' ?>">

                                 <input type="email" class="form-control" placeholder="Email Address" name="email" value="<?=@$employee['email_address']?>">

                                 <small class="text-danger"><?=@$error['email_error']?></small>

                              </div>

                          </div>

						  <!--

                        <div class="form-group">

                              <label class="col-sm-4 col-sm-4 control-label">Department*</label>

                              <div class="col-sm-8 <?php echo(!isset($error['country_error'])) ? '':'has-error'?>">

                                  <select name="dept" class="form-control">

                                      <option value="">Select Department</option>

                                <?php foreach ($departments as $value) {?>

                                        <option value="<?=$value['dep_id']?>" <?php if($value['dep_id'] == @$employee['dep_id']) echo 'selected';?>><?=$value['dep_name']?></option>

                                <?php }?>

                                    </select>

                                <small class="text-danger"><?=@$error['dept_error']?></small>

                              </div>

                          </div>

                            <div class="form-group">

                                <label class="col-sm-4 col-sm-4 control-label">Employee Type*</label>

                                <div class="col-sm-8 <?php echo(!isset($error['type_error'])) ? '':'has-error'?>">

                                    <select name="type" class="form-control">

                                        <option value="">Select Type</option>

                                  <?php foreach ($types as $value) {?>

                                          <option value="<?=$value['type_id']?>" <?php if($value['type_id'] == @$employee['type_id']) echo 'selected';?>><?=$value['type_title']?></option>

                                  <?php }?>

                                      </select>

                                  <small class="text-danger"><?=@$error['type_error']?></small>

                                </div>

                            </div>

							-->

                          <div class="form-group">

                          <label class="col-sm-4 col-sm-4 control-label"></label>

                              <div class="col-sm-8">

                                  <input type="hidden" name="csrf" value="<?=@$csrf?>"/>

                                  <input class="btn btn-success btn-sm pull-right" type="submit" name="submit" value="Update"/>

                               </div>

                          </div>

                      </form>

                  </div>                  

                  </div>

                  <!-- status portation-->

                 





                  

                  <!-- status portation end-->

                 

                  </div>

          </div>



</section>



<script type="text/javascript">

    

    $(document).ready(function(){
        var obj = new Date();
        var today = obj.getFullYear()+'-';
        today += obj.getMonth()+1;
        today += '-'+obj.getDate();
        var end = "23:59";
        
        $('#timepickerone').datetimepicker({

        startDate: today,
        endDate: today+' '+end,
        weekStart: 1,

        todayBtn:  1,

		autoclose: 1,

		todayHighlight: 1,

		startView: 1,

		minView: 0,

		maxView: 1,

		forceParse: 0,
                minuteStep: 1

        });

	$('#timepickertwo').datetimepicker({

        startDate: today,
        endDate: today+' '+end,

        weekStart: 1,

        todayBtn:  1,

        autoclose: 1,

        todayHighlight: 1,

        startView: 1,

        minView: 0,

        maxView: 1,

        forceParse: 0,
        minuteStep: 1

        });

        

        var iValue = <?=$employee['emp_break_time']?>;

        $("#break_time").val(iValue); 

        $("#slider").slider(

        {

          min:0,

          max:60,

          value:iValue,

          slide:function(event,ui) 

          {

            $("#break_time").val(ui.value);

          }

       });

    });

</script>