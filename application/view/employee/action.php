<?php
if(@$action == "activate")
{
?>
<section class="wrapper">
     <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;"> Activate Employee </span>
         </div>
     <!-- top menue bar end -->  
     
            <div class="row">
               <div class="col-lg-12">
                 <div class="col-lg-2">  </div>
                 <div class="col-lg-8 centered select_admin_page_hading"  style="width: 45.66666667%;"> <h1>Out Standing Check-in</h1></div>
                 <div class="col-lg-2"> </div>
            </div>
     
            <div class="row">
               <div class="col-lg-12">
                 <div class="col-lg-2">  </div>
                 <div class="col-lg-8 centered select_admin_page_hading1"  style="width: 45.66666667%;"> 
                 <!-- user data api form start here-->
                 <div class="clearfix margin-top-15p">
                 <form class="col-md-12" action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>employee/action/<?=@$values['url']?>" method="POST">
                    <div class="<?php echo (!isset($error['error'])) ? '':'alert alert-danger' ?>">
                        <span class="">
                       <?=@$error['csrf_error']."<br>".@$error['error']?>
                        </span>
                    </div>
            <?php 
            $i = -1;
            $size = sizeof($unCheckedouts);
              foreach($unCheckedouts as $unCheckedout):
                if(!is_array($unCheckedout))
                {
                    list($hours, $minutes) = explode(':', $unCheckedouts['timezone']);
                    $seconds = $hours * 60 * 60 + $minutes * 60;
                    $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 1);
                    // Workaround for bug #44780
                    if($timezone === false) 
                    {
                        $timezone = timezone_name_from_abbr(get_timezone_abbr($seconds), $seconds, 0);
                    }
                    date_default_timezone_set($timezone);
            ?>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Latest Check-in Time</label>
                        <div class="col-sm-9">
                            <div class="input-group bootstrap-timepicker ">
                                <input type="text" value="<?=date("d/m/Y H:i:s", $unCheckedouts['check_in_timestamp'])?>"  data-date="" data-date-format="" data-link-field="dtp_input3" data-link-format="" class="form-control input-small " readonly>
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            </div>
                            <small class="text-danger"></small>
                        </div>
                    </div>
                     <div class="form-group">
                         <label class="col-sm-3 col-sm-3 control-label">Check-out Time</label>
                        <div class="col-sm-9">
                            <div class="input-group bootstrap-timepicker ">
                                <input class="timepickerone" type="text" name="checkout" placeholder="DD/MM/YYYY HH:MM" value="<?=@$values['checkout']?>"  data-date="" data-date-format="dd/mm/yyyy h:i" data-link-field="dtp_input1" data-link-format="" class="form-control input-small " readonly>
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            </div>
                            <small class="text-danger"><?=@$error['checkout_error']?></small>
                        </div>
                    </div>
            <?php
            break;
            }
            $i++;
                list($hours, $minutes) = explode(':', $unCheckedout['timezone']);
                $seconds = $hours * 60 * 60 + $minutes * 60;
                $timezone = timezone_name_from_abbr('', $seconds, 1);
                // Workaround for bug #44780
                if($timezone === false) 
                {
                    $timezone = timezone_name_from_abbr('', $seconds, 0);
                }
                date_default_timezone_set($timezone);
            ?> 
			
		
                <span class="col-md-12" style="font-weight:bold;text-align: left;margin-left: 12px;font-size: 17px;">  <?=$unch?> <?=$unCheckedout['office_name']?> </span>
                     <div class="form-group" style="height:65px;">
                         <label class="col-sm-3 control-label"><?=($i == $size-1)? 'Latest':'Previous'?> Check-in Time</label>
                        <div class="col-sm-9">
                            <div class="input-group bootstrap-timepicker ">
                                <input type="text" placeholder="" value="<?=date("d/m/Y H:i:s", $unCheckedout['check_in_timestamp'])?>"  data-date="" data-date-format="" data-link-field="dtp_input3" data-link-format="" class="form-control input-small " readonly>
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            </div>
                            <small class="text-danger"></small>
                        </div>
                    </div>
                     <?php
                        if($i == $size-1)
                        { 
                            break;
                        }
                     ?>
					 <hr>
                    <div class="form-group" style="height:65px">
                         <label class="col-sm-3 col-sm-3 control-label">Previous Check-out Time</label>
                        <div class="col-sm-9">
                            <div class="input-group bootstrap-timepicker ">
                                <input type="text" name="checkout[]" placeholder="DD/MM/YYYY HH:MM" value="<?=@$values['checkout'][$i]?>"  data-date="" data-date-format="dd/mm/yyyy h:i" data-link-field="dtp_input1" data-link-format="" class="form-control input-small timepickerone" readonly>
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            </div>
                            <small class="text-danger"><?=@$error['checkout_error'][$i]?></small>
                        </div>
                    </div><hr>
            <?php endforeach?>
                        <input type="hidden" value="<?=@$csrf?>" name="csrf">
<!--                     <div class="form-group">
                         <label class="col-sm-3 col-sm-3 control-label">Check-out Time</label>
                        <div class="col-sm-9">
                            <div class="input-group bootstrap-timepicker ">
                                <input id="timepickerone" type="text" name="checkout" placeholder="DD/MM/YYYY HH:MM" value="<?=@$values['checkout']?>"  data-date="" data-date-format="dd/mm/yyyy h:i" data-link-field="dtp_input1" data-link-format="" class="form-control input-small " readonly>
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            </div>
                            <small class="text-danger"><?=@$error['checkout_error']?></small>
                            <input type="hidden" value="<?=@$csrf?>" name="csrf">
                        </div>
                    </div>-->
                     
                     
                    <div class="form-group" style="margin-left: 16px; margin-right: 16px;">
                        <label class="col-sm-3 col-sm-3 control-label pull-left"></label>
                        <input  name="activate" type="submit" value="submit" class="btn btn-primary btn-lg btn-block" style="background:#071d24;border:none;">
                    </div>
                </form>
                    </div>
                    
                    <!--api user data form end here-->
            </div>            
        </div>
        </div>
      </section>

<script type="text/javascript">
    
    $(document).ready(function(){
        $('.timepickerone').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1,
		
		minuteStep: 1
    });
	
    });
</script>

<?php
}
?>