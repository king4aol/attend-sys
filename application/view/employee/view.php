<?php 
    if(!isset($_POST['submit']))
    {
?>
<section class="wrapper">
     <!-- top menue bar start -->
         <div class="row topbar" > 
          <span style="line-height: 44px;padding-left: 17px;"> </span> 
          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;">View Employees</span>
         </div>
     <!-- top menue bar end -->  
     
            <div class="row">
               <dvi class="col-lg-12">
                 <div class="col-lg-2">  </div>
                 <div class="col-lg-8 centered select_admin_page_hading"> <h3>Select a Company to View Employees</h3></div>
                 <div class="col-lg-2"> </div>
            </div>
            <div class="row">
               <div class="col-lg-12" >
                 <div class="col-lg-2">  </div>
                   <div class="col-lg-8 centered select_admin_page_hading_dropdown">
                     
                        <div class="col-lg-2"> </div>
                        <form class="form-horizontal style-form" autocomplete="off" action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>employee/view" method="POST">
                            <div class="form-group">
                                <div class="col-sm-8 <?php echo(isset($error['admin_error']))?"has-error":""?>">
                                    <div class="<?php echo(isset($error['admin_error']))?"alert alert-danger":""?>">
                                        <?=@$error['admin_error']?>
                                    </div>
                                    <input type="text" name="admin_name" class="form-control" id="admin_name"/>
                                    <input type="hidden" name="admin" class="form-control" id="admin"/>
                                    <div id="predict" class="preres" style="display: none;">
                                        <ul></ul>
                                    </div>
                                    <script type="text/javascript">
                                    $(document).ready(function(){
                                        $("#admin_name").on("keyup click", function(){
                                            var request = "action=select&type=companies&source=admin&input=" + $(this).val();
                                            console.log(request);
                                            $.ajax({
                                            type: "POST",
                                            url: "<?php echo $this->config['domain'].'/'._PUBLIC_PATH_.'/admin/ajax';?>",
                                            data: request,
                                            cache: "FALSE",
                                            success: function(response){
                                                console.log(response);
                                                var str = '';
                                                try
                                                {
                                                    var data = JSON.parse(response);
                                                    //console.log(data.response+" => ");
                                                    $.each(data.response, function(k, v){
                                                        console.log(k + " => "+v);
                                                        str += "<li data-val='"+v['admin_id']+"'>"+v['first_name']+" "+v['last_name']+" ("+v['username']+")</li>"
                                                    });
                                                }
                                                catch(err)
                                                {
                                                    $("#predict").css("display", "none");
                                                    console.log(err.message);
                                                }
                                                $("#predict ul").html(str);
                                                $("#predict").css("display", "block");
                                            },
                                            error: function(){
                                                alert("Something Went Wrong. Please Try Again");
                                            }
                                            });
                                        });
                                        $("#predict ul").on("click", "li", function(){
                                            $("#admin").val($(this).attr("data-val"));
                                            $("#admin_name").val($(this).text());
                                            $("#predict").css("display", "none");
                                        });
                                    });
                                </script>
                                </div>
                            </div>

                            <div class="form-panel"  style="box-shadow:none;">
                                <div class="col-lg-2"> </div>

                               <div class="form-group">
                                 <div class="col-sm-8 centered form_selectadmin_submit_button" >
                                    <input  type="submit" name="submit" value="Next"> 
                                  </div>     
                                 </div>
                            </div>
                        </form> 
                        </div>
                       </div>
                 <div class="col-lg-2"> </div>
    </section>
<?php 
}
else
{
   ?>
<div class="container">
           <div class="col-lg-4"><h2 style="border-left:5px solid #333;"> Employees </h2> </div>
           <div class="col-lg-4"> </div>             
         </div>
          <div class="container" id="employees">
            <div class="col-lg-12">
                <?php 
                if(isset($_POST['success']))
                {
                ?>
                <div class="col-lg-8 pull-left alert alert-success"><?=$_POST['success']?></div>
                  <script>
                      window.history.pushState("", "", "../view");
                  </script>
                <?php
                }
                ?>
               
                <div class="col-lg-4 pull-right" style="margin-right:-146px">
                  <form class="navbar-form" role="search" method="POST">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search" name="query" value="<?=@$query?>">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit" id="search"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                    </div>
                </form>
             </div>        
          </div>
<div class="container">  <div class="col-lg-8 pull-left alert alert-danger" id="error" style="display:none"> </div>
                    
                </div> 
    <table class="table">
    <thead class="otl_list_employes_color">
      <tr>
        <th>SN#</th>
        <th>Employee Name  </th>
        <th>Employee Pin</th>
        <th>Status</th>
        <th>Options</th>
        
     </tr>
    </thead>
    <tbody style="background:#ccc">
         <?php
            $i = 1;
             foreach ($employees as $employee)
             {
        ?> 
        <tr>
            <td class="border_color"><?=$offset + $i++?></td>
            <td class="border_color"><?=$employee['emp_first_name']." ".$employee['emp_last_name'];?></td>
            <td class="border_color"><?=$employee['pin'];?></td>
            <td style="border-bottom:1px solid #fff;">
                <div class="btn-group">
                <?php
                    if($employee['status'] == _ACTIVE_)
                    {
                        echo '<a class="btn btn-xs btn-success" href="#">Activated</a>';
                    }
                    else
                    {
                        echo '<a class="btn btn-xs btn-danger" href="#">Deactivated</a>';
                    }
                ?>
                </div>
            </td>
            <td class="border_color">
                <div class="bs-example">
                    <a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>employee/details/<?=$employee['emp_id']?>"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/details.png" class="img-rounded" title="Details" alt="Details"></a>
                    <a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>attendance/view/<?=$employee['emp_id']?>"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/view_at.png" class="img-rounded" title="View Attendance" alt="Attendance"></a>
                    <a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>employee/edit/<?=$employee['emp_id'];?>"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/edit_at.png" class="img-rounded" title="Edit" alt="Edit"></a>
                    <a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>employee/action/<?=$employee['emp_id']; if($employee['status'] == _ACTIVE_){echo '/deactivate"><img src="'.$this->config['domain'].'/'._PUBLIC_PATH_.'/assets/img/not.png" class="img-rounded" alt="Deactivate" title="Deactivate">';}else{echo '/activate"><img src="'.$this->config['domain'].'/'._PUBLIC_PATH_.'/assets/img/deactive_at.png" class="img-rounded" alt="Activate" title="Activate">';}?></a>
                   <a href="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>employee/action/<?=$employee['emp_id'];?>/delete"><img src="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>assets/img/delete_at.png" class="img-rounded" alt="Delete" title="Delete"></a>
                </div>
            </td>
        </tr> 
             <?php } ?>
    </tbody>
    </table>   
<!--pagination start -->    
    <div class="row">
        <ul class="pagination pull-right" style="margin-top:-15px;margin-right: -16px">
            <li><a href="" id="first_page" style="background:#071d24;border:#071d24;color:#fff"><<</a></li>
            <li><a href="" id="previous_page" style="background:#071d24;border:#071d24;color:#fff"><</a></li>
            <li><a style="padding:1px !important;background:#071d24;"><input id="current_page" type="text" data-current="<?=$current?>" data-total="<?php echo(ceil($total/$limit))?ceil($total/$limit):"1"?>" data-limit="<?=$limit?>" value="<?=$current?>" style="color:#000;width:30px;height:27px;border:none;text-align:center"></a></li>
            <li><a style="padding:1px !important;height: 32px;width: 46px;text-align: center;line-height: 30px;text-decoration: none;background: none;border: none;text-align:left;color:#000"><span style="text-align:left"> &nbsp;of</span> &nbsp; <?php echo(ceil($total/$limit))?ceil($total/$limit):"1"?></a></li>
            <li><a href="" id="next_page" style="background:#071d24;border:#071d24;color:#fff">></a></li>
            <li><a href="" id="last_page" style="background:#071d24;border:#071d24;color:#fff;margin-right:10px;">>></a></li>
        </ul>
        <script type="text/javascript">
            $("#current_page").on("keyup", function(e){
                if($(this).val() != "")
                {
                    var input = parseInt($(this).val());
                    var current = $("#current_page").attr("data-current");
                    var total = $("#current_page").attr("data-total");
                    var limit = $("#current_page").attr("data-limit");
                    if(!isNaN(input))
                    {
                        if(input > total)
                        {
                            input = current;
                        }
                        else if(input <= 0)
                        {
                            input = current;
                        }
                    }
                    else 
                    {
                        input = current;
                    }
                    $(this).val(input);
                    
                    if(e.which == 13)
                    {
                        window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."employee/view/load/"?>"+limit+"/"+(input*limit-limit);
                    }
                }
            });
            $("#first_page, #previous_page, #next_page, #last_page").click(function(e){
                e.preventDefault();
                console.log($(this).attr("id"));
                if($(this).attr("id") == "first_page")
                {
                    window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."employee/view/load/{$limit}/0"?>";
                }
                else if($(this).attr("id") == "last_page")
                {
                    window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."employee/view/load/{$limit}/".($limit * ceil($total/$limit) - $limit)?>";
                }
                else if($(this).attr("id") == "previous_page")
                {
                    <?php 
                        if($offset - $limit <= 0)
                        {
                            echo "var previous = 0;";
                        }
                        else 
                        {
                            echo "var previous = ".($offset-$limit).";";
                        }
                    ?>
                    window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."employee/view/load/{$limit}/";?>" + previous;
                }
                else if($(this).attr("id") == "next_page")
                {
                    if($("#current_page").val() == $("#current_page").attr("data-current"))
                    {
                        <?php
                            if(ceil($total/$limit) == ceil(($offset+$limit)/$limit))
                            {
                                echo "var next = $offset;";
                            }
                            elseif(ceil($total/$limit) == 0 || ceil($total/$limit) == 1)
                            {
                                echo "var next = 0;";
                            }
                            else 
                            {
                                echo "var next = ".($offset+$limit).";";
                            }
                        ?>
                    window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."employee/view/load/{$limit}/";?>"+next;
                    }
                    else 
                    {
                        var input = $("#current_page").val();
                        var total = $("#current_page").attr("data-total");
                        var limit = $("#current_page").attr("data-limit");
                        window.location = "<?=$this->config['domain'].""._PUBLIC_PATH_."employee/view/load/"?>"+limit+"/"+(input*limit-limit);
                    }
                }
            });
        </script>       
    </div>
       <!--pagination end -->     
</div>
<?php } ?>
<script type="text/javascript">
    $(document).on('click', '#search', function(e){
        e.preventDefault();
        var request = $(this).closest("form").serialize()+"&action=search&type=employee&source=admin";
        console.log(request);
        $.ajax({
            type: "POST",
            url: "<?php echo $this->config['domain'].'/'._PUBLIC_PATH_.'/employee/ajax';?>",
            data: request,
            cache: "FALSE",
            success: function(response){
                console.log(response);
                var data = JSON.parse(response);
                if(response.length > 0)
                {
                    if(!data.error)
                    {
                        $("#error").hide();
                        var content = $("#employees",data.response)
                        $("#employees").html(content);
                    } 
                    else 
                    {
                        $("#error").show();
                        $("#error").text(data.response);
                    }
                }
            },
            error: function(){
                alert("Something Went Wrong. Please Try Again");
            }
        });
    });
    </script>