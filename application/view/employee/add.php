<?php 

    if(!isset($_POST['submit']))

    {

?>

<section class="wrapper">

     <!-- top menue bar start -->

         <div class="row topbar" > 

          <span style="line-height: 44px;padding-left: 17px;"> </span> 

          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;">Add Employee</span>

         </div>

     <!-- top menue bar end -->  

     

            <div class="row">

               <dvi class="col-lg-12">

                 <div class="col-lg-2">  </div>

                 <div class="col-lg-8 centered select_admin_page_hading"> <h1>Select Company to Add Employee </h1></div>

                 <div class="col-lg-2"> </div>

            </div>

            <div class="row">

               <div class="col-lg-12" >

                 <div class="col-lg-2">  </div>

                   <div class="col-lg-8 centered select_admin_page_hading_dropdown">

                     

                        <div class="col-lg-2"> </div>

                        <form class="form-horizontal style-form" autocomplete="off" action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>employee/add" method="POST">

                            <div class="form-group">

                                <div class="col-sm-8 <?php echo(isset($error['admin_error']))?"has-error":""?>">

                                    <div class="<?php echo(isset($error['admin_error']))?"alert alert-danger":""?>">

                                        <?=@$error['admin_error']?>

                                    </div>

                                    <input type="text" name="admin_name" class="form-control" id="admin_name"/>

                                    <input type="hidden" name="admin" class="form-control" id="admin"/>

                                    <div id="predict" class="preres" style="display: none;">

                                        <ul></ul>

                                    </div>

                                    <script type="text/javascript">

                                    $(document).ready(function(){

                                        $("#admin_name").on("keyup click", function(){

                                            var request = "action=select&type=companies&source=admin&input=" + $(this).val();

                                            console.log(request);

                                            $.ajax({

                                            type: "POST",

                                            url: "<?php echo $this->config['domain'].'/'._PUBLIC_PATH_.'/admin/ajax';?>",

                                            data: request,

                                            cache: "FALSE",

                                            success: function(response){

                                                console.log(response);

                                                var str = '';

                                                try

                                                {

                                                    var data = JSON.parse(response);

                                                    

                                                    $.each(data.response, function(k, v){

                                                        console.log(k + " => "+v);

                                                        str += "<li data-val='"+v['admin_id']+"'>"+v['first_name']+" "+v['last_name']+" ("+v['username']+")</li>"

                                                    });

                                                }

                                                catch(err)

                                                {

                                                    $("#predict").css("display", "none");

                                                    console.log(err.message);

                                                }

                                                $("#predict ul").html(str);

                                                $("#predict").css("display", "block");

                                            },

                                            error: function(){

                                                alert("Something Went Wrong. Please Try Again");

                                            }

                                            });

                                        });

                                        $("#predict ul").on("click", "li", function(){

                                            $("#admin").val($(this).attr("data-val"));

                                            $("#admin_name").val($(this).text());

                                            $("#predict").css("display", "none");

                                        });

                                    });

                                </script>

                                </div>

                            </div>



                            <div class="form-panel"  style="box-shadow:none;">

                                <div class="col-lg-2"> </div>



                               <div class="form-group">

                                 <div class="col-sm-8 centered form_selectadmin_submit_button" >

                                     <input  type="submit" name="submit" value="Next"> 

                                  </div>     

                                 </div>

                            </div>

                        </form> 

                        </div>

                       </div>

                 <div class="col-lg-2"> </div>

      </section>

    <?php }

        elseif(!isset($_POST['employee']) && isset($_POST['submit']))

        {

    ?>

<section class="wrapper">

     <!-- top menue bar start -->

         <div class="row topbar" > 

          <span style="line-height: 44px;padding-left: 17px;"> </span> 

          <span style="border-left: 4px solid #333;padding-left: 5px;font-size: 16px;"> Add Employee </span>

         </div>

     <!-- top menue bar end --> 

            <div class="row">

             <div class="container">

             <div class="col-lg-6"> 

             <div class="form-panel">

                 <div class="<?php echo (!isset($error['csrf_error'])) ? '':'alert alert-danger' ?>">

                     <?=@$error['csrf_error']?>

                 </div>

                 <div class="<?php echo (!isset($error['emp_limit'])) ? '':'alert alert-danger' ?>">

                     <?=@$error['emp_limit']?>

                 </div>

                 <div class="<?php echo (!isset($error['error'])) ? '':'alert alert-danger' ?>">

                     <?=@$error['error']?>

                 </div>

                 <div class="<?php echo (!isset($res['response'])) ? '':'alert alert-success' ?>">

                     <?=@$res['response']?>

                 </div>

                 <form class="form-horizontal style-form" method="POST" action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>employee/add">

                          <div class="form-group">

                              <label class="col-sm-4 col-sm-4 control-label">First Name*</label>

                              <div class="col-sm-8 <?php echo (!isset($error['firstname_error'])) ? '':'has-error' ?>">

                                  <input type="text" name="firstname" class="form-control" value="<?=@$values['firstname']?>" placeholder="First Name">

                                  <small class="text-danger"><?=@$error['firstname_error']?></small>

                              </div>

                          </div>

                          <div class="form-group">

                              <label class="col-sm-4 col-sm-4 control-label">Last Name*</label>

                              <div class="col-sm-8 <?php echo (!isset($error['firstname_error'])) ? '':'has-error' ?>">

                                  <input type="text" class="form-control" placeholder="Last Name" name="lastname" value="<?=@$values['lastname']?>" placeholder="Lastname"/>

                                  <small class="text-danger"><?=@$error['lastname_error']?></small>

                              </div>

                          </div>

                          <div class="form-group">

                            <label class="col-sm-4 col-sm-4 control-label">Shift Start*</label>

                              <div class="col-sm-8 <?php echo (!isset($error['timing_from_error'])) ? '':'has-error' ?>">

                                <div class="input-group bootstrap-timepicker timepicker">

                                    <input id="timepickerone" data-date="" data-date-format="hh:ii" data-link-field="dtp_input3" data-link-format="hh:ii" id="test" type="text" name="timing_from" placeholder="Timing From" value="<?php if(isset($values['timing_from'])){ echo $values['timing_from'];}?>" class="form-control input-small " readonly>

                                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>

                                </div>

                                  <small class="text-danger"><?=@$error['timing_from_error']?></small>

                              </div>

                          </div>

                          <div class="form-group">

                               <label class="col-sm-4 col-sm-4 control-label">Shift End*</label>

                              <div class="col-sm-8 <?php echo (!isset($error['timing_to_error'])) ? '':'has-error' ?>">

                                  <div class="input-group bootstrap-timepicker timepicker">

                                    <input data-date="" data-date-format="hh:ii" data-link-field="dtp_input3" data-link-format="hh:ii" id="timepickertwo" class="form-control mytimepicker" name="timing_to" type="text" placeholder="Timing To" value="<?php if(isset($values['timing_to'])){ echo $values['timing_to'];}?>" readonly>

                                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>

                                    </div>

                                  <small class="text-danger"><?=@$error['timing_to_error']?></small>

                              </div>

                          </div>

                          <div class="form-group">

                               <label class="col-sm-4 col-sm-4 control-label">Per Hour Rate*</label>

                              <div class="col-sm-8 <?php echo (!isset($error['pay_rate_error'])) ? '':'has-error' ?>">

                                  <input class="form-control" name="pay_rate" value="<?=@$values['pay_rate']?>" placeholder="Pay Rate Per Hour" type="text">

                                  <small class="text-danger"><?=@$error['pay_rate_error']?></small>

                              </div>

                          </div>

<!--                          <div class="form-group">

                               <label class="col-sm-4 col-sm-4 control-label">OverTime Per Hour Rate*</label>

                              <div class="col-sm-8 <?php //echo (!isset($error['overtime_pay_rate_error'])) ? '':'has-error' ?>">

                                  <input type="text" class="form-control" name="overtime_pay_rate" placeholder="Overtime Pay Rate Per Hour" value="<?php//@$values['overtime_pay_rate']?>" placeholder="placeholder">

                                  <small class="text-danger"><?php//@$error['overtime_pay_rate_error']?></small>

                              </div>

                          </div>-->

                          <div class="form-group">

                               <label class="col-sm-4 col-sm-4 control-label">Employee Grace Time</label>

                              <div class="col-sm-8 <?php echo (!isset($error['emp_grace_time_error'])) ? '':'has-error' ?>">

                                  <input type="text" class="form-control" name="emp_grace_time" value="<?=@$values['emp_grace_time']?>" placeholder="Grace Time In Mintues (Global Grace Time: <?=$globalGracetime?>)">

                                  <small class="text-danger"><?=@$error['emp_grace_time_error']?></small>

                              </div>

                          </div>

                          <div class="form-group">

                               <label class="col-sm-4 col-sm-4 control-label">Break Duration (Min)</label>

                              <div class="col-sm-8 <?php echo (!isset($error['break_time_error'])) ? '':'has-error' ?>">

							  <div class="col-sm-3" style="margin-left:-15px;text-align: center;">

                                                              <input type="text" id="break_time" style="text-align:center;" class="form-control" placeholder="Break Duration In Mintues" name="break_time" value="<?=@$values['emp_break_time']?>" readonly="readonly">

                             </div>

							 <div class="col-sm-8" style="margin-top: 10px;width: 242px;float:right;">

								<div id="slider" style="background:#071d24"></div>

                                  <small class="text-danger"><?=@$error['break_time_error']?></small>

                              </div>

                          </div></div>

                          

                          <div class="form-group">

                              <label class="col-sm-4 col-sm-4 control-label">Email</label>

                             <div class="col-sm-8 <?php echo (!isset($error['email_error'])) ? '':'has-error' ?>">

                                 <input type="email" class="form-control"  value="<?=@$values['email']?>" name="email" placeholder="Email Address">

                                 <small class="text-danger"><?=@$error['email_error']?></small>

                              </div>

                          </div>

						  <!--

                            <div class="form-group">

                              <label class="col-sm-4 col-sm-4 control-label">Department*</label>

                              <div class="col-sm-8 <?php echo(!isset($error['dept_error'])) ? '':'has-error'?>">

                                  <select name="dept" class="form-control">

                                      <option value="">Select Department</option>

                                <?php foreach ($departments as $value) {?>

                                        <option value="<?=$value['dep_id']?>" <?php if($value['dep_id'] == @$values['dept']) echo 'selected';?>><?=$value['dep_name']?></option>

                                <?php }?>

                                    </select>

                                <small class="text-danger"><?=@$error['dept_error']?></small>

                              </div>

                          </div>

                            <div class="form-group">

                                <label class="col-sm-4 col-sm-4 control-label">Employee Type*</label>

                                <div class="col-sm-8 <?php echo(!isset($error['type_error'])) ? '':'has-error'?>">

                                    <select name="type" class="form-control">

                                        <option value="">Select Type</option>

                                  <?php foreach ($types as $value) {?>

                                          <option value="<?=$value['type_id']?>" <?php if($value['type_id'] == @$values['type']) echo 'selected';?>><?=$value['type_title']?></option>

                                  <?php }?>

                                      </select>

                                  <small class="text-danger"><?=@$error['type_error']?></small>

                                </div>

                            </div>

							-->

                          <div class="form-group">

                          <label class="col-sm-4 col-sm-4 control-label"></label>

                              <div class="col-sm-8">

                                  <input type="hidden" name="csrf" value="<?=@$csrf?>"/>

                                  <input class="btn btn-success btn-sm pull-right" type="submit" name="employee" value="Add Employee"/>

                               </div>

                          </div>

                      </form>

                  </div>                  

                  </div>

                  <!-- status portation-->

                 





                  

                  <!-- status portation end-->

                 

                  </div>

          </div>

<script type="text/javascript">

    

    $(document).ready(function(){
        var obj = new Date();
        var today = obj.getFullYear()+'-';
        today += obj.getMonth()+1;
        today += '-'+obj.getDate();
        var end = "23:59";
        
        $('#timepickerone').datetimepicker({

        startDate: today,
        endDate: today+' '+end,

        weekStart: 1,

        todayBtn:  1,

		autoclose: 1,

		todayHighlight: 1,

		startView: 1,

		minView: 0,

		maxView: 1,

		forceParse: 0,
                minuteStep: 1

        });

	$('#timepickertwo').datetimepicker({

        startDate: today,
        endDate: today+' '+end,

        weekStart: 1,

        todayBtn:  1,

		autoclose: 1,

		todayHighlight: 1,

		startView: 1,

		minView: 0,

		maxView: 1,

		forceParse: 0,
                minuteStep: 1

        });

        var iValue = <?php echo(isset($values['emp_break_time']))?$values['emp_break_time']:'0'?>;

        $("#break_time").val(iValue); 

        $("#slider").slider(

        {

            min:0,

            max:60,

            value:iValue,

            slide:function(event,ui) 

            {

              $("#break_time").val(ui.value);

            }

       });

    });





</script>



       <?php }

    ?>

