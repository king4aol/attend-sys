<?php
if(!isset($_POST['adminsubmit']) && !isset($_POST['submit']))
{
    ?>
<form action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>settings/gracetime" method="POST" style="height:300px; ">
            <div align="center" >
                <h2 align="center">View employees</h2>
                <div class="error-text"><?=@$error['csrf_error']?></div>
                <table>
               <tr>
                   <td><label><span>Admin</span></label></td>
                   <td>
                       <select name="admin">
                           <option>Select Admin</option>
                           <?php foreach ($admins as $admin) { ?>
                           <option value="<?=$admin['admin_id']?>" <?php if($admin['admin_id'] == @$values['admin']) echo 'selected';?>><?=$admin['first_name']." ".$admin['last_name']?> (<?=$admin['username']?>)</option>
                           <?php } ?>
                       </select>
                       <span class="error"><?=@$error['admin_error']?></span>
                   </td>
               </tr>
               <tr>
                <td>
                    <input type="hidden" name="csrf" value="<?=$csrf?>"/>
                    <input type="submit" name="adminsubmit" value="submit" />
                </td>
               </tr>
                </table>
            </div>
</form>
<?php }else if(isset($_POST['adminsubmit'])){ ?>
<form action="<?=$this->config['domain']?><?=_PUBLIC_PATH_?>settings/gracetime" method="POST" style="height:300px; ">
    <div align="center">
                <h2 align="center">Add Grace Time</h2>
                <div class="error-text"><?=@$error['csrf_error']?>
                </div>
        <table>
            <tr>
                <td>
                    <label><span>Add Grace Time</span></label>
                </td>
            </tr>
            <tr>
                <td>
                <input type="text" placeholder="please enter grace time in minutes" name="grace_time" value="<?=@$values['grace_time']?>"/>
                <span class="error"><?=@$error['grace_time_error']?></span>
                </td>
                <td>
                <input type="hidden" name="csrf" value="<?=$csrf?>"/>
                <input type="submit" name="submit" value="submit"/>
                </td>
            </tr>
        </table>
    </div>
</form>
<?php } ?>