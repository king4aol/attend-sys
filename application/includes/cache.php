<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of cache
 *
 * @author Msajid
 */
class cache {
    //Cache Files Directory
    private static $dir = "../application/cached/";
    //Cache Expires
    private static $expire = 60;
    //File Name
    private static $filename;
    //File Extension
    private static $fileext = "json";
    //File Path
    private static $filePath;
    
    //Save Cache
    public static function saveCache($object = '', $filename = '', $type = '', $expire = '')
    {
        if(!empty($expire))
        {
            self::$expire = $expire;
        }
        self::$filename = trim($filename, "()")."(".time() + self::$expire.")";
        self::$filePath = self::$dir.self::$filename.".".self::$fileext;
        
        $fileHandle = fopen(self::$filePath, "w");
        if($type == "database")
        {
            $obj = $object;
            $object = [];
            while($row = $obj->fetch_assoc())
            {
                $object[] = $row;
            }
        }
        
//        $serializedObject = serialize($object);
//        fwrite($fileHandle, $serializedObject);
//        fclose($fileHandle);
        
        return $object;
    }
    
    //Read Cache
    public static function readCache($filename = '') 
    {
			return false;
        self::$filename = $filename."(*)";
        self::$filePath = self::$dir.self::$filename.".".self::$fileext;
        
        if(!self::isExpired())
        {
            $fileHandle = fopen(self::$filePath, "r");
            $SerializedObject = fread($fileHandle, filesize(self::$filePath));
            fclose($fileHandle);
            
            return @unserialize($SerializedObject);
        }
        return false;
    }
    
    //Check if file expired or not
    private static function isExpired()
    {
        $files = glob(self::$filePath);
        if(is_array($files) && isset($files[0]))
        {
            $file = $files[0];
            $startP = strpos($file, "(");
            $endP = strpos($file, ")");
            $timestamp = substr($file, $startP, $endP);
            if($timestamp > time())
            {
                self::$filename = $file;
                self::$filePath = self::$dir.self::$filename;
                return FALSE;
            }
            else
            {
                self::$filename = $file;
                self::$filePath = self::$dir.self::$filename;
                unlink(self::$filePath);
                return TRUE;
            }
        }
        /*
        if(file_exists(self::$filePath))
        {
            $fileMadeTime = filemtime(self::$filePath);
            $timeEsclaped = time() - $fileMadeTime;
            if($timeEsclaped > self::$expire)
            {
                return true;
            }
            return false;
        }*/
        return true;
    }
    
    
}
