<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of session
 *
 * @author Msajid
 */
class session {
    static private $adminId = "admin_id";
    static private $userId = "user_id";
    static private $name = "name";
    static private $bitMask = "bit_mask";
    static private $type = "admin_type";


    //Start Session
    static public function sessionStart() 
    {
        if(function_exists("version_compare"))
        {
            if(function_exists("phpversion"))
            {
                if(version_compare(phpversion(), "5.4.0", ">="))
                {
                    if(function_exists("session_status"))
                    {
                        if(session_status() == PHP_SESSION_NONE)
                        {
                            session_start();
                            return;
                        }
                    }
                    else if(!isset($_SESSION))
                    {
                        session_start();
                        return;
                    }
                }
            }
        }
        else if(!isset($_SESSION))
        {
            session_start();
            return;
        }
    }
    //Destroy Session
    static public function sessionDestroy() 
    {
        self::sessionStart();
        session_destroy();
        return;
    }
    //Set Session For Web Services
    static public function setSession($sessionId) 
    {
        if(self::validateSession($sessionId))
        {
            session_id($sessionId);
            self::sessionStart();
            return TRUE;
        }
        return FALSE;
    }
    //Validate Session Key
    static public function validateSession($sessionId)
    {
        return preg_match('/^[-,a-zA-Z0-9]{1,128}$/', $sessionId) > 0;
    }
    //Get Current Session ID
    static public function getSession() 
    {
        self::sessionStart();
        return session_id();
    }
    //Login Admin
    static public function loginUser($admin_id = '', $user_id = '', $name = '', $type = '', $bitMask = 0) 
    {
        global $configuration;
        self::sessionStart();
        $_SESSION[$configuration['admin_session_prefix'].self::$adminId] = $admin_id;
        $_SESSION[$configuration['admin_session_prefix'].self::$userId] = $user_id;
        $_SESSION[$configuration['admin_session_prefix'].self::$name] = $name;
        $_SESSION[$configuration['admin_session_prefix'].self::$type] = $type;
        $_SESSION[$configuration['admin_session_prefix'].self::$bitMask] = $bitMask;
    }
    //Check if user is logged in
    static public function isLogin()
    {
        global $configuration;
        
        self::sessionStart();
        if(isset($_SESSION[$configuration['admin_session_prefix'].self::$adminId]) && isset($_SESSION[$configuration['admin_session_prefix'].self::$userId]) && isset($_SESSION[$configuration['admin_session_prefix'].self::$name]))
        {
            return TRUE;
        }
        return FALSE;
    }
    //Get Logged User Type
    static public function getUserType()
    {
        global $configuration;
        if(isset($_SESSION[$configuration['admin_session_prefix'].self::$type]))
        {
            if($_SESSION[$configuration['admin_session_prefix'].self::$type] == _ADMIN_)
            {
                return _ADMIN_;
            }
            else if($_SESSION[$configuration['admin_session_prefix'].self::$type])
            {
                return _CLIENT_;
            }
            return FALSE;
        }
        return FALSE;
    }
    //Get Bit Mask
    static public function getBitMask() 
    {
        global $configuration;
        self::sessionStart();
        if(isset($_SESSION[$configuration['admin_session_prefix'].self::$bitMask]))
        {
            return $_SESSION[$configuration['admin_session_prefix'].self::$bitMask];
        }
        return FALSE;
    }
    //get login admin id
    
    static public function getId()
    {
        global $configuration;
        self::sessionStart();
        if(isset($_SESSION[$configuration['admin_session_prefix'].self::$adminId]))
        {
            return $_SESSION[$configuration['admin_session_prefix'].self::$adminId];
        }
        return false;
    }
    
    //Get User ID
    static public function getUserId()
    {
        global $configuration;
        self::sessionStart();
        if(isset($_SESSION[$configuration['admin_session_prefix'].self::$userId]))
        {
            return $_SESSION[$configuration['admin_session_prefix'].self::$userId];
        }
        return false;
    }
}
