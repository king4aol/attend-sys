<?php

$configuration = array(
    //Developer Mode
    "debug" => true,
    //error log
    "error_log" => true, 
    //Page Render Time
    "render_time" => "30",
    //Admin Session Prefix
    "admin_session_prefix" => "otl_admin_",
    //Domain
    "domain" => "http://otl.prosoftsol.com/admin",
    //Title
    "title" => "Odyssy Time Logic",
    //Charset
    "charset" => "utf-8",
    //timezone
    "timezone" => "Australia/Sydney",
    //Database Information
    "db_host" => "localhost",
    "db_user" => "newotl",
    "db_password" => "TekLabsOtl1235",
    "db_name" => "otlnew",
    "db_table_prefix" => "otl_"
);

//Defining Constants

define("_ADMIN_", "1");
define("_CLIENT_","2");
define("_EMPLOYEE_", "3");
define("_OFFICE_", "4");

define("_DEACTIVE_", "0");
define("_ACTIVE_", "1");
define("_DELETED_", "2");
define("_REQUEST_", "3");
define("_DEFAULT_STATUS_", _ACTIVE_);

define("_CONTROLLER_PATH_", "../application/controller");
define("_MODEL_PATH_", "../application/model");
define("_VIEW_PATH_", "../application/view");
define("_TEMPLATE_PATH_","../application/template");
define("_INCLUDES_PATH_","../application/includes");
define("_LIBRARIES_PATH_", "../application/libs");

//Paths with respect to base directory
define("_APPLICATION_PATH", "application");
define("_PUBLIC_PATH_","");
