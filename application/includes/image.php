<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of image
 *
 * @author PSSLT2540p
 */
class image {
    static private $cryptoStr;
    static private $image;
    static private $imageName;
    static private $imageExt;
    static private $dir = "../shared/photos/";
    static private $imagePrefix = "OTL_";

    //put your code here
    //Decode From Base64
    static private function decode($cryptoType = "base64")
    {
        if($cryptoType == "base64")
        {
            return base64_decode(self::$cryptoStr);
        }
        return FALSE;
    }
    //Validate Image
    static private function validateImg()
    {
        self::$image = self::decode();//imagecreatefromstring(self::decode());
        
        if(!self::$image)
        {
            return FALSE;
        }
        return TRUE;
    }
    //Save Image
    static private function save()
    {
        if(!file_exists(self::$dir))
        {
            mkdir(self::$dir, 0777  , TRUE);
        }
        
        reGenrate:
            
        self::$imageName = self::$imagePrefix."image".time().  rand(0, 1000).self::$imageExt;
        
        if(file_exists(self::$dir.self::$imageName))
        {
            goto reGenrate;
        }
        if(!file_put_contents(self::$dir.self::$imageName, self::$image))
        {
            return FALSE;
        }
        return TRUE;
    }
    //Set Extension
    static private function setExtension() 
    {
        //$handle = finfo_open();
        $file = new finfo(FILEINFO_MIME_TYPE);
        $mime = $file->buffer(self::$image);
        //$mime = finfo_buffer($handle, self::$image, FILEINFO_MIME_TYPE);
        self::$imageExt = self::FileExt($mime);
        return self::$imageExt;
    }
    //Get Extension From Mime Type
    static private function FileExt($contentType)
    {
        $map = array(
            'application/pdf'   => '.pdf',
            'application/zip'   => '.zip',
            'image/gif'         => '.gif',
            'image/jpeg'        => '.jpg',
            'image/png'         => '.png',
            'text/plain'        => '.txt'
        );
        if (isset($map[$contentType]))
        {
            return $map[$contentType];
        }

        $pieces = explode('/', $contentType);
        return '.' . array_pop($pieces);
    }
    //Get Image
    static public function img($string) 
    {
        self::$cryptoStr = str_replace(" ", "+", $string);
        if(self::validateImg())
        {
            if(self::setExtension() != ".txt")
            {
                if(self::save())
                {
                    return self::$imageName;
                }
                return FALSE;
            } 
            else 
            {
                return "nopreview.png";
            }
        }
        return FALSE;
    }
}