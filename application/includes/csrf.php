<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of csrf
 *
 * @author Msajid
 */
class csrf {
    static private $csrfToken;
    static private $length = 32;
    static private $sessionName = "admin_csrf_";
    static private $expiry = 600;
    
    static public function generateCsrf($name = '', $expire = 0) 
    {
        if(function_exists("random_bytes") && function_exists("bin2hex"))
        {
            self::$csrfToken = md5(bin2hex(random_bytes(self::$length)));
        }
        else if(function_exists("openssl_random_pseudo_bytes"))
        {
            self::$csrfToken = md5(openssl_random_pseudo_bytes(self::$length));
        }
        else
        {
            self::$csrfToken = md5(time()."CSRFTOKEN");
        }
        
        session::sessionStart();
        
        $_SESSION[self::$sessionName.$name] = self::$csrfToken;
        $_SESSION[self::$sessionName."{$name}_created"] = time() + $expire;
        return self::$csrfToken;
    }
    //validate
    static public function validateCsrf($token = '', $name = '', $expire = false) 
    {return TRUE;
        session::sessionStart();
        
        if(isset($_SESSION[self::$sessionName.$name]) && isset($_SESSION[self::$sessionName."{$name}_created"]))
        {
            if(strcmp($token, $_SESSION[self::$sessionName.$name]) == 0)
            {
                if((time() - $_SESSION[self::$sessionName."{$name}_created"]) <= self::$expiry)
                {
                    /*if($expire)
                    {
                        $_SESSION[self::$sessionName.$name] = "";
                        $_SESSION[self::$sessionName."{$name}_created"] = "";
                    }*/
                    return TRUE;
                }
                return FALSE;
            }
        }
        return FALSE;
    }
    //Get Expiration Time
    static public function expiration($token = '', $name = '')
    {
       session::sessionStart();
       if(isset($_SESSION[self::$sessionName.$name]) && isset($_SESSION[self::$sessionName."{$name}_created"]))
        {
            if(strcmp($token, $_SESSION[self::$sessionName.$name]) == 0)
            {
                return $_SESSION[self::$sessionName."{$name}_created"];
            }
        }
        return 0;
    }
}
