<?php

$configuration = array(
    //Developer Mode
    "debug" => false,
    //error log
    "error_log" => true, 
    //Page Render Time
    "render_time" => "30",
    //Admin Session Prefix
    "admin_session_prefix" => "otl_index_",
     //Domain
    "domain" => "http://localhost/otl",
    // "domain" => "http://odysseytimelogic.com",
    //Title
    "title" => "Odyssy Time Logic",
    //Charset
    "charset" => "utf-8",
	//timezone
    "timezone" => "UTC",
    //Database Information
    "db_host" => "localhost",
    // "db_user" => "odysseyt_admin",
    // "db_password" => "JTRdF&IhKXH9",
    // "db_name" => "odysseyt_live",
    "db_user" => "root",
    "db_password" => "",
    "db_name" => "attendance_sys",
    "db_table_prefix" => "otl_",
    //Smtp Details
    "smtp_host" => "sg2plcpnl0009.prod.sin2.secureserver.net",
    "smtp_username" => "otl@prosoftsol.com",
    "smtp_password" => "Otl.Otl1238",
    "smtp_secure" => "tls",
    "smtp_port" => "465",
    "smtp_auth" => TRUE
);

//Defining Constants

define("_ADMIN_", "1");
define("_CLIENT_","2");
define("_EMPLOYEE_", "3");
define("_OFFICE_", "4");
define("_DEPARTMENT_", "5");

define("_DEACTIVE_", "0");
define("_ACTIVE_", "1");
define("_DELETED_", "2");
define("_REQUEST_", "3");
define("_DEFAULT_STATUS_", _ACTIVE_);

define("_LATE_", "0");
define("_IN_TIME_", "1");
define("_OVERTIME_", "2");
define("_EARLY_", "3");

define("_DAILY_FLAG_", "1");
define("_DAILY_CHECKIN_", "2");
define("_DAILY_ATTND_", "4");

define("_CONTROLLER_PATH_", "../application/controller");
define("_MODEL_PATH_", "../application/model");
define("_VIEW_PATH_", "../application/view");
define("_TEMPLATE_PATH_","../application/template");
define("_INCLUDES_PATH_","../application/includes");
define("_LIBRARIES_PATH_", "../application/libs");

//Paths with respect to base directory
define("_APPLICATION_PATH", "application");
define("_PUBLIC_PATH_","/");
define("_SHARED_UPLOADS_", "../shared");
