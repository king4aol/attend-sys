<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of sendmail
 *
 * @author PSSLT2540p
 */
class sendmail {
    private $sender;
    private $config;
    static private $instance;
    
    public function __construct() 
    {
        global $configuration;
        $this->sender = new PHPMailer\PHPMailer;
        $this->config = $configuration;
    }
    public function activateEmp($email, $name) 
    {
        $this->sender->fromAccounts(); //Setting From Email
        $this->sender->addAddress($email);
        $this->sender->isHTML(true);   

        $this->sender->Subject = 'Notification Alert';
        $this->sender->Body    = "<b>Dear $name,</b><br>"
        . "<p>You Had Been Activate.</p>";
        
        if($this->sender->send())
        {
            return TRUE;
        }
        else 
        {
            return FALSE;
        }
    }
    public function deactivateEmp($email, $name)
    {
        $this->sender->fromAccounts(); //Setting From Email
        $this->sender->addAddress($email);
        $this->sender->isHTML(true);   

        $this->sender->Subject = 'Notification Alert';
        $this->sender->Body    = "<b>Dear $name,</b><br>"
        . "<p>You Had Been Deactivate. Please Contact Your HR. Thanks<br></p>";
        
        if($this->sender->send())
        {
            return TRUE;
        }
        else 
        {
            return FALSE;
        }
    }
    public function forgetPassword($email, $name, $username, $hash) 
    {
        $this->sender->fromAccounts(); //Setting From Email
        $this->sender->addAddress($email);
        $this->sender->isHTML(true);   

        $this->sender->Subject = 'OTL Password Recovery';
        $this->sender->Body    = "<b>Dear $name,</b><br>"
        . "<p>Please Follow This Url To Reset Your Password<br>"
        . "<a href='{$this->config['domain']}"._PUBLIC_PATH_."access/forgetpassword/reset/{$username}/{$hash}'>"
        . "{$this->config['domain']}"._PUBLIC_PATH_."access/forgetpassword/reset/{$username}/{$hash}</a></p>";
        
        if($this->sender->send())
        {
            return TRUE;
        }
        else 
        {
            return FALSE;
        }              
    }
    public function dailyReport($email, $subject, $body, $attachment)
    {   
        $this->sender = new PHPMailer\PHPMailer;
        $this->sender->fromReports(); //Setting From Email
        $this->sender->addAddress($email);
        $this->sender->isHTML(true);   

        $this->sender->Subject = $subject;
        $this->sender->Body    = $body;
        if(is_array($attachment))
        {
            foreach($attachment as $a)
            {
                $this->sender->addAttachment($a);
            }
        }
        else 
        {
            $this->sender->addAttachment($attachment);
        }
        
        if($this->sender->send())
        {
            return TRUE;
        }
        else 
        {
            return FALSE;
        }
    }
    static public function send() 
    {
        if (is_null(self::$instance ))
        {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
