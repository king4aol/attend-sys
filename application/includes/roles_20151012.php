<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of roles
 *
 * @author Msajid
 */
class roles {
    private $dbTable = __CLASS__;
    static private $instance;
    private $permissions = array(
        "Add/Edit/Delete/View Admin" => 1, 
        "Add/Edit/Delete/View Offices" => 2, 
        "Add/Edit/Delete/View Employee" => 4
        );
    private $client;
    private $admin;
    private $userBit;
    
    public function dbcheck() 
    {
        global $dtable;
        global $configuration;
        $sql = "CREATE TABLE IF NOT EXISTS `{$configuration['db_table_prefix']}{$this->dbTable}` (
        `role_id` int(11) NOT NULL AUTO_INCREMENT,
        `role_title` varchar(50) NOT NULL,
        `permission_mask` int(11) NOT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
        
        $dtable->query($sql);
        if($dtable->is_true)
        {
            $sql = "SELECT * FROM `{$configuration['db_table_prefix']}{$this->dbTable}` LIMIT 1";
            $this->db->query($sql);

            if(!$this->db->is_found)
            {
                return $this->insertBitMask();
            }
            return true;
        }
        return false;
    }
    public function __construct() 
    {
        $this->client = 6;
        $this->admin = 7;
        $this->dbcheck();
    }
    //Get Instance
    public static function getInstance()
    {
        if (is_null( self::$instance ))
        {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    public function adminMask()
    {
        return $this->admin;
    }
    
    public function clientMask()
    {
        return $this->client;
    }
    //Check if User have permission to a specific controller
    public function hasPermission($requiredBit) 
    {
        $this->userBit = session::getBitMask();
        if($this->userBit)
        {
            if($this->userBit & $requiredBit)
            {
                return $this->ownThePage();
            }
            return FALSE;
        }
        return FALSE;
    }
    //Check if a User own the page
    private function ownThePage() 
    {
        if(session::getUserType() == _ADMIN_)
        {
            return TRUE;
        }
        else
        {
            return TRUE;
        }
    }
    //Will be called first time from dbcheck()
    private function insertBitMask()
    {
        global $database;
        $database->transaction();
        foreach($this->permissions as $k => $v)
        {
            $sql = "INSERT INTO `{$configuration['db_table_prefix']}{$this->dbTable}` (`role_title`, `permission_mask`) VALUES('$k', $v)";
            $database->query($sql);
        }
        if($database->commit())
        {
            return true;
        }
        return false;
    }
}
