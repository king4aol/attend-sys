<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of securestr
 *
 * @author Msajid
 */
class securestr {
    static public function clean($string = '', $level = 1, $chars = false, $leave = '') 
    {
        if(is_array($string))
        {
            return array_map("secure::clean",$string);
        }

        $string = preg_replace('/<script[^>]*>([\s\S]*?)<\/script[^>]*>/i', '', $string);      
        switch ($level) 
        {
          case '3':
            if(empty($leave))
            {
                $search = array('@<script[^>]*?>.*?</script>@si',
                             '@<[\/\!]*?[^<>]*?>@si',
                             '@<style[^>]*?>.*?</style>@siU',
                             '@<![\s\S]*?--[ \t\n\r]*>@'
                        ); 
                $string = preg_replace($search, '', $string);              
            }
            
            $string = strip_tags($string,$leave); 
            
            if($chars)
            {
                if(phpversion() >= 5.4)
                {
                    $string = htmlspecialchars($string, ENT_QUOTES | ENT_HTML5,"UTF-8");  
                }
                else
                {
                    $string = htmlspecialchars($string, ENT_QUOTES,"UTF-8");  
                }
            }
            break;
          case '2':
            $string = strip_tags($string,'<b><i><s><p><u><strong><span>');
            break;
          case '1':
            $string = strip_tags($string,'<b><i><s><u><strong><a><pre><code><p><div><span>');
            break;
        }   
        
        $string = str_replace('href=','rel="nofollow" href=', $string);   
        return $string; 
    }
}
