<?php
    class db
    {
        private
            $secure_query,$conn,$result,$multi,$execute, $is_transaction;
        public
            $num_rows,$is_found,$is_true,$last_id,$error,$failed,$affected_rows;

        public function query($query)
        {
            /*if(!$this->conn->ping())
            {
                $this->reconnect();
            }*/
            
            if($this->is_true || !$this->is_transaction)
            {
                if($query == "")
                {
                    return false;
                }
                $this->secure_query=$query;
                if($this->multi)
                {
                    if($this->result = $this->conn->multi_query($this->secure_query))
                    {
                        $this->is_true=true;
                        $this->affected_rows = $this->conn->affected_rows;
                    }
                    else
                    {
                            $this->is_true=false;
                            $this->failed = $this->secure_query;
                            //echo $this->secure_query."\n"; //Echo For Debugging Mode Only
                    }
                }
                else
                {
                    if($this->result = $this->conn->query($this->secure_query))
                    {
                        $this->is_true=true;
                        $this->affected_rows = $this->conn->affected_rows;
                    }
                    else
                    {
                        $this->is_true=false;
                        $this->is_found=false;
                        $this->failed = $this->secure_query;
                        //echo $this->secure_query."\n"; //Echo For Debugging Mode Only
                    }
                }
                if($this->is_true)
                {
                    if(strpos(strtolower($this->secure_query),'select') !== false)
                    {

                        if($this->result->num_rows)
                            @$this->num_rows=$this->result->num_rows;
                        else
                            $this->num_rows = 0;
                        if($this->num_rows>0)
                        {
                            $this->is_found=true;
                        } 
                        else
                        {
                                $this->is_found=false;	
                        }	
                    }
                    if(strpos(strtolower($this->secure_query),'insert') !== false)
                    {
                        @$this->last_id=$this->conn->insert_id;
                    }
                    return true;
                }
                else
                {
                    $this->error=$this->conn->error;
                    return false;
                }
            }
        }

        public function multi_query($query)
        {
            $this->multi = true;
            $this->query($query);
        }

        public function result()
        {
            if($this->is_true)
            {
                return $this->result;
            }
        }
        public function transaction()
        {	
            $this->is_transaction = true;
            $this->conn->autocommit(false);

        }
        public function commit()
        {
            if($this->is_true)
            {
                $this->conn->commit();
                return true;
            }	
            else
            {
                $this->error=$this->conn->error;
                $this->conn->rollback();	
                return false;
            }
        }
        public function rollback()
        {
            $this->conn->rollback();	
            return false;
        }
        function __construct()
        {
            global $configuration;

            $this->multi=false;
            $this->execute=false;
            $this->is_true = true;
            $this->is_found = false;
            $this->is_transaction = false;
            //Create Database if Not Exist
            $this->conn = new mysqli($configuration['db_host'],$configuration['db_user'],$configuration['db_password']);
            if ($this->conn->connect_error) 
            {
                die("Connection failed: " . $this->conn->connect_error);
            }
            else
            {
                $this->query("CREATE DATABASE IF NOT EXISTS `{$configuration['db_name']}`");
                if(!$this->is_true)
                {
                    if($configuration['debug'])
                    {
                       die("Connection failed: " . $this->conn->connect_error);
                    }
                }
            }
            //Then Connect To Database
            $this->conn = new mysqli($configuration['db_host'],$configuration['db_user'],$configuration['db_password'],$configuration['db_name']);
            if ($this->conn->connect_error) 
            {
                if($configuration['debug'])
                {
                   die("Connection failed: " . $this->conn->connect_error);
                }
            }
        }
        function __destruct()
        {
            $this->conn->autocommit(true);
            // unset($this);
        }
        public function reconnect()
        {
            global $configuration;
            $this->conn = new mysqli($configuration['db_host'],$configuration['db_user'],$configuration['db_password'],$configuration['db_name']);
        }
    }

?>