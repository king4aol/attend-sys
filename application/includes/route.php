<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of route
 *
 * @author Umar
 */
class route {
    //routes consist of all controllers and methods
    private static $instance;
    private $controllerName;
    private $controller;
    private $methodName;
    private $params = [];
    private $requiredBit;
    private $userBits;
    private $userId;
    private $username;
    private $configuration;
    private $routes = array(
        "access" => //controller
        array(
            "register" => "4", //method => permissions required
            "registered" => "4",
            "freeregister" => "0",
            "freeregistered" => "0",
            "login" => "0",
            "logout" => "1",
            "forgetpassword" => "0"
        ),
        "admin" =>
        array(
            "view" => "4",
            "edit" => "7",
            "details" => "4",
            "api" => "15",
            "action" => "7",
            "extendDate" => "4",
            "devices" => "15",
            "message" => "15",
            "gracetime" => "15",
            "logo" => "15",
            "ajax" => "1"
        ),
        "employee" =>
        array(
            "add" => "15",
            "view" => "15",
            "ajax" => "1",
            "action" => "7",
            "edit" => "15",
            "details" => "1",
            "import" => "15"
        ),
        "office" =>
        array(
            "add" => "15",
            "view" => "15",
            "edit" => "15",
            "action" => "7",
            "message" => "15",
            "ajax" => "1"
        ),
        "department" =>
        array(
            "add" => "15",
            "view" => "15",
            "edit" => "15",
            "action" => "7",
            "ajax" => "1"
        ),
        "attendance" =>
        array(
            "view" => "15",
            "offices" => "15",
            "export" => "1",
            "reports" => "15",
            "edit" => "1",
            "live" => "1",
            "exportit" => "1",
            "ajax" => "1"
        ),
        "home" =>
        array(
            "index" => "1",
            "ajax" => "1",
            "comingsoon" => "0",
            "support" => "1"
        ),
        "api2" =>
        array(
            "login" => "0",
            "logout" => "1",
            "action" => "2",
            "index" => "1",
            "get" => "2",
            "validate" => "0"
        ),
        "settings" =>
        array(
            "gracetime" => "15",
        ),
        "cron" =>
        array(
            "reports" => 0
        ),
        
        );
    private $setPostVars = array(
        "employee" =>
        array(
            "add" =>
            array(
                "submit" => "",
                "admin" => ""
            ),
            "view" =>
            array(
                "submit" => "",
                "admin" => ""
            ),
            "import" =>
            array(
                "submit" => "",
                "admin" => ""
            )
        ),
        "office" => 
        array(
            "add" =>
            array(
                "adminsubmit" => "",
                "admin" => ""
            ),
            "view" =>
            array(
                "submit" => "",
                "client" => ""
            ),
            "message" =>
            array(
                "submit" => "",
                "admin" => ""
            )
        ),
        "department" => 
        array(
            "add" =>
            array(
                "adminsubmit" => "",
                "admin" => ""
            ),
            "view" =>
            array(
                "submit" => "",
                "client" => ""
            )
        ),
        "attendance" =>
        array(
            "view" =>
            array(
                "submit" => "",
                "admin" => ""
            ),
            "offices" =>
            array(
                "submit" => "",
                "admin" => ""
            ),
            "attendance" =>
            array(
                "submit" => "",
                "admin" => ""
            ),
            "reports" =>
            array(
                "submit" => "",
                "admin" => ""
            )
        ),
        "settings" =>
        array(
            "gracetime" => 
            array(
                "adminsubmit" => "",
                "admin" => ""
            )
        ),
        "admin" =>
        array(
            "devices" =>
            array(
                "submit" => "",
                "admin" => ""
            ),
            "message" =>
            array(
                "submit" => "",
                "admin" => ""
            ),
            "gracetime" =>
            array(
                "submit" => "",
                "admin" => ""
            ),
            "api" =>
            array(
                "submit" => "",
                "admin" => ""
            ),
            "logo" =>
            array(
                "submit" => "",
                "admin" => ""
            )
        )
        
    );
    
    public function __construct() 
    {
        global $configuration;
        $this->configuration = $configuration;
        $this->userId = session::getAdminId();
        $this->username = session::getUsername();
        $this->setPostVars['employee']['add']['admin'] = $this->userId;
        $this->setPostVars['employee']['view']['admin'] = $this->userId;
        $this->setPostVars['employee']['import']['admin'] = $this->userId;
        $this->setPostVars['office']['add']['admin'] = $this->userId;
        $this->setPostVars['office']['message']['admin'] = $this->userId;
        $this->setPostVars['settings']['gracetime']['admin'] = $this->userId;
        $this->setPostVars['office']['view']['client'] = $this->username;
        $this->setPostVars['admin']['devices']['admin'] = $this->userId;
        $this->setPostVars['admin']['message']['admin'] = $this->userId;
        $this->setPostVars['admin']['api']['admin'] = $this->userId;
        $this->setPostVars['admin']['gracetime']['admin'] = $this->userId;
        $this->setPostVars['admin']['logo']['admin'] = $this->userId;
        $this->setPostVars['attendance']['view']['admin'] = $this->userId;
        $this->setPostVars['attendance']['offices']['admin'] = $this->userId;
        $this->setPostVars['attendance']['reports']['admin'] = $this->userId;
        $this->setPostVars['department']['add']['admin'] = $this->userId;
        $this->setPostVars['department']['view']['client'] = $this->username;
        $this->userBits = $this->userBits();
    }
    //Get Instance
    public static function getInstance()
    {
        if (is_null( self::$instance ))
        {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    public function setRoute($controller = '', $method = '', $parameters = [])
    {
        if(is_object($controller))
        {
           $this->controller = $controller;
           $this->controllerName = str_replace("Controller", "", get_class($controller));
           $this->methodName = $method;
           $this->params = $parameters;
           
           if(method_exists($controller, $this->methodName))
           {
               if(array_key_exists($this->controllerName, $this->routes))
               {
                   if(is_array($this->routes[$this->controllerName]))
                   {
                       if(array_key_exists($this->methodName, $this->routes[$this->controllerName]))
                       {
                           $this->requiredBit = $this->routes[$this->controllerName][$this->methodName];
                           //if(session::isLogin())
                           //{
                                /*if(!$this->requiredBit & $this->userBits())
                                {echo "here".$this->setPostVars['employee']['add']['admin'];
                                    if($this->requiredBit == roles::getInstance()->getPermissionBit("login"))
                                    {
                                        global $configuration;
                                        header("Location: {$configuration['domain']}"._PUBLIC_PATH_."access/login");
                                        die();
                                    }
                                    else 
                                    {
                                        $this->controllerName = "error"; 
                                        require_once _CONTROLLER_PATH_."/{$this->controllerName}.php";
                                        $this->controllerName = "errorController"; 
                                        $this->controller = new $this->controllerName;
                                        $this->methodName = "custom";
                                        $this->params = array("Access Denied!", "You Don't Have Access to This Page.Please Contact Administrator");
                                    }
                                }
                                else*/
                               if (($this->requiredBit & $this->userBits) || ($this->requiredBit == 0 && $this->userBits == 0))
                                {
                                    if(array_key_exists($this->controllerName, $this->setPostVars) && $this->userBits & roles::getInstance()->getPermissionBit("set_variables"))
                                    {
                                        if(array_key_exists($this->methodName, $this->setPostVars[$this->controllerName]) || !is_array($this->setPostVars[$this->controllerName]))
                                        {
                                            if(is_array($this->setPostVars[$this->controllerName]))
                                            {
                                                $var = $this->setPostVars[$this->controllerName][$this->methodName];
                                            }
                                            else 
                                            {
                                                $var = $this->setPostVars[$this->controllerName];
                                            }
                                            foreach($var as $a => $b)
                                            {
                                               if(is_array($b))
                                               {
                                                    foreach($b as $c => $d)
                                                    {
                                                        if(is_array($d))
                                                        {
                                                            foreach($d as $e => $f)
                                                            {
                                                               $_POST[$e] = $f;
                                                            }
                                                        }
                                                        else 
                                                        {
                                                            $_POST[$c] = $d; 
                                                        }
                                                    }
                                               }
                                               else 
                                               {
                                                   $_POST[$a] = $b; 
                                               }
                                            }
                                        }
                                    }
                                }
                                elseif ($this->userBits == 0)
                                {
                                    ob_get_clean();
                                    header("Location: {$this->configuration['domain']}"._PUBLIC_PATH_."access/login");
                                    die();
                                }
                                else 
                                {
                                    $this->controllerName = "error"; 
                                    require_once _CONTROLLER_PATH_."/{$this->controllerName}.php";
                                    $this->controllerName = "errorController"; 
                                    $this->controller = new $this->controllerName;
                                    $this->methodName = "_403";
                                }
                           /*}
                           elseif(!($this->controllerName == "access" && $this->methodName == "login"))
                           {
                               header("Location: {$this->configuration['domain']}"._PUBLIC_PATH_."access/login");
                               die();
                           }*/
                       }
                       else 
                       {
                            $this->methodName = "index";
                       }
                   }
                   else 
                   {
                        $this->methodName = "index";
                   }
               }
               else 
               {
                    $this->controllerName = "error"; 
                    require_once _CONTROLLER_PATH_."/{$this->controllerName}.php";
                    $this->controllerName = "errorController"; 
                    $this->controller = new $this->controllerName;
                    $this->methodName = "_404";
               }
           }
           else 
           {
               $this->methodName = "index";
           }
        }
        else
        {
            $this->controllerName = "error"; 
            require_once _CONTROLLER_PATH_."/{$this->controllerName}.php";
            $this->controllerName = "errorController"; 
            $this->controller = new $this->controllerName;
            $this->methodName = "_404";
        } 
        $this->visit();
    }
    
    private function visit()
    {   
        call_user_func_array([$this->controller, $this->methodName], $this->params);
    }
    
    private function userBits()
    {
        $this->userBits = 0;
        
        if(session::isLogin())
        {
            $this->userBits += roles::getInstance()->getPermissionBit("login");
            //$this->userBits += session::getBitMask();
            if(session::getUserType() == _ADMIN_)
            {
                $this->userBits += roles::getInstance()->adminMask();
            }
            else if(session::getUserType() == _CLIENT_)
            {
                $this->userBits += roles::getInstance()->clientMask();
            }
        }
        return $this->userBits;
    }
}
