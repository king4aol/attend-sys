<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of session
 *
 * @author Msajid
 */
class session {
    static private $adminId = "admin_id";
    static private $userId = "user_id";
    static private $username = "username";
    static private $name = "name";
    static private $bitMask = "bit_mask";
    static private $type = "admin_type";
    static private $memberType = "membership_type";
    static private $memberTypeTitle = "membership_type_title";
    static private $logId = "log_id";

        //Start Session
    static public function sessionStart() 
    {
        if(function_exists("version_compare"))
        {
            if(function_exists("phpversion"))
            {
                if(version_compare(phpversion(), "5.4.0", ">="))
                {
                    if(function_exists("session_status"))
                    {
                        if(session_status() !== PHP_SESSION_ACTIVE)
                        {
                            @session_start();
                            return;
                        }
                    }
                    else if(!isset($_SESSION))
                    {
                        @session_start();
                        return;
                    }
                }
            }
        }
        else if(!isset($_SESSION))
        {
            session_start();
            return;
        }
    }
    //Destroy Session
    static public function sessionDestroy() 
    {
        self::sessionStart();
        session_destroy();
        return;
    }
    //Set Session For Web Services
    static public function setSession($sessionId) 
    {
        if(self::validateSession($sessionId))
        {
            session_id($sessionId);
            self::sessionStart();
            if(!self::isLogin())
            {
                require_once dirname(__FILE__)."/../model/api_logs.php";
                $model = new api_logs();
                $data = $model->getDataBySession($sessionId);
                if(is_array($data))
                {
                    require_once dirname(__FILE__)."/../model/access.php";
                    $model = new access();
                    $model->login($data['username'], $data['password']);
                    return TRUE;
                }
                //$model = require_once _CONTROLLER_PATH_."api_logs.php";
            }
            return TRUE;
        }
        return FALSE;
    }
    //Validate Session Key
    static public function validateSession($sessionId)
    {
        return preg_match('/^[-,a-zA-Z0-9]{1,128}$/', $sessionId) > 0;
    }
    //Get Current Session ID
    static public function getSession() 
    {
        self::sessionStart();
        return session_id();
    }
    //Login Admin
    static public function loginUser($admin_id = '', $user_id = '', $name = '', $username = '', $type = '', $memtype = '', $bitMask = 0) 
    {
        global $configuration;
        self::sessionStart();
        $_SESSION[$configuration['admin_session_prefix'].self::$adminId] = $admin_id;
        $_SESSION[$configuration['admin_session_prefix'].self::$userId] = $user_id;
        $_SESSION[$configuration['admin_session_prefix'].self::$username] = $username;
        $_SESSION[$configuration['admin_session_prefix'].self::$name] = $name;
        $_SESSION[$configuration['admin_session_prefix'].self::$type] = $type;
        $_SESSION[$configuration['admin_session_prefix'].self::$memberType] = $memtype;
        $_SESSION[$configuration['admin_session_prefix'].self::$bitMask] = $bitMask;
    }
    //Check if user is logged in
    static public function isLogin()
    {
        global $configuration;
        
        self::sessionStart();
        if(isset($_SESSION[$configuration['admin_session_prefix'].self::$adminId]) && isset($_SESSION[$configuration['admin_session_prefix'].self::$userId]) && isset($_SESSION[$configuration['admin_session_prefix'].self::$name]))
        {
            return TRUE;
        }
        return FALSE;
    }
    //Get Logged User Type
    static public function getUserType()
    {
        global $configuration;
        if(isset($_SESSION[$configuration['admin_session_prefix'].self::$type]))
        {
            if($_SESSION[$configuration['admin_session_prefix'].self::$type] == _ADMIN_)
            {
                return _ADMIN_;
            }
            else if($_SESSION[$configuration['admin_session_prefix'].self::$type] == _CLIENT_)
            {
                return _CLIENT_;
            }
            return FALSE;
        }
        return FALSE;
    }
    static function getMembershipType() 
    {
        global $configuration;
        if(isset($_SESSION[$configuration['admin_session_prefix'].self::$memberType]))
        {
            return $_SESSION[$configuration['admin_session_prefix'].self::$memberType];
        }
        return FALSE;
    }
    static function getMembershipTypeTitle() 
    {
        global $configuration;
        if(isset($_SESSION[$configuration['admin_session_prefix'].self::$memberType]))
        {
            if(isset($_SESSION[$configuration['admin_session_prefix'].self::$memberTypeTitle]))
            {
                return $_SESSION[$configuration['admin_session_prefix'].self::$memberTypeTitle];
            }
            else 
            {               
                if(is_file(__DIR__."/../model/membership_types.php"))
                {
                    require_once __DIR__."/../model/membership_types.php";
                    $obj = new membership_types;
                    $_SESSION[$configuration['admin_session_prefix'].self::$memberTypeTitle] = $obj->getTypeTitle(self::getMembershipType());
                    return $_SESSION[$configuration['admin_session_prefix'].self::$memberTypeTitle];
                }
            }
        }
        return FALSE;
    }
    //Get Bit Mask
    static public function getBitMask() 
    {
        global $configuration;
        self::sessionStart();
        if(isset($_SESSION[$configuration['admin_session_prefix'].self::$bitMask]))
        {
            return $_SESSION[$configuration['admin_session_prefix'].self::$bitMask];
        }
        return FALSE;
    }
    
    //Get User ID
    static public function getUserId()
    {
        global $configuration;
        self::sessionStart();
        if(isset($_SESSION[$configuration['admin_session_prefix'].self::$userId]))
        {
            return $_SESSION[$configuration['admin_session_prefix'].self::$userId];
        }
        return false;
    }
     //Get User Name
    static public function getUsername()
    {
        global $configuration;
        self::sessionStart();
        if(isset($_SESSION[$configuration['admin_session_prefix'].self::$username]))
        {
            return $_SESSION[$configuration['admin_session_prefix'].self::$username];
        }
        return false;
    }
    //Get Admin ID
    static public function getAdminId()
    {
        global $configuration;
        self::sessionStart();
        if(isset($_SESSION[$configuration['admin_session_prefix'].self::$adminId]))
        {
            return $_SESSION[$configuration['admin_session_prefix'].self::$adminId];
        }
        return false;
    }
    //Set Log ID
    static public function setLogId($logId)
    {
        global $configuration;
        self::sessionStart();
        return $_SESSION[$configuration['admin_session_prefix'].self::$logId] = $logId;
    }
    //Get Logs ID
    static public function getLogId()
    {
        global $configuration;
        self::sessionStart();
        if(isset($_SESSION[$configuration['admin_session_prefix'].self::$logId]))
        {
            return $_SESSION[$configuration['admin_session_prefix'].self::$logId];
        }
        return false;
    } 
    //Get Name
    static public function getName()
    {
        global $configuration;
        self::sessionStart();
        if(isset($_SESSION[$configuration['admin_session_prefix'].self::$name]))
        {
            return $_SESSION[$configuration['admin_session_prefix'].self::$name];
        }
        return false;
    }
    //Get Browser
    static public function getOS($user_agent = '') 
    { 
        $os_platform = "Unknown Platform";

        $os_array =   array(
                                '/windows nt 6.2/i'     =>  'Windows 8',
                                '/windows nt 6.1/i'     =>  'Windows 7',
                                '/windows nt 6.0/i'     =>  'Windows Vista',
                                '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                                '/windows nt 5.1/i'     =>  'Windows XP',
                                '/windows xp/i'         =>  'Windows XP',
                                '/windows nt 5.0/i'     =>  'Windows 2000',
                                '/windows me/i'         =>  'Windows ME',
                                '/win98/i'              =>  'Windows 98',
                                '/win95/i'              =>  'Windows 95',
                                '/win16/i'              =>  'Windows 3.11',
                                '/macintosh|mac os x/i' =>  'Mac OS X',
                                '/mac_powerpc/i'        =>  'Mac OS 9',
                                '/linux/i'              =>  'Linux',
                                '/ubuntu/i'             =>  'Ubuntu',
                                '/iphone/i'             =>  'iPhone',
                                '/ipod/i'               =>  'iPod',
                                '/ipad/i'               =>  'iPad',
                                '/android/i'            =>  'Android',
                                '/blackberry/i'         =>  'BlackBerry',
                                '/webos/i'              =>  'Mobile',
                                '/CFNetwork/i'          =>  'iOS',
                                '/java/i'               =>  'Android'
                            );

        foreach ($os_array as $regex => $value) 
        { 
            if(preg_match($regex, $user_agent)) 
            {
                $os_platform    =   $value;
            }
        }   

        return $os_platform;
    }
}
