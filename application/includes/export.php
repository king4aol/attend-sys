<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of exportCSV
 *
 * @author PSSLT2540p
 */
class exportCSV {
    
    //static private $PHPExcel;
    static private $sessionPrefix = "csv_";
    
    static public function setData($data = array(), $id = 'abc') 
    {
        if(is_array($data))
        {
            if(sizeof($data > 0))
            {
                session::sessionStart();
                $_SESSION[self::$sessionPrefix.$id] = $data;
                return TRUE;
            }
        }
        return FALSE;
    }
    
    static public function getData($id = 'abc') 
    {
        session::sessionStart();
        if(isset($_SESSION[self::$sessionPrefix.$id]))
        {
            return $_SESSION[self::$sessionPrefix.$id];
        }
        return array();
    }
}
